import { Injectable } from "@angular/core";
import {
  AngularFirestore,
  AngularFirestoreDocument,
  AngularFirestoreCollection,
  DocumentChangeAction,
  Action,
  DocumentSnapshotDoesNotExist,
  DocumentSnapshotExists,
  QuerySnapshot,
} from "angularfire2/firestore";
import { AngularFireAuth } from "@angular/fire/auth";

import { AngularFireDatabase } from "angularfire2/database";
import { Observable, from, combineLatest, defer, of, pipe } from "rxjs";
import { map, tap, take, switchMap, mergeMap, expand, takeWhile } from "rxjs/operators";
import * as firebase from "firebase/app";
import { firestore } from "firebase/app";
import { CommonService } from "./common.service";

export const docJoin = (afs: AngularFirestore, paths: { [key: string]: string }) => {
  return (source) =>
    defer(() => {
      let parent;
      const keys = Object.keys(paths);

      return source.pipe(
        switchMap((data) => {
          // Save the parent data state
          parent = data;

          // Map each path to an Observable
          const docs$ = keys.map((k) => {
            const fullPath = `${paths[k]}/${parent[k]}`;
            return afs.doc(fullPath).valueChanges();
          });

          // return combineLatest, it waits for all reads to finish
          return combineLatest(docs$);
        }),
        map((arr) => {
          // We now have all the associated douments
          // Reduce them to a single object based on the parent's keys
          const joins = keys.reduce((acc, cur, idx) => {
            return { ...acc, [cur]: arr[idx] };
          }, {});

          // Return the parent doc with the joined objects
          return { ...parent, ...joins };
        })
      );
    });
};

export const leftJoin = (afs: AngularFirestore, field, collection, limit = 100) => {
  return (source) =>
    defer(() => {
      // Operator state
      let collectionData;

      // Track total num of joined doc reads
      let totalJoins = 0;

      return source.pipe(
        switchMap((data) => {
          // Clear mapping on each emitted val ;

          // Save the parent data state
          collectionData = data as any[];

          const reads$ = [];
          for (const doc of collectionData) {
            // Push doc read to Array

            if (doc[field]) {
              // Perform query on join key, with optional limit
              const q = (ref) => ref.where(field, "==", doc[field]).limit(limit);

              reads$.push(afs.collection(collection, q).valueChanges());
            } else {
              reads$.push(of([]));
            }
          }

          return combineLatest(reads$);
        }),
        map((joins) => {
          return collectionData.map((v, i) => {
            totalJoins += joins[i].length;
            return { ...v, [collection]: joins[i] || null };
          });
        }),
        tap((final) => {
          console.log(`Queried ${(final as any).length}, Joined ${totalJoins} docs`);
          totalJoins = 0;
        })
      );
    });
};

export const leftJoinDocument = (afs: AngularFirestore, field, collection) => {
  return (source) =>
    defer(() => {
      // Operator state
      let collectionData;
      const cache = new Map();

      return source.pipe(
        switchMap((data) => {
          // Clear mapping on each emitted val ;
          cache.clear();

          // Save the parent data state
          collectionData = data as any[];

          const reads$ = [];
          let i = 0;
          for (const doc of collectionData) {
            // Skip if doc field does not exist or is already in cache
            if (!doc[field] || cache.get(doc[field])) {
              continue;
            }

            // Push doc read to Array
            reads$.push(afs.collection(collection).doc(doc[field]).valueChanges());
            cache.set(doc[field], i);
            i++;
          }

          return reads$.length ? combineLatest(reads$) : of([]);
        }),
        map((joins) => {
          return collectionData.map((v, i) => {
            const joinIdx = cache.get(v[field]);
            return {
              ...v,
              [field]: { ...joins[joinIdx], id: v[field] } || null,
            };
          });
        }),
        tap((final) => console.log(`Queried ${(final as any).length}, Joined ${cache.size} docs`))
      );
    });
};

export const colletionJoin = (afs: AngularFirestore, category, collection, field, limit) => {
  return (source) =>
    defer(() => {
      // Operator state
      let array;

      // Track total num of joined doc reads
      let totalJoins = 0;

      return source.pipe(
        switchMap((select) => {
          // Clear mapping on each emitted val ;

          // Save the parent data state
          array = category as any[];

          const reads$ = [];
          for (const doc of array) {
            // Push doc read to Array
            console.log("doc", select, field, doc);
            if (doc) {
              // Perform query on join key, with optional limit
              const q = (ref) => ref.where("genre", "==", select).where(field, "array-contains", doc).orderBy("totalLike", "desc").limit(limit);
              const collectionMap = pipe(
                map((docs: QuerySnapshot<any>) => {
                  return docs.docs.map((e) => {
                    return {
                      id: e.id,
                      ...e.data(),
                    } as any;
                  });
                })
              );
              reads$.push(
                afs
                  .collection(collection, q)
                  .snapshotChanges()
                  .pipe(
                    map((actions) => {
                      return actions.map((a) => {
                        const data: any = a.payload.doc.data();
                        const id = a.payload.doc.id;
                        return { id, ...data };
                      });
                    })
                  )
              );
            } else {
              reads$.push(of([]));
            }
          }

          return combineLatest(reads$);
        }),
        map((joins) => {
          return array.map((v, i) => {
            totalJoins += joins[i].length;
            return { ["name"]: v, ["photos"]: joins[i] || null };
          });
        }),
        tap((final) => {
          console.log(`Queried ${(final as any).length}, Joined ${totalJoins} docs`);
          totalJoins = 0;
        })
      );
    });
};

export interface Post {
  userId: string;
  createdAt: Date;
  image: string;
  content: string;
  likeCount: number;
  [key: string]: any;
}

type CollectionPredicate<T> = string | AngularFirestoreCollection<T>;
type DocPredicate<T> = string | AngularFirestoreDocument<T>;

@Injectable({
  providedIn: "root",
})
export class DbService {
  public postsRef: AngularFirestoreCollection<Post>;
  public masterRef;
  constructor(public afs: AngularFirestore, public angularDb: AngularFireDatabase, private common: CommonService, public afAuth: AngularFireAuth) {
    this.masterRef = this.afs.doc(`master/vw6N7ADkDdP1CkbhtBBnN0X3NlF3}`);
  }

  createFsId() {
    return this.afs.createId();
  }

  createdAt() {
    return firebase.firestore.FieldValue.serverTimestamp();
  }

  collection$(path, query?) {
    return this.afs
      .collection(path, query)
      .snapshotChanges()
      .pipe(
        map((actions) => {
          return actions.map((a) => {
            const data: any = a.payload.doc.data();
            const id = a.payload.doc.id;
            return { id, ...data };
          });
        })
      );
  }

  doc$(path): Observable<any> {
    return this.afs
      .doc(path)
      .snapshotChanges()
      .pipe(
        map((doc) => {
          const data: any = doc.payload.data();
          const id = doc.payload.id;
          return { id: doc.payload.id, ...data };
        })
      );
  }

  doc2$<T>(ref: DocPredicate<T>): Observable<T> {
    return this.doc(ref)
      .snapshotChanges()
      .pipe(
        map((doc: Action<DocumentSnapshotDoesNotExist | DocumentSnapshotExists<T>>) => {
          return doc.payload.data() as T;
        })
      );
  }

  col$<T>(ref: CollectionPredicate<T>, queryFn?): Observable<T[]> {
    return this.col(ref, queryFn)
      .snapshotChanges()
      .pipe(
        map((docs: DocumentChangeAction<T>[]) => {
          return docs.map((a: DocumentChangeAction<T>) => a.payload.doc.data()) as T[];
        })
      );
  }

  col<T>(ref: CollectionPredicate<T>, queryFn?): AngularFirestoreCollection<T> {
    return typeof ref === "string" ? this.afs.collection<T>(ref, queryFn) : ref;
  }

  doc<T>(ref: DocPredicate<T>): AngularFirestoreDocument<T> {
    return typeof ref === "string" ? this.afs.doc<T>(ref) : ref;
  }

  /**
   * @param  {string} path 'collection' or 'collection/docID'
   * @param  {object} data new data
   *
   * Creates or updates data on a collection or document.
   **/

  // ** 기본적인 DB처리 **//
  updateAt(path: string, data: Object): Promise<any> {
    const segments = path.split("/").filter((v) => v);
    if (segments.length % 2) {
      // Odd is always a collection
      return this.afs.collection(path).add(data);
    } else {
      // Even is always document
      return this.afs.doc(path).set(data, { merge: true });
    }
  }

  delete(path) {
    return this.afs.doc(path).delete();
  }

  /**
   * @param  {string} path path to document
   *
   * Deletes document from Firestore
   **/

  deleteDoc(path: string): Promise<any> {
    return this.afs.doc(path).delete();
  }

  acceptOrder(obj) {
    return new Promise((resolve, reject) => {
      this.connectOrder(obj)
        .then((success) => {
          const ref = this.afs.doc(`order/${obj.orderId}`);
          return ref
            .update({ connectCompanys: firestore.FieldValue.arrayUnion(obj.companyId) })
            .then((success) => {
              resolve(true);
            })
            .catch((error) => {
              reject(false);
              console.log("error", error);
            });
        })
        .catch((error) => {
          reject(false);
          console.log("error", error);
        });
    });
  }

  acceptagOrder(obj) {
    return new Promise((resolve, reject) => {
      const ref = this.afs.doc(`arrangeorder/${obj.orderId}`);
      return ref
        .update({ connectCompany: obj.companyId })
        .then((success) => {
          resolve(true);
        })
        .catch((error) => {
          reject(false);
          console.log("error", error);
        });
    });
  }

  connectOrder(obj) {
    return new Promise((resolve, reject) => {
      return this.updateAt(`connect`, obj)
        .then((success) => {
          resolve(true);
        })
        .catch((error) => {
          reject(false);
          console.log("error", error);
        });
    });
  }

  payVideoOrder(orderId, companyId) {
    return new Promise((resolve, reject) => {
      const ref = this.afs.doc(`order/${orderId}`);
      return ref
        .update({ paidCompanys: firestore.FieldValue.arrayUnion(companyId) })
        .then((success) => {
          resolve(true);
        })
        .catch((error) => {
          reject(false);
          console.log("error", error);
        });
    });
  }

  refundOrder(orderId, companyId) {
    return new Promise((resolve, reject) => {
      firebase.firestore().runTransaction((transaction) => {
        return transaction
          .get(firebase.firestore().collection("order").doc(orderId))
          .then((docData) => {
            const refundCompanys = docData.data().refundCompanys || {};
            refundCompanys[companyId] = true;
            return transaction.set(firebase.firestore().collection("order").doc(orderId), { refundCompanys }, { merge: true });
          })
          .then((success) => {
            resolve(true);
          })
          .catch((error) => {
            reject(false);
            console.log("error", error);
          });
      });
    });
  }

  refundagOrder(orderId, companyId) {
    return new Promise((resolve, reject) => {
      firebase.firestore().runTransaction((transaction) => {
        return transaction
          .get(firebase.firestore().collection("arrangeorder").doc(orderId))
          .then((docData) => {
            const refundCompanys = docData.data().refundCompanys || {};
            refundCompanys[companyId] = true;
            return transaction.set(firebase.firestore().collection("arrangeorder").doc(orderId), { refundCompanys }, { merge: true });
          })
          .then((success) => {
            resolve(true);
          })
          .catch((error) => {
            reject(false);
            console.log("error", error);
          });
      });
    });
  }

  checkUsername(nickname: string) {
    nickname = nickname.toLowerCase();
    return this.collection$(`users`, (ref) => ref.where("nickname", "==", nickname))
      .pipe(take(1))
      .toPromise();
  }

  checkEmail(email: string) {
    email = email.toLowerCase();
    return this.collection$(`users`, (ref) => ref.where("email", "==", email))
      .pipe(take(1))
      .toPromise();
  }
  userInfo(uid: string) {
    return this.doc$(`users/${uid}`).pipe(take(1)).toPromise();
  }

  appService() {
    const id = "vw6N7ADkDdP1CkbhtBBnN0X3NlF3";
    return this.doc$(`master/${id}`).pipe(take(1)).toPromise();
  }

  async bookMarkEvent(eventId, userId) {
    this.afs.doc(`users/${userId}`).update({
      bookmarkEvent: firestore.FieldValue.arrayUnion(eventId),
    });
  }

  async unBookMarkEvent(eventId, userId) {
    this.afs.doc(`users/${userId}`).update({
      bookmarkEvent: firestore.FieldValue.arrayRemove(eventId),
    });
  }

  async checkHits(eventId, userId) {
    this.afs.doc(`event/${eventId}`).update({
      hits: firestore.FieldValue.arrayUnion(userId),
    });
  }





  /**
   * 
   * @param alarmType 
   * freeLike (자유 좋아요)


   * @param typeId 게시글 uid
   * @param userId 리뷰 작성자, 댓글 작성자 등 uid
   */
  addAlaram(alarmType: string, typeId: string, userId: string) {
    let myUid = firebase.auth().currentUser.uid;

    if (userId != myUid) {
      let item = {
        activeAlarmId: this.common.generateFilename(),
        dateCreated: new Date().toISOString(),
        userId: userId,
      };

      this.updateAt(`activeAlarm/${item.activeAlarmId}`, item);
    }
  }
}
