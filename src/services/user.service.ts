import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: any = {
    uid: '',
    email: '',
    nickname: '',
    birth: '',
    phone: '',
    marketing: false,
    exitSwitch: false,
    dateCreated: '',
    pushId: '',
    alarmSwitch: true,
    password: '',
    confirm_password: '',
  }
  constructor() { }
  setUser(user) {
    this.user = user;
  }

  getUser() {
    return this.user;
  }
}
