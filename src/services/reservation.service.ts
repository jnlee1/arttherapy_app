import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {
  reservation: any = {
    uid: '',
    rsvtDate:'',
    rsvtTime:'',
    rsvtName:'',
    rsvtPnumber:'',
    rsvtGole:'',
    rsvtCenter:'',
    institutionName:'',
  }
  constructor() { }

  setReservation(rsvtInfo) {
    this.reservation = rsvtInfo;
  }

  getReservation() {
    return this.reservation;
  }
}
