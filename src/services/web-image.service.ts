import { Injectable } from "@angular/core";
import { Ng2ImgMaxService } from "ng2-img-max";
import * as firebase from "firebase";
import * as moment from "moment";
import { promise } from "protractor";

@Injectable({
  providedIn: "root",
})
export class WebImageService {

  constructor(private ng2ImgMax: Ng2ImgMaxService) { }

  onImageChange2(images) {
    return new Promise((resolve) => {
      // this.ng2ImgMax.resizeImage(images, 100, 100).subscribe(
      // (result) => {
      // this.getOrientation(images, (orientation) => {
      // this.getBase64(result, orientation).;

      var reader = new FileReader();
      reader.readAsDataURL(images);
      reader.onload = () => {
        var base64 = reader.result;

        console.log("base64", base64);
        resolve(base64);
        // this.resetOrientation(base64, orientation, (resetBase64Image) => {

        //   var byteString = atob(resetBase64Image.split(",")[1]);
        //   var ab = new ArrayBuffer(byteString.length);
        //   var ia = new Uint8Array(ab);
        //   for (var i = 0; i < byteString.length; i++) {
        //     ia[i] = byteString.charCodeAt(i);
        //   }
        //   var bb = new Blob([ab], { type: "image/jpeg" });
        //   var reader = new FileReader();
        //   reader.readAsDataURL(bb);
        //   reader.onloadend = () => {
        //     console.log("enter 3");
        //     var base64data = reader.result;

        //     console.log("base64data", base64data);
        //     var storageRef = firebase.storage().ref();
        //     var file = base64data + "";
        //     var fileName = "image_" + moment().format("x") + ".jpg";
        //     var upRef = "/images/" + fileName;
        //     var uploadTask = storageRef.child(upRef).putString(file, "data_url");
        //     uploadTask.on(
        //       firebase.storage.TaskEvent.STATE_CHANGED,
        //       (snapshot) => {
        //         console.log("enter 3.5");
        //       },
        //       (error) => {
        //         console.log(error);
        //       },
        //       () => {
        //         try {

        //           console.log("enter 4");
        //           storageRef
        //             .child(upRef)
        //             .getDownloadURL()
        //             .then((downloadURL) => {
        //               // return downloadURL;
        //               resolve(downloadURL);
        //             });
        //         } catch (e) {
        //           // console.log(e);
        //           resolve(e);
        //         }
        //       }
        //     );
        //   };
        // });
      };
      reader.onerror = (error) => {
        console.log("Error: ", error);
      };
    });
    // },
    //     (error) => {
    //       console.log("Oh no!", error);
    //     }
    //   );
    // });
  }

  getOrientation(file, callback) {
    var reader: any, target: EventTarget;
    reader = new FileReader();
    reader.onload = (event) => {
      var view = new DataView(event.target.result);

      if (view.getUint16(0, false) != 0xffd8) return callback(-2);

      var length = view.byteLength,
        offset = 2;

      while (offset < length) {
        var marker = view.getUint16(offset, false);
        offset += 2;

        if (marker == 0xffe1) {
          if (view.getUint32((offset += 2), false) != 0x45786966) {
            return callback(-1);
          }
          var little = view.getUint16((offset += 6), false) == 0x4949;
          offset += view.getUint32(offset + 4, little);
          var tags = view.getUint16(offset, little);
          offset += 2;

          for (var i = 0; i < tags; i++) if (view.getUint16(offset + i * 12, little) == 0x0112) return callback(view.getUint16(offset + i * 12 + 8, little));
        } else if ((marker & 0xff00) != 0xff00) break;
        else offset += view.getUint16(offset, false);
      }
      return callback(-1);
    };

    reader.readAsArrayBuffer(file.slice(0, 64 * 1024));
  }

  getBase64(file, orientation) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      var base64 = reader.result;
      this.resetOrientation(base64, orientation, (resetBase64Image) => {
        this.dataURItoBlob(resetBase64Image);
      });
    };
    reader.onerror = (error) => {
      console.log("Error: ", error);
    };
  }

  resetOrientation(srcBase64, srcOrientation, callback) {
    var img = new Image();

    img.onload = () => {
      var width = img.width,
        height = img.height,
        canvas = document.createElement("canvas"),
        ctx = canvas.getContext("2d");

      // set proper canvas dimensions before transform & export
      if (4 < srcOrientation && srcOrientation < 9) {
        canvas.width = height;
        canvas.height = width;
      } else {
        canvas.width = width;
        canvas.height = height;
      }

      // transform context before drawing image
      switch (srcOrientation) {
        case 2:
          ctx.transform(-1, 0, 0, 1, width, 0);
          break;
        case 3:
          ctx.transform(-1, 0, 0, -1, width, height);
          break;
        case 4:
          ctx.transform(1, 0, 0, -1, 0, height);
          break;
        case 5:
          ctx.transform(0, 1, 1, 0, 0, 0);
          break;
        case 6:
          ctx.transform(0, 1, -1, 0, height, 0);
          break;
        case 7:
          ctx.transform(0, -1, -1, 0, height, width);
          break;
        case 8:
          ctx.transform(0, -1, 1, 0, 0, width);
          break;
        default:
          break;
      }

      // draw image
      ctx.drawImage(img, 0, 0);

      // export base64
      callback(canvas.toDataURL());
    };

    img.src = srcBase64;
  }

  dataURItoBlob(dataURI) {
    // const imageTools = new ImageTools();
    // convert base64 to raw binary data held in a string
    var byteString = atob(dataURI.split(",")[1]);
    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    // write the ArrayBuffer to a blob, and you're done
    var bb = new Blob([ab], { type: "image/jpeg" });
    var reader = new FileReader();
    reader.readAsDataURL(bb);
    reader.onloadend = () => {
      var base64data = reader.result;
      this.saveImage(reader.result);
    };
    return bb;
  }

  saveImage(uploadImage) {
    var storageRef = firebase.storage().ref();
    var file = uploadImage;
    var fileName = "image_" + moment().format("x") + ".jpg";
    var upRef = "/images/" + fileName;
    var uploadTask = storageRef.child(upRef).putString(file, "data_url");
    uploadTask.on(
      firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot) => { },
      (error) => {
        console.log(error);
      },
      () => {
        try {
          storageRef
            .child(upRef)
            .getDownloadURL()
            .then((downloadURL) => {
              return downloadURL;
            });
        } catch (e) {
          console.log(e);
        }
      }
    );
  }
}
