import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ImgCheckService {


  chk: boolean = false;
  constructor() { }

  setItem(chk) {
    this.chk = chk;
  }

  getItem() {
    return this.chk;
  }
}
