import { Injectable } from '@angular/core';
import { CameraOptions, Camera } from '@ionic-native/camera/ngx';
import { CommonService } from 'src/services/common.service';
import * as firebase from 'firebase';
import { LoadingService } from 'src/services/loading.service';
@Injectable({
  providedIn: 'root',
})
export class ImageService {
  public cameraoption: CameraOptions = {
    quality: 100,
    allowEdit: false,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    sourceType: this.camera.PictureSourceType.CAMERA,
    correctOrientation: true,
    saveToPhotoAlbum: false,
  };

  public gallery: CameraOptions = {
    // allowEdit: true,
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    correctOrientation: true,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    // maxImagesCount: 15,
  };
  constructor(public camera: Camera, private loading: LoadingService, private common: CommonService) { }

  async getCamera(type) {
    return new Promise<any>(resolve => {
      this.camera.getPicture(this.cameraoption).then(async (url: any) => {
        url = 'data:image/jpeg;base64,' + url;
        const name = this.common.generateFilename();

        this.loading.load();
        firebase
          .storage()
          .ref(`/images/${type}/` + name)
          .putString(url, 'data_url')
          .then(v => {
            console.log('업로드 성공', v);
            v.ref.getDownloadURL().then(async e => {
              this.loading.hide();
              resolve(e);
            });
          })
          .catch(error => {
            this.loading.hide();
            console.log('error', error);
          });
      });
    });
  }
  async getGallery(type) {
    return new Promise<any>(resolve => {
      this.camera.getPicture(this.gallery).then(async (url: any) => {
        url = 'data:image/jpeg;base64,' + url;
        const name = this.common.generateFilename();

        this.loading.load();
        firebase
          .storage()
          .ref(`/images/${type}/` + name)
          .putString(url, 'data_url')
          .then(v => {
            console.log('업로드 성공', v);
            v.ref.getDownloadURL().then(async e => {
              this.loading.hide();
              resolve(e);
            });
          })
          .catch(error => {
            this.loading.hide();
            console.log('error', error);
          });
      });
    });
  }
}
