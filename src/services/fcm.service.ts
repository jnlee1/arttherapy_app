import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
// import { Firebase } from '@ionic-native/firebase/ngx';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { DbService } from './db.service';
import { AuthService } from './auth.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { take } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FcmService {

  constructor(
    // public firebaseNative: Firebase,
    public db: DbService,
    public auth: AuthService,
    public httpClient: HttpClient,
    private platform: Platform,
    private firebaseX: FirebaseX
  ) { }

  /**
   * 토큰 값 (pushId) 받는 함수
   */
  async getToken() {
    let token;

    if (this.platform.is('android')) {
      token = await this.firebaseX.getToken();
      console.log('token', token);
    }

    if (this.platform.is('ios')) {
      token = await this.firebaseX.getToken();
      const perm = await this.firebaseX.grantPermission();
    }

    if (!this.platform.is('cordova')) {
    }

    return this.saveTokenToFirestore(token);
  }

  /**
   * DB에 pushId 저장
   * @param token token값 저장
   */
  saveTokenToFirestore(token: any) {
    if (!token) return;
    const myUid = localStorage.getItem("userId");

    // users에 저장
    const devicesRef = this.db.updateAt(`users/${myUid}`, { pushId: token });
  }

  listenToNotifications() {
    return this.firebaseX.onMessageReceived();
  }

  /**
   * FCM 푸쉬 보내기
   * 
   * @param pushId push 보낼 사용자의 pushId
   * @param title push 제목
   * @param message push 내용
   */
  sendFcm(pushId: string, title: string, message: string) {
    return new Promise((resolve, reject) => {
      let notification = {
        "notification": {
          "title": title,
          "body": message,
          "click_action": "FCM_PLUGIN_ACTIVITY",
          "sound": "default"
        }, "data": {
        },
        "to": pushId,
        "priority": "high"
      };

      // firebase > 프로젝트 설정 > 클라우드 메세징 > 서버키
      const fbkey = 'AAAAo2b-95I:APA91bHTc2JloOqcRfxgvlhrHSGwuPinn_ewZDNYoPi4oYUylt3GBvvBeqeo7LoCSTPQPDB725TXkuNEusP2QtEtfVcroYtdWw6m6OX2lpzFuwTC1FVeOiVhN266jdK_Xi53yg6H14NT';

      const httpOptions = {
        headers: new HttpHeaders({
          "Content-Type": "application/json; charset=utf-8",
          "Authorization": `key=${fbkey}`
        })
      };

      this.httpClient.post('https://fcm.googleapis.com/fcm/send', notification, httpOptions).subscribe(new_data => {
        console.log('result', new_data);
        resolve(new_data);
      }, error => {
        reject(error);
      });

    });
  }










  

}
