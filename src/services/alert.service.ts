import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';

const errorMessages = {
  // Firebase의 auth 에러 메세지 정의
  accountExistsWithDifferentCredential: { title: '계정 안내', subTitle: '이미 있는 계정입니다.' },
  invalidCredential: { title: '인증 오류', subTitle: '로그인 인증에 오류가 발생했습니다.' },
  operationNotAllowed: { title: '로그인 실패!', subTitle: '로그인 과정에서 오류가 발생했습니다. 관리자에게 문의하시길 바랍니다.' },
  userDisabled: { title: '정지 계정', subTitle: '정지된 계정입니다. 관리자에게 연락하시길 바랍니다.' },
  userNotFound: { title: '계정 없음', subTitle: '해당 계정 정보가 없습니다.' },
  wrongPassword: { title: '비밀번호 재설정', subTitle: '비밀번호 재설정을 위해 가입하신 이메일 주소를 입력하여 주시기 바랍니다.' },
  userEmailNotFound: { title: '계정 없음', subTitle: '가입된 이메일 주소가 아닙니다. 다시 확인해 주시기 바랍니다.' },
  invalidEmail: { title: '계정 없음', subTitle: '이메일 형식이 올바르지 않습니다. 올바른 이메일을 입력하여 주시기 바랍니다.' },
  emailAlreadyInUse: { title: '사용할 수 없는 이메일', subTitle: '이미 사용중인 이메일입니다. 다시 확인하시기 바랍니다.' },
  weakPassword: { title: '비밀번호 경고', subTitle: '입력하신 비밀번호가 보안에 취약합니다.' },
  requiresRecentLogin: { title: '인증 만료', subTitle: '인증이 만료되었습니다. 다시 로그인 하시기 바랍니다.' },
  userMismatch: { title: '사용자 불일치', subTitle: '다른 사용자의 인증정보입니다!' },
  providerAlreadyLinked: { title: 'Already Linked', subTitle: 'Sorry, but your account is already linked to this credential.' },
  credentialAlreadyInUse: { title: '불가능 인증정보', subTitle: 'Sorry, but this credential is already used by another user.' },
  toManyrequests: { title: '확인 횟수 초과', subTitle: '잘못된 비밀번호로 확인 횟수가 초과 되었습니다. 잠시 후 다시 시도하시기 바랍니다.' },
};

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  constructor(public alertCtr: AlertController, public toastCtr: ToastController) { }

  async presentAlert(header, subtitle, message?) {
    const alert = await this.alertCtr.create({
      header,
      subHeader: subtitle,
      message,
      buttons: ['확인'],
    });
    await alert.present();
  }

  async askAlert(header, subtitle, message?) {
    const alert = await this.alertCtr.create({
      header: header,
      subHeader: subtitle,
      message: message,
      buttons: ['확인'],
    });
    await alert.present();
  }

  showErrorMessage(code) {
    switch (code) {
      // Firebase Error Messages
      case 'auth/account-exists-with-different-credential':
        this.presentAlert(
          errorMessages.accountExistsWithDifferentCredential['title'],
          errorMessages.accountExistsWithDifferentCredential['subTitle']
        );
        break;
      case 'auth/invalid-credential':
        this.presentAlert(errorMessages.invalidCredential['title'], errorMessages.invalidCredential['subTitle']);
        break;
      case 'auth/operation-not-allowed':
        this.presentAlert(errorMessages.operationNotAllowed['title'], errorMessages.operationNotAllowed['subTitle']);
        break;
      case 'auth/user-disabled':
        this.presentAlert(errorMessages.userDisabled['title'], errorMessages.userDisabled['subTitle']);
        break;
      case 'auth/user-not-found':
        this.presentAlert(errorMessages.userEmailNotFound['title'], errorMessages.userEmailNotFound['subTitle']);
        break;
      case 'auth/wrong-password':
        this.presentAlert(errorMessages.wrongPassword['title'], errorMessages.wrongPassword['subTitle']);
        break;
      case 'auth/invalid-email':
        this.presentAlert(errorMessages.invalidEmail['title'], errorMessages.invalidEmail['subTitle']);
        break;
      case 'auth/email-already-in-use':
        this.presentAlert(errorMessages.emailAlreadyInUse['title'], errorMessages.emailAlreadyInUse['subTitle']);
        break;
      case 'auth/weak-password':
        this.presentAlert(errorMessages.weakPassword['title'], errorMessages.weakPassword['subTitle']);
        break;
      case 'auth/requires-recent-login':
        this.presentAlert(errorMessages.requiresRecentLogin['title'], errorMessages.requiresRecentLogin['subTitle']);
        break;
      case 'auth/user-mismatch':
        this.presentAlert(errorMessages.userMismatch['title'], errorMessages.userMismatch['subTitle']);
        break;
      case 'auth/provider-already-linked':
        this.presentAlert(errorMessages.providerAlreadyLinked['title'], errorMessages.providerAlreadyLinked['subTitle']);
        break;
      case 'auth/credential-already-in-use':
        this.presentAlert(errorMessages.credentialAlreadyInUse['title'], errorMessages.credentialAlreadyInUse['subTitle']);
        break;
      case 'auth/too-many-requests':
        this.presentAlert(errorMessages.toManyrequests['title'], errorMessages.toManyrequests['subTitle']);
        break;
    }
  }

  ///토스트 생성///////
  async presentToast(message) {
    const toast = await this.toastCtr.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
