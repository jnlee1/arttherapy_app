import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { auth } from "firebase/app";
import { AngularFireAuth } from "@angular/fire/auth";
import { Observable, of } from "rxjs";
import { switchMap, take, map, first } from "rxjs/operators";
import { DbService } from "./db.service";
import { Platform } from "@ionic/angular";
import { LoadingController } from "@ionic/angular";
import { AlertService } from "./alert.service";
import * as firebase from "firebase/app";
// import { Storage } from '@ionic/storage';
// import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { LoadingService } from "./loading.service";
import { PushService } from "./push.service";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  user$: Observable<any>;
  userInfo: any;
  constructor(
    public afAuth: AngularFireAuth,
    public db: DbService,
    public router: Router,
    public platform: Platform,
    // public gplus: GooglePlus,
    public loadingController: LoadingController,
    // public storage: Storage,
    public alert: AlertService,
    public loadingService: LoadingService,
    public push: PushService
  ) {
    this.user$ = this.afAuth.authState.pipe(switchMap((user) => (user ? db.doc$(`users/${user.uid}`) : of(null))));

    this.user$.subscribe((data) => {
      console.log("data", data);
      if (data) {
        this.userInfo = data;
      } else {
        this.userInfo = [];
      }
    });

    this.afAuth.authState.subscribe((data) => {
      // console.log('data', data);
      if (data) {
        localStorage.setItem("userId", data.uid);
      }
    });
    this.handleRedirect();
  }
  getUser() {
    return this.user$.pipe(first()).toPromise();
  }

  uid() {
    return this.user$
      .pipe(
        take(1),
        map((u) => u && u.uid)
      )
      .toPromise();
  }

  nickname() {
    return this.user$
      .pipe(
        take(1),
        map((u) => u && u.nickname)
      )
      .toPromise();
  }

  approval() {
    return this.user$
      .pipe(
        take(1),
        map((u) => u && u.activeSwitch)
      )
      .toPromise();
  }

  userId() {
    return this.user$.pipe(
      take(1),
      map((u) => u.id)
    );
  }

  info() {
    return this.user$.pipe(take(1)).toPromise();
  }

  infoR() {
    return this.user$
      .pipe(
        take(1),
        map((u) => u && u.selectedParts)
      )
      .toPromise();
  }

  async anonymousLogin() {
    return new Promise<any>((resolve, reject) => {
      firebase
        .auth()
        .signInAnonymously()
        .then(
          (res) => resolve(res),
          (err) => reject(err)
        );
    });
  }

  async emailLogin(obj) {
    this.loadingService.load();
    try {
      var r = await this.afAuth.auth.signInWithEmailAndPassword(obj.email, obj.password);
      if (r) {
        setTimeout(() => {
          this.loadingService.hide();
          this.router.navigate(["/"]);
        }, 600);
        console.log("Successfully logged in!", r);
        localStorage.setItem("userId", r.user.uid);
      }
    } catch (error) {
      const code = error["code"];
      this.alert.showErrorMessage(code);
      console.error("야기아냐?", error);
    }
  }

  loginUser(value) {
    return new Promise<any>((resolve, reject) => {
      firebase
        .auth()
        .signInWithEmailAndPassword(value.email, value.password)
        .then(
          (res) => resolve(res),
          (err) => reject(err)
        );
    });
  }

  exitLogin(obj) {
    return new Promise<any>((resolve, reject) => {
      try {
        var r = this.afAuth.auth.signInWithEmailAndPassword(obj.email, obj.password);

        if (r) {
          console.log("Successfully logged in!");
        }
      } catch (error) {
        let code = error["code"];
        this.alert.showErrorMessage(code);

        resolve(error);
      }
    });
  }

  loginWithToken(token) {
    // tslint:disable-next-line:no-shadowed-variable
    return new Promise<any>((resolve, reject) => {
      firebase
        .auth()
        .signInWithCustomToken(token)
        .then(
          (res) => resolve(res),
          (err) => reject(err)
        );
    });
  }

  resetPassword(email) {
    this.afAuth.auth
      .sendPasswordResetEmail(email)
      .then(() => {
        this.alert.presentAlert("비밀번호 재설정", '', "비밀번호 재설정 이메일을 발송하였습니다. <br> 가입한 이메일의 메일함을 확인해주세요");
      })
      .catch((err) => {
        console.error(err);
        this.alert.showErrorMessage(err.code);
      });
  }

  async updateUserData(uid, obj?) {
    //사용자 데이터를 firestore에 업로드//

    const path = `users/${uid}`;
    const dateCreated = new Date().toISOString();
    const exitSwitch = false;
    const alarmSwitch = true;

    const data = Object.assign(
      {
        uid,
        dateCreated,
        exitSwitch,
        alarmSwitch,
      },
      obj
    );
    console.log("data", data);

    await this.db.updateAt(path, data);
    await this.loadingService.dismiss();

  }

  async signOut() {
    await this.afAuth.auth.signOut();
    await this.alert.presentToast("로그아웃하였습니다.");
    return this.router.navigate(["/"]);
  }

  registerUser(email, password) {
    return new Promise<any>((resolve, reject) => {
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(
          (res) => resolve(res),
          (err) => reject(err)
        );
    });
  }

  async register(email, password, obj) {
    try {
      var r = await this.afAuth.auth.createUserWithEmailAndPassword(email, password);

      if (r) {
        // this.router.navigate(['/']);
        // localStorage.setItem("userId", r.user.uid);
        return await this.updateUserData(r.user.uid, obj);
      }
    } catch (error) {
      let code = error["code"];
      this.alert.showErrorMessage(code);
      console.error(error);
    }
  }

  //// GOOGLE AUTH
  public async handleRedirect() {
    if (Boolean(this.isRedirect())) {
      return null;
    }
    const loading = await this.loadingService.load();

    const result = await this.afAuth.auth.getRedirectResult();

    if (result.user) {
      await this.updateUserData(result.user);
    }

    this.loadingService.hide();
    await this.setRedirect("false");
    return result;
  }

  setRedirect(val) {
    localStorage.setItem("authRedirect", val);
  }

  isRedirect() {
    return localStorage.getItem("authRedirect");
  }

  changePassword(oldPassword, newPassword) {
    return new Promise((resolve, reject) => {
      const user = this.afAuth.auth.currentUser;
      return this.afAuth.auth
        .signInWithEmailAndPassword(user.email, oldPassword)
        .then((sucess) => {
          user
            .updatePassword(newPassword)
            .then((sucess) => {
              resolve(true);

              this.alert.presentToast("비밀번호가 성공적으로 변경되었습니다.");
            })
            .catch((error) => {
              console.log("error", error);
              let code = error["code"];
              reject(false);

              this.alert.showErrorMessage(code);
            });
        })
        .catch((error) => {
          reject(false);

          let code = error["code"];
          // this.alert.showErrorMessage(code);
          console.log("oldPasswordIncorrect", error);
        });
    });
  }

  exitUser() {
    return new Promise((resolve, reject) => {
      const user = this.afAuth.auth.currentUser;
      this.db
        .updateAt(`users/${user.uid}`, { exitSwitch: true })
        .then(() => {
          user
            .delete()
            .then(() => {
              localStorage.clear();
              this.alert.presentAlert("회원탈퇴", "회원님의 계정이 탈퇴되었습니다.");
              // this.router.navigateByUrl('/login');
              reject(false);
            })
            .catch((error) => {
              console.log("error", error);
            });
        })
        .catch((error) => {
          console.log("error", error);
        });
    });
  }

  sendEmailVerificationUser(user) {
    return new Promise<void>((resolve, reject) => {
      if (firebase.auth().currentUser) {
        user
          .sendEmailVerification()
          .then(() => {
            console.log("LOG Out");
            resolve();
          })
          .catch((error) => {
            reject();
          });
      }
    });
  }

  userDetails() {
    return this.afAuth.auth.currentUser;
  }

  logoutUser() {
    return new Promise<void>((resolve, reject) => {
      if (firebase.auth().currentUser) {
        firebase
          .auth()
          .signOut()
          .then(() => {
            console.log("LOG Out");
            resolve();
          })
          .catch((error) => {
            reject();
          });
      }
    });
  }
}
