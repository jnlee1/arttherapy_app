import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CounselorService {
  [x: string]: any;

  counselor: any = {
    counselorId: '',
    uid: '',
    nickname:'',
    birth:'',
    sex:'',
    imageList: [],
    dateCreated: '',
    analysisSwitch: false,
    feedbackId: '',
    kind:'',
    questionList: [],
    pay:'',
    step:'',
    payMethod:''
  }
  nickname: any;
  kind: any;
  counselorId: string;
  pay: boolean;
  step: string;
  dateCreated: string;
  constructor() { }

  setCounselor(counselor) {
    this.counselor = counselor;
  }

  getCounselor() {
    return this.counselor;
  }
}
