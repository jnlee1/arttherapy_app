import { Injectable } from "@angular/core";
import { IamportCordova } from "@ionic-native/iamport-cordova";

@Injectable({
  providedIn: "root",
})
export class PaymentService {

  constructor() { }

  onClickPayment(params) {
    var userCode = 'imp60434470';                   // 가맹점 식별코드
    var data = {
      pg: 'kcp',                                    // PG사
      pay_method:  params[0],                        // 결제수단 : 'card' or 'trans'
      name: params[1]+' 그림 심리 검사',              // 주문명
      merchant_uid: 'mid_' + new Date().toISOString(),// 주문번호
      amount: '30000',                                 // 결제금액
      buyer_name:  params[2],               // 구매자 이름
      buyer_tel:  params[3],                   // 구매자 연락처
      buyer_email:  params[4],                 // 구매자 이메일
      app_scheme: 'cordovakcp',                     // 앱 URL 스킴
    };
    var titleOptions = {
      text: '그림 심리 검사 결제',                   // 타이틀
      textColor: '#ffffff',                         // 타이틀 색
      textSize: '20',                               // 타이틀 크기
      textAlignment: 'left',                        // 타이틀 정렬 유형
      backgroundColor: '#b52ecb',                   // 타이틀 배경색
      show: true,                                   // 타이틀 유무
      leftButtonType: 'back',                       // 왼쪽 버튼 유형
      leftButtonColor: '#ffffff',                   // 왼쪽 버튼 색
      rightButtonType: 'close',                     // 오른쪽 버튼 유형
      rightButtonColor: '#ffffff',                  // 오른쪽 버튼 색
    };

    return new Promise((resolve) => {
        var params = {
            userCode: userCode,                           // 4-1. 가맹점 식별코드 정의
            data: data,                                   // 4-2. 결제 데이터 정의
            titleOptions: titleOptions,                   // 4-3. 결제창 헤더 옵션 정의
            callback:  function(rsp) {                    // 4-3. 콜백 함수 정의
                resolve(rsp.imp_success);
            }
        };

        // 5. 결제창 호출
        IamportCordova.payment(params)
    });
  }

}
