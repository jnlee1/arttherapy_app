import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  loading;
  public isLoading = false;

  constructor(
    public loader: LoadingController) {
  }

  async load(message?) {
    this.loading = await this.loader.create({
      duration: 5000,
      translucent: true,
      cssClass: 'custom-class custom-loading',
      backdropDismiss: true,
      message
    });
    await this.loading.present();
  }

  async hide() {
    if (this.loading) {
      this.loader.dismiss();
    }
  }

  async present() {
    this.isLoading = true;
    return await this.loader.create({
      cssClass: 'custom-loader-class'
    }).then(a => {
      a.present().then(() => {
        console.log('presented');
        if (!this.isLoading) {
          a.dismiss().then(() => console.log('abort presenting'));
        }
      });
    });
  }

  async dismiss() {
    this.isLoading = false;
    return await this.loader.dismiss().then(() => console.log('dismissed'));
  }
}
