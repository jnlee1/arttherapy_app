import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/guidescreen/guidescreen.module').then(m => m.GuidescreenPageModule),
  },
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule),
  },
  {
    path: 'signup',
    loadChildren: () => import('./pages/account/signup/signup.module').then(m => m.SignupPageModule),
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/account/login/login.module').then(m => m.LoginPageModule),
  },
  {
    path: 'terms-personal-info',
    loadChildren: () => import('./pages/account/terms-personal-info/terms-personal-info.module').then(m => m.TermsPersonalInfoPageModule),
  },
  {
    path: 'terms-service',
    loadChildren: () => import('./pages/account/terms-service/terms-service.module').then(m => m.TermsServicePageModule),
  },
  {
    path: 'home-add-notice',
    loadChildren: () => import('./pages/home-add-notice/home-add-notice.module').then(m => m.HomeAddNoticePageModule),
  },
  {
    path: 'home-add-notice-setting',
    loadChildren: () => import('./pages/home-add-notice-setting/home-add-notice-setting.module').then(m => m.HomeAddNoticeSettingPageModule),
  },
  {
    path: 'home-add-mypage',
    loadChildren: () => import('./pages/home-add-mypage/home-add-mypage.module').then(m => m.HomeAddMypagePageModule),
  },
  {
    path: 'home-add-mypage-setting',
    loadChildren: () => import('./pages/home-add-mypage-setting/home-add-mypage-setting.module').then(m => m.HomeAddMypageSettingPageModule),
  },
  {
    path: 'home-add-mypage-profileedit',
    loadChildren: () =>
      import('./pages/home-add-mypage-profileedit/home-add-mypage-profileedit.module').then(m => m.HomeAddMypageProfileeditPageModule),
  },
  {
    path: 'home-add-mypage-password-edit',
    loadChildren: () =>
      import('./pages/home-add-mypage-password-edit/home-add-mypage-password-edit.module').then(m => m.HomeAddMypagePasswordEditPageModule),
  },
  {
    path: 'home-add-mypage-result',
    loadChildren: () => import('./pages/home-add-mypage-result/home-add-mypage-result.module').then(m => m.HomeAddMypageResultPageModule),
  },
  {
    path: 'image-detail',
    loadChildren: () => import('./pages/image-detail/image-detail.module').then(m => m.ImageDetailPageModule),
  },
  {
    path: 'home-add-howto',
    loadChildren: () => import('./pages/home-add-howto/home-add-howto.module').then(m => m.HomeAddHowtoPageModule),
  },
  {
    path: 'home-add-howto-img',
    loadChildren: () => import('./pages/home-add-howto-img/home-add-howto-img.module').then(m => m.HomeAddHowtoImgPageModule),
  },
  {
    path: 'home-add-howto2',
    loadChildren: () => import('./pages/home-add-howto2/home-add-howto2.module').then(m => m.HomeAddHowto2PageModule),
  },
  {
    path: 'home-add-howto3',
    loadChildren: () => import('./pages/home-add-howto3/home-add-howto3.module').then(m => m.HomeAddHowto3PageModule),
  },
  {
    path: 'home-add-howto4',
    loadChildren: () => import('./pages/home-add-howto4/home-add-howto4.module').then(m => m.HomeAddHowto4PageModule),
  },
  {
    path: 'tab1',
    loadChildren: () => import('./tab1/tab1.module').then( m => m.Tab1PageModule)
  },
  {
    path: 'tab2',
    loadChildren: () => import('./tab2/tab2.module').then( m => m.Tab2PageModule)
  },
  {
    path: 'tab3',
    loadChildren: () => import('./tab3/tab3.module').then( m => m.Tab3PageModule)
  },
  {
    path: 'tab4',
    loadChildren: () => import('./tab4/tab4.module').then( m => m.Tab4PageModule)
  },
  {
    path: 'payment',
    loadChildren: () => import('./pages/payment/payment.module').then( m => m.PaymentPageModule)
  },
  {
    path: 'home-add-draw-result',
    loadChildren: () => import('./pages/home-add-draw-result/home-add-draw-result.module').then( m => m.HomeAddDrawResultPageModule)
  },
  {
    path: 'home-add-howto-question',
    loadChildren: () => import('./pages/home-add-howto-question/home-add-howto-question.module').then( m => m.HomeAddHowtoQuestionPageModule)
  },
  {
    path: 'home-add-mypage-reservation',
    loadChildren: () => import('./pages/home-add-mypage-reservation/home-add-mypage-reservation.module').then( m => m.HomeAddMypageReservationPageModule)
  },
  {
    path: 'survey-depression',
    loadChildren: () => import('./pages/survey-depression/survey-depression.module').then( m => m.SurveyDepressionPageModule)
  },
  {
    path: 'survey-result',
    loadChildren: () => import('./pages/survey-result/survey-result.module').then( m => m.SurveyResultPageModule)
  },
  {
    path: 'tester-info',
    loadChildren: () => import('./pages/tester-info/tester-info.module').then( m => m.TesterInfoPageModule)
  },
  {
    path: 'visit-detailpage',
    loadChildren: () => import('./pages/visit/detail/visit-detail.module').then( m => m.VisitDetailPageModule)
  },
  {
    path: 'visit-reservation',
    loadChildren: () => import('./pages/visit/reservation/reservation.module').then( m => m.ReservationPageModule)
  },
  {
    path: 'refund',
    loadChildren: () => import('./pages/account/refund/refund.module').then( m => m.RefundPageModule)
  },
  {
    path: 'home-add-howto-family-img',
    loadChildren: () => import('./pages/home-add-howto-family-img/home-add-howto-family-img.module').then( m => m.HomeAddHowtoFamilyImgPageModule)
  },
  {
    path: 'home-add-howto-family1',
    loadChildren: () => import('./pages/home-add-howto-family1/home-add-howto-family1.module').then( m => m.HomeAddHowtoFamily1PageModule)
  },
  {
    path: 'home-add-howto-family2',
    loadChildren: () => import('./pages/home-add-howto-family2/home-add-howto-family2.module').then( m => m.HomeAddHowtoFamily2PageModule)
  },
  {
    path: 'survey-stress',
    loadChildren: () => import('./pages/survey-stress/survey-stress.module').then( m => m.SurveyStressPageModule)
  },
  {
    path: 'home-add-mypage-survey',
    loadChildren: () => import('./pages/home-add-mypage-survey/home-add-mypage-survey.module').then( m => m.HomeAddMypageSurveyPageModule)
  },
  {
    path: 'home-add-mypage-progressing',
    loadChildren: () => import('./pages/home-add-mypage-progressing/home-add-mypage-progressing.module').then( m => m.HomeAddMypageProgressingPageModule)
  }
];
@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
