import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tab1',
        loadChildren: () => import('../tab1/tab1.module').then(m => m.Tab1PageModule),
      },
      {
        path: 'tab2',
        loadChildren: () => import('../tab2/tab2.module').then(m => m.Tab2PageModule)
      },
      {
        path: 'tab3',
        loadChildren: () => import('../tab3/tab3.module').then(m => m.Tab3PageModule)
      },
      {
        path: 'tab4',//방문기관
        loadChildren: () => import('../tab4/tab4.module').then( m => m.Tab4PageModule)
      },
      {
        path:'home-add-mypage',//마이페이지
        loadChildren: () => import('../pages/home-add-mypage/home-add-mypage.module').then(m => m.HomeAddMypagePageModule)
      },
      {
        path:'home-add-mypage-progressing',//진행중인그림검사로
        loadChildren: () => import('../pages/home-add-mypage-progressing/home-add-mypage-progressing.module').then(m => m.HomeAddMypageProgressingPageModule)
      },
      {
        path:'home-add-draw-result',//그림검사결과
        loadChildren: () => import('../pages/home-add-draw-result/home-add-draw-result.module').then(m => m.HomeAddDrawResultPageModule)
      },
      {
        path:'home-add-mypage-result',//그림검사 상세 결과
        loadChildren: () => import('../pages/home-add-mypage-result/home-add-mypage-result.module').then(m => m.HomeAddMypageResultPageModule)
      },
      {
        path:'home-add-mypage-reservation',//예약내역
        loadChildren: () => import('../pages/home-add-mypage-reservation/home-add-mypage-reservation.module').then( m => m.HomeAddMypageReservationPageModule)
      },
      {
        path: 'survey-result',//설문지결과
        loadChildren: () => import('../pages/survey-result/survey-result.module').then( m => m.SurveyResultPageModule)
      },
      {
        path: 'home-add-mypage-survey',//설문지 결과 목록
        loadChildren: () => import('../pages/home-add-mypage-survey/home-add-mypage-survey.module').then( m => m.HomeAddMypageSurveyPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/tab1',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '',
    redirectTo: '/tabs/tab1',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
