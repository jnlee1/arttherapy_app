import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, AlertController, NavController, ToastController } from '@ionic/angular';
import { fromEvent } from 'rxjs';
import { take } from 'rxjs/operators';
import { AuthService } from 'src/services/auth.service';
import { CommonService } from 'src/services/common.service';
import { CounselorService } from 'src/services/counselor.service';
import { DbService } from 'src/services/db.service';
import { FCM } from 'plugins/cordova-plugin-fcm-with-dependecy-updated/ionic/FCM';

@Component({
  selector: 'app-tab2',
  templateUrl: './tab2.page.html',
  styleUrls: ['./tab2.page.scss'],
})

// 메뉴명
// 그림검사
export class Tab2Page implements OnInit {

  @ViewChild('slides', { static: false }) slider: IonSlides;

  loginSwitch;
  newAlarmSwitch: boolean;
  alarms$;
  counselor;
  pay;
  step;
  kind;
  counselorId;
  analysisSwitch;
  
  constructor(public alertController: AlertController, 
              private navc: NavController, 
              private auth: AuthService, 
              private db: DbService,
              private counselorService : CounselorService,
              public common : CommonService,
              private toast : ToastController) {
    this.loginSwitch = localStorage.getItem('loginSwitch');
    this.obser(); 
    this.checkAlarm();
    //this.getFCM();
    this.counselor = this.counselorService.getCounselor();
  }

  ngOnInit() {}

  //홈 tab누르면 첫번째 슬라이드로 이동하는 함수
  obser() {
    let click$ = fromEvent(document.getElementById('tab_home'), 'click'); // observable
    const observer = event => {
      this.slider.slideTo(0, 500);
    };
    click$.subscribe(observer);
  }

  //서비스 이용시 로그인 필요 alert
  async login() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '로그인',
      message: '로그인 후 이용 바랍니다.',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: '로그인',
          handler: () => {
            console.log('Confirm Okay');
            this.navc.navigateForward(['/login']);
          },
        },
      ],
    });

    await alert.present();
  }


  //검사 요청 버튼 클릭시
  gohowto(param){
    this.auth.user$.pipe(take(1)).subscribe((user: any) => {
      if (user && user.nickname) {
        this.goTest(param);
      } else {
        this.login();
      }
    });
  }
  

  //검사시작 
  async goTest(param) {
    if(param != 'HTP' && param != '동적가족화'){
      const toast = await this.toast.create({
        message: '준비중입니다.',
        duration: 2000,
      });
      toast.present();

      return;
    }

    this.db.collection$('counselor', ref => ref.where('kind','==',param)
            .where('pay','==',true)
            .where('step','!=','/tabs/home-add-draw-result'))
            .pipe(take(1)).subscribe(async (data:any)=>{

            if(data.length > 0){
              const alert = await this.alertController.create({
                cssClass: 'my-custom-class',
                header: '검사 진행',
                message: '진행 중인 검사가 있습니다.\n확인 하시겠습니까?',
                buttons: [
                  {
                    text: '확인',
                    handler: () => {
                      this.navc.navigateForward(['home-add-mypage-progressing']);
                    },
                  },
                  {
                    text: '아니오.\n신규 검사 시작',
                    handler:()=>{
                      this.goPayment(param);
                    }
                  },
                ],
              });

              await alert.present();
              
            }else{
                  this.goPayment(param);
            }
    })

    }

    //결제화면으로이동
    async goPayment(param){
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '검사 진행',
      message: '결제 화면으로 이동합니다.',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: '확인',
          handler: () => {
            this.navc.navigateForward(['/payment'],{
                queryParams:{
                  kind:param}
              });
          },
        },
      ],
    });

    await alert.present();
  }
  
  //공지사항 이동함수
  gonotice() {
    this.auth.user$.pipe(take(1)).subscribe((user: any) => {
      if (user && user.nickname) {
        this.navc.navigateForward(['home-add-notice']);
      } else {
        this.login();
      }
    });
  }

  //마이페이지 이동 함수
  async gomypage() {
    this.auth.user$.pipe(take(1)).subscribe((user: any) => {
      if (user && user.nickname) {
        this.navc.navigateForward(['tabs/home-add-mypage']);
      } else {
        this.login();
      }
    });
  }

  //새로운 알림 체크함수
  checkAlarm() {
    let pushId;
    let uid = localStorage.getItem('userId');
    this.db.doc$(`users/${uid}`).subscribe(a => (pushId = a.pushId));
    this.alarms$ = this.db.collection$(`alarm`, ref => ref.where('userId', '==', uid).where('check', '==', false).orderBy('dateCreated', 'desc'));
  }

  //오른쪽슬라이드로 이동
  nextSlide(){
    this.slider.slideNext();
  }

  //왼쪽슬라이드로 이동
  preSlide(){
    this.slider.slidePrev();
  }

   //앱에서 메시지를 수신하기 위한 함수
   getFCM() {
    FCM.onNotification().subscribe(e => {
      console.log(e);
      if (e && e.wasTapped) {
        if (e.counselorId) {
          this.navc.navigateForward('/home-add-mypage-result');
        } else {
          this.navc.navigateForward(['/tab']);
        }
      } else {
        console.log('$Received in FOREGROUND$');
      }
    });
    FCM.getInitialPushPayload().then(e => {
      if (e && e.wasTapped) {
        if (e.counselorId) {
          this.navc.navigateForward('/home-add-mypage-result');
        } else {
          this.navc.navigateForward(['/tab']);
        }
      } else {
        console.log('$Received in FOREGROUND$');
      }
    });
  }
}
