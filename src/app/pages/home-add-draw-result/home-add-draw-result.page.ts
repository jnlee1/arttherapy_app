import { AlertService } from './../../../services/alert.service';
import { LoadingService } from './../../../services/loading.service';
import { AuthService } from 'src/services/auth.service';
import { DbService } from 'src/services/db.service';
import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { NavController, AlertController, Platform ,ModalController} from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { pipe } from 'rxjs';
import { take } from 'rxjs/operators';
import { TermsPersonalInfoPage } from '../account/terms-personal-info/terms-personal-info.page';
import { TermsServicePage } from '../account/terms-service/terms-service.page';
import { ValueTransformer } from '@angular/compiler/src/util';

@Component({
  selector: 'app-home-add-draw-result',
  templateUrl: './home-add-draw-result.page.html',
  styleUrls: ['./home-add-draw-result.page.scss'],
})

// 메뉴명
// 마이페이지 > 그림 검사 결과
export class HomeAddDrawResultPage implements OnInit {
  uid;
  user$;
  counselors$;
  moreSeeSwitch: boolean = false;
  date;

  constructor(
    public modalController: ModalController,
    private navc: NavController,
    public alertController: AlertController,
    public db: DbService,
    public afAuth: AngularFireAuth,
    public auth: AuthService,
    private file: File,
    private plt: Platform,
    private transfer: FileTransfer,
    private document: DocumentViewer,
    private androidPermissions: AndroidPermissions,
    private fileOpener: FileOpener,
    private loading: LoadingService,
    private alert: AlertService
  ) {
    this.uid = localStorage.getItem('userId');
    this.getData();
   }

  ngOnInit() {
  }

  
  //더보기 함수
  switchChange() {
    this.moreSeeSwitch = true;
  }

  //유저의 상담서 불러오는 함수
  getData() {
    this.user$ = this.db.doc$(`users/${this.uid}`);
    this.counselors$ = this.db.collection$(`counselor`, ref => ref.where('uid', '==', this.uid)
                                                                  .where('step','==','/tabs/home-add-draw-result')
                                                                  .orderBy('dateCreated', 'desc'));
  }

  //프로필 수정으로 이동하는 함수
  goedit(uid) {
    this.navc.navigateForward(['/home-add-mypage-profileedit'], {
      queryParams: {
        uid,
      },
    });
  }

  //설정으로 이동하는 함수
  gosetting() {
    this.navc.navigateForward(['/home-add-mypage-setting']);
  }

  //해당 상담서의 결과서로 이동하는 함수
  goresult(counselorId) {
    this.navc.navigateForward(['/tabs/home-add-mypage-result'], {
      queryParams: {
        counselorId,
      },
    });
  }

  //다운 alert창
  async down(counselor) {
    this.db
      .doc$(`counselor/${counselor.id}`)
      .pipe(take(1))
      .subscribe(item => {
        counselor = item;
      });
    const alert = await this.alertController.create({
      cssClass: 'al',
      header: '다운로드',
      message: '그림 심리 분석 결과 PDF파일을\n 다운로드 하시겠습니까?',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: '다운로드',
          handler: () => {
            this.downloadPdf(counselor.pdfUrl);
          },
        },
      ],
    });

    await alert.present();
  }

  //PDF 다운함수
  async downloadPdf(pdfFile) {
    let path = await this.getDownloadPath();
    this.loading.load();
    this.transfer
      .create()
      .download(pdfFile, path + new Date().getTime() + '.pdf', true)
      .then(
        entry => {
          this.alert.presentToast(`다운로드를 완료하였습니다.`);
          const url = entry.toURL();
          if (this.plt.is('android')) {
            this.fileOpener
              .open(url, 'application/pdf')
              .then(() => {
                this.loading.hide();
              })
              .catch(error => {
                this.loading.hide();
                console.log('error', error);
              });
          } else {
            const options: DocumentViewerOptions = {
              title: 'My PDF',
            };
            this.document.viewDocument(url, 'application/pdf', options);
          }
        },
        err => {
          console.log('Error', err);
          this.loading.hide();
        }
      );
  }

  //PDF파일 다운받을 경로 지정 함수
  public async getDownloadPath() {
    if (this.plt.is('ios')) {
      return this.file.documentsDirectory;
    }

    await this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(result => {
      if (!result.hasPermission) {
        return this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE);
      }
    });

    return this.file.externalRootDirectory + '/Download/';
  }


}

