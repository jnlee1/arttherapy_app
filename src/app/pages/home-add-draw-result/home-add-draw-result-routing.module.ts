import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddDrawResultPage } from './home-add-draw-result.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddDrawResultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddDrawResultPageRoutingModule {}
