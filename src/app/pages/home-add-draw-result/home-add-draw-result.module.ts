import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddDrawResultPageRoutingModule } from './home-add-draw-result-routing.module';

import { HomeAddDrawResultPage } from './home-add-draw-result.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddDrawResultPageRoutingModule
  ],
  declarations: [HomeAddDrawResultPage]
})
export class HomeAddDrawResultPageModule {}
