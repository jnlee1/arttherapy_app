import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddMypagePageRoutingModule } from './home-add-mypage-routing.module';

import { HomeAddMypagePage } from './home-add-mypage.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddMypagePageRoutingModule
  ],
  declarations: [HomeAddMypagePage]
})
export class HomeAddMypagePageModule {}
