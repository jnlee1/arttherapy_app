import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddMypagePage } from './home-add-mypage.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddMypagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddMypagePageRoutingModule {}
