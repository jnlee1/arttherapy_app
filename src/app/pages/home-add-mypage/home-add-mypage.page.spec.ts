import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAddMypagePage } from './home-add-mypage.page';

describe('HomeAddMypagePage', () => {
  let component: HomeAddMypagePage;
  let fixture: ComponentFixture<HomeAddMypagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAddMypagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAddMypagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
