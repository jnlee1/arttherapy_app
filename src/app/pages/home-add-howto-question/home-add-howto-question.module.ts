import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddHowtoQuestionPageRoutingModule } from './home-add-howto-question-routing.module';

import { HomeAddHowtoQuestionPage } from './home-add-howto-question.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddHowtoQuestionPageRoutingModule
  ],
  declarations: [HomeAddHowtoQuestionPage]
})
export class HomeAddHowtoQuestionPageModule {}
