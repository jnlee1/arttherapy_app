import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddHowtoQuestionPage } from './home-add-howto-question.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddHowtoQuestionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddHowtoQuestionPageRoutingModule {}
