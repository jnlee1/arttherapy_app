import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonSlides, ToastController, NavController } from '@ionic/angular';
import { take } from 'rxjs/operators';
import { CounselorService } from 'src/services/counselor.service';
import { DbService } from 'src/services/db.service';

@Component({
  selector: 'app-home-add-howto-question',
  templateUrl: './home-add-howto-question.page.html',
  styleUrls: ['./home-add-howto-question.page.scss'],
})

// 메뉴명
// 그림검사탭 > 모든 그림검사 > 그림 질문및답변 , 최종 검사요청 화면
export class HomeAddHowtoQuestionPage implements OnInit {
  @ViewChild('slides', { static: false }) slider: IonSlides;

  questionslist=new Array();
  answerlist:any[];
  answer;
  slides: any;
  docId;
  counselor;
  
  constructor(public db:DbService,
              public route:ActivatedRoute,
              private toastController : ToastController,
              public navc : NavController,
              public counselorService : CounselorService) {
              this.counselor = this.counselorService.getCounselor();
    
    this.route.queryParams.subscribe((params)=>{
      this.docId=params['docId'];
      this.getQuestion(params['kind']);
    })
    
  }
  
  ngOnInit() {
  }
  
  //선택한 그림검사 질문 조회
  //이중 FOR문으로 변경 예정
  getQuestion(kind){
    if(kind == null){
      kind = this.counselor.kind;
    }
    
   if(kind == 'HTP'){

    this.db.collection$(`question`,ref=>ref.where('kind','==', 'HTP').where('subKind','==', '집').where('use','==',true))
    .pipe(take(1))
    .subscribe((question:any)=>{
      if(question[0].questionList != null){
            let items = question[0].questionList;
            
            for(var i=0; i<items.length; i++){
              let item={'question':items[i], 'answer':null};
              this.questionslist.push(item);
            }
        }
      });

    this.db.collection$(`question`,ref=>ref.where('kind','==', 'HTP').where('subKind','==', '나무').where('use','==',true))
    .pipe(take(1))
    .subscribe((question:any)=>{
      if(question[0].questionList != null){
        let items = question[0].questionList;

        for(var i=0; i<items.length; i++){
          let item={'question':items[i], 'answer':null};
          this.questionslist.push(item);
          }
      }
      });

    this.db.collection$(`question`,ref=>ref.where('kind','==', 'HTP').where('subKind','==', '사람1').where('use','==',true))
    .pipe(take(1))
    .subscribe((question:any)=>{
      if(question[0].questionList != null){
        let items = question[0].questionList;

        for(var i=0; i<items.length; i++){
          let item={'question':items[i], 'answer':null};
          this.questionslist.push(item);
          }
        }
      });
    
    this.db.collection$(`question`,ref=>ref.where('kind','==', 'HTP').where('subKind','==', '사람2').where('use','==',true))
    .pipe(take(1))
    .subscribe((question:any)=>{
      if(question[0].questionList != null){
        let items = question[0].questionList;

        for(var i=0; i<items.length; i++){
          let item={'question':items[i], 'answer':null};
          this.questionslist.push(item);
          }
        }
      });

    }else{
      this.db.collection$(`question`,ref=>ref.where('kind','==', kind).where('use','==',true))
    .pipe(take(1))
    .subscribe((question:any)=>{
      if(question[0].questionList != null){
            let items = question[0].questionList;
            
            for(var i=0; i<items.length; i++){
              let item={'question':items[i], 'answer':null};
              this.questionslist.push(item);
            }
        }
      });
   }


      console.log(' 최종 질문 리스트 : ' + this.questionslist);
  }

  //답변 수정시
  answerChanged(no,event){
    this.questionslist[no].answer = event.target.value;
  }

  //슬라이드 변경시 마지막 슬라이드 여부 체크
  slideChanged() {
    this.slider.isEnd().then(Boolean=>{
      if(Boolean){
        var saveBtn = <HTMLInputElement> document.getElementById("quesionSaveBtn");
            saveBtn.disabled = false;
      }
    });
  } 

  //경고창
  async alertToast() {
    const toast = await this.toastController.create({
      message: "답변을 입력하세요.",
      duration: 2000,
    });
    toast.present();
  }

  //검사요청시
   quesionSave(){
      for(var i=0; i<this.questionslist.length; i++){
          let item = this.questionslist[i].answer;
          if(item == null){
              this.alertToast();
              this.slider.slideTo(i);
              return;
          }
      }

      this.counselor.questionList = this.questionslist;
      if(!this.docId){
        this.docId = this.counselor.counselorId;
      }

      //질문 저장
      try {
        this.db.updateAt(`counselor/${this.docId}`, {questionList:this.counselor.questionList,step:'/tabs/home-add-draw-result'})
        //검사목록으로이동
        this.successToast();
        this.navc.navigateForward(['/tabs/home-add-draw-result']);
        
      } catch (error) {
        this.errorToast(error)
      }
    


   }

  //검사요청 완료시 Toast
  async successToast() {
    const toast = await this.toastController.create({
      message: "검사요청이 완료 되었습니다.",
      duration: 2000,
    });
    toast.present();
  }

  //error Toast
  async errorToast(error) {
    console.log(error);
    const toast = await this.toastController.create({
      message: "에러가 발생하였습니다. 관리자에게 문의하세요.",
      duration: 2000,
    });
    toast.present();
  }
       
}
