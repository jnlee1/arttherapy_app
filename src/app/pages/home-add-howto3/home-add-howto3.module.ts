import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddHowto3PageRoutingModule } from './home-add-howto3-routing.module';

import { HomeAddHowto3Page } from './home-add-howto3.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddHowto3PageRoutingModule
  ],
  declarations: [HomeAddHowto3Page]
})
export class HomeAddHowto3PageModule {}
