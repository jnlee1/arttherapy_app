import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, NavController } from '@ionic/angular';

@Component({
  selector: 'app-home-add-howto3',
  templateUrl: './home-add-howto3.page.html',
  styleUrls: ['./home-add-howto3.page.scss'],
})

// 메뉴명
// 그림검사탭 > HTP 그림검사 > 그림검사 실시 3번
export class HomeAddHowto3Page implements OnInit {
  @ViewChild('slides', { static: true }) slider: IonSlides;
  constructor(private navc: NavController) { }

  ngOnInit() { this.slider.lockSwipes(true)}
  slideOpts = {
    initialSlide: 0,
    speed: 400,
    allowTouchMove: false,
  };
  slidenext() {
    this.navc.navigateForward('home-add-howto4');
  }
}
