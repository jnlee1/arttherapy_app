import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddHowto3Page } from './home-add-howto3.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddHowto3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddHowto3PageRoutingModule {}
