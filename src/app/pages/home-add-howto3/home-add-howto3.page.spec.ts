import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAddHowto3Page } from './home-add-howto3.page';

describe('HomeAddHowto3Page', () => {
  let component: HomeAddHowto3Page;
  let fixture: ComponentFixture<HomeAddHowto3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAddHowto3Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAddHowto3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
