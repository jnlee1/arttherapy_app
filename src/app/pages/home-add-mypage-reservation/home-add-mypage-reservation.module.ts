import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddMypageReservationPageRoutingModule } from './home-add-mypage-reservation-routing.module';

import { HomeAddMypageReservationPage } from './home-add-mypage-reservation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddMypageReservationPageRoutingModule
  ],
  declarations: [HomeAddMypageReservationPage]
})
export class HomeAddMypageReservationPageModule {}
