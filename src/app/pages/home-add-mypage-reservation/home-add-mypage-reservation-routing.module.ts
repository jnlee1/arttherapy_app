import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddMypageReservationPage } from './home-add-mypage-reservation.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddMypageReservationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddMypageReservationPageRoutingModule {}
