import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAddMypageReservationPage } from './home-add-mypage-reservation.page';

describe('HomeAddMypageReservationPage', () => {
  let component: HomeAddMypageReservationPage;
  let fixture: ComponentFixture<HomeAddMypageReservationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAddMypageReservationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAddMypageReservationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
