import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthService } from 'src/services/auth.service';
import { DbService } from 'src/services/db.service';
import { ReservationService } from 'src/services/reservation.service';

@Component({
  selector: 'app-home-add-mypage-reservation',
  templateUrl: './home-add-mypage-reservation.page.html',
  styleUrls: ['./home-add-mypage-reservation.page.scss'],
})

// 메뉴명
// 마이페이지 > 예약 내역
export class HomeAddMypageReservationPage implements OnInit {

  uid;
  user$;
  rsvtlist$;
  date;


  constructor(private navc : NavController,
              public db : DbService,
              public auth : AuthService,
              public rsvt : ReservationService) {

        this.uid = localStorage.getItem('userId');
        this.getData();
        }

  ngOnInit() {
  }

  //예약내역 조회
  getData() {
    this.user$ = this.db.doc$(`users/${this.uid}`);
    this.rsvtlist$ = this.db.collection$(`reservation`, ref => ref.where('uid', '==', this.uid).orderBy('rsvtDate', 'desc'));
  }

  //기관정보
  fnGoRevt(paramId){
    this.navc.navigateForward(['visit-detailpage'],{
      queryParams:{
        docId:paramId,
        rInfo:true
      }
    });
  }



}
