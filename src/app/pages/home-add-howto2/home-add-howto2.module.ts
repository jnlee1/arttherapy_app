import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddHowto2PageRoutingModule } from './home-add-howto2-routing.module';

import { HomeAddHowto2Page } from './home-add-howto2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddHowto2PageRoutingModule
  ],
  declarations: [HomeAddHowto2Page]
})
export class HomeAddHowto2PageModule {}
