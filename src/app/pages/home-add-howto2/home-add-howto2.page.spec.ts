import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAddHowto2Page } from './home-add-howto2.page';

describe('HomeAddHowto2Page', () => {
  let component: HomeAddHowto2Page;
  let fixture: ComponentFixture<HomeAddHowto2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAddHowto2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAddHowto2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
