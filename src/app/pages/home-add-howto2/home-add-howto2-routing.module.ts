import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddHowto2Page } from './home-add-howto2.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddHowto2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddHowto2PageRoutingModule {}
