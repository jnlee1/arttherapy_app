import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddNoticeSettingPage } from './home-add-notice-setting.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddNoticeSettingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddNoticeSettingPageRoutingModule {}
