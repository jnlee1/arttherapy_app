import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAddNoticeSettingPage } from './home-add-notice-setting.page';

describe('HomeAddNoticeSettingPage', () => {
  let component: HomeAddNoticeSettingPage;
  let fixture: ComponentFixture<HomeAddNoticeSettingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAddNoticeSettingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAddNoticeSettingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
