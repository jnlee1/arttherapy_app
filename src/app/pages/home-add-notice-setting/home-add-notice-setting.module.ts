import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddNoticeSettingPageRoutingModule } from './home-add-notice-setting-routing.module';

import { HomeAddNoticeSettingPage } from './home-add-notice-setting.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddNoticeSettingPageRoutingModule
  ],
  declarations: [HomeAddNoticeSettingPage]
})
export class HomeAddNoticeSettingPageModule {}
