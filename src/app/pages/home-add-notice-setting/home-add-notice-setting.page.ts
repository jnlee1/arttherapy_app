import { Component, OnInit } from '@angular/core';
import { DbService } from 'src/services/db.service';
import { getLocaleDateFormat } from '@angular/common';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home-add-notice-setting',
  templateUrl: './home-add-notice-setting.page.html',
  styleUrls: ['./home-add-notice-setting.page.scss'],
})

// 메뉴명
// 알림내역 > 설정
export class HomeAddNoticeSettingPage implements OnInit {
  alarmSwitch: boolean;
  uid;
  constructor(
    public db: DbService,
  ) {
    this.uid = localStorage.getItem('userId');
    this.getData();
  }

  //유저의 알람여부를 가져오는 함수
  getData() {
    this.db.doc$(`users/${this.uid}`).subscribe(data => {
      this.alarmSwitch = data.alarmSwitch;
    });
  }
  ngOnInit() {
  }

  //유저의 알람여부를 수정하는 함수
  onChange() {
    this.db.updateAt(`users/${this.uid}`, {
      alarmSwitch: this.alarmSwitch,
    });
  }

}
