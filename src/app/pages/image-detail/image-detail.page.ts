import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-image-detail',
  templateUrl: './image-detail.page.html',
  styleUrls: ['./image-detail.page.scss'],
})

// 메뉴명
// 마이페이지 > 그림검사결과 > 
// 자세히 보기 > 이미지 확대창 (이미지 선택하면 확대됨)
export class ImageDetailPage implements OnInit {
  img = '';

  constructor(
    public modalCtrl: ModalController,
    private navParams: NavParams,
  ) {
    this.img = this.navParams.get("url");
  }

  ngOnInit() { }

  //상세이미지창
  cancel() {
    this.modalCtrl.dismiss({
      dismissed: true,
    });
  }
}
