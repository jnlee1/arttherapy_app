import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-survey-result',
  templateUrl: './survey-result.page.html',
  styleUrls: ['./survey-result.page.scss'],
})

// 메뉴명
// 설문지 검사 탭 > 설문지 검사 후 > 검사 결과
export class SurveyResultPage implements OnInit {
  title;
  sum;
  result;
  radioResult;
  randomNumber;

  constructor(public router:ActivatedRoute,private navc:NavController) { 
    this.router.queryParams.subscribe((params) => {
      this.title = params["title"];
      this.sum = params["sum"];
      if(this.title =='우울증' ){
        this.setDeep(this.sum);
      }else{
        this.setStress(this.sum);
      }
    });

    this.randomNumber = Math.floor(Math.random() * (6 - 1)) + 1;
  }

  ngOnInit() {
  }

  //설문지 >우울증
  setDeep(sum){
    if(sum >= 25){
      this.result = '매우 심한 우울감'
      this.radioResult = 'r4'
    }else if( sum >=21){
      this.result ='심한 우울감'
      this.radioResult = 'r3'
    }else if(sum >=16){
      this.result = '약간의/보통의 우울감'
      this.radioResult = 'r2'
    }else{
      this.result ='양호'
      this.radioResult = 'r1'
    }
  }

  //설문지 >스트레스
  setStress(sum){
    if(sum >= 51){
      this.result = '심각'
      this.radioResult = 'r4'
    }else if( sum >=25){
      this.result ='보통'
      this.radioResult = 'r3'
    }else{
      this.result = '양호'
      this.radioResult = 'r2'
    }
  }

  //설문지 >MMPI
  //설문지 >MBTI



  goHome(){
    this.navc.navigateForward(['/tabs/tab3']);
  }

}
