import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddHowtoFamilyImgPage } from './home-add-howto-family-img.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddHowtoFamilyImgPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddHowtoFamilyImgPageRoutingModule {}
