import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddHowtoFamilyImgPageRoutingModule } from './home-add-howto-family-img-routing.module';

import { HomeAddHowtoFamilyImgPage } from './home-add-howto-family-img.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddHowtoFamilyImgPageRoutingModule
  ],
  declarations: [HomeAddHowtoFamilyImgPage]
})
export class HomeAddHowtoFamilyImgPageModule {}
