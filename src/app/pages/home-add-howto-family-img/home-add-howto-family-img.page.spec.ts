import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAddHowtoFamilyImgPage } from './home-add-howto-family-img.page';

describe('HomeAddHowtoFamilyImgPage', () => {
  let component: HomeAddHowtoFamilyImgPage;
  let fixture: ComponentFixture<HomeAddHowtoFamilyImgPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAddHowtoFamilyImgPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAddHowtoFamilyImgPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
