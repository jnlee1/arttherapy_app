import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddHowtoFamily2Page } from './home-add-howto-family2.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddHowtoFamily2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddHowtoFamily2PageRoutingModule {}
