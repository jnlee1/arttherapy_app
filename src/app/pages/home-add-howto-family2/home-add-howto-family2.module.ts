import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddHowtoFamily2PageRoutingModule } from './home-add-howto-family2-routing.module';

import { HomeAddHowtoFamily2Page } from './home-add-howto-family2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddHowtoFamily2PageRoutingModule
  ],
  declarations: [HomeAddHowtoFamily2Page]
})
export class HomeAddHowtoFamily2PageModule {}
