import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, NavController } from '@ionic/angular';
import { CounselorService } from 'src/services/counselor.service';
import { DbService } from 'src/services/db.service';

@Component({
  selector: 'app-home-add-howto-family2',
  templateUrl: './home-add-howto-family2.page.html',
  styleUrls: ['./home-add-howto-family2.page.scss'],
})

// 메뉴명
// 그림검사탭 > 동적가족화 그림검사 > 그림검사 실시 2번
export class HomeAddHowtoFamily2Page implements OnInit {
  @ViewChild('slides', { static: true }) slider: IonSlides;
  counselor;

  constructor(private navc: NavController,
              public counselorService : CounselorService,
              public db : DbService) {
    this.counselor = this.counselorService.getCounselor();
              }

  ngOnInit() {this.slider.lockSwipes(true);}

  slideOpts = {
    initialSlide: 0,
    speed: 400,
    allowTouchMove: false,
  };

  slidenext() {
    this.counselor.step='home-add-howto-family-img';
    this.db.updateAt(`counselor/${this.counselor.counselorId}`,{step:this.counselor.step});
    this.navc.navigateForward('home-add-howto-family-img');
  }
}
