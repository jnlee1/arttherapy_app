import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAddHowtoFamily2Page } from './home-add-howto-family2.page';

describe('HomeAddHowtoFamily2Page', () => {
  let component: HomeAddHowtoFamily2Page;
  let fixture: ComponentFixture<HomeAddHowtoFamily2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAddHowtoFamily2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAddHowtoFamily2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
