import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddNoticePageRoutingModule } from './home-add-notice-routing.module';

import { HomeAddNoticePage } from './home-add-notice.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddNoticePageRoutingModule
  ],
  declarations: [HomeAddNoticePage]
})
export class HomeAddNoticePageModule {}
