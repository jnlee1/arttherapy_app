import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { DbService } from 'src/services/db.service';
import { take } from 'rxjs/internal/operators/take';

@Component({
  selector: 'app-home-add-notice',
  templateUrl: './home-add-notice.page.html',
  styleUrls: ['./home-add-notice.page.scss'],
})

// 메뉴명
// 알림내역
export class HomeAddNoticePage implements OnInit {
  alarms$;
  constructor(
    private navc: NavController,
    public toastController: ToastController,
    public db: DbService,
  ) {
    this.getData();
  }

  ngOnInit() { }

  //유저의 알람들을 가져오는 함수
  getData() {
    let uid = localStorage.getItem('userId');
    this.alarms$ = this.db.collection$('alarm', ref => ref.where('userId', '==', uid).orderBy('dateCreated', 'desc')).subscribe(a => console.log(a));
    this.alarms$ = this.db.collection$('alarm', ref => ref.where('userId', '==', uid).orderBy('dateCreated', 'desc'));
  }

  //다른 페이지로 가면 알람확인이 되는 함수
  ionViewWillLeave() {
    this.alarms$.pipe(take(1)).subscribe(datas => {
      let nocheck = datas.filter(ele => !ele.check);
      nocheck.forEach(element => {
        this.db.updateAt(`alarm/${element.id}`, {
          check: true,
        });
      });
    });
  }

  //알람 설정페이지로 이동하는 함수
  gosetting() {
    this.navc.navigateForward(['/home-add-notice-setting']);
  }

  //결과서 목록으로 이동하는 함수
  goFeedback(counselorId) {
    try {
      this.db.collection$(`feedback`, ref => ref.where('counselorId', '==', counselorId))
        .pipe(take(1)).subscribe((feedback: any) => {
          if (feedback[0].counselorId) {
            this.navc.navigateForward(['/tabs/home-add-mypage-result'], {
              queryParams: {
                counselorId: feedback[0].counselorId,
              }
            })
          }
        });
    } catch (e) {
      this.noneFeedback();
    }
  }

  //결과서가 없는 경우
  async noneFeedback() {
    const toast = await this.toastController.create({
      message: "결과서를 찾을 수 없습니다.",
      duration: 2000,
    });
    toast.present();
  }

//결과 페이지로 이동하는 함수
  goMypage(text) {
    if(text.substring(0,3) == '은행명'){
      return false;
    }else{
      this.navc.navigateForward(['/tabs/home-add-draw-result']);
    }
  }


}
