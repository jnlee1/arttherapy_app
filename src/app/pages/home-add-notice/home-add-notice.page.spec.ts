import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAddNoticePage } from './home-add-notice.page';

describe('HomeAddNoticePage', () => {
  let component: HomeAddNoticePage;
  let fixture: ComponentFixture<HomeAddNoticePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAddNoticePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAddNoticePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
