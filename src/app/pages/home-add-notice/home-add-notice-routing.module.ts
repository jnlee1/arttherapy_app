import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddNoticePage } from './home-add-notice.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddNoticePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddNoticePageRoutingModule {}
