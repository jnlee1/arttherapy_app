import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddMypageProgressingPage } from './home-add-mypage-progressing.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddMypageProgressingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddMypageProgressingPageRoutingModule {}
