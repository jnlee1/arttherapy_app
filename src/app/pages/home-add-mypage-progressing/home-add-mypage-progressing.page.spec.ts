import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAddMypageProgressingPage } from './home-add-mypage-progressing.page';

describe('HomeAddMypageProgressingPage', () => {
  let component: HomeAddMypageProgressingPage;
  let fixture: ComponentFixture<HomeAddMypageProgressingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAddMypageProgressingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAddMypageProgressingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
