import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddMypageProgressingPageRoutingModule } from './home-add-mypage-progressing-routing.module';

import { HomeAddMypageProgressingPage } from './home-add-mypage-progressing.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddMypageProgressingPageRoutingModule
  ],
  declarations: [HomeAddMypageProgressingPage]
})
export class HomeAddMypageProgressingPageModule {}
