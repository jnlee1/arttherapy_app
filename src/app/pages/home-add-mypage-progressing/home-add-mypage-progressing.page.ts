import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonSlides, NavController } from '@ionic/angular';
import { DbService } from 'src/services/db.service';
import { CounselorService } from './../../../services/counselor.service';

@Component({
  selector: 'app-home-add-mypage-progressing',
  templateUrl: './home-add-mypage-progressing.page.html',
  styleUrls: ['./home-add-mypage-progressing.page.scss'],
})

// 메뉴명
// 마이페이지 > 검사 진행(결제한 상태이며 진행중인 검사 목록)
export class HomeAddMypageProgressingPage implements OnInit {
  uid;
  user$;
  items$;
  counselor:any;

  constructor(private db:DbService, private navc:NavController, private CounselorService: CounselorService,) {
    this.counselor = this.CounselorService.getCounselor();
    this.uid = localStorage.getItem('userId');
    this.getData();
   }

  ngOnInit() {
  }

  getData(){
    this.user$ = this.db.doc$(`users/${this.uid}`);
    this.items$ = this.db.collection$(`counselor`,ref=>ref.where('uid','==',this.uid)
                                                          .where('pay','==',true)
                                                          .where('step','!=','/tabs/home-add-draw-result'));
  }

  //진행 중인 검사로 이동
  goNextTest(id,kind,path){
    this.counselor.counselorId = id;
    this.counselor.kind = kind;
    this.navc.navigateForward([path]);
  }
}
