import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { IonSlides, NavController } from '@ionic/angular';

@Component({
  selector: 'app-home-add-howto',
  templateUrl: './home-add-howto.page.html',
  styleUrls: ['./home-add-howto.page.scss'],
})

// 메뉴명
// 그림검사탭 > HTP 그림검사 > 그림검사 실시 1번
export class HomeAddHowtoPage implements OnInit {
  @ViewChild('slides', { static: true }) slider: IonSlides;
  constructor(private navc: NavController) { }

  ngOnInit() {this.slider.lockSwipes(true) }

  slideOpts = {
    initialSlide: 0,
    speed: 400,
    allowTouchMove: false,
  };

  slidenext() {
    this.navc.navigateForward('home-add-howto2');
  }
}
