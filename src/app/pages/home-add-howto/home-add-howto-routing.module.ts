import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddHowtoPage } from './home-add-howto.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddHowtoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddHowtoPageRoutingModule {}
