import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddHowtoPageRoutingModule } from './home-add-howto-routing.module';

import { HomeAddHowtoPage } from './home-add-howto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddHowtoPageRoutingModule
  ],
  declarations: [HomeAddHowtoPage]
})
export class HomeAddHowtoPageModule {}
