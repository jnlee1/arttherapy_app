import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddHowto4PageRoutingModule } from './home-add-howto4-routing.module';

import { HomeAddHowto4Page } from './home-add-howto4.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddHowto4PageRoutingModule
  ],
  declarations: [HomeAddHowto4Page]
})
export class HomeAddHowto4PageModule {}
