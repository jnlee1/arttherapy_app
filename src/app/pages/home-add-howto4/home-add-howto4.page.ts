import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, NavController } from '@ionic/angular';
import { CounselorService } from 'src/services/counselor.service';
import { DbService } from 'src/services/db.service';

@Component({
  selector: 'app-home-add-howto4',
  templateUrl: './home-add-howto4.page.html',
  styleUrls: ['./home-add-howto4.page.scss'],
})

// 메뉴명
// 그림검사탭 > HTP 그림검사 > 그림검사 실시 4번
export class HomeAddHowto4Page implements OnInit {
  @ViewChild('slides', { static: true }) slider: IonSlides;
  counselor;

  constructor(private navc: NavController,
              public counselorService : CounselorService,
              public db : DbService) {
    this.counselor = this.counselorService.getCounselor();
              }

  ngOnInit() {this.slider.lockSwipes(true)}

  slideOpts = {
    initialSlide: 0,
    speed: 400,
    allowTouchMove: false,
  };

  slidenext() {
    this.counselor.step='home-add-howto-img';
    this.db.updateAt(`counselor/${this.counselor.counselorId}`,{step:this.counselor.step});
    this.navc.navigateForward('home-add-howto-img');
  }
}
