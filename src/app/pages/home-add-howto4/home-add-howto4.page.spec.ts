import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAddHowto4Page } from './home-add-howto4.page';

describe('HomeAddHowto4Page', () => {
  let component: HomeAddHowto4Page;
  let fixture: ComponentFixture<HomeAddHowto4Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAddHowto4Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAddHowto4Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
