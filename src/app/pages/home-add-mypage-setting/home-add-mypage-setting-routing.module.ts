import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddMypageSettingPage } from './home-add-mypage-setting.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddMypageSettingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddMypageSettingPageRoutingModule {}
