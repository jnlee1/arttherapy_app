import { AuthService } from 'src/services/auth.service';
import { Component, OnInit } from '@angular/core';
import { ModalController, NavController, AlertController, ToastController } from '@ionic/angular';
import { TermsServicePage } from '../account/terms-service/terms-service.page';
import { TermsPersonalInfoPage } from '../account/terms-personal-info/terms-personal-info.page';
import { LoadingService } from 'src/services/loading.service';

@Component({
  selector: 'app-home-add-mypage-setting',
  templateUrl: './home-add-mypage-setting.page.html',
  styleUrls: ['./home-add-mypage-setting.page.scss'],
})

// 메뉴명
// 마이 페이지 > 설정
export class HomeAddMypageSettingPage implements OnInit {
  constructor(
    public modalController: ModalController,
    private navc: NavController,
    public load: LoadingService,
    public alertController: AlertController,
    public toastController: ToastController,
    public auth: AuthService,
  ) { }

  ngOnInit() { }

  //서비스정보 모달창 띄어주는 함수
  async service() {
    const modal = await this.modalController.create({
      component: TermsServicePage,
      cssClass: 'my-custom-class',
    });
    return await modal.present();
  }

  //개인정보이용약관 모달창 띄어주는 함수
  async personal() {
    const modal = await this.modalController.create({
      component: TermsPersonalInfoPage,
      cssClass: 'my-custom-class',
    });
    return await modal.present();
  }

  //비밀번호 수정페이지로 이동하는 함수
  gopassword() {
    this.navc.navigateForward(['/home-add-mypage-password-edit']);
  }

  //로그아웃 기능
  async logout() {
    const alert = await this.alertController.create({
      cssClass: 'al',
      header: '로그아웃',
      message: '로그아웃 하시겠습니까?',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: '로그아웃',
          handler: () => {
            this.auth.logoutUser().then(() => {
              localStorage.clear();
              this.navc.navigateRoot(['/tabs/tab1']).then(() => {
                this.navc.navigateForward(['/login']).then(() => {
                  this.successLogout();
                });
              });
            });
          }
        }
      ]
    });

    await alert.present();
  }

  async successLogout() {
    const toast = await this.toastController.create({
      message: "정상적으로 로그아웃 처리 되었습니다.",
      duration: 2000,
    });
    toast.present();
  }

  //회원탈퇴 기능
  async membership() {
    const alert = await this.alertController.create({
      cssClass: 'al',
      header: '회원탈퇴',
      message: '회원탈퇴를 진행하시겠습니까?<br>탈퇴한 계정은 복구 할 수 없습니다.',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: '회원탈퇴',
          handler: () => {
            this.auth.exitUser();
            this.navc.navigateRoot(['/login']).then(() => {
              this.successExit();
            });
          },
        },
      ],
    });

    await alert.present();
  }

  async successExit() {
    const toast = await this.toastController.create({
      message: "정상적으로 회원탈퇴 처리 되었습니다.",
      duration: 2000,
    });
    toast.present();
  }
}
