import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddMypageSettingPageRoutingModule } from './home-add-mypage-setting-routing.module';

import { HomeAddMypageSettingPage } from './home-add-mypage-setting.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddMypageSettingPageRoutingModule
  ],
  declarations: [HomeAddMypageSettingPage]
})
export class HomeAddMypageSettingPageModule {}
