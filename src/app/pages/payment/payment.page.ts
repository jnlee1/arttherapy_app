import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';
import { take } from 'rxjs/operators';
import { AuthService } from 'src/services/auth.service';
import { DbService } from 'src/services/db.service';
// 2. 아임포트 코르도바 플러그인 임포트
import { IamportCordova } from '@ionic-native/iamport-cordova';
import { CounselorService } from 'src/services/counselor.service';
import { CommonService } from 'src/services/common.service';
import { PaymentService } from "src/services/payment.service";

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})

// 메뉴명
// 그림검사탭 > 모든 검사요청 > 결제화면
export class PaymentPage implements OnInit {
  user$;
  user:any={};
  uid;
  btnNext:boolean=false;
  divHidden:boolean=false;
  method;
  pay:boolean=false;
  kind;
  counselor;
  

  constructor(private navc : NavController,
              private db : DbService,
              private auth : AuthService,
              private route:ActivatedRoute,
              public alertController: AlertController,
              private common :CommonService,
              private counselorService : CounselorService,
              private payment : PaymentService
              ) {
      this.counselor = this.counselorService.getCounselor();
      this.route.queryParams.subscribe((params) => {
      this.kind = params["kind"];
    }); }

  ngOnInit() {
    this.uid = localStorage.getItem('userId');
    if(this.uid == 'gwUU2uhhCNZCchyLZunnC81WPH03'||this.uid == 'wVbSQ2dtL5ejWK8NUZejbmrlho82'){
      this.btnNext = true;
    }
    this.getData();
  }

  //유저의 정보를 가져오는 함수
  getData() {
    this.auth.user$.pipe(take(1)).subscribe((userInfo:any)=>{
      this.user=userInfo;
    })
  }

   //결제 후 alert
   async resultAlert(param){
    console.log('결제 여부' +param);
    if(param == 'true'){
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: '결제 완료',
        message: '검사를 진행합니다.',
        buttons: [
          {
            text: '확인',
            handler: () => {
              this.pay =true;
              this.createCounselor('/tester-info');
            },
          },
        ],
      });

      await alert.present();
    }else{
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: '미 결제',
        message: '결제를 다시 시도하세요.',
        buttons: [
          {
            text: '확인',
            handler: () => {
              this.navc.navigateForward(['/tabs/tab2']);
            },
          },
        ],
      });

      await alert.present();
    }
   }


  //다음화면으로 이동
   next(){
    this.navc.navigateForward(['/home-add-howto']);
  }

  //취소하고 그림검사화면으로 이동
  async goHome(){
    if(this.method == null){
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: '결제 취소',
        message: '결제수단을 선택하지 않았습니다.\n결제를 취소하시겠습니까?',
        buttons: [
          {
            text: '취소',
            role: 'cancel',
            cssClass: 'secondary',
          },
          {
            text: '확인',
            handler: () => {
              this.navc.navigateForward(['/tabs/tab2']);
            },
          },
        ],
      });

      await alert.present();
    }else if(this.method == 'nopassbook'){
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: '홈으로',
        message: '무통장 정보를 메모에 적어두셨습니까?',
        buttons: [
          {
            text: '취소',
            role: 'cancel',
            cssClass: 'secondary',
          },
          {
            text: '확인',
            handler: () => {
              this.createCounselor('/tabs/tab2');
            },
          },
        ],
      });
      await alert.present();

    }else{
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: '결제 준비',
        message: '카드를 준비하셨습니까?',
        buttons: [
          {
            text: '결제하기',
            handler: () => {
              let payInfo = new Array();
              payInfo.push(this.method),
              payInfo.push(this.kind),
              payInfo.push(this.user.nickname),
              payInfo.push(this.user.phone),
              payInfo.push(this.user.email),
              
              this.payment.onClickPayment(payInfo).then((value)=>{
                  this.resultAlert(value);
              });
            },
          },
        ],
      });
      await alert.present();
    }
  }

  goPayment(param,method){
    this.method = method;
    this.divHidden=param;
  }

  //상담 아이디 생성
  createCounselor(param){
    this.auth.user$.pipe(take(1)).subscribe((user: any) => {
      if (user && user.nickname) {
        this.counselor.counselorId = this.common.generateFilename();
        this.counselor.dateCreated =  new Date().toISOString();
        this.counselor.nickname=user.nickname;
        this.counselor.kind=this.kind;
        this.counselor.payMethod = this.method
        this.counselor.pay=this.pay;
        this.counselor.step='tester-info';
        this.counselor.uid=user.uid;
        this.saveCounselorInfo(param);
      }
    });
  }

  //결제정보 저장
  saveCounselorInfo(param){
    this.db.updateAt(`counselor/${this.counselor.counselorId}`, this.counselor);
    this.navc.navigateForward([param]);
  }

  //결제 연동 모듈 결과 메세지
  async kcpPayResult(msg){
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '결제 결과',
      message: msg,
      buttons: [
        {
          text: '확인',
          handler: () => {
            this.navc.navigateForward(['/tabs/tab2']);
          },
        },
      ],
    });

    await alert.present();
  }

  
  //무조건 결제 취소하고 그림검사화면으로이동
  async goBack(){
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '결제 취소',
      message: '결제를 취소하시겠습니까?',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: '확인',
          handler: () => {
            this.navc.navigateForward(['/tabs/tab2']);
          },
        },
      ],
    });

    await alert.present();

}


  
}