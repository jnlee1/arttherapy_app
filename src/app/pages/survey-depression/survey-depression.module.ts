import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SurveyDepressionPageRoutingModule } from './survey-depression-routing.module';

import { SurveyDepressionPage } from './survey-depression.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SurveyDepressionPageRoutingModule
  ],
  declarations: [SurveyDepressionPage]
})
export class SurveyDepressionPageModule {}
