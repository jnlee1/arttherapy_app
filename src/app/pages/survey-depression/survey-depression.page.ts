import { Component, OnInit } from '@angular/core';
import { ToastController, NavController } from '@ionic/angular';
import { take } from 'rxjs/operators';
import { AuthService } from 'src/services/auth.service';
import { CommonService } from 'src/services/common.service';
import { DbService } from 'src/services/db.service';

@Component({
  selector: 'app-survey-depression',
  templateUrl: './survey-depression.page.html',
  styleUrls: ['./survey-depression.page.scss'],
})

// 메뉴명
// 설문지 검사 탭 > 우울증 검사
export class SurveyDepressionPage implements OnInit {

  selValue = new Array(20);//질문지 개수
  selSum : any;//선택한 값의 합계 점수
  docId;
  result;

  constructor(
              public toastController : ToastController,
              public navCtrl : NavController,
              public db: DbService,
              private auth: AuthService,
              private common:CommonService
  ) { }

  ngOnInit() {
  }

  
  //답변 선택 1안
  public ansList01 = [
    { answer: '극히드물다(1일이하)', value : 0 },
    { answer: '가끔(1~2일)' , value : 1 },
    { answer: '자주(3~4일)', value : 2  },
    { answer: '거의 대부분(5~7일)', value : 3  }
  ];

  //답변 선택 2안(5,10,15번 문제에 적용)
  public ansList02 = [
    { answer: '극히드물다(1일이하)', value : 3 },
    { answer: '가끔(1~2일)' , value : 2 },
    { answer: '자주(3~4일)', value : 1  },
    { answer: '거의 대부분(5~7일)', value : 0  }
  ];

  //우울증 질문 선택시 답변값 
  fnSelect(selval, no){
    this.selValue[no] = selval;
    console.log("select No "+ (Number(no)+1) +"번 : "+  this.selValue[no]);
    //this.goNext(no);
  }

  //검사자가 선택한 우울증 총 점수
  async fnDepSum(){
    this.selSum =0;//합계 점수 초기화
    for(var i=0; i<this.selValue.length; i++ ){
      if(this.selValue[i] ==null)
      {
        //경고
        const toast = await this.toastController.create({
          message: (i+1) + '번 답변을 선택하세요 :)',
          duration: 1000
        });
        toast.present();

       // this.content.scrollToPoint(0,240*Number(i),200);

        return;
      }else{
        this.selSum += this.selValue[i];
      }
    }


    this.auth.user$.pipe(take(1)).subscribe((user: any) => {
      if(this.selSum >= 25){
        this.result = '매우 심한 우울감'
      }else if( this.selSum >=21){
        this.result ='심한 우울감'
      }else if(this.selSum >=16){
        this.result = '약간의/보통의 우울감'
      }else{
        this.result ='양호'
      }

      if (user && user.nickname) {
        this.docId = this.common.generateFilename();
        this.db.updateAt(`survey/${this.docId}`,{uid:user.uid,sum:this.selSum,kind:'우울증',result:this.result,created:new Date().toISOString()})
      }
    });

    this.navCtrl.navigateForward(['/tabs/survey-result'],{
      queryParams :{
        title:'우울증',
        sum:this.selSum
      }
    });

      
  }
   
  //다음문제로 이동
  // @ViewChild(IonContent, {static:true}) content: any;
  // goNext(no){

  //   if(no == 0){
  //     return;
  //   }

  //   var x = 0;
  //   var y = Number(no)+1;  

  //   this.content.scrollToPoint(x,240*y,200);
  //   console.log("문제 번호  : " +  y + " Y값 높이 : " + 200*y);
  // }

  //스크롤 TOP 버튼
  goTop(){
   // this.content.scrollToPoint(0,0,200);
  }


}
