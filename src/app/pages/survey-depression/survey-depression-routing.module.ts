import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SurveyDepressionPage } from './survey-depression.page';

const routes: Routes = [
  {
    path: '',
    component: SurveyDepressionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SurveyDepressionPageRoutingModule {}
