import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GuidescreenPage } from './guidescreen.page';

describe('GuidescreenPage', () => {
  let component: GuidescreenPage;
  let fixture: ComponentFixture<GuidescreenPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GuidescreenPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GuidescreenPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
