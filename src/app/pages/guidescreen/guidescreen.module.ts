import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GuidescreenPageRoutingModule } from './guidescreen-routing.module';

import { GuidescreenPage } from './guidescreen.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GuidescreenPageRoutingModule
  ],
  declarations: [GuidescreenPage]
})
export class GuidescreenPageModule {}
