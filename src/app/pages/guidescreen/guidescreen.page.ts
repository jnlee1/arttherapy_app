import { Component, OnInit } from '@angular/core';
import { Network } from '@ionic-native/network/ngx';
import { AlertController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-guidescreen',
  templateUrl: './guidescreen.page.html',
  styleUrls: ['./guidescreen.page.scss'],
})

// 메뉴명
// 앱 실행  > 가이드 화면(4개 슬라이드)
export class GuidescreenPage implements OnInit {
  constructor(private navc: NavController, private network:Network, private alertController:AlertController) {
    
  }

  ngOnInit() {
    this.network.onDisconnect().subscribe(() => {
      console.log('네트워크 연결 실패');
          this.alertConfirmNetwork();
          return;
      });

    //로그인 상태일 경우, 가이드 화면 없이 바로 홈화면 으로 이동
    if (localStorage.loginSwitch == 'true') {
      this.gohome();
    }
  }
  gohome() {
    this.navc.navigateForward(['/tabs/tab1']);
  }

  slidesDidLoad(slides){
    slides.startAutoplay();
  }

  //네트워크가 없을 경우
  async alertConfirmNetwork() {
    const alert = await this.alertController.create({
      cssClass: 'noExistNetwork',
      header: '네트워크 오류',
      message: '네트워크를 연결 하고\n실행 하시길 바랍니다.',
      buttons: [
        {
          text: '확인',
          handler:()=>{
            navigator['app'].exitApp();
          }
        },
      ],
    });
    await alert.present();
  }
}
