import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GuidescreenPage } from './guidescreen.page';

const routes: Routes = [
  {
    path: '',
    component: GuidescreenPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GuidescreenPageRoutingModule {}
