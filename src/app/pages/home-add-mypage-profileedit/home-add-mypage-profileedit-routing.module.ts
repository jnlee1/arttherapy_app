import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddMypageProfileeditPage } from './home-add-mypage-profileedit.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddMypageProfileeditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddMypageProfileeditPageRoutingModule {}
