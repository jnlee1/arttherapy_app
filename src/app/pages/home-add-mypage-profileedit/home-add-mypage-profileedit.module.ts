import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddMypageProfileeditPageRoutingModule } from './home-add-mypage-profileedit-routing.module';

import { HomeAddMypageProfileeditPage } from './home-add-mypage-profileedit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddMypageProfileeditPageRoutingModule
  ],
  declarations: [HomeAddMypageProfileeditPage]
})
export class HomeAddMypageProfileeditPageModule {}
