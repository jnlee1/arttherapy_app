import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAddMypageProfileeditPage } from './home-add-mypage-profileedit.page';

describe('HomeAddMypageProfileeditPage', () => {
  let component: HomeAddMypageProfileeditPage;
  let fixture: ComponentFixture<HomeAddMypageProfileeditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAddMypageProfileeditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAddMypageProfileeditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
