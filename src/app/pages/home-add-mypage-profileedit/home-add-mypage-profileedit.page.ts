import { DbService } from 'src/services/db.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { take, map } from 'rxjs/operators';
import { Keyboard } from '@ionic-native/keyboard/ngx';
@Component({
  selector: 'app-home-add-mypage-profileedit',
  templateUrl: './home-add-mypage-profileedit.page.html',
  styleUrls: ['./home-add-mypage-profileedit.page.scss'],
})

// 메뉴명
// 마이페이지 > 내정보 > 내정보 수정 
export class HomeAddMypageProfileeditPage implements OnInit {
  uid;
  user: any = {};
  user$;
  yearList = [];
  yearOptions: any = {
    cssClass: 'yearcustom',
  };
  constructor(
    public db: DbService,
    private navc: NavController,
    public toastController: ToastController,
    private keyboard: Keyboard,
    private router: ActivatedRoute,
  ) {
    this.router.queryParams.subscribe((params) => {
      this.uid = params["uid"];
      this.getData();
      this.getList();
    });
  }

  ngOnInit() { }

  //년도를 설정해주는 함수
  getList() {
    let start = 1;
    let end = 100;
    let nowYear = new Date().getFullYear();

    for (let i = start; i <= end; i++) {
      this.yearList.push(nowYear - i + 1);
    }
  }

  //유저의 정보를 가져오는 함수
  getData() {
    this.user$ = this.db.doc$(`users/${this.uid}`)
      .pipe(map(datas => {
        this.user = datas;
        return datas;
      }));
  }

  //유저의 정보를 수정하는 함수
  editInfo() {
    this.db.updateAt(`users/${this.uid}`, this.user).then(success => {
      this.successEdit();
      this.navc.pop();
    })
  }

  async successEdit() {
    const toast = await this.toastController.create({
      message: "회원정보 수정이 완료되었습니다",
      duration: 2000,
    });
    toast.present();
  }

  //닉네임 길이제한
  nicknameExpressionChk: boolean = true;
  nicknamewRegularExpression() {
    if (this.user.nickname.length < 2 || this.user.nickname.length > 10) {
      this.nicknameExpressionChk = false;
    } else {
      this.nicknameExpressionChk = true;
    }
    if (this.user.nickname.length >= 10 && this.user.nickname.length >= 10) {
      this.keyboard.hide();
    }
  }

  //핸드폰 형식제한
  phoneExpressionChk = true;
  phoneRegularExpression() {
    var regExp = /(01[016789])([1-9]{1}[0-9]{2,3})([0-9]{4})$/;
    if (regExp.test(this.user.phone)) {
      this.phoneExpressionChk = true;
    } else {
      this.phoneExpressionChk = false;
    }
  }
}
