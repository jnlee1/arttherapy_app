import { Component, OnInit, ViewChild } from '@angular/core';
import { ToastController, NavController, IonContent } from '@ionic/angular';
import { AngularFirestore } from 'angularfire2/firestore';
import { take } from 'rxjs/operators';
import { AuthService } from 'src/services/auth.service';
import { CommonService } from 'src/services/common.service';
import { DbService } from 'src/services/db.service';

@Component({
  selector: 'app-survey-stress',
  templateUrl: './survey-stress.page.html',
  styleUrls: ['./survey-stress.page.scss'],
})

// 메뉴명
// 설문지 검사 탭 > 스트레스 검사
export class SurveyStressPage implements OnInit {

  selValue = new Array(43);//질문지 개수
  selSum : any;//선택한 값의 합계 점수
  docId;
  result;

  constructor(public toastController : ToastController,
              private navCtrl : NavController,
              public angFireSto : AngularFirestore,
              private db : DbService,
              private common : CommonService,
              private auth : AuthService) { }

  ngOnInit() {
  }

  //답변 선택 1안
  public ansList01 = [
    { answer: '전혀 그렇지않다', value : 1 },
    { answer: '그렇지않다' , value : 2 },
    { answer: '그렇다', value : 3  },
    { answer: '매우 그렇다', value : 4  }
  ];

  //답변 선택 2안
  public ansList02 = [
    { answer: '전혀 그렇지않다', value : 4 },
    { answer: '그렇지않다' , value : 3 },
    { answer: '그렇다', value : 2  },
    { answer: '매우 그렇다', value : 1  }
  ];

  fnSelect(selval, no){
    this.selValue[no] = selval;
    console.log("select No "+ (Number(no)+1) +" : "+  this.selValue[no]);
  }

  //검사자가 선택한 우울증 총 점수
  async fnStsSum(){
    this.selSum =0;//합계 점수 초기화
    for(var i=0; i<this.selValue.length; i++ ){
      if(this.selValue[i] ==null)
      {
        //경고
        const toast = await this.toastController.create({
          message: (i+1) + '번 답변을 선택하세요 :)',
          duration: 1000
        });
        toast.present();
        return;
      }else{
        this.selSum += this.selValue[i];
      }
    }
    try {
      this.auth.user$.pipe(take(1)).subscribe((user: any) => {
        if(this.selSum >= 51){
          this.result = '심각'
        }else if( this.selSum >=25){
          this.result = '보통'
        }else{
          this.result = '양호'
        }

        if (user && user.nickname) {
          this.docId = this.common.generateFilename();
          this.db.updateAt(`survey/${this.docId}`,{uid:user.uid,sum:this.selSum,kind:'스트레스',result:this.result,created:new Date().toISOString()})
        }
      });

      this.navCtrl.navigateForward(['/tabs/survey-result'],{
        queryParams :{
          title:'스트레스',
          sum:this.selSum
        }
      });
      
    } catch (error) {
      const toast = await this.toastController.create({
        message: "오류발생하였습니다. 다시 시도 하세요.",
        duration: 2000
      });
      toast.present();
      return;
    }
      
  }

  //다음문제로 이동
  // @ViewChild(IonContent, {static:true}) content: any;
  // goNext(no){
  //   if(no == 0){
  //     return;
  //   }

  //   var x = 0;
  //   var y = Number(no)+1;

  //   if(no < 24){
  //     y = 240*y
  //   }else{
  //     y = 250*y
  //   }

  //   this.content.scrollToPoint(x,y,200);
  //   console.log("클릭 횟수 : " +  y);
  // }

  //스크롤 TOP 버튼
  // goTop(){
  //   this.content.scrollToPoint(0,0,200);
  // }
  



}
