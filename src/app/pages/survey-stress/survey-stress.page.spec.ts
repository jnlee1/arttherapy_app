import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SurveyStressPage } from './survey-stress.page';

describe('SurveyStressPage', () => {
  let component: SurveyStressPage;
  let fixture: ComponentFixture<SurveyStressPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveyStressPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SurveyStressPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
