import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SurveyStressPageRoutingModule } from './survey-stress-routing.module';

import { SurveyStressPage } from './survey-stress.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SurveyStressPageRoutingModule
  ],
  declarations: [SurveyStressPage]
})
export class SurveyStressPageModule {}
