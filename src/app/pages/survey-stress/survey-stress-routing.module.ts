import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SurveyStressPage } from './survey-stress.page';

const routes: Routes = [
  {
    path: '',
    component: SurveyStressPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SurveyStressPageRoutingModule {}
