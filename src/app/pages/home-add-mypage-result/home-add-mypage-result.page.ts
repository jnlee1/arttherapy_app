import { leftJoinDocument } from './../../../services/db.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { ImageDetailPage } from '../image-detail/image-detail.page';
import { DbService } from 'src/services/db.service';
import { CommonService } from 'src/services/common.service';
import { AuthService } from 'src/services/auth.service';
import { take, map } from 'rxjs/operators';

@Component({
  selector: 'app-home-add-mypage-result',
  templateUrl: './home-add-mypage-result.page.html',
  styleUrls: ['./home-add-mypage-result.page.scss'],
})

// 메뉴명
// 마이페이지 > 그림 검사 결과 > 자세히 보기 > 검사 분석 결과
export class HomeAddMypageResultPage implements OnInit {
  counselorId;
  counselor$;
  viewType = 0;
  constructor(
    public db: DbService,
    public modalController: ModalController,
    private router: ActivatedRoute,
  ) {
    this.router.queryParams.subscribe((params) => {
      this.counselorId = params["counselorId"];
      this.getData();
    });
  }

  ngOnInit() { }

  //상담서와 결과서를 가져오는 함수
  getData() {
    this.counselor$ = this.db.collection$(`counselor`, ref => ref.where('counselorId', '==', this.counselorId))
      .pipe(leftJoinDocument(this.db.afs, 'feedbackId', 'feedback'))
      .pipe(map(datas => {
        if (datas) {
          console.log(datas[0]);
          return datas[0];
        }
      }));
  }

  //상세이미지창을 띄어주는 함수
  async imgdetail(url) {
    const modal = await this.modalController.create({
      component: ImageDetailPage,
      cssClass: 'my-custom-class',
      componentProps: {
        url,
      },
    });
    return await modal.present();
  }
}
