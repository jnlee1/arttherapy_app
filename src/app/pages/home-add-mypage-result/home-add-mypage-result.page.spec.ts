import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAddMypageResultPage } from './home-add-mypage-result.page';

describe('HomeAddMypageResultPage', () => {
  let component: HomeAddMypageResultPage;
  let fixture: ComponentFixture<HomeAddMypageResultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAddMypageResultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAddMypageResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
