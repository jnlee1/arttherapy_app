import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddMypageResultPageRoutingModule } from './home-add-mypage-result-routing.module';

import { HomeAddMypageResultPage } from './home-add-mypage-result.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddMypageResultPageRoutingModule
  ],
  declarations: [HomeAddMypageResultPage]
})
export class HomeAddMypageResultPageModule {}
