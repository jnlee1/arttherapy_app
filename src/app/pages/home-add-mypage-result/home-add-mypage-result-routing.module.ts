import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddMypageResultPage } from './home-add-mypage-result.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddMypageResultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddMypageResultPageRoutingModule {}
