import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddMypageSurveyPageRoutingModule } from './home-add-mypage-survey-routing.module';

import { HomeAddMypageSurveyPage } from './home-add-mypage-survey.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddMypageSurveyPageRoutingModule
  ],
  declarations: [HomeAddMypageSurveyPage]
})
export class HomeAddMypageSurveyPageModule {}
