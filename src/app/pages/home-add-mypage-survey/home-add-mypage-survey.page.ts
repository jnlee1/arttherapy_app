import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/services/auth.service';
import { DbService } from 'src/services/db.service';

@Component({
  selector: 'app-home-add-mypage-survey',
  templateUrl: './home-add-mypage-survey.page.html',
  styleUrls: ['./home-add-mypage-survey.page.scss'],
})

// 메뉴명
// 마이페이지 > 설문지 검사 목록
export class HomeAddMypageSurveyPage implements OnInit {
  uid;
  user$;
  surveyList$;
  constructor(
              private db : DbService) {

        this.uid = localStorage.getItem('userId');
        this.getData();
        }

  ngOnInit() {
  }

  //설문지검사목록 조회
  getData() {
    this.user$ = this.db.doc$(`users/${this.uid}`);
    this.surveyList$ = this.db.collection$(`survey`, ref => ref.where('uid', '==', this.uid).orderBy('created', 'desc'));;
  }

}
