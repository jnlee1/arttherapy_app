import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAddMypageSurveyPage } from './home-add-mypage-survey.page';

describe('HomeAddMypageSurveyPage', () => {
  let component: HomeAddMypageSurveyPage;
  let fixture: ComponentFixture<HomeAddMypageSurveyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAddMypageSurveyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAddMypageSurveyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
