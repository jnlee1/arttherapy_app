import { Ng2ImgMaxService } from 'ng2-img-max';
import { WebImageService } from './../../../services/web-image.service';
import { ImgCheckService } from './../../../services/img-check.service';
import { LoadingService } from './../../../services/loading.service';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { AuthService } from 'src/services/auth.service';
import { CommonService } from './../../../services/common.service';
import { CounselorService } from './../../../services/counselor.service';
import { DbService } from 'src/services/db.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActionSheetController, NavController, AlertController, ToastController, Platform, LoadingController } from '@ionic/angular';
import { ImageService } from 'src/services/image.service';
import { File } from '@ionic-native/file/ngx';
import * as firebase from 'firebase';
import { Camera } from '@ionic-native/camera/ngx';
import * as moment from 'moment';
import { resolve } from 'dns';
import { rejects } from 'assert';
import { ActivatedRoute } from '@angular/router';
import { getLocaleDateFormat } from '@angular/common';
import { take } from 'rxjs/operators';
// import { PhotoLibrary } from '@ionic-native/photo-library/ngx';
@Component({
  selector: 'app-home-add-howto-img',
  templateUrl: './home-add-howto-img.page.html',
  styleUrls: ['./home-add-howto-img.page.scss'],
})

// 메뉴명
// 그림검사탭 > HTP 그림검사 > 그림검사 업로드
export class HomeAddHowtoImgPage implements OnInit {

  @ViewChild("imageInput2", { static: true }) imageInput2: ElementRef;

  imageList: any = [
    { order: '1', orderName: '집', name: 'housePic', url: '' },
    { order: '2', orderName: '나무', name: 'treePic', url: '' },
    { order: '3', orderName: '사람1', name: 'personPic', url: '' },
    { order: '4', orderName: '사람2', name: 'oppositePic', url: '' },
  ];
  counselor: any;
  private storage = firebase.storage().ref();
  constructor(
    public actionSheetController: ActionSheetController,
    public db: DbService,
    public image: ImageService,
    public alertController: AlertController,
    private navc: NavController,
    public toastController: ToastController,
    private CounselorService: CounselorService,
    private common: CommonService,
    private imagePicker: ImagePicker,
    private auth: AuthService,
    public loadingController: LoadingController,
    private loading: LoadingService,
    private platform: Platform,
    private file: File,
    private imgchk: ImgCheckService,
    private webImage: WebImageService,
    private ng2ImgMax: Ng2ImgMaxService,
    private route:ActivatedRoute,
  ) {
    this.counselor = this.CounselorService.getCounselor();

  }
  ngOnInit() {}

  //이미지 첨부 선택 함수
  async add(item) {
    console.log(item);
    const actionSheet = await this.actionSheetController.create({
      header: '파일첨부',
      cssClass: 'cam',
      buttons: [
        {
          text: '카메라',
          handler: () => {
            this.image.getCamera('confirm').then(url => {
              this.imgchk.setItem(true);
              item.url = url;
            });
          },
        },
        {
          text: '갤러리 이동',
          handler: () => {
            this.addImages();
          },
        },
        {
          text: '취소',
          role: 'cancel'
        }
      ]
    });
    await actionSheet.present();
  }

  //갤러리 선택시 실행
  addImages() {
    this.imageInput2.nativeElement.click();
  }

  //항목별 이미지 업로드 함수
  async onImageChange(event) {
    let imageLength = event.target.files.length;
    let results = event.target.files;
    console.log('results', results);
    console.log('this.imageList', this.imageList);
    this.loading.present();
    if (results[0]) {
      await this.webImage.onImageChange2(results[0]).then((downloadURL1) => {
        console.log("downloadURL1", downloadURL1);
        this.imgchk.setItem(true);
        let idx_tmp_chk = false;
        this.urlChange(downloadURL1).then((data) => {
          for (let idx = 0; idx < 4; idx++) {
            if (!this.imageList[idx].url && !idx_tmp_chk) {
              this.imageList[idx].url = data;
              // this.imageList[0].url = data;
              idx_tmp_chk = true;
            }
          }
          this.loading.dismiss();
        });

        if (imageLength == 1) {
          this.loading.hide();
        }

        if (results[1]) {
          this.webImage.onImageChange2(results[1]).then((downloadURL2) => {
            this.imgchk.setItem(true);
            let idx_tmp_chk = false;
            this.urlChange(downloadURL2).then((data) => {
              for (let idx = 0; idx < 4; idx++) {
                if (!this.imageList[idx].url && !idx_tmp_chk) {
                  this.imageList[idx].url = data;
                  // this.imageList[1].url = data;
                  idx_tmp_chk = true;
                }
              }
              this.loading.dismiss();
            });

            if (imageLength == 2) {
              this.loading.hide();
            }

            if (results[2]) {
              this.webImage.onImageChange2(results[2]).then((downloadURL3) => {
                this.imgchk.setItem(true);
                let idx_tmp_chk = false;
                this.urlChange(downloadURL3).then((data) => {
                  for (let idx = 0; idx < 4; idx++) {
                    if (!this.imageList[idx].url && !idx_tmp_chk) {
                      this.imageList[idx].url = data;
                      // this.imageList[2].url = data;
                      idx_tmp_chk = true;
                    }
                  }
                  this.loading.dismiss();
                });

                if (imageLength == 3) {
                  this.loading.hide();
                }

                if (results[3]) {
                  this.webImage.onImageChange2(results[3]).then((downloadURL4) => {
                    this.imgchk.setItem(true);
                    let idx_tmp_chk = false;
                    this.urlChange(downloadURL4).then((data) => {
                      for (let idx = 0; idx < 4; idx++) {
                        if (!this.imageList[idx].url && !idx_tmp_chk) {
                          this.imageList[idx].url = data;
                          // this.imageList[3].url = data;
                          idx_tmp_chk = true;
                        }
                      }
                      this.loading.dismiss();
                    });
                  });
                }
              });
            }
          });
        }
      });
    }

  }

  //이미지 지우는 함수
  deleteImage(index) {
    this.imageList[index].url = '';
    let cnt = 0;
    for (let i = 0; i < 4; i++) {
      if (this.imageList[i].url) {
        cnt++;
      }
    }
    if (cnt == 0) {
      this.imgchk.setItem(false);
    }
  }

  //뒤로가기
  back() {
    this.imageList.some(async element => {
      if (element.url) {
        this.stop();
      } else {
        await this.navc.pop();
        this.imgchk.setItem(false);
      }
    });
  }

  //이미지 첨부시 뒤로갈때 나오는 alert
  async stop() {
    const alert = await this.alertController.create({
      header: '그림업로드 종료',
      message: '업로드한 이미지가 저장되지 않습니다.<br>종료하시겠습니까?',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
        },
        {
          text: '확인',
          handler: async () => {
            this.imgchk.setItem(false);
            await this.navc.navigateRoot(['/tabs/tab1']);
          },
        },
      ],
    });

    await alert.present();
  }

  //이미지 업로드시 나오는 로딩
  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Please wait...',
      duration: 2000
    });
    await loading.present();

    const { role, data } = await loading.onDidDismiss();
    console.log('Loading dismissed!');
  }

  //이미지 주소 base64 변환
  async urlChange(base64) {
    return new Promise((resolve, rejects) => {
      var storageRef = this.storage;
      var fileName = "image" + moment().format("x") + ".jpg";
      var upRef = `/images/` + fileName;
      var uploadTask = storageRef.child(upRef).putString(base64, 'data_url');
      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED, (snapshot) => {
      }, (error) => {
        console.log(error);
      }, () => {
        try {
          this.storage.child(upRef).getDownloadURL().then((downloadURL) => {
            resolve(downloadURL);
          });
        } catch (e) {
          console.log(e);
          rejects(e);
        }
      });
    });
  }

  //검사요청 확인 alert
  async updateAlert() {
    const alert = await this.alertController.create({
      header: "그림 업로드",
      message: "업로드 하시겠습니까?",
      buttons: [
        {
          text: '취소',
          role: 'cancel',
        },
        {
          text: '확인',
          handler: async () => {
            this.loading.present();
           // this.counselor.counselorId = this.common.generateFilename();
            this.counselor.uid = this.auth.userDetails().uid;
            this.counselor.imageList = this.imageList;
            this.counselor.dateCreated = new Date().toISOString();
            this.counselor.step = 'home-add-howto-question';
            this.db.updateAt(`counselor/${this.counselor.counselorId}`, {uid :this.counselor.uid,
                                                                         imageList:this.counselor.imageList,
                                                                         dateCreated:this.counselor.dateCreated,
                                                                         step:this.counselor.step});
            this.loading.dismiss();
            this.successToast();
            this.imgchk.setItem(false);
            this.navc.navigateForward(['/home-add-howto-question'],{
              queryParams:{
                kind:this.counselor.kind,
                docId:this.counselor.counselorId
              }
            });
          },
        },
      ],
    });

    await alert.present();
  }

  //검사요청 완료시 Toast
  async successToast() {
    const toast = await this.toastController.create({
      message: "그림이 업로드 되었습니다.",
      duration: 2000,
    });
    toast.present();
  }
}
