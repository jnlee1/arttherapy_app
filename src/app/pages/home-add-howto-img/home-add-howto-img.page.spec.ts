import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAddHowtoImgPage } from './home-add-howto-img.page';

describe('HomeAddHowtoImgPage', () => {
  let component: HomeAddHowtoImgPage;
  let fixture: ComponentFixture<HomeAddHowtoImgPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAddHowtoImgPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAddHowtoImgPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
