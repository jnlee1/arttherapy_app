import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddHowtoImgPageRoutingModule } from './home-add-howto-img-routing.module';

import { HomeAddHowtoImgPage } from './home-add-howto-img.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddHowtoImgPageRoutingModule
  ],
  declarations: [HomeAddHowtoImgPage]
})
export class HomeAddHowtoImgPageModule {}
