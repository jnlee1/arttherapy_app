import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddHowtoImgPage } from './home-add-howto-img.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddHowtoImgPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddHowtoImgPageRoutingModule {}
