import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DbService } from 'src/services/db.service';

@Component({
  selector: 'app-refund',
  templateUrl: './refund.page.html',
  styleUrls: ['./refund.page.scss'],
})

// 메뉴명
// 마이페이지 > 환불규정
export class RefundPage implements OnInit {
  info$;

  constructor(
    public modalCtrl: ModalController,
    public db: DbService,) {
    this.getData();
  }

  ngOnInit() { }

  getData() {
    this.info$ = this.db.doc$("master/term");
  }

  dismiss() {
    this.modalCtrl.dismiss({
      dismissed: true,
    });
  }
}
