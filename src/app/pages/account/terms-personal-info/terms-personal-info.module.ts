import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TermsPersonalInfoPageRoutingModule } from './terms-personal-info-routing.module';

import { TermsPersonalInfoPage } from './terms-personal-info.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TermsPersonalInfoPageRoutingModule
  ],
  declarations: [TermsPersonalInfoPage]
})
export class TermsPersonalInfoPageModule {}
