import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TermsPersonalInfoPage } from './terms-personal-info.page';

const routes: Routes = [
  {
    path: '',
    component: TermsPersonalInfoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TermsPersonalInfoPageRoutingModule {}
