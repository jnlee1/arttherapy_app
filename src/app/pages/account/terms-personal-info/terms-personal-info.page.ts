import { DbService } from 'src/services/db.service';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-terms-personal-info',
  templateUrl: './terms-personal-info.page.html',
  styleUrls: ['./terms-personal-info.page.scss'],
})

// 메뉴명
// 마이페이지 > 개인정보방침
export class TermsPersonalInfoPage implements OnInit {
  info$;

  constructor(
    public modalCtrl: ModalController,
    public db: DbService,) {
    this.getData();
  }

  ngOnInit() { }

  getData() {
    this.info$ = this.db.doc$("master/term");
  }

  dismiss() {
    this.modalCtrl.dismiss({
      dismissed: true,
    });
  }
}
