import { Ng2ImgMaxModule } from 'ng2-img-max';
import { LoadingService } from './../../../../services/loading.service';
import { AuthService } from './../../../../services/auth.service';
import { DbService } from './../../../../services/db.service';
import { CommonService } from './../../../../services/common.service';
import { UserService } from './../../../../services/user.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalController, NavController, AlertController, ToastController, IonInput } from '@ionic/angular';
import { TermsPersonalInfoPage } from '../terms-personal-info/terms-personal-info.page';
import { TermsServicePage } from '../terms-service/terms-service.page';
import { take } from 'rxjs/operators';
import { Keyboard } from '@ionic-native/keyboard/ngx';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})

// 메뉴명
// 로그인 > 회원가입
export class SignupPage implements OnInit {
  agreeAllTerms = false;
  firstChk: boolean = false;
  agree = {
    serviceAgree: false,
    personalAgree: false,
    marketing: false, //선택
  };
  yearList = [];
  user: any;
  birth;
  email;
  nickname;
  phone;
  password;
  confirm_password: any;
  @ViewChild('inputId', { static: false }) inputEl: IonInput;
  passwordCheck = false;
  emailcheck = false;

  yearOptions: any = {
    cssClass: 'yearcustom',
  };
  constructor(
    public modalController: ModalController,
    private navc: NavController,
    public userService: UserService,
    public alertController: AlertController,
    public toastController: ToastController,
    public common: CommonService,
    public db: DbService,
    public auth: AuthService,
    public load: LoadingService,
    private keyboard: Keyboard
  ) {
    this.user = this.userService.getUser();
    console.log(this.user);
    this.getList();
  }

  ngOnInit() { }

  //태어난 년도 지정 함수
  getList() {
    let start = 1;
    let end = 100;
    let nowYear = new Date().getFullYear();

    for (let i = start; i <= end; i++) {
      this.yearList.push(nowYear - i + 1);
    }
  }

  //서비스이용약관 모달창
  async service() {
    const modal = await this.modalController.create({
      component: TermsServicePage,
      cssClass: 'my-custom-class',
    });
    return await modal.present();
  }

  //개인정보이용약관 모달창
  async personal() {
    const modal = await this.modalController.create({
      component: TermsPersonalInfoPage,
      cssClass: 'my-custom-class',
    });
    return await modal.present();
  }

  //닉네임 표현 제한
  nicknameExpressionChk: boolean = false;
  nicknamewRegularExpression($event) {
    let nickname = $event.target.value;
    console.log(nickname, nickname.length);
    if (this.nickname.length < 2 || this.nickname.length > 10) {
      this.nicknameExpressionChk = false;
    } else {
      this.nicknameExpressionChk = true;
    }
    if (this.nickname.length >= 10 && nickname.length >= 10) {
      this.keyboard.hide();
    }
  }

  //이메일 표현 제한
  emailExpressionChk: boolean = false;
  emailRegularExpression() {
    var reg = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
    if (reg.test(this.email)) {
      this.emailExpressionChk = true;
    } else {
      this.emailExpressionChk = false;
    }
  }

  //비밀번호 표현 제한
  pwExpressionChk: boolean = false;
  pwRegularExpression() {
    // var regExp = /^[A-za-z0-9]{5,15}$/g;
    var regExp = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/;
    if (regExp.test(this.password)) {
      this.pwExpressionChk = true;
    } else {
      this.pwExpressionChk = false;
    }
  }

  //헨드폰 표현제한
  phoneExpressionChk = true;
  phoneRegularExpression() {
    var regExp = /(01[016789])([1-9]{1}[0-9]{2,3})([0-9]{4})$/;
    if (regExp.test(this.phone)) {
      this.phoneExpressionChk = true;
    } else {
      this.phoneExpressionChk = false;
    }
  }

  //비밀번호 같은지 비교하는 함수
  matchingPasswords() {
    if (this.password && this.confirm_password) {
      if (this.password !== this.confirm_password) {
        this.passwordCheck = false;
        return true;
      } else {
        this.passwordCheck = true;
        return false;
      }
    }
  }

  //가입시 입력한 정보 설정하는 함수
  setData() {
    this.user.email = this.email;
    this.user.password = this.password;
    this.user.confirm_password = this.confirm_password;
    this.user.nickname = this.nickname;
    this.user.birth = this.birth;
    this.user.phone = this.phone;
    this.user.dateCreated = new Date().toISOString();
    this.user.marketing = this.agree.marketing;
  }

  // 중복된 계정
  async presentAlertAlready() {
    var reg = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;

    if (reg.test(this.email)) {
      this.db
        .collection$(`users`, ref => ref.where('email', '==', this.email).where('exitSwitch', '==', false))
        .pipe(take(1))
        .subscribe(async e => {
          if (e[0]) {
            this.presentAlertEmail();
          } else {
            this.emailcheck = true;
          }
        });
    } else {
      this.presentAlertEmail();
    }
  }

  //회원가입 함수
  goLogin() {
    if (!this.agree.serviceAgree || !this.agree.personalAgree) {
      this.firstChk = true;
      return;
    }

    this.firstChk = false;

    this.setData();
    console.log(this.user);

    this.db
      .collection$(`users`, ref => ref.where('email', '==', this.email).where('exitSwitch', '==', false))
      .pipe(take(1))
      .subscribe(datas => {
        if (datas.length > 0) {
          this.presentAlertEmail();
        } else {
          this.presentAlertAlready();
        }
      });

    this.load.load();
    this.auth
      .registerUser(this.user.email, this.user.password)
      .then(userCredential => {
        delete this.user.password;
        delete this.user.confirm_password;
        this.load.hide();
        this.registerDetail(userCredential, this.user);
      })
      .catch(error => {
        this.load.hide();
        let code = error['code'];
        console.log('code', code);
      });
  }

  //이메일, 비밀번호로 가입하는 함수
  async registerDetail(userCredential, obj) {
    let user = userCredential.user;
    this.user.uid = user.uid;
    this.db.updateAt(`users/${user.uid}`, this.user).then(success => {
      this.successJoin();
      this.navc.navigateRoot(['/tabs/tab1']).then(() => {
        this.auth.logoutUser();
        this.navc.navigateForward(['/login']);
      });
    });
  }

  //회원가입 성공
  async successJoin() {
    const toast = await this.toastController.create({
      message: '회원가입이 정상적으로 완료되었습니다.',
      duration: 2000,
    });
    toast.present();
  }

  //이미 가입된 email
  async presentAlertEmail() {
    const alert = await this.alertController.create({
      cssClass: 'wrong_pop wrong_t_pop already_pop',
      header: '가입된 계정',
      message: `이미 가입된 계정입니다. <br> 확인 후 로그인 해주시길 바랍니다.`,
      buttons: [
        {
          text: '확인',
          cssClass: 'pop_ok',
          handler: () => {
            console.log('Confirm Okay');
          },
        },
      ],
    });
    await alert.present();
  }

  //가입하지 않고 뒤로가기 처리 함수
  async warning() {
    const alert = await this.alertController.create({
      header: '회원가입 종료',
      message: '입력하신 내용이  저장되지 않습니다.<br> 이전 페이지로 이동 하시겠습니까?',
      buttons: [
        {
          text: '종료',
          handler: () => {
            this.navc.pop();
          },
        },
        {
          text: '취소',
        },
      ],
    });

    await alert.present();
  }

  //전체동의 함수
  checkAgreeAll(num) {
    if (this.agreeAllTerms && num == 0) {
      this.agree.marketing = true;
      this.agree.personalAgree = true;
      this.agree.serviceAgree = true;
    } else if (num == 0 && this.agree.marketing && this.agree.personalAgree && this.agree.serviceAgree) {
      this.agree.marketing = false;
      this.agree.personalAgree = false;
      this.agree.serviceAgree = false;
    }

    if (this.agree.marketing && this.agree.personalAgree && this.agree.serviceAgree && num != 0) {
      this.agreeAllTerms = true;
    } else if (num != 0) {
      this.agreeAllTerms = false;
    }
  }
}
