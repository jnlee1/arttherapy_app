import { FcmService } from 'src/services/fcm.service';
import { AlertService } from './../../../../services/alert.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController, Platform } from '@ionic/angular';
import { Button } from 'protractor';
import { take } from 'rxjs/operators';
import { CommonService } from 'src/services/common.service';
import { DbService } from 'src/services/db.service';
import { AuthService } from 'src/services/auth.service';
import { LoadingService } from 'src/services/loading.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

// 메뉴명
// 로그인 화면
export class LoginPage implements OnInit {
  public account = {
    email: '',
    password: '',
  };
  constructor(
    public alertController: AlertController,
    private navc: NavController,
    public toastController: ToastController,
    public common: CommonService,
    public db: DbService,
    public auth: AuthService,
    public load: LoadingService,
    public afAuth: AngularFireAuth,
    public alert: AlertService,
    private fcm: FcmService,
  ) {
    this.account.email = '';
    this.account.password = '';
  }

  ngOnInit() { }

  //로그인 함수
  async login() {
    try {
      var r = await this.afAuth.auth.signInWithEmailAndPassword(this.account.email, this.account.password);
      localStorage.setItem('userId', r.user.uid);
      localStorage.setItem('loginSwitch', 'true');
      this.navc.navigateRoot(['/tabs/tab2']);
      this.alert.presentToast('로그인 하였습니다.');
      this.fcm.getToken();
    } catch (error) {
      let code = error['code'];
      console.log(error);
      this.alert.showErrorMessage(code);
    }
  }

  //비밀번호 오류 처리 함수
  async alertConfirmPasswordFail() {
    const alert = await this.alertController.create({
      cssClass: 'failPassword',
      message: '입력하신 비밀번호가 틀렸습니다. 다시 확인하시고 입력하시기 바랍니다.',
      buttons: [
        {
          text: '확인',
          handler: () => { },
        },
      ],
    });
    await alert.present();
  }

  //가입된 이메일인지 체크하는 함수
  emailExisted(email) {
    this.db
      .collection$(`users`, ref => ref.where('email', '==', email.pwsRe))
      .pipe(take(1))
      .subscribe(datas => {
        if (datas.length > 0) {
          this.auth.resetPassword(email.pwsRe);
        } else {
          this.alertConfirmEmailNo();
        }
      });
  }

  //비밀번호 재설정 alert
  async resetpassword() {
    const alert = await this.alertController.create({
      cssClass: 'resetpassword',
      header: '비밀번호 재설정',
      message: '비밀번호 재설정을 위해 가입하신 이메일 주소를 \n입력하여 주시기 바랍니다.',
      inputs: [
        {
          name: 'pwsRe',
          type: 'email',
          placeholder: '이메일을 입력해주세요.',
        },
      ],
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: '확인',
          handler: data => {
            this.emailExisted(data);
          },
        },
      ],
    });

    await alert.present();
  }

  //로그인시 입력한 이메일이 없을 경우
  async alertConfirmEmailNo() {
    const alert = await this.alertController.create({
      cssClass: 'noExistEmail',
      header: '이메일 오류',
      message: '입력하신 이메일 정보가 없습니다.\n 다시 확인하시기 바랍니다.',
      buttons: [
        {
          text: '확인',
        },
      ],
    });
    await alert.present();
  }

  //회원가입으로 이동하는 함수
  gosignup() {
    this.navc.navigateForward(['/signup']);
  }
}
