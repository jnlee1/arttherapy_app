import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TermsServicePage } from './terms-service.page';

describe('TermsServicePage', () => {
  let component: TermsServicePage;
  let fixture: ComponentFixture<TermsServicePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TermsServicePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TermsServicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
