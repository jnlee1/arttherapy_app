import { DbService } from 'src/services/db.service';
import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-terms-service',
  templateUrl: './terms-service.page.html',
  styleUrls: ['./terms-service.page.scss'],
})

// 메뉴명
// 마이페이지 > 이용약관
export class TermsServicePage implements OnInit {
  info$;

  constructor(
    public modalCtrl: ModalController,
    public db: DbService) {
    this.getData();
  }

  ngOnInit() { }

  getData() {
    this.info$ = this.db.doc$("master/term");
  }

  dismiss() {
    this.modalCtrl.dismiss({
      dismissed: true,
    });
  }
}
