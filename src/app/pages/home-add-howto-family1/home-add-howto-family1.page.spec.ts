import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAddHowtoFamily1Page } from './home-add-howto-family1.page';

describe('HomeAddHowtoFamily1Page', () => {
  let component: HomeAddHowtoFamily1Page;
  let fixture: ComponentFixture<HomeAddHowtoFamily1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAddHowtoFamily1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAddHowtoFamily1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
