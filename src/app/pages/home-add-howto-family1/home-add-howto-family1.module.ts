import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAddHowtoFamily1PageRoutingModule } from './home-add-howto-family1-routing.module';

import { HomeAddHowtoFamily1Page } from './home-add-howto-family1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAddHowtoFamily1PageRoutingModule
  ],
  declarations: [HomeAddHowtoFamily1Page]
})
export class HomeAddHowtoFamily1PageModule {}
