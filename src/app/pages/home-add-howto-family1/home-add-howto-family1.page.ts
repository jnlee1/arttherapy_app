import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, NavController } from '@ionic/angular';

@Component({
  selector: 'app-home-add-howto-family1',
  templateUrl: './home-add-howto-family1.page.html',
  styleUrls: ['./home-add-howto-family1.page.scss'],
})

// 메뉴명
// 그림검사탭 > 동적가족화 그림검사 > 그림검사 실시 1번
export class HomeAddHowtoFamily1Page implements OnInit {
  @ViewChild('slides', { static: true }) slider: IonSlides;
  constructor(private navc: NavController) {
   
   }

  ngOnInit( ) { this.slider.lockSwipes(true);}

  slideOpts = {
    initialSlide: 0,
    speed: 400,
    allowTouchMove: false,
  };

  slidenext() {
    this.navc.navigateForward('home-add-howto-family2');
  }
}
