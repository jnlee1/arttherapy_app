import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddHowtoFamily1Page } from './home-add-howto-family1.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddHowtoFamily1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddHowtoFamily1PageRoutingModule {}
