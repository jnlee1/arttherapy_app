import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertController, NavController } from '@ionic/angular';
import { take } from 'rxjs/operators';
import { DbService } from 'src/services/db.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-visit-detail',
  templateUrl: './visit-detail.page.html',
  styleUrls: ['./visit-detail.page.scss'],
})

// 메뉴명
// 방문기관 탭 > 기관 선택 > 기관 정보 상세 보기
export class VisitDetailPage implements OnInit {

  institution$;
  isttNm;
  docId;
  loginSwitch;
  rInfo;


  constructor(private navc: NavController,
              private db: DbService,
              private router: ActivatedRoute,
              private iab: InAppBrowser,
              private auth: AuthService,
              public alertController: AlertController) { 
                
    this.router.queryParams.subscribe((params) => {
      this.docId = params["docId"];
      this.rInfo = params["rInfo"];
      this.getInstitutionInfo();
    });
  }

  ngOnInit() {
  }

  fnGoRevt(paramId,paramName){
    this.auth.user$.pipe(take(1)).subscribe((user: any) => {
      if (user && user.nickname) {
        this.navc.navigateForward(['visit-reservation'],{
          queryParams:{
            docId:paramId,
            isttNm:paramName
          }
        });
      } else {
        this.login();
      }
    });

  }

//서비스 이용시 로그인 필요 alert
async login() {
  const alert = await this.alertController.create({
    cssClass: 'my-custom-class',
    header: '로그인',
    message: '로그인 후 이용 바랍니다.',
    buttons: [
      {
        text: '취소',
        role: 'cancel',
        cssClass: 'secondary',
      },
      {
        text: '로그인',
        handler: () => {
          console.log('Confirm Okay');
          this.navc.navigateForward(['/login']);
        },
      },
    ],
  });

  await alert.present();
}

  
  //상담기관 정보 조회
  getInstitutionInfo(){
    try{
      this.db.collection$('institution', ref => ref.where('docId', '==', this.docId))
      .pipe(take(1))
      .subscribe((institution:any) => {
        if(institution[0].docId){
          this.institution$ = institution[0];
        }
      });
    }catch(e){
      this.navc.navigateForward(['tab4']);
    }
  }

 
}
