import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { NavController, ToastController } from '@ionic/angular';
import { take } from 'rxjs/operators';
import { DbService } from 'src/services/db.service';
import { ReservationService } from 'src/services/reservation.service';

@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.page.html',
  styleUrls: ['./reservation.page.scss'],
})

// 메뉴명
// 방문기관 탭 > 기관 선택 > 예약하기
export class ReservationPage implements OnInit {

  minDate: String = new Date().toISOString();
  maxDate: String = new Date(new Date().setMonth(new Date().getMonth() + 3)).toISOString();
  uid;
  isttNm;
  rsvtDocId;
  rsvtDate;
  rsvtTime;
  rsvtName;
  rsvtPnumber;
  rsvtPnumber1;
  rsvtPnumber2;
  rsvtPnumber3;
  rsvtGole;
  rsvtCenter;
  centerList=[];
  rsvtInfo:any;
  yearOptions: any = {
    cssClass: 'yearcustom',
  };

  constructor(
    private navc: NavController,
    private toastController : ToastController,
    public db : DbService,
    public rsvtc : ReservationService,
    private router: ActivatedRoute,
    private keyboard: Keyboard
  ) { 
    this.rsvtInfo = this.rsvtc.getReservation();
    if(localStorage.getItem('userId')){
      this.uid = localStorage.getItem('userId');
    }

    this.router.queryParams.subscribe((params) => {
      this.rsvtDocId = params["docId"];
      this.isttNm = params["isttNm"];
      this.getRsvtCenter(this.rsvtDocId);
    });
   
    
  }

  ngOnInit() {
  }


  //예약신청하기
  async rsvtSet(){
    try{
      if(localStorage.getItem('userId')){
        this.rsvtInfo.uid = localStorage.getItem('userId');
      }
      this.rsvtInfo.rsvtDate = this.rsvtDate;
      this.rsvtInfo.rsvtTime = this.rsvtTime;
      this.rsvtInfo.rsvtName = this.rsvtName;
      this.rsvtInfo.rsvtPnumber = this.rsvtPnumber1+this.rsvtPnumber2+this.rsvtPnumber3;
      this.rsvtInfo.rsvtGole = this.rsvtGole;
      this.rsvtInfo.institutionName = this.isttNm;
      this.rsvtInfo.institutionId = this.rsvtDocId;

      this.reservationSave(this.rsvtInfo);

    }catch(e){
      console.log( "에러 발생" + e);
      const toast = await this.toastController.create({
        message : "에러 발생" + e,
        duration:2000
      });
      toast.present();

    }
  }

  //예약 정보 저장
  reservationSave(rsvtInfo){
    this.db.updateAt(`reservation`, rsvtInfo).then(success =>{
      this.succRsvt();
      this.navc.pop();
      
      //마이페이지>예약내역에서 예약 확인
      this.navc.navigateForward(['/tabs/home-add-mypage-reservation']);
    })
  }


  //예약완료
  async succRsvt(){
    const toast = await this.toastController.create({
      message : "예약이 완료되었습니다.",
      duration:2000
    });
    toast.present();
  }


  getRsvtCenter(docId){
    try{
      this.db.collection$('institution', ref => ref.where('docId', '==', docId))
      .pipe(take(1))
      .subscribe((institution:any) => {
        if(institution[0].docId){
          let list = institution[0].centerList;
          for(var i=0; i<list.length; i++){
            this.centerList.push(institution[0].centerList[i]);
          }
        }
      });
    }catch(e){
      this.navc.navigateForward(['tab4']);
    }

  }

    //닉네임 표현 제한
    nicknameExpressionChk: boolean = false;
    nicknamewRegularExpression($event) {
      let nickname = $event.target.value;
      console.log(nickname, nickname.length);
      if (this.rsvtName.length < 2 || this.rsvtName.length > 10) {
        this.nicknameExpressionChk = false;
      } else {
        this.nicknameExpressionChk = true;
      }
      if (this.rsvtName.length >= 10 && nickname.length >= 10) {
        this.keyboard.hide();
      }
    }

  //헨드폰 표현제한
  phoneExpressionChk = true;
  phoneRegularExpression($event) {
    var regExp = /([0-9])$/;
    if (regExp.test($event.target.value)) {
      this.phoneExpressionChk = true;
    } else {
      this.phoneExpressionChk = false;
    }
  }

  //상담목적 입력 여부
  goleRegularExpressionChk = true;
  goleRegularExpression($event){
    let gole = $event.target.value;
    if(gole.length <2 || gole.length>10){
      this.goleRegularExpressionChk=false;
    }else{
      this.goleRegularExpressionChk=true;
    }
    if(this.rsvtGole.length>=10 && gole.length>=10){
      this.keyboard.hide();
    }

  }


}
