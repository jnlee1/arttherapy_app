import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAddMypagePasswordEditPage } from './home-add-mypage-password-edit.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAddMypagePasswordEditPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAddMypagePasswordEditPageRoutingModule { }
