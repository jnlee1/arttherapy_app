import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { HomeAddMypagePasswordEditPageRoutingModule } from './home-add-mypage-password-edit-routing.module';

import { HomeAddMypagePasswordEditPage } from './home-add-mypage-password-edit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    HomeAddMypagePasswordEditPageRoutingModule
  ],
  declarations: [HomeAddMypagePasswordEditPage]
})
export class HomeAddMypagePasswordEditPageModule { }
