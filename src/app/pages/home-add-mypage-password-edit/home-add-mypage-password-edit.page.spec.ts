import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeAddMypagePasswordEditPage } from './home-add-mypage-password-edit.page';

describe('HomeAddMypagePasswordEditPage', () => {
  let component: HomeAddMypagePasswordEditPage;
  let fixture: ComponentFixture<HomeAddMypagePasswordEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeAddMypagePasswordEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeAddMypagePasswordEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
