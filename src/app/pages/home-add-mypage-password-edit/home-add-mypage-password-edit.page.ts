import { Component, OnInit } from '@angular/core';
import { ToastController, AlertController, NavController } from '@ionic/angular';
import { LoadingService } from 'src/services/loading.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as firebase from 'firebase';
import { AlertService } from 'src/services/alert.service';
import { Validator } from 'src/validator';

@Component({
  selector: 'app-home-add-mypage-password-edit',
  templateUrl: './home-add-mypage-password-edit.page.html',
  styleUrls: ['./home-add-mypage-password-edit.page.scss'],
})

// 메뉴명
// 마이페이지 > 설정 > 비밀번호 변경
export class HomeAddMypagePasswordEditPage implements OnInit {
  password;
  confirm_password;
  oldPassword;
  uid;
  public emailPasswordForm: FormGroup;

  constructor(
    public toastController: ToastController,
    public alertController: AlertController,
    private loading: LoadingService,
    private navc: NavController,
    private alert: AlertService,
    public formBuilder: FormBuilder,
  ) {
    this.uid = localStorage.getItem("userId");
    this.emailPasswordForm = formBuilder.group(
      {
        oldPassword: Validator.passwordValidator,
        password: Validator.passwordValidator,
        confirm_password: Validator.confirm_passwordValidator,
      },
    );
  }

  ngOnInit() {
  }

  //입력한 비밀번호 같은지 비교
  passwordCheck = false;
  matchingPasswords() {
    if (this.password && this.confirm_password) {
      if (this.password !== this.confirm_password) {
        this.passwordCheck = false;
        return true;
      } else {
        this.passwordCheck = true;
        return false;
      }
    }
  }

  async successPassword() {
    const toast = await this.toastController.create({
      message: "비밀번호가 정상적으로 변경되었습니다.",
      duration: 2000,
    });
    toast.present();
  }

  async failPassword() {
    const alert = await this.alertController.create({
      cssClass: "existEmail",
      header: "비밀번호 변경 실패",
      message: "현재 비밀번호가 일치하지 않습니다. <br> 확인 후 다시 시도해주세요.",
      buttons: [
        {
          text: '확인',
          handler: () => { },
        },
      ],
    });
    await alert.present();
  }

  //비밀번호 바꿔주는 함수
  changePassword() {
    this.loading.load();
    var user = firebase.auth().currentUser;
    firebase
      .auth()
      .signInWithEmailAndPassword(user.email, this.emailPasswordForm.value["oldPassword"])
      .then((sucess) => {
        user
          .updatePassword(this.emailPasswordForm.value["password"])
          .then((sucess) => {
            this.loading.hide();
            this.successPassword().then(() => {
              this.navc.pop();
            })
          })
          .catch((error) => {
            this.loading.hide();
            console.log("error", error);
            let code = error["code"];
            this.failPassword();
          });
      })
      .catch((error) => {
        this.loading.hide();
        let code = error["code"];
        this.alert.showErrorMessage(code);
        console.log("oldPasswordIncorrect", error);
      });
  }

  //비밀번호 제한
  pwExpressionChk: boolean = false;
  pwRegularExpression() {
    // var regExp = /^[A-za-z0-9]{5,15}$/g;
    var regExp = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/;
    if (regExp.test(this.password)) {
      this.pwExpressionChk = true;
    } else {
      this.pwExpressionChk = false;
    }
  }

}
