import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TesterInfoPage } from './tester-info.page';

describe('TesterInfoPage', () => {
  let component: TesterInfoPage;
  let fixture: ComponentFixture<TesterInfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TesterInfoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TesterInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
