import { Component, OnInit } from '@angular/core';
import { NavController, ToastController } from '@ionic/angular';
import { AuthService } from 'src/services/auth.service';
import { CounselorService } from 'src/services/counselor.service';
import { DbService } from 'src/services/db.service';

@Component({
  selector: 'app-tester-info',
  templateUrl: './tester-info.page.html',
  styleUrls: ['./tester-info.page.scss'],
})

// 메뉴명
// 그림검사 탭 > 검사요청 > 검사자 정보 입력 화면
export class TesterInfoPage implements OnInit {

  uid;
  user$;
  counselor: any;
  testerNM : String;
  testerYY : String;
  testerSX : String;
  divHidden : boolean;


  constructor(
          private navCtrl : NavController,
          public toastController : ToastController,
          public db: DbService,
          public counselorService : CounselorService,
          public auth : AuthService) { }

  ngOnInit() {
    this.counselor = this.counselorService.getCounselor();

    this.uid=localStorage.getItem('userId');
    this.getData();
  }

  getData(){
    this.user$ = this.db.doc$(`users/${this.uid}`);
  }

  //검사 시작
  async htp01Move(){
    if(this.divHidden != null){

      if(this.testerSX == null){
        //경고
        const toast = await this.toastController.create({
          message:"성별을 선택하세요.",
          duration: 1000
        });
        toast.present();
        return;

      }

      if(this.divHidden == false){
        //본인일경우
        this.counselor.nickname = this.auth.userInfo.nickname;
        this.counselor.birth = this.auth.userInfo.birth;
        this.counselor.sex =  this.testerSX;
        
      }else{
        if(this.testerNM == null || this.testerYY == null){
            //경고
            const toast = await this.toastController.create({
              message:"항목을 모두 입력하세요.",
              duration: 1000
            });
            toast.present();
            return;
            
          }else{
            //본인이 아닌 검사자정보
            this.counselor.nickname = this.testerNM;
            this.counselor.birth = this.testerYY;
            this.counselor.sex = this.testerSX;
          }
      }

      if(this.counselor.kind == 'HTP'){
        this.navCtrl.navigateForward("home-add-howto");
        this.counselor.step='home-add-howto';
      }else if(this.counselor.kind == '동적가족화'){
        this.navCtrl.navigateForward("home-add-howto-family1");
        this.counselor.step='home-add-howto-family1';
      }

      this.db.updateAt(`counselor/${this.counselor.counselorId}`,{step:this.counselor.step,
                                                                  nickname:this.counselor.nickname,
                                                                  birth:this.counselor.birth,
                                                                  sex:this.counselor.sex});
    }else{
      //경고
       const toast = await this.toastController.create({
        message:"검사자 정보를 선택하세요.",
        duration: 1000
      });
      toast.present();
      return;
    }
  }

  //성별 저장
  fnSetSx(kind){
    this.testerSX = kind;
  }

  //검사자 정보 div show/hidden
  testInfoHidden(param){
    this.divHidden = param;
  }

}
