import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TesterInfoPageRoutingModule } from './tester-info-routing.module';

import { TesterInfoPage } from './tester-info.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TesterInfoPageRoutingModule
  ],
  declarations: [TesterInfoPage]
})
export class TesterInfoPageModule {}
