import { Component, OnInit } from '@angular/core';
import { AlertController, NavController, ToastController } from '@ionic/angular';
import { take } from 'rxjs/operators';
import { AuthService } from 'src/services/auth.service';
import { DbService } from 'src/services/db.service';

@Component({
  selector: 'app-tab3',
  templateUrl: './tab3.page.html',
  styleUrls: ['./tab3.page.scss'],
})

// 메뉴명
// 설문지 검사
export class Tab3Page implements OnInit {

  loginSwitch;
  alarms$;

  constructor(public alertController: AlertController,
              private navc: NavController, 
              private auth: AuthService, 
              private db: DbService,
              private toast : ToastController) {

    this.loginSwitch = localStorage.getItem('loginSwitch');
    this.checkAlarm(); }

  ngOnInit() {
  }


  
  //서비스 이용시 로그인 필요 alert
  async login() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '로그인',
      message: '로그인 후 이용 바랍니다.',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: '로그인',
          handler: () => {
            console.log('Confirm Okay');
            this.navc.navigateForward(['/login']);
          },
        },
      ],
    });

    await alert.present();
  }

  //공지사항 이동함수
  gonotice() {
    this.auth.user$.pipe(take(1)).subscribe((user: any) => {
      if (user && user.nickname) {
        this.navc.navigateForward(['home-add-notice']);
      } else {
        this.login();
      }
    });
  }

  //마이페이지 이동 함수
  async gomypage() {
    this.auth.user$.pipe(take(1)).subscribe((user: any) => {
      if (user && user.nickname) {
        this.navc.navigateForward(['tabs/home-add-mypage']);
      } else {
        this.login();
      }
    });
  }

  //새로운 알림 체크함수
  checkAlarm() {
    let pushId;
    let uid = localStorage.getItem('userId');
    this.db.doc$(`users/${uid}`).subscribe(a => (pushId = a.pushId));
    this.alarms$ = this.db.collection$(`alarm`, ref => ref.where('userId', '==', uid).where('check', '==', false).orderBy('dateCreated', 'desc'));
  }

  //우울증설문지 검사 이동
  goSurveyDep(){
    this.navc.navigateForward(['survey-depression']);
  }
  //스트레스설문지 검사 이동
  goSurveySts(){
    this.navc.navigateForward(['survey-stress']);
  }

  async goReady(){
    const toast = await this.toast.create({
      message: '준비중입니다.',
      duration: 1000,
    });
    toast.present();

    return;
  }

}
