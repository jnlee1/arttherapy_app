import { ImgCheckService } from './../services/img-check.service';
import { Router } from '@angular/router';
import { Component } from '@angular/core';

import { Platform, ToastController, NavController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private alertController: AlertController,
    private toast: ToastController,
    private imgchk: ImgCheckService,
    private navCtrl: NavController) {
    this.initializeApp();
    this.backbutton();
  }

  // initializeApp() {
  //   this.platform.ready().then(() => {
  //     this.statusBar.styleDefault();
  //     this.splashScreen.hide();
  //   });
  // }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      if (this.platform.is('android')) this.statusBar.styleLightContent();
      else {
        this.statusBar.overlaysWebView(false);
        this.statusBar.backgroundColorByHexString('#fff');
      }
      this.splashScreen.hide();
    });
  }


  lastTimeBackPress = 0;
  timePeriodToExit = 2000;

  async openToast() {
    const toast = await this.toast.create({
      message: '다시 한번 누르면 종료됩니다.',
      duration: 2000,
      cssClass: 'toast_wrap',
    });
    await toast.present();
  }

  // 안드로이드만 작용하는 함수 뒤로가기 버튼 작동 함수
  backbutton() {
    this.platform.backButton.subscribeWithPriority(0, () => {
      let url = this.router.url;
      if (url) {
        if (url.indexOf('home-add-howto-img') > -1) {

          if (this.imgchk.getItem()) {
            this.stop();
            this.imgchk.setItem(false);
          } else {
            this.navCtrl.pop();
          }

        } else {
          switch (url) {
            case '/tabs':
            case '/tabs/home': {
              if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
                navigator['app'].exitApp();
              } else {
                this.openToast();
                this.lastTimeBackPress = new Date().getTime();
              }
              break;
            }

            default: {
              this.navCtrl.pop();
              break;
            }
          }
        }
      }

    });
  }

  async stop() {
    const alert = await this.alertController.create({
      header: '검사요청 종료',
      message: '업로드한 이미지가 저장되지 않습니다.<br> 검사요청을 종료하시겠습니까?',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
        },
        {
          text: '확인',
          handler: async () => {
            await this.navCtrl.navigateRoot(['/tabs/home']);
          },
        },
      ],
    });

    await alert.present();
  }

}
