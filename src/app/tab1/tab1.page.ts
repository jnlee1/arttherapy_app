import { Component, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { IonSlides, AlertController, NavController } from '@ionic/angular';
import { FCM } from 'plugins/cordova-plugin-fcm-with-dependecy-updated/ionic/FCM';
import { take } from 'rxjs/operators';
import { AuthService } from 'src/services/auth.service';
import { DbService } from 'src/services/db.service';

@Component({
  selector: 'app-tab1',
  templateUrl: './tab1.page.html',
  styleUrls: ['./tab1.page.scss'],
})

// 메뉴명
// 홈탭-온아트테라피 홈페이지
export class Tab1Page implements OnInit {

  @ViewChild('slides', { static: false }) slider: IonSlides;

  loginSwitch;
  newAlarmSwitch: boolean;
  alarms$;
  homepage:any='';
  constructor(public alertController: AlertController, private navc: NavController, private auth: AuthService, private db: DbService,
              private sanitizer: DomSanitizer) {
      this.loginSwitch = localStorage.getItem('loginSwitch');
      this.checkAlarm();
      this.getFCM();
      this.homepage=this.sanitizer.bypassSecurityTrustResourceUrl('https://www.onarttherapy.co.kr/About');
    }
    
    ngOnInit() { }
    

  //서비스 이용시 로그인 필요 alert
  async login() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '로그인',
      message: '로그인 후 이용 바랍니다.',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: '로그인',
          handler: () => {
            console.log('Confirm Okay');
            this.navc.navigateForward(['/login']);
          },
        },
      ],
    });

    await alert.present();
  }

  //공지사항 이동함수
  gonotice() {
    this.auth.user$.pipe(take(1)).subscribe((user: any) => {
      if (user && user.nickname) {
        this.navc.navigateForward(['home-add-notice']);
      } else {
        this.login();
      }
    });
  }

  //마이페이지 이동 함수
  async gomypage() {
    this.auth.user$.pipe(take(1)).subscribe((user: any) => {
      if (user && user.nickname) {
        this.navc.navigateForward(['tabs/home-add-mypage']);
      } else {
        this.login();
      }
    });
  }

  //새로운 알림 체크함수
  checkAlarm() {
    let pushId;
    let uid = localStorage.getItem('userId');
    this.db.doc$(`users/${uid}`).subscribe(a => (pushId = a.pushId));
    this.alarms$ = this.db.collection$(`alarm`, ref => ref.where('userId', '==', uid).where('check', '==', false).orderBy('dateCreated', 'desc'));
  }

  //앱에서 메시지를 수신하기 위한 함수
  getFCM() {
    FCM.onNotification().subscribe(e => {
      console.log(e);
      if (e && e.wasTapped) {
        if (e.counselorId) {
          this.navc.navigateForward('/home-add-mypage-result');
        } else {
          this.navc.navigateForward(['/tab']);
        }
      } else {
        console.log('$Received in FOREGROUND$');
      }
    });
    FCM.getInitialPushPayload().then(e => {
      if (e && e.wasTapped) {
        if (e.counselorId) {
          this.navc.navigateForward('/home-add-mypage-result');
        } else {
          this.navc.navigateForward(['/tab']);
        }
      } else {
        console.log('$Received in FOREGROUND$');
      }
    });
  }



  
}
