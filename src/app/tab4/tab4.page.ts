import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, AlertController, NavController } from '@ionic/angular';
import { take } from 'rxjs/operators';
import { AuthService } from 'src/services/auth.service';
import { DbService } from 'src/services/db.service';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})

// 메뉴명
// 방문기관
export class Tab4Page implements OnInit {

  institutiones$;
  moreSeeSwitch: boolean = false;

  @ViewChild('slides', { static: false }) slider: IonSlides;

  loginSwitch;
  newAlarmSwitch: boolean;
  alarms$;
  constructor(public alertController: AlertController, private navc: NavController, private auth: AuthService, private db: DbService) {
    this.loginSwitch = localStorage.getItem('loginSwitch');
    //this.checkAlarm();
    //this.getFCM();
    this.getInstitution();
  }

  ngOnInit() { }

  //서비스 이용시 로그인 필요 alert
  async login() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: '로그인',
      message: '로그인 후 이용 바랍니다.',
      buttons: [
        {
          text: '취소',
          role: 'cancel',
          cssClass: 'secondary',
        },
        {
          text: '로그인',
          handler: () => {
            console.log('Confirm Okay');
            this.navc.navigateForward(['/login']);
          },
        },
      ],
    });

    await alert.present();
  }

  //공지사항 이동함수
  gonotice() {
    this.auth.user$.pipe(take(1)).subscribe((user: any) => {
      if (user && user.nickname) {
        this.navc.navigateForward(['home-add-notice']);
      } else {
        this.login();
      }
    });
  }

  //마이페이지 이동 함수
  async gomypage() {
    this.auth.user$.pipe(take(1)).subscribe((user: any) => {
      if (user && user.nickname) {
        this.navc.navigateForward(['tabs/home-add-mypage']);
      } else {
        this.login();
      }
    });
  }

  //상담기관 목록 조회
  getInstitution(){
    this.institutiones$ = this.db.collection$(`institution`, ref => ref.orderBy('name'));
  }

  //상담기관 자세히보기
  fnGoVisit(paramId){
   this.navc.navigateForward(['visit-detailpage'],{
     queryParams : {
       docId:paramId
     }
   });
  }



}
