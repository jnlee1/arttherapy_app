import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Camera } from '@ionic-native/camera/ngx';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireMessagingModule } from '@angular/fire/messaging';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import * as firebase from 'firebase';
import { File } from '@ionic-native/file/ngx';
import { FileTransfer } from '@ionic-native/file-transfer/ngx';
import { HttpClientModule } from '@angular/common/http';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { FirebaseX } from '@ionic-native/firebase-x/ngx';
import { DocumentViewer } from '@ionic-native/document-viewer/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';
import { PhotoLibrary } from '@ionic-native/photo-library/ngx';
import { Ng2ImgMaxModule } from "ng2-img-max";
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Network } from '@ionic-native/network/ngx';

firebase.initializeApp({
  apiKey: "AIzaSyBgc9L0gqpHKdDnHE3BtqwYVZMolskgcLI",
  authDomain: "psychological-4c86e.firebaseapp.com",
  databaseURL: "https://psychological-4c86e.firebaseio.com",
  projectId: "psychological-4c86e",
  storageBucket: "psychological-4c86e.appspot.com",
  messagingSenderId: "701807654802",
  appId: "1:701807654802:web:7f8ab9d9df1af5f3ffb34b",
  measurementId: "G-LH7Z6KTG6J"
});

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot({ mode: 'ios' }),
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyBgc9L0gqpHKdDnHE3BtqwYVZMolskgcLI",
      authDomain: "psychological-4c86e.firebaseapp.com",
      databaseURL: "https://psychological-4c86e.firebaseio.com",
      projectId: "psychological-4c86e",
      storageBucket: "psychological-4c86e.appspot.com",
      messagingSenderId: "701807654802",
      appId: "1:701807654802:web:7f8ab9d9df1af5f3ffb34b",
      measurementId: "G-LH7Z6KTG6J"
    }),
    AppRoutingModule,
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireMessagingModule,
    ReactiveFormsModule,
    HttpClientModule,
    Ng2ImgMaxModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Camera,
    AndroidPermissions,
    File,
    FileTransfer,
    DocumentViewer,
    FirebaseX,
    FileOpener,
    ImagePicker,
    Keyboard,
    PhotoLibrary,
    InAppBrowser,
    Network,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
