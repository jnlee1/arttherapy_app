(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-add-notice-setting-home-add-notice-setting-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-notice-setting/home-add-notice-setting.page.html":
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-notice-setting/home-add-notice-setting.page.html ***!
  \***********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <div id=\"headerInner\">\n    <ion-toolbar>\n      <ion-buttons class=\"back\" slot=\"start\">\n        <ion-back-button text=\"\" icon=\"\"></ion-back-button>\n      </ion-buttons>\n      <ion-title>알림내역</ion-title>\n    </ion-toolbar>\n\n  </div>\n</ion-header>\n\n<ion-content>\n  <ion-item>\n    <h2>\n      알림\n    </h2>\n\n    <ion-toggle slot=\"end\" [(ngModel)]=\"alarmSwitch\" (ionChange)=\"onChange()\"></ion-toggle>\n  </ion-item>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/home-add-notice-setting/home-add-notice-setting-routing.module.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/home-add-notice-setting/home-add-notice-setting-routing.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: HomeAddNoticeSettingPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAddNoticeSettingPageRoutingModule", function() { return HomeAddNoticeSettingPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_add_notice_setting_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home-add-notice-setting.page */ "./src/app/pages/home-add-notice-setting/home-add-notice-setting.page.ts");




const routes = [
    {
        path: '',
        component: _home_add_notice_setting_page__WEBPACK_IMPORTED_MODULE_3__["HomeAddNoticeSettingPage"]
    }
];
let HomeAddNoticeSettingPageRoutingModule = class HomeAddNoticeSettingPageRoutingModule {
};
HomeAddNoticeSettingPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomeAddNoticeSettingPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/home-add-notice-setting/home-add-notice-setting.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/home-add-notice-setting/home-add-notice-setting.module.ts ***!
  \*********************************************************************************/
/*! exports provided: HomeAddNoticeSettingPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAddNoticeSettingPageModule", function() { return HomeAddNoticeSettingPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _home_add_notice_setting_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-add-notice-setting-routing.module */ "./src/app/pages/home-add-notice-setting/home-add-notice-setting-routing.module.ts");
/* harmony import */ var _home_add_notice_setting_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-add-notice-setting.page */ "./src/app/pages/home-add-notice-setting/home-add-notice-setting.page.ts");







let HomeAddNoticeSettingPageModule = class HomeAddNoticeSettingPageModule {
};
HomeAddNoticeSettingPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _home_add_notice_setting_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeAddNoticeSettingPageRoutingModule"]
        ],
        declarations: [_home_add_notice_setting_page__WEBPACK_IMPORTED_MODULE_6__["HomeAddNoticeSettingPage"]]
    })
], HomeAddNoticeSettingPageModule);



/***/ }),

/***/ "./src/app/pages/home-add-notice-setting/home-add-notice-setting.page.scss":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/home-add-notice-setting/home-add-notice-setting.page.scss ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-item {\n  padding: 18px 22px;\n  border-bottom: 1px solid #eaeaea;\n}\nion-item h2 {\n  margin: 0;\n  font-size: 15px;\n  font-weight: 600;\n  color: #333333;\n}\nion-item ion-toggle {\n  --handle-border-radius: 50%;\n  width: 52px;\n  height: 30px;\n  padding: 0;\n  --background-checked: center / cover no-repeat url('toggle.png');\n  --handle-box-shadow: 0 4px 8px rgba(44, 39, 56, 0.02);\n  --handle-height: 24px;\n  --handle-max-height: 24px;\n  --handle-width: 24px;\n  --handle-spacing: 4px;\n  --handle-background: center / cover no-repeat url('tooglei.png');\n  --background: center / cover no-repeat url('Off.png');\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtbm90aWNlLXNldHRpbmcvaG9tZS1hZGQtbm90aWNlLXNldHRpbmcucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFzQkEsZ0NBQUE7QUFwQkY7QUFERTtFQUNFLFNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FBR0o7QUFBRTtFQUNFLDJCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EsZ0VBQUE7RUFDQSxxREFBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7RUFDQSxvQkFBQTtFQUNBLHFCQUFBO0VBQ0EsZ0VBQUE7RUFDQSxxREFBQTtBQUVKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtbm90aWNlLXNldHRpbmcvaG9tZS1hZGQtbm90aWNlLXNldHRpbmcucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWl0ZW0ge1xuICBwYWRkaW5nOiAxOHB4IDIycHg7XG4gIGgyIHtcbiAgICBtYXJnaW46IDA7XG4gICAgZm9udC1zaXplOiAxNXB4O1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgY29sb3I6ICMzMzMzMzM7XG4gIH1cblxuICBpb24tdG9nZ2xlIHtcbiAgICAtLWhhbmRsZS1ib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgd2lkdGg6IDUycHg7XG4gICAgaGVpZ2h0OiAzMHB4O1xuICAgIHBhZGRpbmc6IDA7XG4gICAgLS1iYWNrZ3JvdW5kLWNoZWNrZWQ6IGNlbnRlciAvIGNvdmVyIG5vLXJlcGVhdCB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZ3MvdG9nZ2xlLnBuZyk7XG4gICAgLS1oYW5kbGUtYm94LXNoYWRvdzogMCA0cHggOHB4IHJnYmEoNDQsIDM5LCA1NiwgMC4wMik7XG4gICAgLS1oYW5kbGUtaGVpZ2h0OiAyNHB4O1xuICAgIC0taGFuZGxlLW1heC1oZWlnaHQ6IDI0cHg7XG4gICAgLS1oYW5kbGUtd2lkdGg6IDI0cHg7XG4gICAgLS1oYW5kbGUtc3BhY2luZzogNHB4O1xuICAgIC0taGFuZGxlLWJhY2tncm91bmQ6IGNlbnRlciAvIGNvdmVyIG5vLXJlcGVhdCB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZ3MvdG9vZ2xlaS5wbmcpO1xuICAgIC0tYmFja2dyb3VuZDogY2VudGVyIC8gY292ZXIgbm8tcmVwZWF0IHVybCguLi8uLi8uLi9hc3NldHMvaW1ncy9Db250cm9scy9Td2l0Y2gvT2ZmLnBuZyk7XG4gIH1cbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlYWVhZWE7XG59XG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/home-add-notice-setting/home-add-notice-setting.page.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/home-add-notice-setting/home-add-notice-setting.page.ts ***!
  \*******************************************************************************/
/*! exports provided: HomeAddNoticeSettingPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAddNoticeSettingPage", function() { return HomeAddNoticeSettingPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var src_services_db_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/services/db.service */ "./src/services/db.service.ts");



let HomeAddNoticeSettingPage = class HomeAddNoticeSettingPage {
    constructor(db) {
        this.db = db;
        this.uid = localStorage.getItem('userId');
        this.getData();
    }
    getData() {
        this.db.doc$(`users/${this.uid}`).subscribe(data => {
            this.alarmSwitch = data.alarmSwitch;
        });
    }
    ngOnInit() {
    }
    onChange() {
        this.db.updateAt(`users/${this.uid}`, {
            alarmSwitch: this.alarmSwitch,
        });
    }
};
HomeAddNoticeSettingPage.ctorParameters = () => [
    { type: src_services_db_service__WEBPACK_IMPORTED_MODULE_2__["DbService"] }
];
HomeAddNoticeSettingPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-add-notice-setting',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home-add-notice-setting.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-notice-setting/home-add-notice-setting.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home-add-notice-setting.page.scss */ "./src/app/pages/home-add-notice-setting/home-add-notice-setting.page.scss")).default]
    })
], HomeAddNoticeSettingPage);



/***/ })

}]);
//# sourceMappingURL=pages-home-add-notice-setting-home-add-notice-setting-module-es2015.js.map