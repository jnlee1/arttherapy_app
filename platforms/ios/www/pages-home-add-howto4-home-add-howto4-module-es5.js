(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-add-howto4-home-add-howto4-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-howto4/home-add-howto4.page.html":
    /*!*******************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-howto4/home-add-howto4.page.html ***!
      \*******************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesHomeAddHowto4HomeAddHowto4PageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <div id=\"headerInner\">\n    <ion-toolbar>\n      <ion-buttons class=\"back\" slot=\"start\">\n        <ion-back-button text=\"\" icon=\"\"></ion-back-button>\n      </ion-buttons>\n      <ion-title>검사 실시 방법</ion-title>\n    </ion-toolbar>\n\n  </div>\n</ion-header>\n\n<ion-content>\n  <div class=\"allWrap\">\n\n    <div class=\"wrap\">\n      <div class=\"slideWrap\">\n        <div class=\"slideAll\">\n          <div class=\"conInner\">\n            <div class=\"step\">\n              <span>\n                4\n              </span>\n            </div>\n\n            <div class=\"stepInner\">\n              <div>\n                <h2>\n                  사람을 그리고 나면 새로운 <br>종이 한 장을 세로로 놓고 <br>“ 앞에 그림 사람과 반대되는 <br>성을 그리세요.”\n                </h2>\n\n\n\n                <span class=\"m2\">\n                  예) 첫번째 남자를 그렸으면 두번째는 <br>여자를 그리세요.\n                </span>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"btn_wrap\">\n      <span class=\"intro\">\n        안내에 맞게 그림을 그리셨다면<br>\n        다음버튼을 눌러주세요.\n      </span>\n      <ion-button expand=\"block\" class=\"grbtn\" (click)=\"slidenext()\">\n        다음\n      </ion-button>\n    </div>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/home-add-howto4/home-add-howto4-routing.module.ts":
    /*!*************************************************************************!*\
      !*** ./src/app/pages/home-add-howto4/home-add-howto4-routing.module.ts ***!
      \*************************************************************************/

    /*! exports provided: HomeAddHowto4PageRoutingModule */

    /***/
    function srcAppPagesHomeAddHowto4HomeAddHowto4RoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddHowto4PageRoutingModule", function () {
        return HomeAddHowto4PageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _home_add_howto4_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./home-add-howto4.page */
      "./src/app/pages/home-add-howto4/home-add-howto4.page.ts");

      var routes = [{
        path: '',
        component: _home_add_howto4_page__WEBPACK_IMPORTED_MODULE_3__["HomeAddHowto4Page"]
      }];

      var HomeAddHowto4PageRoutingModule = function HomeAddHowto4PageRoutingModule() {
        _classCallCheck(this, HomeAddHowto4PageRoutingModule);
      };

      HomeAddHowto4PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], HomeAddHowto4PageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/home-add-howto4/home-add-howto4.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/home-add-howto4/home-add-howto4.module.ts ***!
      \*****************************************************************/

    /*! exports provided: HomeAddHowto4PageModule */

    /***/
    function srcAppPagesHomeAddHowto4HomeAddHowto4ModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddHowto4PageModule", function () {
        return HomeAddHowto4PageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _home_add_howto4_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./home-add-howto4-routing.module */
      "./src/app/pages/home-add-howto4/home-add-howto4-routing.module.ts");
      /* harmony import */


      var _home_add_howto4_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./home-add-howto4.page */
      "./src/app/pages/home-add-howto4/home-add-howto4.page.ts");

      var HomeAddHowto4PageModule = function HomeAddHowto4PageModule() {
        _classCallCheck(this, HomeAddHowto4PageModule);
      };

      HomeAddHowto4PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _home_add_howto4_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeAddHowto4PageRoutingModule"]],
        declarations: [_home_add_howto4_page__WEBPACK_IMPORTED_MODULE_6__["HomeAddHowto4Page"]]
      })], HomeAddHowto4PageModule);
      /***/
    },

    /***/
    "./src/app/pages/home-add-howto4/home-add-howto4.page.scss":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/home-add-howto4/home-add-howto4.page.scss ***!
      \*****************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesHomeAddHowto4HomeAddHowto4PageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "h2 {\n  white-space: pre-line;\n  margin: 0;\n  font-weight: 600;\n  font-size: 0.9375rem;\n  color: #161616;\n  line-height: 1.4rem;\n  letter-spacing: -0.2px;\n  margin-bottom: 1rem;\n  text-align: center;\n}\n\n.m1 {\n  font-size: 0.875rem;\n  font-weight: 600;\n  line-height: 1.188rem;\n  color: #333;\n  letter-spacing: -0.2px;\n  display: block;\n}\n\n.m2 {\n  font-size: 0.9375rem;\n  font-weight: 600;\n  line-height: 1.188rem;\n  color: #333;\n  letter-spacing: -0.2px;\n  display: block;\n  text-align: center;\n  text-align: center;\n}\n\n.m1,\n.m2 {\n  white-space: pre-line;\n}\n\n.slideAll {\n  height: 100%;\n  width: 100%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\nion-slide {\n  width: 100%;\n}\n\nion-slides {\n  height: 59.86%;\n  width: 100%;\n}\n\n.wrap {\n  height: calc(100% - 138px);\n  background: #ffffff;\n}\n\n.btn_wrap {\n  background: #ffffff;\n}\n\n.slideWrap {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  height: 100%;\n  width: 100%;\n}\n\n.conInner {\n  background: #f9f9f9;\n  border-top: 1px solid #eee;\n  border-bottom: 1px solid #eee;\n  height: 59.86%;\n  width: 100%;\n  padding-top: 9.77%;\n  padding-bottom: 23.46%;\n  overflow: hidden;\n}\n\n.step {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  width: 50px;\n  height: 50px;\n  border-radius: 50%;\n  background: linear-gradient(313deg, #2b5ac3 0%, #1b98e7 100%);\n  margin: 0 auto;\n}\n\n.step span {\n  z-index: 1;\n  font-size: 30px;\n  text-transform: uppercase;\n  background: linear-gradient(313deg, #2b5ac3 0%, #1b98e7 100%);\n  -webkit-background-clip: text;\n  -webkit-text-fill-color: transparent;\n}\n\n.stepInner {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  height: 100%;\n}\n\n.step::before {\n  position: absolute;\n  border-radius: 50%;\n  content: \"\";\n  display: block;\n  width: 42px;\n  height: 42px;\n  background: #fff;\n}\n\n@media (max-height: 736px) {\n  .conInner {\n    height: 75%;\n  }\n}\n\n@media (max-height: 570px) {\n  .conInner {\n    height: 80%;\n  }\n\n  .step {\n    width: 50px;\n    height: 50px;\n  }\n\n  .conInner {\n    padding-top: 7%;\n  }\n\n  .step::before {\n    width: 42px;\n    height: 42px;\n  }\n\n  h2 {\n    line-height: 20px;\n    font-size: 16px;\n  }\n}\n\n.intro {\n  font-size: 14px;\n  font-weight: 600;\n  line-height: 19px;\n  color: #545454;\n  display: block;\n  text-align: center;\n  margin-bottom: 24px;\n}\n\n.grbtn {\n  width: calc(100% - 26px - 26px);\n  margin: 0 auto;\n}\n\n.allWrap {\n  height: calc(100vh - 100px);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtaG93dG80L2hvbWUtYWRkLWhvd3RvNC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxxQkFBQTtFQUNBLFNBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0FBRUY7O0FBQ0E7RUFDRSxvQkFBQTtFQUVBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQUNGOztBQUVBOztFQUVFLHFCQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBRUY7O0FBQ0E7RUFDRSxXQUFBO0FBRUY7O0FBQ0E7RUFFRSxjQUFBO0VBQ0EsV0FBQTtBQUNGOztBQUVBO0VBQ0UsMEJBQUE7RUFDQSxtQkFBQTtBQUNGOztBQUVBO0VBQ0UsbUJBQUE7QUFDRjs7QUFFQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUFDRjs7QUFFQTtFQUNFLG1CQUFBO0VBQ0EsMEJBQUE7RUFDQSw2QkFBQTtFQUNBLGNBQUE7RUFFQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0FBQUY7O0FBR0E7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSw2REFBQTtFQUNBLGNBQUE7QUFBRjs7QUFFRTtFQUNFLFVBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7RUFDQSw2REFBQTtFQUNBLDZCQUFBO0VBQ0Esb0NBQUE7QUFBSjs7QUFJQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtBQURGOztBQUlBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQURGOztBQUdBO0VBQ0U7SUFDRSxXQUFBO0VBQUY7QUFDRjs7QUFHQTtFQUNFO0lBQ0UsV0FBQTtFQURGOztFQUdBO0lBQ0UsV0FBQTtJQUNBLFlBQUE7RUFBRjs7RUFHQTtJQUNFLGVBQUE7RUFBRjs7RUFHQTtJQUNFLFdBQUE7SUFDQSxZQUFBO0VBQUY7O0VBR0E7SUFDRSxpQkFBQTtJQUNBLGVBQUE7RUFBRjtBQUNGOztBQU9BO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUFMRjs7QUFRQTtFQUNFLCtCQUFBO0VBQ0EsY0FBQTtBQUxGOztBQVFBO0VBQ0UsMkJBQUE7QUFMRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUtYWRkLWhvd3RvNC9ob21lLWFkZC1ob3d0bzQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaDIge1xuICB3aGl0ZS1zcGFjZTogcHJlLWxpbmU7XG4gIG1hcmdpbjogMDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiAwLjkzNzVyZW07XG4gIGNvbG9yOiAjMTYxNjE2O1xuICBsaW5lLWhlaWdodDogMS40cmVtO1xuICBsZXR0ZXItc3BhY2luZzogLTAuMnB4O1xuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4ubTEge1xuICBmb250LXNpemU6IDAuODc1cmVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBsaW5lLWhlaWdodDogMS4xODhyZW07XG4gIGNvbG9yOiAjMzMzO1xuICBsZXR0ZXItc3BhY2luZzogLTAuMnB4O1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuLm0yIHtcbiAgZm9udC1zaXplOiAwLjkzNzVyZW07XG5cbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbGluZS1oZWlnaHQ6IDEuMTg4cmVtO1xuICBjb2xvcjogIzMzMztcbiAgbGV0dGVyLXNwYWNpbmc6IC0wLjJweDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ubTEsXG4ubTIge1xuICB3aGl0ZS1zcGFjZTogcHJlLWxpbmU7XG59XG4uc2xpZGVBbGwge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuaW9uLXNsaWRlIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbmlvbi1zbGlkZXMge1xuICAvLyAgIGhlaWdodDogODAlO1xuICBoZWlnaHQ6IDU5Ljg2JTtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi53cmFwIHtcbiAgaGVpZ2h0OiBjYWxjKDEwMCUgLSAxMzhweCk7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG59XG5cbi5idG5fd3JhcCB7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG59XG5cbi5zbGlkZVdyYXAge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmNvbklubmVyIHtcbiAgYmFja2dyb3VuZDogI2Y5ZjlmOTtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNlZWU7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWVlO1xuICBoZWlnaHQ6IDU5Ljg2JTtcblxuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZy10b3A6IDkuNzclO1xuICBwYWRkaW5nLWJvdHRvbTogMjMuNDYlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4uc3RlcCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiA1MHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgzMTNkZWcsIHJnYmEoNDMsIDkwLCAxOTUsIDEpIDAlLCByZ2JhKDI3LCAxNTIsIDIzMSwgMSkgMTAwJSk7XG4gIG1hcmdpbjogMCBhdXRvO1xuICAvLyAgIG1hcmdpbi1ib3R0b206IDIzLjE4JTtcbiAgc3BhbiB7XG4gICAgei1pbmRleDogMTtcbiAgICBmb250LXNpemU6IDMwcHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMzEzZGVnLCByZ2JhKDQzLCA5MCwgMTk1LCAxKSAwJSwgcmdiYSgyNywgMTUyLCAyMzEsIDEpIDEwMCUpO1xuICAgIC13ZWJraXQtYmFja2dyb3VuZC1jbGlwOiB0ZXh0O1xuICAgIC13ZWJraXQtdGV4dC1maWxsLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgfVxufVxuXG4uc3RlcElubmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLnN0ZXA6OmJlZm9yZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBjb250ZW50OiAnJztcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiA0MnB4O1xuICBoZWlnaHQ6IDQycHg7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG59XG5AbWVkaWEgKG1heC1oZWlnaHQ6IDczNnB4KSB7XG4gIC5jb25Jbm5lciB7XG4gICAgaGVpZ2h0OiA3NSU7XG4gIH1cbn1cblxuQG1lZGlhIChtYXgtaGVpZ2h0OiA1NzBweCkge1xuICAuY29uSW5uZXIge1xuICAgIGhlaWdodDogODAlO1xuICB9XG4gIC5zdGVwIHtcbiAgICB3aWR0aDogNTBweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gIH1cblxuICAuY29uSW5uZXIge1xuICAgIHBhZGRpbmctdG9wOiA3JTtcbiAgfVxuXG4gIC5zdGVwOjpiZWZvcmUge1xuICAgIHdpZHRoOiA0MnB4O1xuICAgIGhlaWdodDogNDJweDtcbiAgfVxuXG4gIGgyIHtcbiAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gIH1cblxuICAubTIge1xuICAgIC8vIGZvbnQtc2l6ZTogMTZweDtcbiAgfVxufVxuXG4uaW50cm8ge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGxpbmUtaGVpZ2h0OiAxOXB4O1xuICBjb2xvcjogIzU0NTQ1NDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcbn1cblxuLmdyYnRuIHtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDI2cHggLSAyNnB4KTtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG5cbi5hbGxXcmFwIHtcbiAgaGVpZ2h0OiBjYWxjKDEwMHZoIC0gMTAwcHgpO1xufVxuIl19 */";
      /***/
    },

    /***/
    "./src/app/pages/home-add-howto4/home-add-howto4.page.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/home-add-howto4/home-add-howto4.page.ts ***!
      \***************************************************************/

    /*! exports provided: HomeAddHowto4Page */

    /***/
    function srcAppPagesHomeAddHowto4HomeAddHowto4PageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddHowto4Page", function () {
        return HomeAddHowto4Page;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var HomeAddHowto4Page = /*#__PURE__*/function () {
        function HomeAddHowto4Page(navc) {
          _classCallCheck(this, HomeAddHowto4Page);

          this.navc = navc;
          this.slideOpts = {
            initialSlide: 0,
            speed: 400,
            allowTouchMove: false
          };
        }

        _createClass(HomeAddHowto4Page, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "slidenext",
          value: function slidenext() {
            this.navc.navigateForward('home-add-howto-img');
          }
        }]);

        return HomeAddHowto4Page;
      }();

      HomeAddHowto4Page.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }];
      };

      HomeAddHowto4Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-add-howto4',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./home-add-howto4.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-howto4/home-add-howto4.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./home-add-howto4.page.scss */
        "./src/app/pages/home-add-howto4/home-add-howto4.page.scss"))["default"]]
      })], HomeAddHowto4Page);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-home-add-howto4-home-add-howto4-module-es5.js.map