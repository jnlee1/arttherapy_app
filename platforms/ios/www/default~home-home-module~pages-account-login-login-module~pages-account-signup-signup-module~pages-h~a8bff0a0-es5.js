(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~home-home-module~pages-account-login-login-module~pages-account-signup-signup-module~pages-h~a8bff0a0"], {
    /***/
    "./src/services/alert.service.ts":
    /*!***************************************!*\
      !*** ./src/services/alert.service.ts ***!
      \***************************************/

    /*! exports provided: AlertService */

    /***/
    function srcServicesAlertServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AlertService", function () {
        return AlertService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var errorMessages = {
        // Firebase의 auth 에러 메세지 정의
        accountExistsWithDifferentCredential: {
          title: '계정 안내',
          subTitle: '이미 있는 계정입니다.'
        },
        invalidCredential: {
          title: '인증 오류',
          subTitle: '로그인 인증에 오류가 발생했습니다.'
        },
        operationNotAllowed: {
          title: '로그인 실패!',
          subTitle: '로그인 과정에서 오류가 발생했습니다. 관리자에게 문의하시길 바랍니다.'
        },
        userDisabled: {
          title: '정지 계정',
          subTitle: '정지된 계정입니다. 관리자에게 연락하시길 바랍니다.'
        },
        userNotFound: {
          title: '계정 없음',
          subTitle: '해당 계정 정보가 없습니다.'
        },
        // userEmailNotFound: { title: '가입된 이메일 주소가 아닙니다.', subTitle: '다시 확인해 주시기 바랍니다.' },
        wrongPassword: {
          title: '비밀번호 재설정',
          subTitle: '비밀번호 재설정을 위해 가입하신 이메일 주소를\n 입력하여 주시기 바랍니다.'
        },
        // invalidEmail: { title: '계정 없음', subTitle: '입력하신 이메일로 가입된 회원정보가 없습니다. \n 회원가입을 진행해 주시길 바랍니다.' },
        userEmailNotFound: {
          title: '계정 없음',
          subTitle: '가입된 이메일 주소가 아닙니다.\n다시 확인해 주시기 바랍니다.'
        },
        // wrongPassword: { title: '비밀번호 재설정', subTitle: '비밀번호 재설정을 위해 가입하신 이메일 주소를 입력하여 주시기 바랍니다.' },
        invalidEmail: {
          title: '계정 없음',
          subTitle: '이메일 형식이 올바르지 않습니다. \n 올바른 이메일을 입력하여 주시기 바랍니다.'
        },
        emailAlreadyInUse: {
          title: '사용할 수 없는 이메일',
          subTitle: '이미 사용중인 이메일입니다. 다시 확인하시기 바랍니다.'
        },
        weakPassword: {
          title: '비밀번호 경고',
          subTitle: '입력하신 비밀번호가 보안에 취약합니다.'
        },
        requiresRecentLogin: {
          title: '인증 만료',
          subTitle: '인증이 만료되었습니다. 다시 로그인 하시기 바랍니다.'
        },
        userMismatch: {
          title: '사용자 불일치',
          subTitle: '다른 사용자의 인증정보입니다!'
        },
        providerAlreadyLinked: {
          title: 'Already Linked',
          subTitle: 'Sorry, but your account is already linked to this credential.'
        },
        credentialAlreadyInUse: {
          title: '불가능 인증정보',
          subTitle: 'Sorry, but this credential is already used by another user.'
        },
        toManyrequests: {
          title: '확인 횟수 초과',
          subTitle: '잘못된 비밀번호로 확인 횟수가 초과 되었습니다. \n잠시 후 다시 시도하시기 바랍니다.'
        }
      };

      var AlertService = /*#__PURE__*/function () {
        function AlertService(alertCtr, toastCtr) {
          _classCallCheck(this, AlertService);

          this.alertCtr = alertCtr;
          this.toastCtr = toastCtr;
        }

        _createClass(AlertService, [{
          key: "presentAlert",
          value: function presentAlert(header, subtitle, message) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var alert;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.alertCtr.create({
                        header: header,
                        subHeader: subtitle,
                        message: message,
                        buttons: ['확인']
                      });

                    case 2:
                      alert = _context.sent;
                      _context.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "askAlert",
          value: function askAlert(header, subtitle, message) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var alert;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.alertCtr.create({
                        header: header,
                        subHeader: subtitle,
                        message: message,
                        buttons: ['확인']
                      });

                    case 2:
                      alert = _context2.sent;
                      _context2.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "showErrorMessage",
          value: function showErrorMessage(code) {
            switch (code) {
              // Firebase Error Messages
              case 'auth/account-exists-with-different-credential':
                this.presentAlert(errorMessages.accountExistsWithDifferentCredential['title'], errorMessages.accountExistsWithDifferentCredential['subTitle']);
                break;

              case 'auth/invalid-credential':
                this.presentAlert(errorMessages.invalidCredential['title'], errorMessages.invalidCredential['subTitle']);
                break;

              case 'auth/operation-not-allowed':
                this.presentAlert(errorMessages.operationNotAllowed['title'], errorMessages.operationNotAllowed['subTitle']);
                break;

              case 'auth/user-disabled':
                this.presentAlert(errorMessages.userDisabled['title'], errorMessages.userDisabled['subTitle']);
                break;

              case 'auth/user-not-found':
                this.presentAlert(errorMessages.userEmailNotFound['title'], errorMessages.userEmailNotFound['subTitle']);
                break;

              case 'auth/wrong-password':
                this.presentAlert(errorMessages.wrongPassword['title'], errorMessages.wrongPassword['subTitle']);
                break;

              case 'auth/invalid-email':
                this.presentAlert(errorMessages.invalidEmail['title'], errorMessages.invalidEmail['subTitle']);
                break;

              case 'auth/email-already-in-use':
                this.presentAlert(errorMessages.emailAlreadyInUse['title'], errorMessages.emailAlreadyInUse['subTitle']);
                break;

              case 'auth/weak-password':
                this.presentAlert(errorMessages.weakPassword['title'], errorMessages.weakPassword['subTitle']);
                break;

              case 'auth/requires-recent-login':
                this.presentAlert(errorMessages.requiresRecentLogin['title'], errorMessages.requiresRecentLogin['subTitle']);
                break;

              case 'auth/user-mismatch':
                this.presentAlert(errorMessages.userMismatch['title'], errorMessages.userMismatch['subTitle']);
                break;

              case 'auth/provider-already-linked':
                this.presentAlert(errorMessages.providerAlreadyLinked['title'], errorMessages.providerAlreadyLinked['subTitle']);
                break;

              case 'auth/credential-already-in-use':
                this.presentAlert(errorMessages.credentialAlreadyInUse['title'], errorMessages.credentialAlreadyInUse['subTitle']);
                break;

              case 'auth/too-many-requests':
                this.presentAlert(errorMessages.toManyrequests['title'], errorMessages.toManyrequests['subTitle']);
                break;
            }
          } ///토스트 생성///////

        }, {
          key: "presentToast",
          value: function presentToast(message) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var toast;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.toastCtr.create({
                        message: message,
                        duration: 2000
                      });

                    case 2:
                      toast = _context3.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }]);

        return AlertService;
      }();

      AlertService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }];
      };

      AlertService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], AlertService);
      /***/
    },

    /***/
    "./src/services/auth.service.ts":
    /*!**************************************!*\
      !*** ./src/services/auth.service.ts ***!
      \**************************************/

    /*! exports provided: AuthService */

    /***/
    function srcServicesAuthServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthService", function () {
        return AuthService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/fire/auth */
      "./node_modules/@angular/fire/__ivy_ngcc__/auth/es2015/index.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _db_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./db.service */
      "./src/services/db.service.ts");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _alert_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./alert.service */
      "./src/services/alert.service.ts");
      /* harmony import */


      var firebase_app__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! firebase/app */
      "./node_modules/firebase/app/dist/index.cjs.js");
      /* harmony import */


      var firebase_app__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_9__);
      /* harmony import */


      var _loading_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! ./loading.service */
      "./src/services/loading.service.ts");
      /* harmony import */


      var _push_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! ./push.service */
      "./src/services/push.service.ts"); // import { Storage } from '@ionic/storage';
      // import { GooglePlus } from '@ionic-native/google-plus/ngx';


      var AuthService = /*#__PURE__*/function () {
        function AuthService(afAuth, db, router, platform, // public gplus: GooglePlus,
        loadingController, // public storage: Storage,
        alert, loadingService, push) {
          var _this = this;

          _classCallCheck(this, AuthService);

          this.afAuth = afAuth;
          this.db = db;
          this.router = router;
          this.platform = platform;
          this.loadingController = loadingController;
          this.alert = alert;
          this.loadingService = loadingService;
          this.push = push;
          this.user$ = this.afAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])(function (user) {
            return user ? db.doc$("users/".concat(user.uid)) : Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(null);
          }));
          this.user$.subscribe(function (data) {
            console.log("data", data);

            if (data) {
              _this.userInfo = data;
            } else {
              _this.userInfo = [];
            }
          });
          this.afAuth.authState.subscribe(function (data) {
            // console.log('data', data);
            if (data) {
              localStorage.setItem("userId", data.uid);
            }
          });
          this.handleRedirect();
        }

        _createClass(AuthService, [{
          key: "getUser",
          value: function getUser() {
            return this.user$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["first"])()).toPromise();
          }
        }, {
          key: "uid",
          value: function uid() {
            return this.user$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (u) {
              return u && u.uid;
            })).toPromise();
          }
        }, {
          key: "nickname",
          value: function nickname() {
            return this.user$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (u) {
              return u && u.nickname;
            })).toPromise();
          }
        }, {
          key: "approval",
          value: function approval() {
            return this.user$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (u) {
              return u && u.activeSwitch;
            })).toPromise();
          }
        }, {
          key: "userId",
          value: function userId() {
            return this.user$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (u) {
              return u.id;
            }));
          }
        }, {
          key: "info",
          value: function info() {
            return this.user$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1)).toPromise();
          }
        }, {
          key: "infoR",
          value: function infoR() {
            return this.user$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(function (u) {
              return u && u.selectedParts;
            })).toPromise();
          }
        }, {
          key: "anonymousLogin",
          value: function anonymousLogin() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      return _context4.abrupt("return", new Promise(function (resolve, reject) {
                        firebase_app__WEBPACK_IMPORTED_MODULE_9__["auth"]().signInAnonymously().then(function (res) {
                          return resolve(res);
                        }, function (err) {
                          return reject(err);
                        });
                      }));

                    case 1:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4);
            }));
          }
        }, {
          key: "emailLogin",
          value: function emailLogin(obj) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var _this2 = this;

              var r, code;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      this.loadingService.load();
                      _context5.prev = 1;
                      _context5.next = 4;
                      return this.afAuth.auth.signInWithEmailAndPassword(obj.email, obj.password);

                    case 4:
                      r = _context5.sent;

                      if (r) {
                        setTimeout(function () {
                          _this2.loadingService.hide();

                          _this2.router.navigate(["/"]);
                        }, 600);
                        console.log("Successfully logged in!", r);
                        localStorage.setItem("userId", r.user.uid);
                      }

                      _context5.next = 13;
                      break;

                    case 8:
                      _context5.prev = 8;
                      _context5.t0 = _context5["catch"](1);
                      code = _context5.t0["code"];
                      this.alert.showErrorMessage(code);
                      console.error("야기아냐?", _context5.t0);

                    case 13:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this, [[1, 8]]);
            }));
          }
        }, {
          key: "loginUser",
          value: function loginUser(value) {
            return new Promise(function (resolve, reject) {
              firebase_app__WEBPACK_IMPORTED_MODULE_9__["auth"]().signInWithEmailAndPassword(value.email, value.password).then(function (res) {
                return resolve(res);
              }, function (err) {
                return reject(err);
              });
            });
          }
        }, {
          key: "exitLogin",
          value: function exitLogin(obj) {
            var _this3 = this;

            return new Promise(function (resolve, reject) {
              try {
                var r = _this3.afAuth.auth.signInWithEmailAndPassword(obj.email, obj.password);

                if (r) {
                  console.log("Successfully logged in!");
                }
              } catch (error) {
                var code = error["code"];

                _this3.alert.showErrorMessage(code);

                resolve(error);
              }
            });
          }
        }, {
          key: "loginWithToken",
          value: function loginWithToken(token) {
            // tslint:disable-next-line:no-shadowed-variable
            return new Promise(function (resolve, reject) {
              firebase_app__WEBPACK_IMPORTED_MODULE_9__["auth"]().signInWithCustomToken(token).then(function (res) {
                return resolve(res);
              }, function (err) {
                return reject(err);
              });
            });
          }
        }, {
          key: "resetPassword",
          value: function resetPassword(email) {
            var _this4 = this;

            this.afAuth.auth.sendPasswordResetEmail(email).then(function () {
              _this4.alert.presentAlert("비밀번호 재설정", '', "비밀번호 재설정 이메일을 발송하였습니다. <br> 가입한 이메일의 메일함을 확인해주세요");
            })["catch"](function (err) {
              console.error(err);

              _this4.alert.showErrorMessage(err.code);
            });
          }
        }, {
          key: "updateUserData",
          value: function updateUserData(uid, obj) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var path, dateCreated, exitSwitch, alarmSwitch, data;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      //사용자 데이터를 firestore에 업로드//
                      path = "users/".concat(uid);
                      dateCreated = new Date().toISOString();
                      exitSwitch = false;
                      alarmSwitch = true;
                      data = Object.assign({
                        uid: uid,
                        dateCreated: dateCreated,
                        exitSwitch: exitSwitch,
                        alarmSwitch: alarmSwitch
                      }, obj);
                      console.log("data", data);
                      _context6.next = 8;
                      return this.db.updateAt(path, data);

                    case 8:
                      _context6.next = 10;
                      return this.loadingService.dismiss();

                    case 10:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }, {
          key: "signOut",
          value: function signOut() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
              return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                  switch (_context7.prev = _context7.next) {
                    case 0:
                      _context7.next = 2;
                      return this.afAuth.auth.signOut();

                    case 2:
                      _context7.next = 4;
                      return this.alert.presentToast("로그아웃하였습니다.");

                    case 4:
                      return _context7.abrupt("return", this.router.navigate(["/"]));

                    case 5:
                    case "end":
                      return _context7.stop();
                  }
                }
              }, _callee7, this);
            }));
          }
        }, {
          key: "registerUser",
          value: function registerUser(email, password) {
            return new Promise(function (resolve, reject) {
              firebase_app__WEBPACK_IMPORTED_MODULE_9__["auth"]().createUserWithEmailAndPassword(email, password).then(function (res) {
                return resolve(res);
              }, function (err) {
                return reject(err);
              });
            });
          }
        }, {
          key: "register",
          value: function register(email, password, obj) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
              var r, code;
              return regeneratorRuntime.wrap(function _callee8$(_context8) {
                while (1) {
                  switch (_context8.prev = _context8.next) {
                    case 0:
                      _context8.prev = 0;
                      _context8.next = 3;
                      return this.afAuth.auth.createUserWithEmailAndPassword(email, password);

                    case 3:
                      r = _context8.sent;

                      if (!r) {
                        _context8.next = 8;
                        break;
                      }

                      _context8.next = 7;
                      return this.updateUserData(r.user.uid, obj);

                    case 7:
                      return _context8.abrupt("return", _context8.sent);

                    case 8:
                      _context8.next = 15;
                      break;

                    case 10:
                      _context8.prev = 10;
                      _context8.t0 = _context8["catch"](0);
                      code = _context8.t0["code"];
                      this.alert.showErrorMessage(code);
                      console.error(_context8.t0);

                    case 15:
                    case "end":
                      return _context8.stop();
                  }
                }
              }, _callee8, this, [[0, 10]]);
            }));
          } //// GOOGLE AUTH

        }, {
          key: "handleRedirect",
          value: function handleRedirect() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
              var loading, result;
              return regeneratorRuntime.wrap(function _callee9$(_context9) {
                while (1) {
                  switch (_context9.prev = _context9.next) {
                    case 0:
                      if (!Boolean(this.isRedirect())) {
                        _context9.next = 2;
                        break;
                      }

                      return _context9.abrupt("return", null);

                    case 2:
                      _context9.next = 4;
                      return this.loadingService.load();

                    case 4:
                      loading = _context9.sent;
                      _context9.next = 7;
                      return this.afAuth.auth.getRedirectResult();

                    case 7:
                      result = _context9.sent;

                      if (!result.user) {
                        _context9.next = 11;
                        break;
                      }

                      _context9.next = 11;
                      return this.updateUserData(result.user);

                    case 11:
                      this.loadingService.hide();
                      _context9.next = 14;
                      return this.setRedirect("false");

                    case 14:
                      return _context9.abrupt("return", result);

                    case 15:
                    case "end":
                      return _context9.stop();
                  }
                }
              }, _callee9, this);
            }));
          }
        }, {
          key: "setRedirect",
          value: function setRedirect(val) {
            localStorage.setItem("authRedirect", val);
          }
        }, {
          key: "isRedirect",
          value: function isRedirect() {
            return localStorage.getItem("authRedirect");
          }
        }, {
          key: "changePassword",
          value: function changePassword(oldPassword, newPassword) {
            var _this5 = this;

            return new Promise(function (resolve, reject) {
              var user = _this5.afAuth.auth.currentUser;
              return _this5.afAuth.auth.signInWithEmailAndPassword(user.email, oldPassword).then(function (sucess) {
                user.updatePassword(newPassword).then(function (sucess) {
                  resolve(true);

                  _this5.alert.presentToast("비밀번호가 성공적으로 변경되었습니다.");
                })["catch"](function (error) {
                  console.log("error", error);
                  var code = error["code"];
                  reject(false);

                  _this5.alert.showErrorMessage(code);
                });
              })["catch"](function (error) {
                reject(false);
                var code = error["code"]; // this.alert.showErrorMessage(code);

                console.log("oldPasswordIncorrect", error);
              });
            });
          }
        }, {
          key: "exitUser",
          value: function exitUser() {
            var _this6 = this;

            return new Promise(function (resolve, reject) {
              var user = _this6.afAuth.auth.currentUser;

              _this6.db.updateAt("users/".concat(user.uid), {
                exitSwitch: true
              }).then(function () {
                user["delete"]().then(function () {
                  localStorage.clear();

                  _this6.alert.presentAlert("회원탈퇴", "회원님의 계정이 탈퇴되었습니다."); // this.router.navigateByUrl('/login');


                  reject(false);
                })["catch"](function (error) {
                  console.log("error", error);
                });
              })["catch"](function (error) {
                console.log("error", error);
              });
            });
          }
        }, {
          key: "sendEmailVerificationUser",
          value: function sendEmailVerificationUser(user) {
            return new Promise(function (resolve, reject) {
              if (firebase_app__WEBPACK_IMPORTED_MODULE_9__["auth"]().currentUser) {
                user.sendEmailVerification().then(function () {
                  console.log("LOG Out");
                  resolve();
                })["catch"](function (error) {
                  reject();
                });
              }
            });
          }
        }, {
          key: "userDetails",
          value: function userDetails() {
            return this.afAuth.auth.currentUser;
          }
        }, {
          key: "logoutUser",
          value: function logoutUser() {
            return new Promise(function (resolve, reject) {
              if (firebase_app__WEBPACK_IMPORTED_MODULE_9__["auth"]().currentUser) {
                firebase_app__WEBPACK_IMPORTED_MODULE_9__["auth"]().signOut().then(function () {
                  console.log("LOG Out");
                  resolve();
                })["catch"](function (error) {
                  reject();
                });
              }
            });
          }
        }]);

        return AuthService;
      }();

      AuthService.ctorParameters = function () {
        return [{
          type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"]
        }, {
          type: _db_service__WEBPACK_IMPORTED_MODULE_6__["DbService"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"]
        }, {
          type: _alert_service__WEBPACK_IMPORTED_MODULE_8__["AlertService"]
        }, {
          type: _loading_service__WEBPACK_IMPORTED_MODULE_10__["LoadingService"]
        }, {
          type: _push_service__WEBPACK_IMPORTED_MODULE_11__["PushService"]
        }];
      };

      AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
      })], AuthService);
      /***/
    },

    /***/
    "./src/services/loading.service.ts":
    /*!*****************************************!*\
      !*** ./src/services/loading.service.ts ***!
      \*****************************************/

    /*! exports provided: LoadingService */

    /***/
    function srcServicesLoadingServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoadingService", function () {
        return LoadingService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var LoadingService = /*#__PURE__*/function () {
        function LoadingService(loader) {
          _classCallCheck(this, LoadingService);

          this.loader = loader;
          this.isLoading = false;
        }

        _createClass(LoadingService, [{
          key: "load",
          value: function load(message) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee10() {
              return regeneratorRuntime.wrap(function _callee10$(_context10) {
                while (1) {
                  switch (_context10.prev = _context10.next) {
                    case 0:
                      _context10.next = 2;
                      return this.loader.create({
                        duration: 5000,
                        translucent: true,
                        cssClass: 'custom-class custom-loading',
                        backdropDismiss: true,
                        message: message
                      });

                    case 2:
                      this.loading = _context10.sent;
                      _context10.next = 5;
                      return this.loading.present();

                    case 5:
                    case "end":
                      return _context10.stop();
                  }
                }
              }, _callee10, this);
            }));
          }
        }, {
          key: "hide",
          value: function hide() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee11() {
              return regeneratorRuntime.wrap(function _callee11$(_context11) {
                while (1) {
                  switch (_context11.prev = _context11.next) {
                    case 0:
                      if (this.loading) {
                        this.loader.dismiss();
                      }

                    case 1:
                    case "end":
                      return _context11.stop();
                  }
                }
              }, _callee11, this);
            }));
          }
        }, {
          key: "present",
          value: function present() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee12() {
              var _this7 = this;

              return regeneratorRuntime.wrap(function _callee12$(_context12) {
                while (1) {
                  switch (_context12.prev = _context12.next) {
                    case 0:
                      this.isLoading = true;
                      _context12.next = 3;
                      return this.loader.create({
                        cssClass: 'custom-loader-class'
                      }).then(function (a) {
                        a.present().then(function () {
                          console.log('presented');

                          if (!_this7.isLoading) {
                            a.dismiss().then(function () {
                              return console.log('abort presenting');
                            });
                          }
                        });
                      });

                    case 3:
                      return _context12.abrupt("return", _context12.sent);

                    case 4:
                    case "end":
                      return _context12.stop();
                  }
                }
              }, _callee12, this);
            }));
          }
        }, {
          key: "dismiss",
          value: function dismiss() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee13() {
              return regeneratorRuntime.wrap(function _callee13$(_context13) {
                while (1) {
                  switch (_context13.prev = _context13.next) {
                    case 0:
                      this.isLoading = false;
                      _context13.next = 3;
                      return this.loader.dismiss().then(function () {
                        return console.log('dismissed');
                      });

                    case 3:
                      return _context13.abrupt("return", _context13.sent);

                    case 4:
                    case "end":
                      return _context13.stop();
                  }
                }
              }, _callee13, this);
            }));
          }
        }]);

        return LoadingService;
      }();

      LoadingService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }];
      };

      LoadingService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], LoadingService);
      /***/
    },

    /***/
    "./src/services/push.service.ts":
    /*!**************************************!*\
      !*** ./src/services/push.service.ts ***!
      \**************************************/

    /*! exports provided: PushService */

    /***/
    function srcServicesPushServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PushService", function () {
        return PushService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var PushService = function PushService() {
        _classCallCheck(this, PushService);
      };

      PushService.ctorParameters = function () {
        return [];
      };

      PushService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], PushService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=default~home-home-module~pages-account-login-login-module~pages-account-signup-signup-module~pages-h~a8bff0a0-es5.js.map