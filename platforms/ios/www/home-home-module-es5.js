(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html":
    /*!***************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
      \***************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppHomeHomePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <div id=\"headerInner\">\n    <!-- <div id=\"headerInner\" *ngIf=\"alarms$ | async\"> -->\n    <ion-toolbar>\n      <ion-buttons slot=\"start\">\n        <ion-button (click)=\"gomypage()\">\n          <img src=\"assets/imgs/user.png\" alt=\"\">\n        </ion-button>\n      </ion-buttons>\n      <ion-title>그림 심리 상담</ion-title>\n      <ion-buttons slot=\"end\" class=\"bellWrap\">\n        <ion-button (click)=\"gonotice()\">\n          <img src=\"assets/imgs/bell.png\" alt=\"\">\n        </ion-button>\n        <ion-badge *ngIf=\"(alarms$ | async)?.length > 0\">z</ion-badge>\n\n      </ion-buttons>\n    </ion-toolbar>\n    <!-- <ion-badge *ngIf=\"(alarms$ | async)?.length > 0\">z</ion-badge> -->\n  </div>\n</ion-header>\n\n<ion-content>\n  <ion-slides pager #slides>\n    <ion-slide>\n      <div class=\"slideWrap\">\n        <div class=\"sW1\">\n          <div>\n            <h2>\n              그림검사란?\n            </h2>\n            <span>\n              가장 기본적인 미술 치료 기법입니다.<br>\n              집, 나무, 사람을 그린 그림을 토대로<br>\n              상담자가 현재 어떤 생각을 가지고 있는지<br>\n              알 수 있습니다.<br>\n              <div class=\"break\"></div>\n              집은 가정, 가족관계를 상징합니다.<br>\n              가정생활과 가족관계에 대한 인지, 감정,<br>\n              태도와 현재 혹은 장래와 과거의 가정에<br>\n              대한 소망에 대해 알 수 있습니다.<br>\n            </span>\n          </div>\n        </div>\n      </div>\n      <ion-button class=\"grbtn\" expand=\"block\" (click)=\"gohowto()\">\n        검사요청\n      </ion-button>\n    </ion-slide>\n\n    <ion-slide>\n      <div class=\"slideWrap\">\n        <div class=\"sW2\">\n          <div>\n            <h2>\n              그림검사란?\n            </h2>\n            <span>\n              가장 기본적인 미술 치료 기법입니다.<br>\n              집, 나무, 사람을 그린 그림을 토대로<br>\n              상담자가 현재 어떤 생각을 가지고 있는지<br>\n              알 수 있습니다.<br>\n              <div class=\"break\"></div>\n              집은 가정, 가족관계를 상징합니다.<br>\n              가정생활과 가족관계에 대한 인지, 감정,<br>\n              태도와 현재 혹은 장래와 과거의 가정에<br>\n              대한 소망에 대해 알 수 있습니다.<br>\n            </span>\n          </div>\n        </div>\n      </div>\n      <ion-button class=\"grbtn\" expand=\"block\" (click)=\"gohowto()\">\n        검사요청\n      </ion-button>\n    </ion-slide>\n\n    <ion-slide>\n      <div class=\"slideWrap\">\n        <div class=\"sW2\">\n          <div>\n            <h2>\n              그림검사란?\n            </h2>\n            <span>\n              가장 기본적인 미술 치료 기법입니다.<br>\n              집, 나무, 사람을 그린 그림을 토대로<br>\n              상담자가 현재 어떤 생각을 가지고 있는지<br>\n              알 수 있습니다.<br>\n              <div class=\"break\"></div>\n              집은 가정, 가족관계를 상징합니다.<br>\n              가정생활과 가족관계에 대한 인지, 감정,<br>\n              태도와 현재 혹은 장래와 과거의 가정에<br>\n              대한 소망에 대해 알 수 있습니다.<br>\n            </span>\n          </div>\n        </div>\n      </div>\n      <ion-button class=\"grbtn\" expand=\"block\" (click)=\"gohowto()\">\n        검사요청\n      </ion-button>\n    </ion-slide>\n\n    <ion-slide>\n      <div class=\"slideWrap\">\n        <div class=\"sW2\">\n          <div>\n            <h2>\n              그림검사란?\n            </h2>\n            <span>\n              가장 기본적인 미술 치료 기법입니다.<br>\n              집, 나무, 사람을 그린 그림을 토대로<br>\n              상담자가 현재 어떤 생각을 가지고 있는지<br>\n              알 수 있습니다.<br>\n              <div class=\"break\"></div>\n              집은 가정, 가족관계를 상징합니다.<br>\n              가정생활과 가족관계에 대한 인지, 감정,<br>\n              태도와 현재 혹은 장래와 과거의 가정에<br>\n              대한 소망에 대해 알 수 있습니다.<br>\n            </span>\n          </div>\n        </div>\n      </div>\n      <ion-button class=\"grbtn\" expand=\"block\" (click)=\"gohowto()\">\n        검사요청\n      </ion-button>\n    </ion-slide>\n  </ion-slides>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/home/home-routing.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/home/home-routing.module.ts ***!
      \*********************************************/

    /*! exports provided: HomePageRoutingModule */

    /***/
    function srcAppHomeHomeRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function () {
        return HomePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./home.page */
      "./src/app/home/home.page.ts");

      var routes = [{
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"]
      }];

      var HomePageRoutingModule = function HomePageRoutingModule() {
        _classCallCheck(this, HomePageRoutingModule);
      };

      HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], HomePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/home/home.module.ts":
    /*!*************************************!*\
      !*** ./src/app/home/home.module.ts ***!
      \*************************************/

    /*! exports provided: HomePageModule */

    /***/
    function srcAppHomeHomeModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomePageModule", function () {
        return HomePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _home_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./home-routing.module */
      "./src/app/home/home-routing.module.ts");
      /* harmony import */


      var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./home.page */
      "./src/app/home/home.page.ts");

      var HomePageModule = function HomePageModule() {
        _classCallCheck(this, HomePageModule);
      };

      HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _home_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomePageRoutingModule"]],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
      })], HomePageModule);
      /***/
    },

    /***/
    "./src/app/home/home.page.scss":
    /*!*************************************!*\
      !*** ./src/app/home/home.page.scss ***!
      \*************************************/

    /*! exports provided: default */

    /***/
    function srcAppHomeHomePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-badge {\n  position: absolute;\n  z-index: 9;\n  right: 23px;\n  min-width: 7px;\n  min-height: 7px;\n  max-width: 7px;\n  max-height: 7px;\n  width: 7px;\n  height: 7px;\n  -webkit-padding-start: 0;\n          padding-inline-start: 0;\n  -webkit-padding-end: 0;\n          padding-inline-end: 0;\n  --background: #f7bd00;\n  top: 8px;\n  --padding-top: 0;\n  --padding-end: 0;\n  --padding-bottom: 0;\n  --padding-start: 0;\n  --color: transparent;\n  border-radius: 50%;\n}\n\n.bellWrap {\n  position: relative;\n}\n\nion-slide {\n  padding: 18px 16px;\n}\n\n.sW1,\n.sW2 {\n  height: 100%;\n  width: 100%;\n  overflow: hidden;\n  border-radius: 6px;\n  background-size: cover !important;\n  background-position: center !important;\n  padding-left: 29px;\n  padding-top: 12.44%;\n}\n\n.sW1 {\n  background: url('homs.png') no-repeat;\n}\n\n.sW2 {\n  background: url('homs2.png') no-repeat;\n}\n\nion-slides {\n  height: 100%;\n}\n\nion-slides ion-button {\n  position: absolute;\n  z-index: 999;\n  z-index: 99999;\n  bottom: 62px;\n  width: calc(100% - 26px - 26px);\n  left: 50%;\n  transform: translateX(-50%);\n  box-shadow: 0 2px 12px rgba(0, 0, 0, 0.3);\n}\n\nion-slides ion-button.ion-activated {\n  opacity: 0.9;\n}\n\n.slideWrap {\n  height: 100%;\n  width: 100%;\n  text-align: start;\n}\n\n.slideWrap h2 {\n  margin: 0;\n  font-size: 1.375rem;\n  font-weight: 600;\n  color: #ffffff;\n  text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n  margin-bottom: 7.77%;\n}\n\n.slideWrap span {\n  display: block;\n  font-size: 0.875rem;\n  font-weight: 600;\n  line-height: 1.563rem;\n  letter-spacing: -0.4px;\n  text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n  color: #fff;\n}\n\n.slideWrap span .break {\n  min-height: 1.563rem;\n}\n\n@media (max-height: 670px) {\n  ion-slides ion-button {\n    bottom: 3.125rem;\n  }\n  ion-slides ion-slide {\n    padding: 1rem;\n  }\n\n  .grbtn {\n    height: 3.125rem !important;\n  }\n}\n\n@media (max-width: 330px) {\n  .slideWrap span {\n    line-height: 1.125rem;\n  }\n  .slideWrap span .break {\n    min-height: 1.125rem;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSx3QkFBQTtVQUFBLHVCQUFBO0VBQ0Esc0JBQUE7VUFBQSxxQkFBQTtFQUNBLHFCQUFBO0VBQ0EsUUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtBQUNGOztBQUNBO0VBQ0Usa0JBQUE7QUFFRjs7QUFBQTtFQUNFLGtCQUFBO0FBR0Y7O0FBREE7O0VBRUUsWUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsaUNBQUE7RUFDQSxzQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUFJRjs7QUFEQTtFQUNFLHFDQUFBO0FBSUY7O0FBREE7RUFDRSxzQ0FBQTtBQUlGOztBQURBO0VBQ0UsWUFBQTtBQUlGOztBQUhFO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSwrQkFBQTtFQUNBLFNBQUE7RUFDQSwyQkFBQTtFQUNBLHlDQUFBO0FBS0o7O0FBRkU7RUFDRSxZQUFBO0FBSUo7O0FBQUE7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0FBR0Y7O0FBRkU7RUFDRSxTQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSx5Q0FBQTtFQUNBLG9CQUFBO0FBSUo7O0FBREU7RUFDRSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0Esc0JBQUE7RUFDQSx5Q0FBQTtFQUNBLFdBQUE7QUFHSjs7QUFGSTtFQUNFLG9CQUFBO0FBSU47O0FBQ0E7RUFFSTtJQUNFLGdCQUFBO0VBQ0o7RUFFRTtJQUNFLGFBQUE7RUFBSjs7RUFJQTtJQUNFLDJCQUFBO0VBREY7QUFDRjs7QUFJQTtFQUVJO0lBRUUscUJBQUE7RUFKSjtFQUtJO0lBQ0Usb0JBQUE7RUFITjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1iYWRnZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogOTtcbiAgcmlnaHQ6IDIzcHg7XG4gIG1pbi13aWR0aDogN3B4O1xuICBtaW4taGVpZ2h0OiA3cHg7XG4gIG1heC13aWR0aDogN3B4O1xuICBtYXgtaGVpZ2h0OiA3cHg7XG4gIHdpZHRoOiA3cHg7XG4gIGhlaWdodDogN3B4O1xuICBwYWRkaW5nLWlubGluZS1zdGFydDogMDtcbiAgcGFkZGluZy1pbmxpbmUtZW5kOiAwO1xuICAtLWJhY2tncm91bmQ6ICNmN2JkMDA7XG4gIHRvcDogOHB4O1xuICAtLXBhZGRpbmctdG9wOiAwO1xuICAtLXBhZGRpbmctZW5kOiAwO1xuICAtLXBhZGRpbmctYm90dG9tOiAwO1xuICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gIC0tY29sb3I6IHRyYW5zcGFyZW50O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG59XG4uYmVsbFdyYXAge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5pb24tc2xpZGUge1xuICBwYWRkaW5nOiAxOHB4IDE2cHg7XG59XG4uc1cxLFxuLnNXMiB7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlciAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgIWltcG9ydGFudDtcbiAgcGFkZGluZy1sZWZ0OiAyOXB4O1xuICBwYWRkaW5nLXRvcDogMTIuNDQlO1xufVxuXG4uc1cxIHtcbiAgYmFja2dyb3VuZDogdXJsKC4uLy4uL2Fzc2V0cy9pbWdzL2hvbXMucG5nKSBuby1yZXBlYXQ7XG59XG5cbi5zVzIge1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vYXNzZXRzL2ltZ3MvaG9tczIucG5nKSBuby1yZXBlYXQ7XG59XG5cbmlvbi1zbGlkZXMge1xuICBoZWlnaHQ6IDEwMCU7XG4gIGlvbi1idXR0b24ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB6LWluZGV4OiA5OTk7XG4gICAgei1pbmRleDogOTk5OTk7XG4gICAgYm90dG9tOiA2MnB4O1xuICAgIHdpZHRoOiBjYWxjKDEwMCUgLSAyNnB4IC0gMjZweCk7XG4gICAgbGVmdDogNTAlO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKTtcbiAgICBib3gtc2hhZG93OiAwIDJweCAxMnB4IHJnYmEoMCwgMCwgMCwgMC4zKTtcbiAgfVxuXG4gIGlvbi1idXR0b24uaW9uLWFjdGl2YXRlZCB7XG4gICAgb3BhY2l0eTogMC45O1xuICB9XG59XG5cbi5zbGlkZVdyYXAge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICB0ZXh0LWFsaWduOiBzdGFydDtcbiAgaDIge1xuICAgIG1hcmdpbjogMDtcbiAgICBmb250LXNpemU6IDEuMzc1cmVtO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgY29sb3I6ICNmZmZmZmY7XG4gICAgdGV4dC1zaGFkb3c6IDAgMnB4IDRweCByZ2JhKDAsIDAsIDAsIDAuNSk7XG4gICAgbWFyZ2luLWJvdHRvbTogNy43NyU7XG4gIH1cblxuICBzcGFuIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgbGluZS1oZWlnaHQ6IDEuNTYzcmVtO1xuICAgIGxldHRlci1zcGFjaW5nOiAtMC40cHg7XG4gICAgdGV4dC1zaGFkb3c6IDAgMnB4IDRweCByZ2JhKDAsIDAsIDAsIDAuNSk7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgLmJyZWFrIHtcbiAgICAgIG1pbi1oZWlnaHQ6IDEuNTYzcmVtO1xuICAgIH1cbiAgfVxufVxuXG5AbWVkaWEgKG1heC1oZWlnaHQ6IDY3MHB4KSB7XG4gIGlvbi1zbGlkZXMge1xuICAgIGlvbi1idXR0b24ge1xuICAgICAgYm90dG9tOiAzLjEyNXJlbTtcbiAgICB9XG5cbiAgICBpb24tc2xpZGUge1xuICAgICAgcGFkZGluZzogMXJlbTtcbiAgICB9XG4gIH1cblxuICAuZ3JidG4ge1xuICAgIGhlaWdodDogMy4xMjVyZW0gIWltcG9ydGFudDtcbiAgfVxufVxuXG5AbWVkaWEgKG1heC13aWR0aDogMzMwcHgpIHtcbiAgLnNsaWRlV3JhcCB7XG4gICAgc3BhbiB7XG4gICAgICAvLyBmb250LXNpemU6IDAuNzVyZW07XG4gICAgICBsaW5lLWhlaWdodDogMS4xMjVyZW07XG4gICAgICAuYnJlYWsge1xuICAgICAgICBtaW4taGVpZ2h0OiAxLjEyNXJlbTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/home/home.page.ts":
    /*!***********************************!*\
      !*** ./src/app/home/home.page.ts ***!
      \***********************************/

    /*! exports provided: HomePage */

    /***/
    function srcAppHomeHomePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomePage", function () {
        return HomePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var src_services_db_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! src/services/db.service */
      "./src/services/db.service.ts");
      /* harmony import */


      var src_services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! src/services/auth.service */
      "./src/services/auth.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");

      var HomePage = /*#__PURE__*/function () {
        function HomePage(alertController, navc, auth, db) {
          _classCallCheck(this, HomePage);

          this.alertController = alertController;
          this.navc = navc;
          this.auth = auth;
          this.db = db;
          this.loginSwitch = localStorage.getItem('loginSwitch');
          this.checkAlarm();
          this.obser(); // this.getFCM();
        }

        _createClass(HomePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "obser",
          value: function obser() {
            var _this = this;

            var click$ = Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["fromEvent"])(document.getElementById('tab_home'), 'click'); // observable

            var observer = function observer(event) {
              _this.slider.slideTo(0, 500);
            };

            click$.subscribe(observer);
          }
        }, {
          key: "login",
          value: function login() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this2 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.alertController.create({
                        cssClass: 'my-custom-class',
                        header: '로그인',
                        message: '로그인 후 이용 바랍니다.',
                        buttons: [{
                          text: '취소',
                          role: 'cancel',
                          cssClass: 'secondary'
                        }, {
                          text: '로그인',
                          handler: function handler() {
                            console.log('Confirm Okay');

                            _this2.navc.navigateForward(['/login']);
                          }
                        }]
                      });

                    case 2:
                      alert = _context.sent;
                      _context.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "gohowto",
          value: function gohowto() {
            var _this3 = this;

            this.auth.user$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1)).subscribe(function (user) {
              if (user && user.nickname) {
                _this3.navc.navigateForward(['/home-add-howto']);
              } else {
                _this3.login();
              }
            });
          }
        }, {
          key: "gonotice",
          value: function gonotice() {
            var _this4 = this;

            this.auth.user$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1)).subscribe(function (user) {
              if (user && user.nickname) {
                _this4.navc.navigateForward(['home-add-notice']);
              } else {
                _this4.login();
              }
            });
          }
        }, {
          key: "gomypage",
          value: function gomypage() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this5 = this;

              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      this.auth.user$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1)).subscribe(function (user) {
                        if (user && user.nickname) {
                          _this5.navc.navigateForward(['home-add-mypage']);
                        } else {
                          _this5.login();
                        }
                      });

                    case 1:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "checkAlarm",
          value: function checkAlarm() {
            var pushId;
            var uid = localStorage.getItem('userId');
            this.db.doc$("users/".concat(uid)).subscribe(function (a) {
              return pushId = a.pushId;
            });
            this.alarms$ = this.db.collection$("alarm", function (ref) {
              return ref.where('userId', '==', uid).where('check', '==', false).orderBy('dateCreated', 'desc');
            });
          }
        }]);

        return HomePage;
      }();

      HomePage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
        }, {
          type: src_services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"]
        }, {
          type: src_services_db_service__WEBPACK_IMPORTED_MODULE_1__["DbService"]
        }];
      };

      HomePage.propDecorators = {
        slider: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"],
          args: ['slides', {
            "static": false
          }]
        }]
      };
      HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./home.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./home.page.scss */
        "./src/app/home/home.page.scss"))["default"]]
      })], HomePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=home-home-module-es5.js.map