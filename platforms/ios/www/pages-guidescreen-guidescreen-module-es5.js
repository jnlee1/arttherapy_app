(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-guidescreen-guidescreen-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/guidescreen/guidescreen.page.html":
    /*!***********************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/guidescreen/guidescreen.page.html ***!
      \***********************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesGuidescreenGuidescreenPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <div class=\"allCon\">\n    <ion-header>\n      <div id=\"headerInner\">\n        <ion-title>\n          arTree\n        </ion-title>\n      </div>\n    </ion-header>\n\n    <ion-slides pager>\n      <ion-slide>\n        <div class=\"sW\">\n\n          <div class=\"textBox\">\n            <span>\n              You can find out yourself easily through the arTree.<br>\n              나의 마음을 쉽게 알아볼 수 있습니다.\n            </span>\n          </div>\n\n\n        </div>\n      </ion-slide>\n\n      <ion-slide>\n        <div class=\"sW\">\n          <div class=\"textBox textBox2\">\n            <span class=\"txtsub\">\n              arTree는 AI기반 그림 심리 검사를<br> 기반으로 합니다.<br>\n              어린이, 직장인, 노인<br> 누구나 쉽게 심리 검사가 가능합니다.<br>\n              내가 그린 그림으로 <br>심리검사가 가능합니다.<br>\n              원하는 시간과 장소에서 <br>검사하여 종합적인 심리 상태를<br> 검사 가능합니다.\n            </span>\n\n            <!-- <span>\n            지금 상담 받아보세요.\n          </span> -->\n          </div>\n        </div>\n      </ion-slide>\n\n      <ion-slide>\n        <div class=\"slideWrap\">\n          <div class=\"sW\">\n            <div class=\"textBox\">\n              <span>\n                그림심리상담을 통해<br>\n                현재 나의 속마음을<br>\n                알아보세요.\n              </span>\n\n            </div>\n          </div>\n          <ion-button expand=\"block\" (click)=\"gohome()\">\n            시작하기\n          </ion-button>\n        </div>\n      </ion-slide>\n    </ion-slides>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/guidescreen/guidescreen-routing.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/guidescreen/guidescreen-routing.module.ts ***!
      \*****************************************************************/

    /*! exports provided: GuidescreenPageRoutingModule */

    /***/
    function srcAppPagesGuidescreenGuidescreenRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GuidescreenPageRoutingModule", function () {
        return GuidescreenPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _guidescreen_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./guidescreen.page */
      "./src/app/pages/guidescreen/guidescreen.page.ts");

      var routes = [{
        path: '',
        component: _guidescreen_page__WEBPACK_IMPORTED_MODULE_3__["GuidescreenPage"]
      }];

      var GuidescreenPageRoutingModule = function GuidescreenPageRoutingModule() {
        _classCallCheck(this, GuidescreenPageRoutingModule);
      };

      GuidescreenPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], GuidescreenPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/guidescreen/guidescreen.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/pages/guidescreen/guidescreen.module.ts ***!
      \*********************************************************/

    /*! exports provided: GuidescreenPageModule */

    /***/
    function srcAppPagesGuidescreenGuidescreenModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GuidescreenPageModule", function () {
        return GuidescreenPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _guidescreen_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./guidescreen-routing.module */
      "./src/app/pages/guidescreen/guidescreen-routing.module.ts");
      /* harmony import */


      var _guidescreen_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./guidescreen.page */
      "./src/app/pages/guidescreen/guidescreen.page.ts");

      var GuidescreenPageModule = function GuidescreenPageModule() {
        _classCallCheck(this, GuidescreenPageModule);
      };

      GuidescreenPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _guidescreen_routing_module__WEBPACK_IMPORTED_MODULE_5__["GuidescreenPageRoutingModule"]],
        declarations: [_guidescreen_page__WEBPACK_IMPORTED_MODULE_6__["GuidescreenPage"]]
      })], GuidescreenPageModule);
      /***/
    },

    /***/
    "./src/app/pages/guidescreen/guidescreen.page.scss":
    /*!*********************************************************!*\
      !*** ./src/app/pages/guidescreen/guidescreen.page.scss ***!
      \*********************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesGuidescreenGuidescreenPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-content {\n  --background: transparent;\n  background: transparent;\n}\n\nion-content::part(background) {\n  background: transparent;\n}\n\n.allCon {\n  background: #1998e6;\n  /* Old browsers */\n  /* FF3.6-15 */\n  /* Chrome10-25,Safari5.1-6 */\n  background: linear-gradient(135deg, #1998e6 0%, #2b5bc3 100%);\n  background: url('intro1.png') no-repeat center/cover;\n  background-size: cover;\n  background-position: center;\n  width: 100%;\n  min-width: 100%;\n  max-width: 100%;\n  height: 100%;\n  min-height: 100%;\n  max-height: 100%;\n  /* Permalink - use to edit and share this gradient: https://colorzilla.com/gradient-editor/#1998e6+0,2b5bc3+100 */\n}\n\nion-header {\n  position: absolute;\n  /* ... */\n  /* Status bar height on iOS 10 */\n  padding-top: 20px;\n  /* Status bar height on iOS 11.0 */\n  padding-top: constant(safe-area-inset-top);\n  /* Status bar height on iOS 11+ */\n  padding-top: env(safe-area-inset-top);\n}\n\nion-header #headerInner {\n  background: transparent;\n  height: 56px;\n}\n\nion-header ion-title {\n  color: #fff;\n}\n\nion-slide ion-button {\n  position: absolute;\n  z-index: 999;\n  z-index: 99999;\n  bottom: 89px;\n  width: calc(100% - 16px - 16px);\n  left: 50%;\n  transform: translateX(-50%);\n  height: 56px;\n  margin: 0;\n  --border-radius: 4px;\n  --background: #fff;\n  --background-activated: #fff;\n  --background-focused: #fff;\n  --background-hover: #fff;\n  --color: #2666ca;\n  font-size: 0.9375rem;\n  font-weight: 600;\n  box-shadow: 0 2px 12px rgba(0, 0, 0, 0.3);\n}\n\nion-slide ion-button.ion-activated {\n  opacity: 0.8;\n}\n\nion-slide .sW {\n  height: 100%;\n  width: 100%;\n}\n\nion-slides {\n  height: 100%;\n}\n\n.slideWrap {\n  height: 100%;\n  width: 100%;\n}\n\n.textBox {\n  padding: 0 36px;\n  padding-top: 63%;\n}\n\n.textBox span {\n  font-size: 1.125rem;\n  font-weight: 600;\n  color: #fff;\n  line-height: 30px;\n  letter-spacing: -0.09px;\n  display: block;\n  text-align: start;\n  text-shadow: 0 2px 4px rgba(0, 0, 0, 0.5);\n  word-break: break-word;\n}\n\n.txtsub {\n  margin-bottom: 31px;\n}\n\n@media (max-width: 330px) {\n  .textBox {\n    padding-left: 20px;\n  }\n  .textBox span {\n    font-size: 19px;\n  }\n\n  .textBox2 {\n    padding-top: 33%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZ3VpZGVzY3JlZW4vZ3VpZGVzY3JlZW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBUUUseUJBQUE7RUFDQSx1QkFBQTtBQU5GOztBQVVBO0VBQ0UsdUJBQUE7QUFQRjs7QUFXQTtFQUNFLG1CQUFBO0VBQXFCLGlCQUFBO0VBQ2dELGFBQUE7RUFDRSw0QkFBQTtFQUN2RSw2REFBQTtFQUNBLG9EQUFBO0VBQ0Esc0JBQUE7RUFDQSwyQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBRUEsaUhBQUE7QUFORjs7QUFRQTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLGdDQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQ0FBQTtFQUNBLDBDQUFBO0VBQ0EsaUNBQUE7RUFDQSxxQ0FBQTtBQUxGOztBQU9FO0VBQ0UsdUJBQUE7RUFDQSxZQUFBO0FBTEo7O0FBUUU7RUFDRSxXQUFBO0FBTko7O0FBV0U7RUFDRSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLCtCQUFBO0VBQ0EsU0FBQTtFQUNBLDJCQUFBO0VBQ0EsWUFBQTtFQUNBLFNBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNEJBQUE7RUFDQSwwQkFBQTtFQUNBLHdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0VBQ0EseUNBQUE7QUFSSjs7QUFXRTtFQUNFLFlBQUE7QUFUSjs7QUFXRTtFQUNFLFlBQUE7RUFDQSxXQUFBO0FBVEo7O0FBYUE7RUFDRSxZQUFBO0FBVkY7O0FBbUJBO0VBQ0UsWUFBQTtFQUNBLFdBQUE7QUFoQkY7O0FBbUJBO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0FBaEJGOztBQWlCRTtFQUNFLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSx1QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLHlDQUFBO0VBQ0Esc0JBQUE7QUFmSjs7QUFrQkE7RUFDRSxtQkFBQTtBQWZGOztBQWtCQTtFQUNFO0lBQ0Usa0JBQUE7RUFmRjtFQWdCRTtJQUNFLGVBQUE7RUFkSjs7RUFrQkE7SUFDRSxnQkFBQTtFQWZGO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ndWlkZXNjcmVlbi9ndWlkZXNjcmVlbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY29udGVudCB7XG4gIC8vIC0tYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWdzL2ludHJvMS5wbmcpIG5vLXJlcGVhdDtcblxuICAvLyAtLWJhY2tncm91bmQ6IGNlbnRlciAvIGNvdmVyIG5vLXJlcGVhdCB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZ3MvaW50cm8xLnBuZyk7XG5cbiAgLy8gYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgLy8gYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuXG4gIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuXG5cbmlvbi1jb250ZW50OjpwYXJ0KGJhY2tncm91bmQpe1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cblxuXG4uYWxsQ29uIHtcbiAgYmFja2dyb3VuZDogIzE5OThlNjsgLyogT2xkIGJyb3dzZXJzICovXG4gIGJhY2tncm91bmQ6IC1tb3otbGluZWFyLWdyYWRpZW50KC00NWRlZywgICMxOTk4ZTYgMCUsICMyYjViYzMgMTAwJSk7IC8qIEZGMy42LTE1ICovXG4gIGJhY2tncm91bmQ6IC13ZWJraXQtbGluZWFyLWdyYWRpZW50KC00NWRlZywgICMxOTk4ZTYgMCUsIzJiNWJjMyAxMDAlKTsgLyogQ2hyb21lMTAtMjUsU2FmYXJpNS4xLTYgKi9cbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDEzNWRlZywgICMxOTk4ZTYgMCUsIzJiNWJjMyAxMDAlKTtcbiAgYmFja2dyb3VuZDp1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZ3MvaW50cm8xLnBuZykgbm8tcmVwZWF0IGNlbnRlciAvIGNvdmVyIDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuICB3aWR0aDogMTAwJTtcbiAgbWluLXdpZHRoOiAxMDAlO1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgbWluLWhlaWdodDogMTAwJTtcbiAgbWF4LWhlaWdodDogMTAwJTtcblxuICAvKiBQZXJtYWxpbmsgLSB1c2UgdG8gZWRpdCBhbmQgc2hhcmUgdGhpcyBncmFkaWVudDogaHR0cHM6Ly9jb2xvcnppbGxhLmNvbS9ncmFkaWVudC1lZGl0b3IvIzE5OThlNiswLDJiNWJjMysxMDAgKi9cbn1cbmlvbi1oZWFkZXIge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIC8qIC4uLiAqL1xuICAvKiBTdGF0dXMgYmFyIGhlaWdodCBvbiBpT1MgMTAgKi9cbiAgcGFkZGluZy10b3A6IDIwcHg7XG4gIC8qIFN0YXR1cyBiYXIgaGVpZ2h0IG9uIGlPUyAxMS4wICovXG4gIHBhZGRpbmctdG9wOiBjb25zdGFudChzYWZlLWFyZWEtaW5zZXQtdG9wKTtcbiAgLyogU3RhdHVzIGJhciBoZWlnaHQgb24gaU9TIDExKyAqL1xuICBwYWRkaW5nLXRvcDogZW52KHNhZmUtYXJlYS1pbnNldC10b3ApO1xuXG4gICNoZWFkZXJJbm5lciB7XG4gICAgYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XG4gICAgaGVpZ2h0OiA1NnB4O1xuICB9XG5cbiAgaW9uLXRpdGxlIHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgfVxufVxuXG5pb24tc2xpZGUge1xuICBpb24tYnV0dG9uIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgei1pbmRleDogOTk5O1xuICAgIHotaW5kZXg6IDk5OTk5O1xuICAgIGJvdHRvbTogODlweDtcbiAgICB3aWR0aDogY2FsYygxMDAlIC0gMTZweCAtIDE2cHgpO1xuICAgIGxlZnQ6IDUwJTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSk7XG4gICAgaGVpZ2h0OiA1NnB4O1xuICAgIG1hcmdpbjogMDtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDRweDtcbiAgICAtLWJhY2tncm91bmQ6ICNmZmY7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogI2ZmZjtcbiAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogI2ZmZjtcbiAgICAtLWJhY2tncm91bmQtaG92ZXI6ICNmZmY7XG4gICAgLS1jb2xvcjogIzI2NjZjYTtcbiAgICBmb250LXNpemU6IDAuOTM3NXJlbTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGJveC1zaGFkb3c6IDAgMnB4IDEycHggcmdiYSgwLCAwLCAwLCAwLjMpO1xuICB9XG5cbiAgaW9uLWJ1dHRvbi5pb24tYWN0aXZhdGVkIHtcbiAgICBvcGFjaXR5OiAwLjg7XG4gIH1cbiAgLnNXIHtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbn1cblxuaW9uLXNsaWRlcyB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLy8gaW9uLXNsaWRlIHtcbi8vICAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDE2NGRlZywgcmdiYSg0MSwgMTcxLCAyMjYsIDEpIDAlLCByZ2JhKDQyLCA4NSwgMTkxLCAxKSAxMDAlKTtcbi8vICAgYmFja2dyb3VuZDogdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWdzL2ludHJvMS5wbmcpIG5vLXJlcGVhdDtcbi8vICAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbi8vICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xuLy8gfVxuLnNsaWRlV3JhcCB7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi50ZXh0Qm94IHtcbiAgcGFkZGluZzogMCAzNnB4O1xuICBwYWRkaW5nLXRvcDogNjMlO1xuICBzcGFuIHtcbiAgICBmb250LXNpemU6IDEuMTI1cmVtO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgbGluZS1oZWlnaHQ6IDMwcHg7XG4gICAgbGV0dGVyLXNwYWNpbmc6IC0wLjA5cHg7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgdGV4dC1hbGlnbjogc3RhcnQ7XG4gICAgdGV4dC1zaGFkb3c6IDAgMnB4IDRweCByZ2JhKDAsIDAsIDAsIDAuNSk7XG4gICAgd29yZC1icmVhazogYnJlYWstd29yZDtcbiAgfVxufVxuLnR4dHN1YiB7XG4gIG1hcmdpbi1ib3R0b206IDMxcHg7XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOiAzMzBweCkge1xuICAudGV4dEJveCB7XG4gICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xuICAgIHNwYW4ge1xuICAgICAgZm9udC1zaXplOiAxOXB4O1xuICAgIH1cbiAgfVxuXG4gIC50ZXh0Qm94MiB7XG4gICAgcGFkZGluZy10b3A6IDMzJTtcbiAgfVxufVxuIl19 */";
      /***/
    },

    /***/
    "./src/app/pages/guidescreen/guidescreen.page.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/guidescreen/guidescreen.page.ts ***!
      \*******************************************************/

    /*! exports provided: GuidescreenPage */

    /***/
    function srcAppPagesGuidescreenGuidescreenPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "GuidescreenPage", function () {
        return GuidescreenPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var GuidescreenPage = /*#__PURE__*/function () {
        function GuidescreenPage(navc) {
          _classCallCheck(this, GuidescreenPage);

          this.navc = navc;

          if (localStorage.loginSwitch == 'true') {
            this.navc.navigateForward(['/tabs/home']);
          }
        }

        _createClass(GuidescreenPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "gohome",
          value: function gohome() {
            this.navc.navigateForward(['/tabs/home']);
          }
        }]);

        return GuidescreenPage;
      }();

      GuidescreenPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }];
      };

      GuidescreenPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-guidescreen',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./guidescreen.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/guidescreen/guidescreen.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./guidescreen.page.scss */
        "./src/app/pages/guidescreen/guidescreen.page.scss"))["default"]]
      })], GuidescreenPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-guidescreen-guidescreen-module-es5.js.map