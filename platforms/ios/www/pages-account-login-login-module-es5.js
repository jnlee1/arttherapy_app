(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-account-login-login-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/login/login.page.html":
    /*!*******************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/login/login.page.html ***!
      \*******************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesAccountLoginLoginPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <div id=\"headerInner\">\n    <ion-toolbar>\n      <ion-buttons slot=\"start\" class=\"back\">\n        <ion-back-button text=\"\" icon=\"\"></ion-back-button>\n      </ion-buttons>\n      <ion-title>로그인</ion-title>\n    </ion-toolbar>\n  </div>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <div>\n      <ion-input placeholder=\"이메일을 입력해주세요.\" type=\"email\" [(ngModel)]=\"account.email\"></ion-input>\n      <ion-input placeholder=\"비밀번호를 입력해주세요.\" type=\"password\" [(ngModel)]=\"account.password\"></ion-input>\n\n      <ion-button expand=\"block\" class=\"grbtn\" [disabled]=!account.email||!account.password (click)=\"login()\">\n        로그인\n      </ion-button>\n\n      <div class=\"cbtn\">\n        <ion-button fill=\"clear\" (click)=\"resetpassword()\">\n          비밀번호 찾기\n        </ion-button>\n\n        <ion-button fill=\"clear\" (click)=\"gosignup()\">\n          회원가입\n        </ion-button>\n      </div>\n    </div>\n  </ion-list>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/account/login/login-routing.module.ts":
    /*!*************************************************************!*\
      !*** ./src/app/pages/account/login/login-routing.module.ts ***!
      \*************************************************************/

    /*! exports provided: LoginPageRoutingModule */

    /***/
    function srcAppPagesAccountLoginLoginRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageRoutingModule", function () {
        return LoginPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/pages/account/login/login.page.ts");

      var routes = [{
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_3__["LoginPage"]
      }];

      var LoginPageRoutingModule = function LoginPageRoutingModule() {
        _classCallCheck(this, LoginPageRoutingModule);
      };

      LoginPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], LoginPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/account/login/login.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/pages/account/login/login.module.ts ***!
      \*****************************************************/

    /*! exports provided: LoginPageModule */

    /***/
    function srcAppPagesAccountLoginLoginModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPageModule", function () {
        return LoginPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _login_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./login-routing.module */
      "./src/app/pages/account/login/login-routing.module.ts");
      /* harmony import */


      var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./login.page */
      "./src/app/pages/account/login/login.page.ts");

      var LoginPageModule = function LoginPageModule() {
        _classCallCheck(this, LoginPageModule);
      };

      LoginPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _login_routing_module__WEBPACK_IMPORTED_MODULE_5__["LoginPageRoutingModule"]],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
      })], LoginPageModule);
      /***/
    },

    /***/
    "./src/app/pages/account/login/login.page.scss":
    /*!*****************************************************!*\
      !*** ./src/app/pages/account/login/login.page.scss ***!
      \*****************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesAccountLoginLoginPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-list {\n  height: 100%;\n  display: flex;\n  align-items: center;\n  overflow: hidden;\n}\nion-list div {\n  width: 100%;\n  padding: 0 1.5rem;\n}\nion-input:first-of-type {\n  margin-bottom: 1.125rem;\n}\nion-input:last-of-type {\n  margin-bottom: 3.625rem;\n}\n.grbtn {\n  margin-bottom: 1rem;\n}\n.cbtn {\n  padding: 0 !important;\n  display: flex;\n  align-items: center;\n  justify-content: flex-end;\n}\n.cbtn .button-clear {\n  --border-radius: 0;\n  --padding-bottom: 0;\n  --padding-start: 0;\n  --padding-end: 0;\n  margin: 0;\n  height: 1.25rem;\n  font-size: 0.8125rem;\n  font-weight: 600;\n  color: #333333;\n}\n.cbtn .button-clear:first-of-type {\n  padding-right: 1rem;\n  position: relative;\n}\n.cbtn .button-clear:last-of-type {\n  padding-left: 1rem;\n}\n.cbtn .button-clear:first-of-type::after {\n  display: block;\n  content: \"\";\n  width: 100%;\n  height: 1.125rem;\n  border-right: 1px solid #d7d7d7;\n  position: absolute;\n  top: 50%;\n  right: 0;\n  transform: translateY(-50%);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWNjb3VudC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUFDRjtBQUFFO0VBQ0UsV0FBQTtFQUNBLGlCQUFBO0FBRUo7QUFFQTtFQUNFLHVCQUFBO0FBQ0Y7QUFFQTtFQUNFLHVCQUFBO0FBQ0Y7QUFFQTtFQUNFLG1CQUFBO0FBQ0Y7QUFFQTtFQUNFLHFCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7QUFDRjtBQUNFO0VBQ0Usa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FBQ0o7QUFFRTtFQUNFLG1CQUFBO0VBQ0Esa0JBQUE7QUFBSjtBQUdFO0VBQ0Usa0JBQUE7QUFESjtBQUlFO0VBQ0UsY0FBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSwrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFFBQUE7RUFDQSwyQkFBQTtBQUZKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvYWNjb3VudC9sb2dpbi9sb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tbGlzdCB7XG4gIGhlaWdodDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgZGl2IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwYWRkaW5nOiAwIDEuNXJlbTtcbiAgfVxufVxuXG5pb24taW5wdXQ6Zmlyc3Qtb2YtdHlwZSB7XG4gIG1hcmdpbi1ib3R0b206IDEuMTI1cmVtO1xufVxuXG5pb24taW5wdXQ6bGFzdC1vZi10eXBlIHtcbiAgbWFyZ2luLWJvdHRvbTogMy42MjVyZW07XG59XG5cbi5ncmJ0biB7XG4gIG1hcmdpbi1ib3R0b206IDFyZW07XG59XG5cbi5jYnRuIHtcbiAgcGFkZGluZzogMCAhaW1wb3J0YW50O1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuXG4gIC5idXR0b24tY2xlYXIge1xuICAgIC0tYm9yZGVyLXJhZGl1czogMDtcbiAgICAtLXBhZGRpbmctYm90dG9tOiAwO1xuICAgIC0tcGFkZGluZy1zdGFydDogMDtcbiAgICAtLXBhZGRpbmctZW5kOiAwO1xuICAgIG1hcmdpbjogMDtcbiAgICBoZWlnaHQ6IDEuMjVyZW07XG4gICAgZm9udC1zaXplOiAwLjgxMjVyZW07XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBjb2xvcjogIzMzMzMzMztcbiAgfVxuXG4gIC5idXR0b24tY2xlYXI6Zmlyc3Qtb2YtdHlwZSB7XG4gICAgcGFkZGluZy1yaWdodDogMXJlbTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIH1cblxuICAuYnV0dG9uLWNsZWFyOmxhc3Qtb2YtdHlwZSB7XG4gICAgcGFkZGluZy1sZWZ0OiAxcmVtO1xuICB9XG5cbiAgLmJ1dHRvbi1jbGVhcjpmaXJzdC1vZi10eXBlOjphZnRlciB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgY29udGVudDogXCJcIjtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEuMTI1cmVtO1xuICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNkN2Q3ZDc7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogNTAlO1xuICAgIHJpZ2h0OiAwO1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWSgtNTAlKTtcbiAgfVxufVxuIl19 */";
      /***/
    },

    /***/
    "./src/app/pages/account/login/login.page.ts":
    /*!***************************************************!*\
      !*** ./src/app/pages/account/login/login.page.ts ***!
      \***************************************************/

    /*! exports provided: LoginPage */

    /***/
    function srcAppPagesAccountLoginLoginPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoginPage", function () {
        return LoginPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var src_services_fcm_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! src/services/fcm.service */
      "./src/services/fcm.service.ts");
      /* harmony import */


      var _services_alert_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./../../../../services/alert.service */
      "./src/services/alert.service.ts");
      /* harmony import */


      var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/fire/auth */
      "./node_modules/@angular/fire/__ivy_ngcc__/auth/es2015/index.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var src_services_common_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/services/common.service */
      "./src/services/common.service.ts");
      /* harmony import */


      var src_services_db_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! src/services/db.service */
      "./src/services/db.service.ts");
      /* harmony import */


      var src_services_auth_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! src/services/auth.service */
      "./src/services/auth.service.ts");
      /* harmony import */


      var src_services_loading_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! src/services/loading.service */
      "./src/services/loading.service.ts");

      var LoginPage = /*#__PURE__*/function () {
        function LoginPage(alertController, navc, toastController, common, db, auth, load, afAuth, alert, fcm) {
          _classCallCheck(this, LoginPage);

          this.alertController = alertController;
          this.navc = navc;
          this.toastController = toastController;
          this.common = common;
          this.db = db;
          this.auth = auth;
          this.load = load;
          this.afAuth = afAuth;
          this.alert = alert;
          this.fcm = fcm;
          this.account = {
            email: '',
            password: ''
          }; // this.auth.logoutUser().then(() => {
          //   localStorage.clear();
          // });

          this.account.email = '';
          this.account.password = '';
        }

        _createClass(LoginPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "login",
          value: function login() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var r, code;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.prev = 0;
                      _context.next = 3;
                      return this.afAuth.auth.signInWithEmailAndPassword(this.account.email, this.account.password);

                    case 3:
                      r = _context.sent;
                      localStorage.setItem('userId', r.user.uid);
                      localStorage.setItem('loginSwitch', 'true');
                      this.navc.navigateRoot(['/tabs/home']);
                      this.alert.presentToast('로그인 하였습니다.');
                      this.fcm.getToken();
                      _context.next = 16;
                      break;

                    case 11:
                      _context.prev = 11;
                      _context.t0 = _context["catch"](0);
                      code = _context.t0['code'];
                      console.log(_context.t0);
                      this.alert.showErrorMessage(code);

                    case 16:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this, [[0, 11]]);
            }));
          }
        }, {
          key: "alertConfirmPasswordFail",
          value: function alertConfirmPasswordFail() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var alert;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.alertController.create({
                        cssClass: 'failPassword',
                        message: '입력하신 비밀번호가 틀렸습니다.\n 다시 확인하시고 입력하시기 바랍니다.',
                        buttons: [{
                          text: '확인',
                          handler: function handler() {}
                        }]
                      });

                    case 2:
                      alert = _context2.sent;
                      _context2.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "emailExisted",
          value: function emailExisted(email) {
            var _this = this;

            this.db.collection$("users", function (ref) {
              return ref.where('email', '==', email.pwsRe);
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1)).subscribe(function (datas) {
              if (datas.length > 0) {
                _this.auth.resetPassword(email.pwsRe);
              } else {
                _this.alertConfirmEmailNo();
              }
            });
          }
        }, {
          key: "resetpassword",
          value: function resetpassword() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this2 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.alertController.create({
                        cssClass: 'resetpassword',
                        header: '비밀번호 재설정',
                        message: '비밀번호 재설정을 위해 가입하신 이메일 주소를 \n입력하여 주시기 바랍니다.',
                        inputs: [{
                          name: 'pwsRe',
                          type: 'email',
                          placeholder: '이메일을 입력해주세요.'
                        }],
                        buttons: [{
                          text: '취소',
                          role: 'cancel',
                          cssClass: 'secondary'
                        }, {
                          text: '확인',
                          handler: function handler(data) {
                            _this2.emailExisted(data);
                          }
                        }]
                      });

                    case 2:
                      alert = _context3.sent;
                      _context3.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "alertConfirmEmailNo",
          value: function alertConfirmEmailNo() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var alert;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.alertController.create({
                        cssClass: 'noExistEmail',
                        header: '이메일 오류',
                        message: '입력하신 이메일 정보가 없습니다.\n 다시 확인하시기 바랍니다.',
                        buttons: [{
                          text: '확인'
                        }]
                      });

                    case 2:
                      alert = _context4.sent;
                      _context4.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "gosignup",
          value: function gosignup() {
            this.navc.navigateForward(['/signup']);
          }
        }]);

        return LoginPage;
      }();

      LoginPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["ToastController"]
        }, {
          type: src_services_common_service__WEBPACK_IMPORTED_MODULE_7__["CommonService"]
        }, {
          type: src_services_db_service__WEBPACK_IMPORTED_MODULE_8__["DbService"]
        }, {
          type: src_services_auth_service__WEBPACK_IMPORTED_MODULE_9__["AuthService"]
        }, {
          type: src_services_loading_service__WEBPACK_IMPORTED_MODULE_10__["LoadingService"]
        }, {
          type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"]
        }, {
          type: _services_alert_service__WEBPACK_IMPORTED_MODULE_2__["AlertService"]
        }, {
          type: src_services_fcm_service__WEBPACK_IMPORTED_MODULE_1__["FcmService"]
        }];
      };

      LoginPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-login',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./login.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/login/login.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./login.page.scss */
        "./src/app/pages/account/login/login.page.scss"))["default"]]
      })], LoginPage);
      /***/
    },

    /***/
    "./src/services/fcm.service.ts":
    /*!*************************************!*\
      !*** ./src/services/fcm.service.ts ***!
      \*************************************/

    /*! exports provided: FcmService */

    /***/
    function srcServicesFcmServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "FcmService", function () {
        return FcmService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ionic_native_firebase_x_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic-native/firebase-x/ngx */
      "./node_modules/@ionic-native/firebase-x/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _db_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ./db.service */
      "./src/services/db.service.ts");
      /* harmony import */


      var _auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./auth.service */
      "./src/services/auth.service.ts");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js"); // import { Firebase } from '@ionic-native/firebase/ngx';


      var FcmService = /*#__PURE__*/function () {
        function FcmService( // public firebaseNative: Firebase,
        db, auth, httpClient, platform, firebaseX) {
          _classCallCheck(this, FcmService);

          this.db = db;
          this.auth = auth;
          this.httpClient = httpClient;
          this.platform = platform;
          this.firebaseX = firebaseX;
        }
        /**
         * 토큰 값 (pushId) 받는 함수
         */


        _createClass(FcmService, [{
          key: "getToken",
          value: function getToken() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var token, perm;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      if (!this.platform.is('android')) {
                        _context5.next = 5;
                        break;
                      }

                      _context5.next = 3;
                      return this.firebaseX.getToken();

                    case 3:
                      token = _context5.sent;
                      console.log('token', token);

                    case 5:
                      if (!this.platform.is('ios')) {
                        _context5.next = 12;
                        break;
                      }

                      _context5.next = 8;
                      return this.firebaseX.getToken();

                    case 8:
                      token = _context5.sent;
                      _context5.next = 11;
                      return this.firebaseX.grantPermission();

                    case 11:
                      perm = _context5.sent;

                    case 12:
                      if (!this.platform.is('cordova')) {}

                      return _context5.abrupt("return", this.saveTokenToFirestore(token));

                    case 14:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
          /**
           * DB에 pushId 저장
           * @param token token값 저장
           */

        }, {
          key: "saveTokenToFirestore",
          value: function saveTokenToFirestore(token) {
            if (!token) return;
            var myUid = localStorage.getItem("userId"); // users에 저장

            var devicesRef = this.db.updateAt("users/".concat(myUid), {
              pushId: token
            });
          }
        }, {
          key: "listenToNotifications",
          value: function listenToNotifications() {
            return this.firebaseX.onMessageReceived();
          }
          /**
           * FCM 푸쉬 보내기
           *
           * @param pushId push 보낼 사용자의 pushId
           * @param title push 제목
           * @param message push 내용
           */

        }, {
          key: "sendFcm",
          value: function sendFcm(pushId, title, message) {
            var _this3 = this;

            return new Promise(function (resolve, reject) {
              var notification = {
                "notification": {
                  "title": title,
                  "body": message,
                  "click_action": "FCM_PLUGIN_ACTIVITY",
                  "sound": "default"
                },
                "data": {},
                "to": pushId,
                "priority": "high"
              }; // firebase > 프로젝트 설정 > 클라우드 메세징 > 서버키

              var fbkey = 'AAAAORw5uRc:APA91bHdxfqFudv7I_tWw76FDob8Vpw7NC9-hocrAsJjWOTlF_BbvX71XeRos2LQ0ODToTQ-RilxTgJ2w3NsA8iASrnaJ4Vd-QirrNgnvvDn6xW_bjpYDGNj8NUFAkFa0C_6LqwGFiQr';
              var httpOptions = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpHeaders"]({
                  "Content-Type": "application/json; charset=utf-8",
                  "Authorization": "key=".concat(fbkey)
                })
              };

              _this3.httpClient.post('https://fcm.googleapis.com/fcm/send', notification, httpOptions).subscribe(function (new_data) {
                console.log('result', new_data);
                resolve(new_data);
              }, function (error) {
                reject(error);
              });
            });
          }
        }]);

        return FcmService;
      }();

      FcmService.ctorParameters = function () {
        return [{
          type: _db_service__WEBPACK_IMPORTED_MODULE_4__["DbService"]
        }, {
          type: _auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]
        }, {
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"]
        }, {
          type: _ionic_native_firebase_x_ngx__WEBPACK_IMPORTED_MODULE_3__["FirebaseX"]
        }];
      };

      FcmService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], FcmService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-account-login-login-module-es5.js.map