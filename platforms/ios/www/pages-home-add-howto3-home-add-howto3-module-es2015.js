(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-add-howto3-home-add-howto3-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-howto3/home-add-howto3.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-howto3/home-add-howto3.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <div id=\"headerInner\">\n    <ion-toolbar>\n      <ion-buttons class=\"back\" slot=\"start\">\n        <ion-back-button text=\"\" icon=\"\"></ion-back-button>\n      </ion-buttons>\n      <ion-title>검사 실시 방법</ion-title>\n    </ion-toolbar>\n\n  </div>\n</ion-header>\n\n<ion-content>\n  <div class=\"allWrap\">\n    <div class=\"wrap\">\n      <div class=\"slideWrap\">\n        <div class=\"slideAll\">\n          <div class=\"conInner\">\n            <div class=\"step\">\n              <span>\n                3\n              </span>\n            </div>\n\n            <div class=\"stepInner\">\n              <div>\n                <h2>\n                  나무를 그리고 나면 새로운<br> 종이 한 장을 세로로 놓고<br> “사람을 그리세요. <br>단, 사람을 그릴때 막대 인물상이나 <br>만화처럼 그리지말고 <br>사람의 전체를\n                  그리세요.”\n                </h2>\n\n\n\n                <span class=\"m2\">\n                  “ 얼굴만 아니라 전신을 그려보세요.”\n                </span>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"btn_wrap\">\n      <span class=\"intro\">\n        안내에 맞게 그림을 그리셨다면<br>\n        다음버튼을 눌러주세요.\n      </span>\n      <ion-button expand=\"block\" class=\"grbtn\" (click)=\"slidenext()\">\n        다음\n      </ion-button>\n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/home-add-howto3/home-add-howto3-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/home-add-howto3/home-add-howto3-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: HomeAddHowto3PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAddHowto3PageRoutingModule", function() { return HomeAddHowto3PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_add_howto3_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home-add-howto3.page */ "./src/app/pages/home-add-howto3/home-add-howto3.page.ts");




const routes = [
    {
        path: '',
        component: _home_add_howto3_page__WEBPACK_IMPORTED_MODULE_3__["HomeAddHowto3Page"]
    }
];
let HomeAddHowto3PageRoutingModule = class HomeAddHowto3PageRoutingModule {
};
HomeAddHowto3PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomeAddHowto3PageRoutingModule);



/***/ }),

/***/ "./src/app/pages/home-add-howto3/home-add-howto3.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/home-add-howto3/home-add-howto3.module.ts ***!
  \*****************************************************************/
/*! exports provided: HomeAddHowto3PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAddHowto3PageModule", function() { return HomeAddHowto3PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _home_add_howto3_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-add-howto3-routing.module */ "./src/app/pages/home-add-howto3/home-add-howto3-routing.module.ts");
/* harmony import */ var _home_add_howto3_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-add-howto3.page */ "./src/app/pages/home-add-howto3/home-add-howto3.page.ts");







let HomeAddHowto3PageModule = class HomeAddHowto3PageModule {
};
HomeAddHowto3PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _home_add_howto3_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeAddHowto3PageRoutingModule"]
        ],
        declarations: [_home_add_howto3_page__WEBPACK_IMPORTED_MODULE_6__["HomeAddHowto3Page"]]
    })
], HomeAddHowto3PageModule);



/***/ }),

/***/ "./src/app/pages/home-add-howto3/home-add-howto3.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/home-add-howto3/home-add-howto3.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("h2 {\n  white-space: pre-line;\n  margin: 0;\n  font-weight: 600;\n  font-size: 0.9375rem;\n  color: #161616;\n  line-height: 1.4rem;\n  letter-spacing: -0.2px;\n  margin-bottom: 1rem;\n  text-align: center;\n}\n\n.m1 {\n  font-size: 0.875rem;\n  font-weight: 600;\n  line-height: 1.188rem;\n  color: #333;\n  letter-spacing: -0.2px;\n  display: block;\n}\n\n.m2 {\n  font-size: 0.9375rem;\n  font-weight: 600;\n  line-height: 1.188rem;\n  color: #333;\n  letter-spacing: -0.2px;\n  display: block;\n  text-align: center;\n}\n\n.m1,\n.m2 {\n  white-space: pre-line;\n}\n\n.slideAll {\n  height: 100%;\n  width: 100%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\nion-slide {\n  width: 100%;\n}\n\nion-slides {\n  height: 59.86%;\n  width: 100%;\n}\n\n.wrap {\n  height: calc(100% - 138px);\n}\n\n.slideWrap {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  height: 100%;\n  width: 100%;\n}\n\n.conInner {\n  background: #f9f9f9;\n  border-top: 1px solid #eee;\n  border-bottom: 1px solid #eee;\n  height: 59.86%;\n  width: 100%;\n  padding-top: 9.77%;\n  padding-bottom: 23.46%;\n  overflow: hidden;\n}\n\n.step {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  width: 50px;\n  height: 50px;\n  border-radius: 50%;\n  background: linear-gradient(313deg, #2b5ac3 0%, #1b98e7 100%);\n  margin: 0 auto;\n}\n\n.step span {\n  z-index: 1;\n  font-size: 30px;\n  text-transform: uppercase;\n  background: linear-gradient(313deg, #2b5ac3 0%, #1b98e7 100%);\n  -webkit-background-clip: text;\n  -webkit-text-fill-color: transparent;\n}\n\n.stepInner {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  height: 100%;\n}\n\n.step::before {\n  position: absolute;\n  border-radius: 50%;\n  content: \"\";\n  display: block;\n  width: 42px;\n  height: 42px;\n  background: #fff;\n}\n\n@media (max-height: 736px) {\n  .conInner {\n    height: 75%;\n  }\n}\n\n@media (max-height: 570px) {\n  .conInner {\n    height: 80%;\n  }\n\n  .step {\n    width: 50px;\n    height: 50px;\n  }\n\n  .conInner {\n    padding-top: 7%;\n  }\n\n  .step::before {\n    width: 42px;\n    height: 42px;\n  }\n\n  h2 {\n    line-height: 20px;\n    font-size: 16px;\n  }\n}\n\n.intro {\n  font-size: 14px;\n  font-weight: 600;\n  line-height: 19px;\n  color: #545454;\n  display: block;\n  text-align: center;\n  margin-bottom: 24px;\n}\n\n.grbtn {\n  width: calc(100% - 26px - 26px);\n  margin: 0 auto;\n}\n\n.allWrap {\n  height: calc(100vh - 100px);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtaG93dG8zL2hvbWUtYWRkLWhvd3RvMy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxxQkFBQTtFQUNBLFNBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0FBRUY7O0FBQ0E7RUFDRSxvQkFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUFFRjs7QUFDQTs7RUFFRSxxQkFBQTtBQUVGOztBQUNBO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQUVGOztBQUNBO0VBQ0UsV0FBQTtBQUVGOztBQUNBO0VBRUUsY0FBQTtFQUNBLFdBQUE7QUFDRjs7QUFFQTtFQUNFLDBCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBQ0Y7O0FBRUE7RUFDRSxtQkFBQTtFQUNBLDBCQUFBO0VBQ0EsNkJBQUE7RUFDQSxjQUFBO0VBRUEsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxnQkFBQTtBQUFGOztBQUdBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsNkRBQUE7RUFDQSxjQUFBO0FBQUY7O0FBRUU7RUFDRSxVQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0VBQ0EsNkRBQUE7RUFDQSw2QkFBQTtFQUNBLG9DQUFBO0FBQUo7O0FBSUE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7QUFERjs7QUFJQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7QUFERjs7QUFHQTtFQUNFO0lBQ0UsV0FBQTtFQUFGO0FBQ0Y7O0FBR0E7RUFDRTtJQUNFLFdBQUE7RUFERjs7RUFHQTtJQUNFLFdBQUE7SUFDQSxZQUFBO0VBQUY7O0VBR0E7SUFDRSxlQUFBO0VBQUY7O0VBR0E7SUFDRSxXQUFBO0lBQ0EsWUFBQTtFQUFGOztFQUdBO0lBQ0UsaUJBQUE7SUFDQSxlQUFBO0VBQUY7QUFDRjs7QUFPQTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FBTEY7O0FBUUE7RUFDRSwrQkFBQTtFQUNBLGNBQUE7QUFMRjs7QUFRQTtFQUNFLDJCQUFBO0FBTEYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9ob21lLWFkZC1ob3d0bzMvaG9tZS1hZGQtaG93dG8zLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImgyIHtcbiAgd2hpdGUtc3BhY2U6IHByZS1saW5lO1xuICBtYXJnaW46IDA7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGZvbnQtc2l6ZTogMC45Mzc1cmVtO1xuICBjb2xvcjogIzE2MTYxNjtcbiAgbGluZS1oZWlnaHQ6IDEuNHJlbTtcbiAgbGV0dGVyLXNwYWNpbmc6IC0wLjJweDtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuLm0xIHtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbGluZS1oZWlnaHQ6IDEuMTg4cmVtO1xuICBjb2xvcjogIzMzMztcbiAgbGV0dGVyLXNwYWNpbmc6IC0wLjJweDtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5tMiB7XG4gIGZvbnQtc2l6ZTogMC45Mzc1cmVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBsaW5lLWhlaWdodDogMS4xODhyZW07XG4gIGNvbG9yOiAjMzMzO1xuICBsZXR0ZXItc3BhY2luZzogLTAuMnB4O1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ubTEsXG4ubTIge1xuICB3aGl0ZS1zcGFjZTogcHJlLWxpbmU7XG59XG5cbi5zbGlkZUFsbCB7XG4gIGhlaWdodDogMTAwJTtcbiAgd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xufVxuXG5pb24tc2xpZGUge1xuICB3aWR0aDogMTAwJTtcbn1cblxuaW9uLXNsaWRlcyB7XG4gIC8vICAgaGVpZ2h0OiA4MCU7XG4gIGhlaWdodDogNTkuODYlO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLndyYXAge1xuICBoZWlnaHQ6IGNhbGMoMTAwJSAtIDEzOHB4KTtcbn1cblxuLnNsaWRlV3JhcCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uY29uSW5uZXIge1xuICBiYWNrZ3JvdW5kOiAjZjlmOWY5O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2VlZTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlZWU7XG4gIGhlaWdodDogNTkuODYlO1xuXG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nLXRvcDogOS43NyU7XG4gIHBhZGRpbmctYm90dG9tOiAyMy40NiU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5zdGVwIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHdpZHRoOiA1MHB4O1xuICBoZWlnaHQ6IDUwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDMxM2RlZywgcmdiYSg0MywgOTAsIDE5NSwgMSkgMCUsIHJnYmEoMjcsIDE1MiwgMjMxLCAxKSAxMDAlKTtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIC8vICAgbWFyZ2luLWJvdHRvbTogMjMuMTglO1xuICBzcGFuIHtcbiAgICB6LWluZGV4OiAxO1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgzMTNkZWcsIHJnYmEoNDMsIDkwLCAxOTUsIDEpIDAlLCByZ2JhKDI3LCAxNTIsIDIzMSwgMSkgMTAwJSk7XG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XG4gICAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IHRyYW5zcGFyZW50O1xuICB9XG59XG5cbi5zdGVwSW5uZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4uc3RlcDo6YmVmb3JlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGNvbnRlbnQ6ICcnO1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDQycHg7XG4gIGhlaWdodDogNDJweDtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbn1cbkBtZWRpYSAobWF4LWhlaWdodDogNzM2cHgpIHtcbiAgLmNvbklubmVyIHtcbiAgICBoZWlnaHQ6IDc1JTtcbiAgfVxufVxuXG5AbWVkaWEgKG1heC1oZWlnaHQ6IDU3MHB4KSB7XG4gIC5jb25Jbm5lciB7XG4gICAgaGVpZ2h0OiA4MCU7XG4gIH1cbiAgLnN0ZXAge1xuICAgIHdpZHRoOiA1MHB4O1xuICAgIGhlaWdodDogNTBweDtcbiAgfVxuXG4gIC5jb25Jbm5lciB7XG4gICAgcGFkZGluZy10b3A6IDclO1xuICB9XG5cbiAgLnN0ZXA6OmJlZm9yZSB7XG4gICAgd2lkdGg6IDQycHg7XG4gICAgaGVpZ2h0OiA0MnB4O1xuICB9XG5cbiAgaDIge1xuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgfVxuXG4gIC5tMiB7XG4gICAgLy8gZm9udC1zaXplOiAxNnB4O1xuICB9XG59XG5cbi5pbnRybyB7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbGluZS1oZWlnaHQ6IDE5cHg7XG4gIGNvbG9yOiAjNTQ1NDU0O1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW4tYm90dG9tOiAyNHB4O1xufVxuXG4uZ3JidG4ge1xuICB3aWR0aDogY2FsYygxMDAlIC0gMjZweCAtIDI2cHgpO1xuICBtYXJnaW46IDAgYXV0bztcbn1cblxuLmFsbFdyYXAge1xuICBoZWlnaHQ6IGNhbGMoMTAwdmggLSAxMDBweCk7XG59XG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/home-add-howto3/home-add-howto3.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/home-add-howto3/home-add-howto3.page.ts ***!
  \***************************************************************/
/*! exports provided: HomeAddHowto3Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAddHowto3Page", function() { return HomeAddHowto3Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let HomeAddHowto3Page = class HomeAddHowto3Page {
    constructor(navc) {
        this.navc = navc;
        this.slideOpts = {
            initialSlide: 0,
            speed: 400,
            allowTouchMove: false,
        };
    }
    // how = [
    //   {
    //     step: '3',
    //     suggest:
    //       '나무를 그리고 나면 새로운\n 종이 한 장을 세로로 놓고\n “사람을 그리세요. \n단, 사람을 그릴때 막대 인물상이나 \n만화처럼 그리지말고 \n사람의 전체를 그리세요.”',
    //     material2: '“ 얼굴만 아니라 전신을 그려보세요.”',
    //   },
    // ];
    ngOnInit() { }
    slidenext() {
        this.navc.navigateForward('home-add-howto4');
    }
};
HomeAddHowto3Page.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
];
HomeAddHowto3Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-add-howto3',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home-add-howto3.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-howto3/home-add-howto3.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home-add-howto3.page.scss */ "./src/app/pages/home-add-howto3/home-add-howto3.page.scss")).default]
    })
], HomeAddHowto3Page);



/***/ })

}]);
//# sourceMappingURL=pages-home-add-howto3-home-add-howto3-module-es2015.js.map