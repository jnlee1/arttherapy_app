(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-add-mypage-home-add-mypage-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-mypage/home-add-mypage.page.html":
    /*!*******************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-mypage/home-add-mypage.page.html ***!
      \*******************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesHomeAddMypageHomeAddMypagePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <div id=\"headerInner\">\n    <ion-toolbar>\n      <ion-buttons class=\"back\" slot=\"start\">\n        <ion-back-button text=\"\" icon=\"\"></ion-back-button>\n      </ion-buttons>\n      <ion-title>마이페이지</ion-title>\n      <ion-buttons slot=\"end\">\n        <ion-button (click)=\"gosetting()\">\n          <img src=\"assets/imgs/setting.png\" alt=\"\">\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n\n  </div>\n</ion-header>\n<ion-content>\n  <div *ngIf=\"user$ | async as user\">\n    <ion-item class=\"profile\">\n      <h2>\n        {{user.nickname}}님\n      </h2>\n\n      <ion-button slot=\"end\" fill=\"clear\" (click)=\"goedit(user.uid)\">\n        내 정보수정 >\n      </ion-button>\n    </ion-item>\n    <div *ngIf=\"counselors$|async as counselors\">\n      <ion-list>\n        <ion-item class=\"listT\">\n          <h3>\n            검사내역\n          </h3>\n\n          <span slot=\"end\">\n            총 {{counselors.length}}번 검사\n          </span>\n        </ion-item>\n\n        <div *ngFor=\"let item of counselors; let i = index\">\n          <ion-item class=\"reseult\" *ngIf=\"4 > i || moreSeeSwitch\">\n            <span slot=\"start\" class=\"preview\">\n              <img src=\"{{item.imageList[1].url}}\" alt=\"\">\n\n            </span>\n            <!-- <ion-avatar slot=\"start\">\n        </ion-avatar> -->\n\n            <ion-label>\n              <h4>{{item.dateCreated | date:'yyyy년 MM월 dd일'}} 검사내역</h4>\n\n              <div>\n                <ion-button class=\"analysis\" fill=\"outline\" *ngIf=\"!item.analysisSwitch && !item?.failAnalyze\">\n                  분석중\n                </ion-button>\n                <ion-button class=\"analysis\" fill=\"outline\" *ngIf=\"!item.analysisSwitch && item?.failAnalyze\">\n                  분석실패\n                </ion-button>\n                <div *ngIf=\"item.analysisSwitch\">\n                  <ion-button class=\"view\" fill=\"outline\" (click)=\"goresult(item.counselorId)\">\n                    보기\n                  </ion-button>\n\n                  <ion-button class=\"down\" fill=\"outline\" (click)=\"down(item)\">\n                    다운\n                  </ion-button>\n                </div>\n              </div>\n            </ion-label>\n          </ion-item>\n        </div>\n\n\n        <ion-button class=\"more\" fill=\"clear\" expand=\"block\" *ngIf=\"counselors.length > 4 && !moreSeeSwitch\"\n          (click)=\"switchChange()\">\n          더보기\n        </ion-button>\n      </ion-list>\n\n      <div class=\"none\" *ngIf=\"!counselors\">\n        검사내역이 없습니다.\n      </div>\n    </div>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/home-add-mypage/home-add-mypage-routing.module.ts":
    /*!*************************************************************************!*\
      !*** ./src/app/pages/home-add-mypage/home-add-mypage-routing.module.ts ***!
      \*************************************************************************/

    /*! exports provided: HomeAddMypagePageRoutingModule */

    /***/
    function srcAppPagesHomeAddMypageHomeAddMypageRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddMypagePageRoutingModule", function () {
        return HomeAddMypagePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _home_add_mypage_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./home-add-mypage.page */
      "./src/app/pages/home-add-mypage/home-add-mypage.page.ts");

      var routes = [{
        path: '',
        component: _home_add_mypage_page__WEBPACK_IMPORTED_MODULE_3__["HomeAddMypagePage"]
      }];

      var HomeAddMypagePageRoutingModule = function HomeAddMypagePageRoutingModule() {
        _classCallCheck(this, HomeAddMypagePageRoutingModule);
      };

      HomeAddMypagePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], HomeAddMypagePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/home-add-mypage/home-add-mypage.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/home-add-mypage/home-add-mypage.module.ts ***!
      \*****************************************************************/

    /*! exports provided: HomeAddMypagePageModule */

    /***/
    function srcAppPagesHomeAddMypageHomeAddMypageModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddMypagePageModule", function () {
        return HomeAddMypagePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _home_add_mypage_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./home-add-mypage-routing.module */
      "./src/app/pages/home-add-mypage/home-add-mypage-routing.module.ts");
      /* harmony import */


      var _home_add_mypage_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./home-add-mypage.page */
      "./src/app/pages/home-add-mypage/home-add-mypage.page.ts");

      var HomeAddMypagePageModule = function HomeAddMypagePageModule() {
        _classCallCheck(this, HomeAddMypagePageModule);
      };

      HomeAddMypagePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _home_add_mypage_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeAddMypagePageRoutingModule"]],
        declarations: [_home_add_mypage_page__WEBPACK_IMPORTED_MODULE_6__["HomeAddMypagePage"]]
      })], HomeAddMypagePageModule);
      /***/
    },

    /***/
    "./src/app/pages/home-add-mypage/home-add-mypage.page.scss":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/home-add-mypage/home-add-mypage.page.scss ***!
      \*****************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesHomeAddMypageHomeAddMypagePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".profile {\n  border-bottom: 1px solid #eeeeee;\n  --padding-start: 1.563rem;\n  --padding-end: 1.563rem;\n  --padding-top: 1.2rem;\n  --padding-bottom: 1.2rem;\n}\n.profile h2 {\n  margin: 0;\n  font-size: 1rem;\n  font-weight: 600;\n  letter-spacing: -0.4px;\n  color: #333333;\n}\n.profile .button-clear {\n  --color: #9d9d9d;\n  font-size: 0.9375rem;\n  font-weight: 600;\n  letter-spacing: -0.4px;\n  margin: 0;\n  height: auto;\n  --padding-start: 0;\n  --padding-end: 0;\n  --border-radius: 0;\n}\n.profile::after {\n  content: \"\";\n  display: block;\n  background: #f9f9f9;\n  padding-bottom: 8px;\n}\n.listT {\n  --padding-start: 1.563rem;\n  --padding-end: 1.563rem;\n  --padding-top: 1.063rem;\n  --padding-bottom: 1.063rem;\n}\n.listT h3 {\n  margin: 0;\n  font-size: 1rem;\n  color: #333333;\n  font-weight: 600;\n  letter-spacing: -0.4px;\n}\n.listT span {\n  font-size: 0.875rem;\n  font-weight: 600;\n  color: #333333;\n  letter-spacing: -0.4px;\n  margin: 0;\n}\n.reseult {\n  --padding-top: 0.875rem;\n  --padding-start: 1.5rem;\n  --padding-end: 1.5rem;\n  --padding-bottom: 0.875rem;\n  border-bottom: 1px solid #eeeeee;\n}\n.reseult h4 {\n  margin: 0;\n  font-weight: 600;\n  color: #333;\n  letter-spacing: -0.4px;\n  margin-bottom: 1.2rem;\n  text-align: center;\n  font-size: 0.9375rem;\n}\n.reseult ion-label {\n  margin: 0;\n}\n.reseult ion-label div {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n.reseult ion-label div .button-outline {\n  margin: 0;\n  padding: 0;\n  --border-radius: 14px;\n  height: 1.75rem;\n  font-size: 0.875rem;\n  font-weight: 600;\n  --padding-top: 0.375rem;\n  --padding-start: 1.25rem;\n  --padding-end: 1.25rem;\n  --padding-bottom: 0.375rem;\n}\n.reseult ion-label div .view.ion-activated,\n.reseult ion-label div .down.ion-activated {\n  opacity: 0.8;\n}\n.reseult ion-label div .view {\n  margin-right: 0.5rem;\n}\n.reseult ion-label div .analysis {\n  --color: #295fc6;\n  --background: #c2c2c2;\n  --border-width: 0;\n  --background-activated: #c2c2c2;\n  --background-focused: #c2c2c2;\n  --background-focused: #c2c2c2;\n  --background-hover: #c2c2c2;\n  --background-focused-opacity: 0.1;\n  --color-activated: #295fc6;\n}\n.reseult ion-label div .view {\n  --color: #fff;\n  --background: center / cover no-repeat url('toggle.png');\n  background: center/cover no-repeat url('toggle.png');\n  --border-width: 0;\n  --background-activated: center / cover no-repeat url('toggle.png');\n  --background-focused: center / cover no-repeat url('toggle.png');\n  --background-focused: center / cover no-repeat url('toggle.png');\n  --background-hover: center / cover no-repeat url('toggle.png');\n  --background-focused-opacity: 0.1;\n  --color-activated: #fff;\n  border-radius: 14px;\n}\n.reseult ion-label div .down {\n  --color: #2768cb;\n  --background: #fff;\n  --background-activated: #fff;\n  --background-focused: #fff;\n  --background-focused: #fff;\n  --background-hover: #fff;\n  --background-focused-opacity: 1;\n  --border-color: #2768cb;\n  --color-activated: #2768cb;\n}\n.more {\n  --padding-top: 1rem;\n  --padding-bottom: 1rem;\n  margin: 0;\n  height: auto;\n  --color: #9d9d9d;\n  font-size: 0.9375rem;\n  font-weight: 600;\n  letter-spacing: -0.4px;\n}\n.none {\n  height: calc(100vh - 56px - 86px);\n  text-align: center;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  font-size: 18px;\n  font-weight: 600;\n  color: #333;\n}\n.preview {\n  max-width: 4.375rem;\n  max-height: 4.375rem;\n  min-width: 4.375rem;\n  min-height: 4.375rem;\n  width: 4.375rem;\n  height: 4.375rem;\n  margin: 0;\n  border: 2px solid #333;\n  box-sizing: content-box;\n  border-radius: 50%;\n  overflow: hidden;\n}\n.preview img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: cover;\n     object-fit: cover;\n  display: block;\n}\n@media (max-width: 330px) {\n  .reseult h4 {\n    font-size: 12px;\n  }\n\n  .preview {\n    max-width: 60px;\n    max-height: 60px;\n    min-width: 60px;\n    min-height: 60px;\n    width: 60px;\n    height: 60px;\n  }\n\n  .profile .button-clear {\n    font-size: 13px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtbXlwYWdlL2hvbWUtYWRkLW15cGFnZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQ0FBQTtFQUVBLHlCQUFBO0VBQ0EsdUJBQUE7RUFDQSxxQkFBQTtFQUNBLHdCQUFBO0FBQUY7QUFFRTtFQUNFLFNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxzQkFBQTtFQUNBLGNBQUE7QUFBSjtBQUdFO0VBQ0UsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQURKO0FBS0E7RUFDRSxXQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUFGRjtBQUtBO0VBQ0UseUJBQUE7RUFDQSx1QkFBQTtFQUNBLHVCQUFBO0VBQ0EsMEJBQUE7QUFGRjtBQUlFO0VBQ0UsU0FBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxzQkFBQTtBQUZKO0FBS0U7RUFDRSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLHNCQUFBO0VBQ0EsU0FBQTtBQUhKO0FBT0E7RUFDRSx1QkFBQTtFQUNBLHVCQUFBO0VBQ0EscUJBQUE7RUFDQSwwQkFBQTtFQUNBLGdDQUFBO0FBSkY7QUF3QkU7RUFDRSxTQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0Esb0JBQUE7QUF0Qko7QUF5QkU7RUFDRSxTQUFBO0FBdkJKO0FBeUJJO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUF2Qk47QUF5Qk07RUFDRSxTQUFBO0VBQ0EsVUFBQTtFQUNBLHFCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtFQUNBLHdCQUFBO0VBQ0Esc0JBQUE7RUFDQSwwQkFBQTtBQXZCUjtBQXlCTTs7RUFFRSxZQUFBO0FBdkJSO0FBMEJNO0VBQ0Usb0JBQUE7QUF4QlI7QUEwQk07RUFDRSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSwrQkFBQTtFQUNBLDZCQUFBO0VBQ0EsNkJBQUE7RUFDQSwyQkFBQTtFQUNBLGlDQUFBO0VBQ0EsMEJBQUE7QUF4QlI7QUEyQk07RUFDRSxhQUFBO0VBQ0Esd0RBQUE7RUFDQSxvREFBQTtFQUNBLGlCQUFBO0VBQ0Esa0VBQUE7RUFDQSxnRUFBQTtFQUNBLGdFQUFBO0VBQ0EsOERBQUE7RUFDQSxpQ0FBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7QUF6QlI7QUE0Qk07RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsNEJBQUE7RUFDQSwwQkFBQTtFQUNBLDBCQUFBO0VBQ0Esd0JBQUE7RUFDQSwrQkFBQTtFQUNBLHVCQUFBO0VBQ0EsMEJBQUE7QUExQlI7QUFnQ0E7RUFDRSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsU0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxzQkFBQTtBQTdCRjtBQWdDQTtFQUNFLGlDQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUE3QkY7QUFnQ0E7RUFDRSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFNBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQTdCRjtBQThCRTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7S0FBQSxpQkFBQTtFQUNBLGNBQUE7QUE1Qko7QUFnQ0E7RUFFSTtJQUNFLGVBQUE7RUE5Qko7O0VBa0NBO0lBQ0UsZUFBQTtJQUNBLGdCQUFBO0lBQ0EsZUFBQTtJQUNBLGdCQUFBO0lBQ0EsV0FBQTtJQUNBLFlBQUE7RUEvQkY7O0VBbUNFO0lBQ0UsZUFBQTtFQWhDSjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtbXlwYWdlL2hvbWUtYWRkLW15cGFnZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucHJvZmlsZSB7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWVlZWVlO1xuXG4gIC0tcGFkZGluZy1zdGFydDogMS41NjNyZW07XG4gIC0tcGFkZGluZy1lbmQ6IDEuNTYzcmVtO1xuICAtLXBhZGRpbmctdG9wOiAxLjJyZW07XG4gIC0tcGFkZGluZy1ib3R0b206IDEuMnJlbTtcblxuICBoMiB7XG4gICAgbWFyZ2luOiAwO1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAtMC40cHg7XG4gICAgY29sb3I6ICMzMzMzMzM7XG4gIH1cblxuICAuYnV0dG9uLWNsZWFyIHtcbiAgICAtLWNvbG9yOiAjOWQ5ZDlkO1xuICAgIGZvbnQtc2l6ZTogMC45Mzc1cmVtO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgbGV0dGVyLXNwYWNpbmc6IC0wLjRweDtcbiAgICBtYXJnaW46IDA7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIC0tcGFkZGluZy1zdGFydDogMDtcbiAgICAtLXBhZGRpbmctZW5kOiAwO1xuICAgIC0tYm9yZGVyLXJhZGl1czogMDtcbiAgfVxufVxuXG4ucHJvZmlsZTo6YWZ0ZXIge1xuICBjb250ZW50OiAnJztcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJhY2tncm91bmQ6ICNmOWY5Zjk7XG4gIHBhZGRpbmctYm90dG9tOiA4cHg7XG59XG5cbi5saXN0VCB7XG4gIC0tcGFkZGluZy1zdGFydDogMS41NjNyZW07XG4gIC0tcGFkZGluZy1lbmQ6IDEuNTYzcmVtO1xuICAtLXBhZGRpbmctdG9wOiAxLjA2M3JlbTtcbiAgLS1wYWRkaW5nLWJvdHRvbTogMS4wNjNyZW07XG5cbiAgaDMge1xuICAgIG1hcmdpbjogMDtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgY29sb3I6ICMzMzMzMzM7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBsZXR0ZXItc3BhY2luZzogLTAuNHB4O1xuICB9XG5cbiAgc3BhbiB7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGNvbG9yOiAjMzMzMzMzO1xuICAgIGxldHRlci1zcGFjaW5nOiAtMC40cHg7XG4gICAgbWFyZ2luOiAwO1xuICB9XG59XG5cbi5yZXNldWx0IHtcbiAgLS1wYWRkaW5nLXRvcDogMC44NzVyZW07XG4gIC0tcGFkZGluZy1zdGFydDogMS41cmVtO1xuICAtLXBhZGRpbmctZW5kOiAxLjVyZW07XG4gIC0tcGFkZGluZy1ib3R0b206IDAuODc1cmVtO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2VlZWVlZTtcbiAgLy8gaW9uLWF2YXRhciB7XG4gIC8vICAgbWF4LXdpZHRoOiA4MHB4O1xuICAvLyAgIG1heC1oZWlnaHQ6IDgwcHg7XG4gIC8vICAgbWluLXdpZHRoOiA4MHB4O1xuICAvLyAgIG1pbi1oZWlnaHQ6IDgwcHg7XG4gIC8vICAgd2lkdGg6IDgwcHg7XG4gIC8vICAgaGVpZ2h0OiA4MHB4O1xuICAvLyAgIG1hcmdpbjogMDtcbiAgLy8gICBib3JkZXI6IDJweCBzb2xpZCAjMzMzO1xuICAvLyAgIGJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xuXG4gIC8vICAgaW1nIHtcbiAgLy8gICAgIHdpZHRoOiAxMDAlO1xuICAvLyAgICAgaGVpZ2h0OiAxMDAlO1xuICAvLyAgICAgb2JqZWN0LWZpdDogY292ZXI7XG4gIC8vICAgICBkaXNwbGF5OiBibG9jaztcbiAgLy8gICB9XG4gIC8vIH1cblxuICBoNCB7XG4gICAgbWFyZ2luOiAwO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gICAgY29sb3I6ICMzMzM7XG4gICAgbGV0dGVyLXNwYWNpbmc6IC0wLjRweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxLjJyZW07XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGZvbnQtc2l6ZTogMC45Mzc1cmVtO1xuICB9XG5cbiAgaW9uLWxhYmVsIHtcbiAgICBtYXJnaW46IDA7XG5cbiAgICBkaXYge1xuICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcblxuICAgICAgLmJ1dHRvbi1vdXRsaW5lIHtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICBwYWRkaW5nOiAwO1xuICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDE0cHg7XG4gICAgICAgIGhlaWdodDogMS43NXJlbTtcbiAgICAgICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICAgICAgLS1wYWRkaW5nLXRvcDogMC4zNzVyZW07XG4gICAgICAgIC0tcGFkZGluZy1zdGFydDogMS4yNXJlbTtcbiAgICAgICAgLS1wYWRkaW5nLWVuZDogMS4yNXJlbTtcbiAgICAgICAgLS1wYWRkaW5nLWJvdHRvbTogMC4zNzVyZW07XG4gICAgICB9XG4gICAgICAudmlldy5pb24tYWN0aXZhdGVkLFxuICAgICAgLmRvd24uaW9uLWFjdGl2YXRlZCB7XG4gICAgICAgIG9wYWNpdHk6IDAuODtcbiAgICAgIH1cblxuICAgICAgLnZpZXcge1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IDAuNXJlbTtcbiAgICAgIH1cbiAgICAgIC5hbmFseXNpcyB7XG4gICAgICAgIC0tY29sb3I6ICMyOTVmYzY7XG4gICAgICAgIC0tYmFja2dyb3VuZDogI2MyYzJjMjtcbiAgICAgICAgLS1ib3JkZXItd2lkdGg6IDA7XG4gICAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6ICNjMmMyYzI7XG4gICAgICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAjYzJjMmMyO1xuICAgICAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogI2MyYzJjMjtcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWhvdmVyOiAjYzJjMmMyO1xuICAgICAgICAtLWJhY2tncm91bmQtZm9jdXNlZC1vcGFjaXR5OiAwLjE7XG4gICAgICAgIC0tY29sb3ItYWN0aXZhdGVkOiAjMjk1ZmM2O1xuICAgICAgfVxuXG4gICAgICAudmlldyB7XG4gICAgICAgIC0tY29sb3I6ICNmZmY7XG4gICAgICAgIC0tYmFja2dyb3VuZDogY2VudGVyIC8gY292ZXIgbm8tcmVwZWF0IHVybCguLi8uLi8uLi9hc3NldHMvaW1ncy90b2dnbGUucG5nKTtcbiAgICAgICAgYmFja2dyb3VuZDogY2VudGVyIC8gY292ZXIgbm8tcmVwZWF0IHVybCguLi8uLi8uLi9hc3NldHMvaW1ncy90b2dnbGUucG5nKTtcbiAgICAgICAgLS1ib3JkZXItd2lkdGg6IDA7XG4gICAgICAgIC0tYmFja2dyb3VuZC1hY3RpdmF0ZWQ6IGNlbnRlciAvIGNvdmVyIG5vLXJlcGVhdCB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZ3MvdG9nZ2xlLnBuZyk7XG4gICAgICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiBjZW50ZXIgLyBjb3ZlciBuby1yZXBlYXQgdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWdzL3RvZ2dsZS5wbmcpO1xuICAgICAgICAtLWJhY2tncm91bmQtZm9jdXNlZDogY2VudGVyIC8gY292ZXIgbm8tcmVwZWF0IHVybCguLi8uLi8uLi9hc3NldHMvaW1ncy90b2dnbGUucG5nKTtcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWhvdmVyOiBjZW50ZXIgLyBjb3ZlciBuby1yZXBlYXQgdXJsKC4uLy4uLy4uL2Fzc2V0cy9pbWdzL3RvZ2dsZS5wbmcpO1xuICAgICAgICAtLWJhY2tncm91bmQtZm9jdXNlZC1vcGFjaXR5OiAwLjE7XG4gICAgICAgIC0tY29sb3ItYWN0aXZhdGVkOiAjZmZmO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxNHB4O1xuICAgICAgfVxuXG4gICAgICAuZG93biB7XG4gICAgICAgIC0tY29sb3I6ICMyNzY4Y2I7XG4gICAgICAgIC0tYmFja2dyb3VuZDogI2ZmZjtcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogI2ZmZjtcbiAgICAgICAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6ICNmZmY7XG4gICAgICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkOiAjZmZmO1xuICAgICAgICAtLWJhY2tncm91bmQtaG92ZXI6ICNmZmY7XG4gICAgICAgIC0tYmFja2dyb3VuZC1mb2N1c2VkLW9wYWNpdHk6IDE7XG4gICAgICAgIC0tYm9yZGVyLWNvbG9yOiAjMjc2OGNiO1xuICAgICAgICAtLWNvbG9yLWFjdGl2YXRlZDogIzI3NjhjYjtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuLm1vcmUge1xuICAtLXBhZGRpbmctdG9wOiAxcmVtO1xuICAtLXBhZGRpbmctYm90dG9tOiAxcmVtO1xuICBtYXJnaW46IDA7XG4gIGhlaWdodDogYXV0bztcbiAgLS1jb2xvcjogIzlkOWQ5ZDtcbiAgZm9udC1zaXplOiAwLjkzNzVyZW07XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGxldHRlci1zcGFjaW5nOiAtMC40cHg7XG59XG5cbi5ub25lIHtcbiAgaGVpZ2h0OiBjYWxjKDEwMHZoIC0gNTZweCAtIDg2cHgpO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBmb250LXNpemU6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjMzMzO1xufVxuXG4ucHJldmlldyB7XG4gIG1heC13aWR0aDogNC4zNzVyZW07XG4gIG1heC1oZWlnaHQ6IDQuMzc1cmVtO1xuICBtaW4td2lkdGg6IDQuMzc1cmVtO1xuICBtaW4taGVpZ2h0OiA0LjM3NXJlbTtcbiAgd2lkdGg6IDQuMzc1cmVtO1xuICBoZWlnaHQ6IDQuMzc1cmVtO1xuICBtYXJnaW46IDA7XG4gIGJvcmRlcjogMnB4IHNvbGlkICMzMzM7XG4gIGJveC1zaXppbmc6IGNvbnRlbnQtYm94O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIGltZyB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICB9XG59XG5cbkBtZWRpYSAobWF4LXdpZHRoOiAzMzBweCkge1xuICAucmVzZXVsdCB7XG4gICAgaDQge1xuICAgICAgZm9udC1zaXplOiAxMnB4O1xuICAgIH1cbiAgfVxuXG4gIC5wcmV2aWV3IHtcbiAgICBtYXgtd2lkdGg6IDYwcHg7XG4gICAgbWF4LWhlaWdodDogNjBweDtcbiAgICBtaW4td2lkdGg6IDYwcHg7XG4gICAgbWluLWhlaWdodDogNjBweDtcbiAgICB3aWR0aDogNjBweDtcbiAgICBoZWlnaHQ6IDYwcHg7XG4gIH1cblxuICAucHJvZmlsZSB7XG4gICAgLmJ1dHRvbi1jbGVhciB7XG4gICAgICBmb250LXNpemU6IDEzcHg7XG4gICAgfVxuICB9XG59XG4iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/home-add-mypage/home-add-mypage.page.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/home-add-mypage/home-add-mypage.page.ts ***!
      \***************************************************************/

    /*! exports provided: HomeAddMypagePage */

    /***/
    function srcAppPagesHomeAddMypageHomeAddMypagePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddMypagePage", function () {
        return HomeAddMypagePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _services_alert_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! ./../../../services/alert.service */
      "./src/services/alert.service.ts");
      /* harmony import */


      var _services_loading_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./../../../services/loading.service */
      "./src/services/loading.service.ts");
      /* harmony import */


      var src_services_auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/services/auth.service */
      "./src/services/auth.service.ts");
      /* harmony import */


      var src_services_db_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! src/services/db.service */
      "./src/services/db.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/fire/auth */
      "./node_modules/@angular/fire/__ivy_ngcc__/auth/es2015/index.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! @ionic-native/file/ngx */
      "./node_modules/@ionic-native/file/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
      /*! @ionic-native/android-permissions/ngx */
      "./node_modules/@ionic-native/android-permissions/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
      /*! @ionic-native/file-transfer/ngx */
      "./node_modules/@ionic-native/file-transfer/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_document_viewer_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
      /*! @ionic-native/document-viewer/ngx */
      "./node_modules/@ionic-native/document-viewer/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
      /*! @ionic-native/file-opener/ngx */
      "./node_modules/@ionic-native/file-opener/__ivy_ngcc__/ngx/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");

      var HomeAddMypagePage = /*#__PURE__*/function () {
        function HomeAddMypagePage(navc, alertController, db, afAuth, auth, file, plt, transfer, document, androidPermissions, fileOpener, loading, alert) {
          _classCallCheck(this, HomeAddMypagePage);

          this.navc = navc;
          this.alertController = alertController;
          this.db = db;
          this.afAuth = afAuth;
          this.auth = auth;
          this.file = file;
          this.plt = plt;
          this.transfer = transfer;
          this.document = document;
          this.androidPermissions = androidPermissions;
          this.fileOpener = fileOpener;
          this.loading = loading;
          this.alert = alert;
          this.moreSeeSwitch = false; // this.uid = this.auth.userDetails().uid;

          this.uid = localStorage.getItem('userId'); // console.log('auth', this.auth);

          this.getData();
        }

        _createClass(HomeAddMypagePage, [{
          key: "switchChange",
          value: function switchChange() {
            this.moreSeeSwitch = true;
          }
        }, {
          key: "getData",
          value: function getData() {
            var _this = this;

            this.user$ = this.db.doc$("users/".concat(this.uid));
            this.counselors$ = this.db.collection$("counselor", function (ref) {
              return ref.where('uid', '==', _this.uid).orderBy('dateCreated', 'desc');
            });
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "goedit",
          value: function goedit(uid) {
            this.navc.navigateForward(['/home-add-mypage-profileedit'], {
              queryParams: {
                uid: uid
              }
            });
          }
        }, {
          key: "gosetting",
          value: function gosetting() {
            this.navc.navigateForward(['/home-add-mypage-setting']);
          }
        }, {
          key: "goresult",
          value: function goresult(counselorId) {
            this.navc.navigateForward(['/home-add-mypage-result'], {
              queryParams: {
                counselorId: counselorId
              }
            });
          }
        }, {
          key: "down",
          value: function down(counselor) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var _this2 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      this.db.doc$("counselor/".concat(counselor.id)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_13__["take"])(1)).subscribe(function (item) {
                        counselor = item;
                      });
                      _context.next = 3;
                      return this.alertController.create({
                        cssClass: 'al',
                        header: '다운로드',
                        message: '그림 심리 분석 결과 PDF파일을\n 다운로드 하시겠습니까?',
                        buttons: [{
                          text: '취소',
                          role: 'cancel',
                          cssClass: 'secondary'
                        }, {
                          text: '다운로드',
                          handler: function handler() {
                            _this2.downloadPdf(counselor.pdfUrl);
                          }
                        }]
                      });

                    case 3:
                      alert = _context.sent;
                      _context.next = 6;
                      return alert.present();

                    case 6:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "downloadPdf",
          value: function downloadPdf(pdfFile) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var _this3 = this;

              var path;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.getDownloadPath();

                    case 2:
                      path = _context2.sent;
                      this.loading.load();
                      this.transfer.create().download(pdfFile, path + new Date().getTime() + '.pdf', true).then(function (entry) {
                        _this3.alert.presentToast("\uB2E4\uC6B4\uB85C\uB4DC\uB97C \uC644\uB8CC\uD558\uC600\uC2B5\uB2C8\uB2E4.");

                        var url = entry.toURL();

                        if (_this3.plt.is('android')) {
                          _this3.fileOpener.open(url, 'application/pdf').then(function () {
                            _this3.loading.hide();
                          })["catch"](function (error) {
                            _this3.loading.hide();

                            console.log('error', error);
                          });
                        } else {
                          var options = {
                            title: 'My PDF'
                          };

                          _this3.document.viewDocument(url, 'application/pdf', options);
                        }
                      }, function (err) {
                        console.log('Error', err);

                        _this3.loading.hide();
                      });

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "getDownloadPath",
          value: function getDownloadPath() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this4 = this;

              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      if (!this.plt.is('ios')) {
                        _context3.next = 2;
                        break;
                      }

                      return _context3.abrupt("return", this.file.documentsDirectory);

                    case 2:
                      _context3.next = 4;
                      return this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(function (result) {
                        if (!result.hasPermission) {
                          return _this4.androidPermissions.requestPermission(_this4.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE);
                        }
                      });

                    case 4:
                      return _context3.abrupt("return", this.file.externalRootDirectory + '/Download/');

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }]);

        return HomeAddMypagePage;
      }();

      HomeAddMypagePage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"]
        }, {
          type: src_services_db_service__WEBPACK_IMPORTED_MODULE_4__["DbService"]
        }, {
          type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_6__["AngularFireAuth"]
        }, {
          type: src_services_auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"]
        }, {
          type: _ionic_native_file_ngx__WEBPACK_IMPORTED_MODULE_8__["File"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"]
        }, {
          type: _ionic_native_file_transfer_ngx__WEBPACK_IMPORTED_MODULE_10__["FileTransfer"]
        }, {
          type: _ionic_native_document_viewer_ngx__WEBPACK_IMPORTED_MODULE_11__["DocumentViewer"]
        }, {
          type: _ionic_native_android_permissions_ngx__WEBPACK_IMPORTED_MODULE_9__["AndroidPermissions"]
        }, {
          type: _ionic_native_file_opener_ngx__WEBPACK_IMPORTED_MODULE_12__["FileOpener"]
        }, {
          type: _services_loading_service__WEBPACK_IMPORTED_MODULE_2__["LoadingService"]
        }, {
          type: _services_alert_service__WEBPACK_IMPORTED_MODULE_1__["AlertService"]
        }];
      };

      HomeAddMypagePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["Component"])({
        selector: 'app-home-add-mypage',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./home-add-mypage.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-mypage/home-add-mypage.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./home-add-mypage.page.scss */
        "./src/app/pages/home-add-mypage/home-add-mypage.page.scss"))["default"]]
      })], HomeAddMypagePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-home-add-mypage-home-add-mypage-module-es5.js.map