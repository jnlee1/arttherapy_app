(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-add-howto2-home-add-howto2-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-howto2/home-add-howto2.page.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-howto2/home-add-howto2.page.html ***!
  \*******************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <div id=\"headerInner\">\n    <ion-toolbar>\n      <ion-buttons class=\"back\" slot=\"start\">\n        <ion-back-button text=\"\" icon=\"\"></ion-back-button>\n      </ion-buttons>\n      <ion-title>검사 실시 방법</ion-title>\n    </ion-toolbar>\n\n  </div>\n</ion-header>\n\n<ion-content>\n  <div class=\"allWrap\">\n\n\n    <div class=\"wrap\">\n\n      <div class=\"slideWrap\">\n        <div class=\"slideAll\">\n\n          <div class=\"conInner\">\n            <div class=\"step\">\n              <span>\n                2\n              </span>\n            </div>\n\n            <div class=\"stepInner\">\n              <div>\n                <h2>\n                  집을 그리고 나면 새로운\n                  <br> 종이 한 장을 세로로 놓고 <br> “ 나무를 그리세요.”\n                </h2>\n\n\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n    <div class=\"btn_wrap\">\n      <span class=\"intro\">\n        안내에 맞게 그림을 그리셨다면<br>\n        다음버튼을 눌러주세요.\n      </span>\n      <ion-button expand=\"block\" class=\"grbtn\" (click)=\"slidenext()\">\n        다음\n      </ion-button>\n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/home-add-howto2/home-add-howto2-routing.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/home-add-howto2/home-add-howto2-routing.module.ts ***!
  \*************************************************************************/
/*! exports provided: HomeAddHowto2PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAddHowto2PageRoutingModule", function() { return HomeAddHowto2PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_add_howto2_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home-add-howto2.page */ "./src/app/pages/home-add-howto2/home-add-howto2.page.ts");




const routes = [
    {
        path: '',
        component: _home_add_howto2_page__WEBPACK_IMPORTED_MODULE_3__["HomeAddHowto2Page"]
    }
];
let HomeAddHowto2PageRoutingModule = class HomeAddHowto2PageRoutingModule {
};
HomeAddHowto2PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomeAddHowto2PageRoutingModule);



/***/ }),

/***/ "./src/app/pages/home-add-howto2/home-add-howto2.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/home-add-howto2/home-add-howto2.module.ts ***!
  \*****************************************************************/
/*! exports provided: HomeAddHowto2PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAddHowto2PageModule", function() { return HomeAddHowto2PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _home_add_howto2_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-add-howto2-routing.module */ "./src/app/pages/home-add-howto2/home-add-howto2-routing.module.ts");
/* harmony import */ var _home_add_howto2_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-add-howto2.page */ "./src/app/pages/home-add-howto2/home-add-howto2.page.ts");







let HomeAddHowto2PageModule = class HomeAddHowto2PageModule {
};
HomeAddHowto2PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _home_add_howto2_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeAddHowto2PageRoutingModule"]
        ],
        declarations: [_home_add_howto2_page__WEBPACK_IMPORTED_MODULE_6__["HomeAddHowto2Page"]]
    })
], HomeAddHowto2PageModule);



/***/ }),

/***/ "./src/app/pages/home-add-howto2/home-add-howto2.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/home-add-howto2/home-add-howto2.page.scss ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("h2 {\n  white-space: pre-line;\n  margin: 0;\n  font-weight: 600;\n  font-size: 0.9375rem;\n  color: #161616;\n  line-height: 1.4rem;\n  letter-spacing: -0.2px;\n  margin-bottom: 1rem;\n}\n\n.m1 {\n  font-size: 0.875rem;\n  font-weight: 600;\n  line-height: 1.188rem;\n  color: #333;\n  letter-spacing: -0.2px;\n  display: block;\n}\n\n.m2 {\n  font-size: 1rem;\n  font-weight: 600;\n  line-height: 1.188rem;\n  color: #333;\n  letter-spacing: -0.2px;\n  display: block;\n  text-align: center;\n}\n\n.m1,\n.m2 {\n  white-space: pre-line;\n}\n\n.slideAll {\n  height: 100%;\n  width: 100%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.wrap {\n  height: calc(100% - 138px);\n  background: #ffffff;\n}\n\n.btn_wrap {\n  background: #ffffff;\n}\n\n.slideWrap {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  height: 100%;\n  width: 100%;\n}\n\n.conInner {\n  background: #f9f9f9;\n  border-top: 1px solid #eee;\n  border-bottom: 1px solid #eee;\n  height: 59.86%;\n  width: 100%;\n  padding-top: 9.77%;\n  padding-bottom: 23.46%;\n  overflow: hidden;\n}\n\n.step {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  width: 50px;\n  height: 50px;\n  border-radius: 50%;\n  background: linear-gradient(313deg, #2b5ac3 0%, #1b98e7 100%);\n  margin: 0 auto;\n}\n\n.step span {\n  z-index: 1;\n  font-size: 30px;\n  text-transform: uppercase;\n  background: linear-gradient(313deg, #2b5ac3 0%, #1b98e7 100%);\n  -webkit-background-clip: text;\n  -webkit-text-fill-color: transparent;\n}\n\n.stepInner {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  height: 100%;\n  text-align: center;\n}\n\n.step::before {\n  position: absolute;\n  border-radius: 50%;\n  content: \"\";\n  display: block;\n  width: 42px;\n  height: 42px;\n  background: #fff;\n}\n\n@media (max-height: 736px) {\n  .conInner {\n    height: 75%;\n  }\n}\n\n@media (max-height: 570px) {\n  .conInner {\n    height: 80%;\n  }\n\n  .step {\n    width: 50px;\n    height: 50px;\n  }\n\n  .conInner {\n    padding-top: 7%;\n  }\n\n  .step::before {\n    width: 42px;\n    height: 42px;\n  }\n\n  h2 {\n    line-height: 20px;\n    font-size: 16px;\n  }\n}\n\n.intro {\n  font-size: 14px;\n  font-weight: 600;\n  line-height: 19px;\n  color: #545454;\n  display: block;\n  text-align: center;\n  margin-bottom: 24px;\n}\n\n.grbtn {\n  width: calc(100% - 26px - 26px);\n  margin: 0 auto;\n}\n\n.allWrap {\n  height: calc(100vh - 100px);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtaG93dG8yL2hvbWUtYWRkLWhvd3RvMi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxxQkFBQTtFQUNBLFNBQUE7RUFDQSxnQkFBQTtFQUNBLG9CQUFBO0VBRUEsY0FBQTtFQUNBLG1CQUFBO0VBRUEsc0JBQUE7RUFDQSxtQkFBQTtBQURGOztBQUdBO0VBQ0UsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0EsY0FBQTtBQUFGOztBQUdBO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUFBRjs7QUFHQTs7RUFFRSxxQkFBQTtBQUFGOztBQUdBO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQUFGOztBQUdBO0VBQ0UsMEJBQUE7RUFDQSxtQkFBQTtBQUFGOztBQUdBO0VBQ0UsbUJBQUE7QUFBRjs7QUFHQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUFBRjs7QUFHQTtFQUNFLG1CQUFBO0VBQ0EsMEJBQUE7RUFDQSw2QkFBQTtFQUNBLGNBQUE7RUFFQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtFQUNBLGdCQUFBO0FBREY7O0FBSUE7RUFDRSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSw2REFBQTtFQUNBLGNBQUE7QUFERjs7QUFHRTtFQUNFLFVBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7RUFDQSw2REFBQTtFQUNBLDZCQUFBO0VBQ0Esb0NBQUE7QUFESjs7QUFLQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FBRkY7O0FBS0E7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FBRkY7O0FBSUE7RUFDRTtJQUNFLFdBQUE7RUFERjtBQUNGOztBQUlBO0VBQ0U7SUFDRSxXQUFBO0VBRkY7O0VBSUE7SUFDRSxXQUFBO0lBQ0EsWUFBQTtFQURGOztFQUlBO0lBQ0UsZUFBQTtFQURGOztFQUlBO0lBQ0UsV0FBQTtJQUNBLFlBQUE7RUFERjs7RUFJQTtJQUNFLGlCQUFBO0lBQ0EsZUFBQTtFQURGO0FBQ0Y7O0FBUUE7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQU5GOztBQVNBO0VBQ0UsK0JBQUE7RUFDQSxjQUFBO0FBTkY7O0FBUUE7RUFDRSwyQkFBQTtBQUxGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtaG93dG8yL2hvbWUtYWRkLWhvd3RvMi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJoMiB7XG4gIHdoaXRlLXNwYWNlOiBwcmUtbGluZTtcbiAgbWFyZ2luOiAwO1xuICBmb250LXdlaWdodDogNjAwO1xuICBmb250LXNpemU6IDAuOTM3NXJlbTtcblxuICBjb2xvcjogIzE2MTYxNjtcbiAgbGluZS1oZWlnaHQ6IDEuNHJlbTtcblxuICBsZXR0ZXItc3BhY2luZzogLTAuMnB4O1xuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xufVxuLm0xIHtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbGluZS1oZWlnaHQ6IDEuMTg4cmVtO1xuICBjb2xvcjogIzMzMztcbiAgbGV0dGVyLXNwYWNpbmc6IC0wLjJweDtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5tMiB7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbGluZS1oZWlnaHQ6IDEuMTg4cmVtO1xuICBjb2xvcjogIzMzMztcbiAgbGV0dGVyLXNwYWNpbmc6IC0wLjJweDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLm0xLFxuLm0yIHtcbiAgd2hpdGUtc3BhY2U6IHByZS1saW5lO1xufVxuXG4uc2xpZGVBbGwge1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLndyYXAge1xuICBoZWlnaHQ6IGNhbGMoMTAwJSAtIDEzOHB4KTtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLmJ0bl93cmFwIHtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbn1cblxuLnNsaWRlV3JhcCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uY29uSW5uZXIge1xuICBiYWNrZ3JvdW5kOiAjZjlmOWY5O1xuICBib3JkZXItdG9wOiAxcHggc29saWQgI2VlZTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNlZWU7XG4gIGhlaWdodDogNTkuODYlO1xuXG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nLXRvcDogOS43NyU7XG4gIHBhZGRpbmctYm90dG9tOiAyMy40NiU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5zdGVwIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHdpZHRoOiA1MHB4O1xuICBoZWlnaHQ6IDUwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYmFja2dyb3VuZDogbGluZWFyLWdyYWRpZW50KDMxM2RlZywgcmdiYSg0MywgOTAsIDE5NSwgMSkgMCUsIHJnYmEoMjcsIDE1MiwgMjMxLCAxKSAxMDAlKTtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIC8vICAgbWFyZ2luLWJvdHRvbTogMjMuMTglO1xuICBzcGFuIHtcbiAgICB6LWluZGV4OiAxO1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgzMTNkZWcsIHJnYmEoNDMsIDkwLCAxOTUsIDEpIDAlLCByZ2JhKDI3LCAxNTIsIDIzMSwgMSkgMTAwJSk7XG4gICAgLXdlYmtpdC1iYWNrZ3JvdW5kLWNsaXA6IHRleHQ7XG4gICAgLXdlYmtpdC10ZXh0LWZpbGwtY29sb3I6IHRyYW5zcGFyZW50O1xuICB9XG59XG5cbi5zdGVwSW5uZXIge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgaGVpZ2h0OiAxMDAlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5zdGVwOjpiZWZvcmUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgY29udGVudDogJyc7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogNDJweDtcbiAgaGVpZ2h0OiA0MnB4O1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xufVxuQG1lZGlhIChtYXgtaGVpZ2h0OiA3MzZweCkge1xuICAuY29uSW5uZXIge1xuICAgIGhlaWdodDogNzUlO1xuICB9XG59XG5cbkBtZWRpYSAobWF4LWhlaWdodDogNTcwcHgpIHtcbiAgLmNvbklubmVyIHtcbiAgICBoZWlnaHQ6IDgwJTtcbiAgfVxuICAuc3RlcCB7XG4gICAgd2lkdGg6IDUwcHg7XG4gICAgaGVpZ2h0OiA1MHB4O1xuICB9XG5cbiAgLmNvbklubmVyIHtcbiAgICBwYWRkaW5nLXRvcDogNyU7XG4gIH1cblxuICAuc3RlcDo6YmVmb3JlIHtcbiAgICB3aWR0aDogNDJweDtcbiAgICBoZWlnaHQ6IDQycHg7XG4gIH1cblxuICBoMiB7XG4gICAgbGluZS1oZWlnaHQ6IDIwcHg7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICB9XG5cbiAgLm0yIHtcbiAgICAvLyBmb250LXNpemU6IDE2cHg7XG4gIH1cbn1cblxuLmludHJvIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBsaW5lLWhlaWdodDogMTlweDtcbiAgY29sb3I6ICM1NDU0NTQ7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1ib3R0b206IDI0cHg7XG59XG5cbi5ncmJ0biB7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSAyNnB4IC0gMjZweCk7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuLmFsbFdyYXAge1xuICBoZWlnaHQ6IGNhbGMoMTAwdmggLSAxMDBweCk7XG59XG4iXX0= */");

/***/ }),

/***/ "./src/app/pages/home-add-howto2/home-add-howto2.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/home-add-howto2/home-add-howto2.page.ts ***!
  \***************************************************************/
/*! exports provided: HomeAddHowto2Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAddHowto2Page", function() { return HomeAddHowto2Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let HomeAddHowto2Page = class HomeAddHowto2Page {
    // how = [
    //   {
    //     step: '2',
    //     suggest: '집을 그리고 나면 새로운 \n 종이 한 장을 세로로 놓고 \n “ 나무를 그리세요.”',
    //   },
    // ];
    constructor(navc) {
        this.navc = navc;
        this.slideOpts = {
            initialSlide: 0,
            speed: 400,
            allowTouchMove: false,
        };
    }
    ngOnInit() { }
    slidenext() {
        this.navc.navigateForward('home-add-howto3');
    }
};
HomeAddHowto2Page.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] }
];
HomeAddHowto2Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-add-howto2',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home-add-howto2.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-howto2/home-add-howto2.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home-add-howto2.page.scss */ "./src/app/pages/home-add-howto2/home-add-howto2.page.scss")).default]
    })
], HomeAddHowto2Page);



/***/ })

}]);
//# sourceMappingURL=pages-home-add-howto2-home-add-howto2-module-es2015.js.map