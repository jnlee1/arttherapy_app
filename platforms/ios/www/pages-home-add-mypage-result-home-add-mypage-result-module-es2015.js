(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-add-mypage-result-home-add-mypage-result-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-mypage-result/home-add-mypage-result.page.html":
/*!*********************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-mypage-result/home-add-mypage-result.page.html ***!
  \*********************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <div id=\"headerInner\">\n    <ion-toolbar>\n      <ion-buttons class=\"back\" slot=\"start\">\n        <ion-back-button text=\"\" icon=\"\"></ion-back-button>\n      </ion-buttons>\n      <ion-title>검사 실시 결과</ion-title>\n    </ion-toolbar>\n\n  </div>\n</ion-header>\n\n<ion-content>\n  <div *ngIf=\"counselor$ | async as counselor\" class=\"wrap\">\n    <ion-slides pager>\n      <ion-slide *ngFor=\"let item of counselor.imageList\">\n        <div (click)=\"imgdetail(item.url)\">\n          <img [src]='item.url' alt=\"\">\n        </div>\n      </ion-slide>\n    </ion-slides>\n    <ion-segment [(ngModel)]=\"viewType\">\n      <ion-segment-button value=\"0\" class=\"v1\">\n        분석결과\n      </ion-segment-button>\n      <ion-segment-button value=\"1\" class=\"v2\">\n        상세결과\n      </ion-segment-button>\n      <ion-segment-button value=\"2\" class=\"v3\">\n        치료가이드\n      </ion-segment-button>\n    </ion-segment>\n\n    <div [ngSwitch]=\"viewType\" class=\"conWrap\">\n      <ion-list class=\"v1Con\" *ngSwitchCase=\"0\">\n        <div>\n          {{counselor?.feedbackId.result}}\n        </div>\n\n      </ion-list>\n\n      <ion-list class=\"v2Con\" *ngSwitchCase=\"1\">\n        <div>\n          {{counselor?.feedbackId.detailResult}}\n        </div>\n\n      </ion-list>\n\n\n      <ion-list class=\"v3Con\" *ngSwitchCase=\"2\">\n        <div>\n          {{counselor?.feedbackId.guide}}\n        </div>\n\n      </ion-list>\n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/home-add-mypage-result/home-add-mypage-result-routing.module.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/home-add-mypage-result/home-add-mypage-result-routing.module.ts ***!
  \***************************************************************************************/
/*! exports provided: HomeAddMypageResultPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAddMypageResultPageRoutingModule", function() { return HomeAddMypageResultPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_add_mypage_result_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home-add-mypage-result.page */ "./src/app/pages/home-add-mypage-result/home-add-mypage-result.page.ts");




const routes = [
    {
        path: '',
        component: _home_add_mypage_result_page__WEBPACK_IMPORTED_MODULE_3__["HomeAddMypageResultPage"]
    }
];
let HomeAddMypageResultPageRoutingModule = class HomeAddMypageResultPageRoutingModule {
};
HomeAddMypageResultPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomeAddMypageResultPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/home-add-mypage-result/home-add-mypage-result.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/home-add-mypage-result/home-add-mypage-result.module.ts ***!
  \*******************************************************************************/
/*! exports provided: HomeAddMypageResultPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAddMypageResultPageModule", function() { return HomeAddMypageResultPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _home_add_mypage_result_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-add-mypage-result-routing.module */ "./src/app/pages/home-add-mypage-result/home-add-mypage-result-routing.module.ts");
/* harmony import */ var _home_add_mypage_result_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-add-mypage-result.page */ "./src/app/pages/home-add-mypage-result/home-add-mypage-result.page.ts");







let HomeAddMypageResultPageModule = class HomeAddMypageResultPageModule {
};
HomeAddMypageResultPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _home_add_mypage_result_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeAddMypageResultPageRoutingModule"]
        ],
        declarations: [_home_add_mypage_result_page__WEBPACK_IMPORTED_MODULE_6__["HomeAddMypageResultPage"]]
    })
], HomeAddMypageResultPageModule);



/***/ }),

/***/ "./src/app/pages/home-add-mypage-result/home-add-mypage-result.page.scss":
/*!*******************************************************************************!*\
  !*** ./src/app/pages/home-add-mypage-result/home-add-mypage-result.page.scss ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-slides {\n  height: 46.46%;\n}\nion-slides div {\n  width: 100%;\n  height: 100%;\n  background: #f8f8f8;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\nion-slides img {\n  width: 100%;\n  height: 100%;\n  -o-object-fit: contain;\n     object-fit: contain;\n}\nion-segment {\n  background: #fff;\n  border-radius: 0;\n  height: 43px;\n  --ripple-color: #fff;\n}\nion-segment-button::before {\n  display: none;\n}\nion-segment-button {\n  --background: #fff;\n  --background-checked: none;\n  --background-hover: none;\n  --background-hover-opacity: 0;\n  --background-focused: none;\n  --background-focused-opacity: 0;\n  --border-radius: 0;\n  --border-width: 0;\n  --indicator-box-shadow: none;\n  --indicator-color: transparent;\n  --padding-top: 0;\n  --padding-end: 0;\n  --padding-bottom: 0;\n  --padding-start: 0;\n  margin-top: 0;\n  margin-bottom: 0;\n  position: relative;\n  flex-basis: 0px;\n  flex-direction: row;\n  min-width: 33.33%;\n  min-height: 43px;\n  font-size: 15px;\n  font-weight: 600;\n  line-height: 43px;\n  color: #161616;\n}\n.v1.segment-button-checked {\n  --background-checked: #c3e2dd;\n}\n.v2.segment-button-checked {\n  --background-checked: #e5c1c5;\n}\n.v3.segment-button-checked {\n  --background-checked: #f2eee5;\n}\n.v1Con {\n  height: 100%;\n  background: #c3e2dd;\n  padding: 4px;\n}\n.v2Con {\n  height: 100%;\n  background: #e5c1c5;\n  padding: 4px;\n}\n.v3Con {\n  height: 100%;\n  background: #f2eee5;\n  padding: 4px;\n}\nion-list > div {\n  white-space: pre-line;\n  background: #ffffff;\n  padding: 20px;\n  color: #161616;\n  font-weight: 600;\n  font-size: 15px;\n  line-height: 28px;\n  letter-spacing: -0.28px;\n  height: 100%;\n  overflow: auto;\n}\n.conWrap {\n  height: calc(100% - 43px - 46.46%);\n  overflow: auto;\n}\n.top {\n  min-height: 52.44%;\n}\n.wrap {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtbXlwYWdlLXJlc3VsdC9ob21lLWFkZC1teXBhZ2UtcmVzdWx0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGNBQUE7QUFDRjtBQUNFO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBQ0o7QUFDRTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7S0FBQSxtQkFBQTtBQUNKO0FBR0E7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLG9CQUFBO0FBQUY7QUFFQTtFQUNFLGFBQUE7QUFDRjtBQUNBO0VBQ0Usa0JBQUE7RUFDQSwwQkFBQTtFQUNBLHdCQUFBO0VBQ0EsNkJBQUE7RUFDQSwwQkFBQTtFQUNBLCtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLDRCQUFBO0VBQ0EsOEJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQUVGO0FBQ0E7RUFDRSw2QkFBQTtBQUVGO0FBQ0E7RUFDRSw2QkFBQTtBQUVGO0FBQ0E7RUFDRSw2QkFBQTtBQUVGO0FBQ0E7RUFDRSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FBRUY7QUFDQTtFQUNFLFlBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUFFRjtBQUNBO0VBQ0UsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQUVGO0FBQ0E7RUFDRSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7QUFFRjtBQUNBO0VBQ0Usa0NBQUE7RUFDQSxjQUFBO0FBRUY7QUFDQTtFQUNFLGtCQUFBO0FBRUY7QUFDQTtFQUNFLFlBQUE7QUFFRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUtYWRkLW15cGFnZS1yZXN1bHQvaG9tZS1hZGQtbXlwYWdlLXJlc3VsdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tc2xpZGVzIHtcbiAgaGVpZ2h0OiA0Ni40NiU7XG5cbiAgZGl2IHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgYmFja2dyb3VuZDogI2Y4ZjhmODtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIH1cbiAgaW1nIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgb2JqZWN0LWZpdDogY29udGFpbjtcbiAgfVxufVxuXG5pb24tc2VnbWVudCB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIGhlaWdodDogNDNweDtcbiAgLS1yaXBwbGUtY29sb3I6ICNmZmY7XG59XG5pb24tc2VnbWVudC1idXR0b246OmJlZm9yZSB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5pb24tc2VnbWVudC1idXR0b24ge1xuICAtLWJhY2tncm91bmQ6ICNmZmY7XG4gIC0tYmFja2dyb3VuZC1jaGVja2VkOiBub25lO1xuICAtLWJhY2tncm91bmQtaG92ZXI6IG5vbmU7XG4gIC0tYmFja2dyb3VuZC1ob3Zlci1vcGFjaXR5OiAwO1xuICAtLWJhY2tncm91bmQtZm9jdXNlZDogbm9uZTtcbiAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQtb3BhY2l0eTogMDtcbiAgLS1ib3JkZXItcmFkaXVzOiAwO1xuICAtLWJvcmRlci13aWR0aDogMDtcbiAgLS1pbmRpY2F0b3ItYm94LXNoYWRvdzogbm9uZTtcbiAgLS1pbmRpY2F0b3ItY29sb3I6IHRyYW5zcGFyZW50O1xuICAtLXBhZGRpbmctdG9wOiAwO1xuICAtLXBhZGRpbmctZW5kOiAwO1xuICAtLXBhZGRpbmctYm90dG9tOiAwO1xuICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gIG1hcmdpbi10b3A6IDA7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZmxleC1iYXNpczogMHB4O1xuICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICBtaW4td2lkdGg6IDMzLjMzJTtcbiAgbWluLWhlaWdodDogNDNweDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBsaW5lLWhlaWdodDogNDNweDtcbiAgY29sb3I6ICMxNjE2MTY7XG59XG5cbi52MS5zZWdtZW50LWJ1dHRvbi1jaGVja2VkIHtcbiAgLS1iYWNrZ3JvdW5kLWNoZWNrZWQ6ICNjM2UyZGQ7XG59XG5cbi52Mi5zZWdtZW50LWJ1dHRvbi1jaGVja2VkIHtcbiAgLS1iYWNrZ3JvdW5kLWNoZWNrZWQ6ICNlNWMxYzU7XG59XG5cbi52My5zZWdtZW50LWJ1dHRvbi1jaGVja2VkIHtcbiAgLS1iYWNrZ3JvdW5kLWNoZWNrZWQ6ICNmMmVlZTU7XG59XG5cbi52MUNvbiB7XG4gIGhlaWdodDogMTAwJTtcbiAgYmFja2dyb3VuZDogI2MzZTJkZDtcbiAgcGFkZGluZzogNHB4O1xufVxuXG4udjJDb24ge1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQ6ICNlNWMxYzU7XG4gIHBhZGRpbmc6IDRweDtcbn1cblxuLnYzQ29uIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICBiYWNrZ3JvdW5kOiAjZjJlZWU1O1xuICBwYWRkaW5nOiA0cHg7XG59XG5cbmlvbi1saXN0ID4gZGl2IHtcbiAgd2hpdGUtc3BhY2U6IHByZS1saW5lO1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICBwYWRkaW5nOiAyMHB4O1xuICBjb2xvcjogIzE2MTYxNjtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBsaW5lLWhlaWdodDogMjhweDtcbiAgbGV0dGVyLXNwYWNpbmc6IC0wLjI4cHg7XG4gIGhlaWdodDogMTAwJTtcbiAgb3ZlcmZsb3c6IGF1dG87XG59XG5cbi5jb25XcmFwIHtcbiAgaGVpZ2h0OiBjYWxjKDEwMCUgLSA0M3B4IC0gNDYuNDYlKTtcbiAgb3ZlcmZsb3c6IGF1dG87XG59XG5cbi50b3Age1xuICBtaW4taGVpZ2h0OiA1Mi40NCU7XG59XG5cbi53cmFwIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuIl19 */");

/***/ }),

/***/ "./src/app/pages/home-add-mypage-result/home-add-mypage-result.page.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/pages/home-add-mypage-result/home-add-mypage-result.page.ts ***!
  \*****************************************************************************/
/*! exports provided: HomeAddMypageResultPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAddMypageResultPage", function() { return HomeAddMypageResultPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_db_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../services/db.service */ "./src/services/db.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _image_detail_image_detail_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../image-detail/image-detail.page */ "./src/app/pages/image-detail/image-detail.page.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");








let HomeAddMypageResultPage = class HomeAddMypageResultPage {
    constructor(db, modalController, router) {
        this.db = db;
        this.modalController = modalController;
        this.router = router;
        this.viewType = 0;
        this.router.queryParams.subscribe((params) => {
            this.counselorId = params["counselorId"];
            this.getData();
        });
    }
    ngOnInit() { }
    getData() {
        this.counselor$ = this.db.collection$(`counselor`, ref => ref.where('counselorId', '==', this.counselorId))
            .pipe(Object(_services_db_service__WEBPACK_IMPORTED_MODULE_1__["leftJoinDocument"])(this.db.afs, 'feedbackId', 'feedback'))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(datas => {
            if (datas) {
                console.log(datas[0]);
                return datas[0];
            }
        }));
    }
    imgdetail(url) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _image_detail_image_detail_page__WEBPACK_IMPORTED_MODULE_5__["ImageDetailPage"],
                cssClass: 'my-custom-class',
                componentProps: {
                    url,
                },
            });
            return yield modal.present();
        });
    }
};
HomeAddMypageResultPage.ctorParameters = () => [
    { type: _services_db_service__WEBPACK_IMPORTED_MODULE_1__["DbService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
HomeAddMypageResultPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-home-add-mypage-result',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home-add-mypage-result.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-mypage-result/home-add-mypage-result.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home-add-mypage-result.page.scss */ "./src/app/pages/home-add-mypage-result/home-add-mypage-result.page.scss")).default]
    })
], HomeAddMypageResultPage);



/***/ })

}]);
//# sourceMappingURL=pages-home-add-mypage-result-home-add-mypage-result-module-es2015.js.map