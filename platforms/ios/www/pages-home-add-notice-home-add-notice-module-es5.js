(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-add-notice-home-add-notice-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-notice/home-add-notice.page.html":
    /*!*******************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-notice/home-add-notice.page.html ***!
      \*******************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesHomeAddNoticeHomeAddNoticePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <div id=\"headerInner\">\n    <ion-toolbar>\n      <ion-buttons class=\"back\" slot=\"start\">\n        <ion-back-button text=\"\" icon=\"\"></ion-back-button>\n      </ion-buttons>\n      <ion-title>알림내역</ion-title>\n      <ion-buttons slot=\"end\">\n        <ion-button (click)=\"gosetting()\">\n          <img src=\"assets/imgs/setting.png\" alt=\"\">\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n\n  </div>\n</ion-header>\n\n<ion-content>\n  <div *ngIf=\"alarms$ | async as alarms\">\n    <div *ngIf=\"alarms.length > 0\">\n      <ion-list>\n        <div class=\"notice\" *ngFor=\"let item of alarms\" (click)=\"goMypage()\">\n          <div class=\"ntWrap\">\n            <div *ngIf=\"!item.check\">\n              <ion-badge>N</ion-badge>\n            </div>\n            <h2>\n              {{item.text}}\n            </h2>\n          </div>\n          <span>\n            {{item.dateCreated | date : 'yyyy.MM.dd'}}\n          </span>\n        </div>\n      </ion-list>\n    </div>\n    <div class=\"none\" *ngIf=\"alarms.length == 0\">\n      알림 메세지가 없습니다.\n    </div>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./node_modules/rxjs/internal/Observable.js":
    /*!**************************************************!*\
      !*** ./node_modules/rxjs/internal/Observable.js ***!
      \**************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalObservableJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var canReportError_1 = __webpack_require__(
      /*! ./util/canReportError */
      "./node_modules/rxjs/internal/util/canReportError.js");

      var toSubscriber_1 = __webpack_require__(
      /*! ./util/toSubscriber */
      "./node_modules/rxjs/internal/util/toSubscriber.js");

      var observable_1 = __webpack_require__(
      /*! ./symbol/observable */
      "./node_modules/rxjs/internal/symbol/observable.js");

      var pipe_1 = __webpack_require__(
      /*! ./util/pipe */
      "./node_modules/rxjs/internal/util/pipe.js");

      var config_1 = __webpack_require__(
      /*! ./config */
      "./node_modules/rxjs/internal/config.js");

      var Observable = function () {
        function Observable(subscribe) {
          this._isScalar = false;

          if (subscribe) {
            this._subscribe = subscribe;
          }
        }

        Observable.prototype.lift = function (operator) {
          var observable = new Observable();
          observable.source = this;
          observable.operator = operator;
          return observable;
        };

        Observable.prototype.subscribe = function (observerOrNext, error, complete) {
          var operator = this.operator;
          var sink = toSubscriber_1.toSubscriber(observerOrNext, error, complete);

          if (operator) {
            sink.add(operator.call(sink, this.source));
          } else {
            sink.add(this.source || config_1.config.useDeprecatedSynchronousErrorHandling && !sink.syncErrorThrowable ? this._subscribe(sink) : this._trySubscribe(sink));
          }

          if (config_1.config.useDeprecatedSynchronousErrorHandling) {
            if (sink.syncErrorThrowable) {
              sink.syncErrorThrowable = false;

              if (sink.syncErrorThrown) {
                throw sink.syncErrorValue;
              }
            }
          }

          return sink;
        };

        Observable.prototype._trySubscribe = function (sink) {
          try {
            return this._subscribe(sink);
          } catch (err) {
            if (config_1.config.useDeprecatedSynchronousErrorHandling) {
              sink.syncErrorThrown = true;
              sink.syncErrorValue = err;
            }

            if (canReportError_1.canReportError(sink)) {
              sink.error(err);
            } else {
              console.warn(err);
            }
          }
        };

        Observable.prototype.forEach = function (next, promiseCtor) {
          var _this = this;

          promiseCtor = getPromiseCtor(promiseCtor);
          return new promiseCtor(function (resolve, reject) {
            var subscription;
            subscription = _this.subscribe(function (value) {
              try {
                next(value);
              } catch (err) {
                reject(err);

                if (subscription) {
                  subscription.unsubscribe();
                }
              }
            }, reject, resolve);
          });
        };

        Observable.prototype._subscribe = function (subscriber) {
          var source = this.source;
          return source && source.subscribe(subscriber);
        };

        Observable.prototype[observable_1.observable] = function () {
          return this;
        };

        Observable.prototype.pipe = function () {
          var operations = [];

          for (var _i = 0; _i < arguments.length; _i++) {
            operations[_i] = arguments[_i];
          }

          if (operations.length === 0) {
            return this;
          }

          return pipe_1.pipeFromArray(operations)(this);
        };

        Observable.prototype.toPromise = function (promiseCtor) {
          var _this = this;

          promiseCtor = getPromiseCtor(promiseCtor);
          return new promiseCtor(function (resolve, reject) {
            var value;

            _this.subscribe(function (x) {
              return value = x;
            }, function (err) {
              return reject(err);
            }, function () {
              return resolve(value);
            });
          });
        };

        Observable.create = function (subscribe) {
          return new Observable(subscribe);
        };

        return Observable;
      }();

      exports.Observable = Observable;

      function getPromiseCtor(promiseCtor) {
        if (!promiseCtor) {
          promiseCtor = config_1.config.Promise || Promise;
        }

        if (!promiseCtor) {
          throw new Error('no Promise impl found');
        }

        return promiseCtor;
      } //# sourceMappingURL=Observable.js.map

      /***/

    },

    /***/
    "./node_modules/rxjs/internal/Observer.js":
    /*!************************************************!*\
      !*** ./node_modules/rxjs/internal/Observer.js ***!
      \************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalObserverJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var config_1 = __webpack_require__(
      /*! ./config */
      "./node_modules/rxjs/internal/config.js");

      var hostReportError_1 = __webpack_require__(
      /*! ./util/hostReportError */
      "./node_modules/rxjs/internal/util/hostReportError.js");

      exports.empty = {
        closed: true,
        next: function next(value) {},
        error: function error(err) {
          if (config_1.config.useDeprecatedSynchronousErrorHandling) {
            throw err;
          } else {
            hostReportError_1.hostReportError(err);
          }
        },
        complete: function complete() {}
      }; //# sourceMappingURL=Observer.js.map

      /***/
    },

    /***/
    "./node_modules/rxjs/internal/Subscriber.js":
    /*!**************************************************!*\
      !*** ./node_modules/rxjs/internal/Subscriber.js ***!
      \**************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalSubscriberJs(module, exports, __webpack_require__) {
      "use strict";

      var __extends = this && this.__extends || function () {
        var _extendStatics = function extendStatics(d, b) {
          _extendStatics = Object.setPrototypeOf || {
            __proto__: []
          } instanceof Array && function (d, b) {
            d.__proto__ = b;
          } || function (d, b) {
            for (var p in b) {
              if (b.hasOwnProperty(p)) d[p] = b[p];
            }
          };

          return _extendStatics(d, b);
        };

        return function (d, b) {
          _extendStatics(d, b);

          function __() {
            this.constructor = d;
          }

          d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
      }();

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var isFunction_1 = __webpack_require__(
      /*! ./util/isFunction */
      "./node_modules/rxjs/internal/util/isFunction.js");

      var Observer_1 = __webpack_require__(
      /*! ./Observer */
      "./node_modules/rxjs/internal/Observer.js");

      var Subscription_1 = __webpack_require__(
      /*! ./Subscription */
      "./node_modules/rxjs/internal/Subscription.js");

      var rxSubscriber_1 = __webpack_require__(
      /*! ../internal/symbol/rxSubscriber */
      "./node_modules/rxjs/internal/symbol/rxSubscriber.js");

      var config_1 = __webpack_require__(
      /*! ./config */
      "./node_modules/rxjs/internal/config.js");

      var hostReportError_1 = __webpack_require__(
      /*! ./util/hostReportError */
      "./node_modules/rxjs/internal/util/hostReportError.js");

      var Subscriber = function (_super) {
        __extends(Subscriber, _super);

        function Subscriber(destinationOrNext, error, complete) {
          var _this = _super.call(this) || this;

          _this.syncErrorValue = null;
          _this.syncErrorThrown = false;
          _this.syncErrorThrowable = false;
          _this.isStopped = false;

          switch (arguments.length) {
            case 0:
              _this.destination = Observer_1.empty;
              break;

            case 1:
              if (!destinationOrNext) {
                _this.destination = Observer_1.empty;
                break;
              }

              if (typeof destinationOrNext === 'object') {
                if (destinationOrNext instanceof Subscriber) {
                  _this.syncErrorThrowable = destinationOrNext.syncErrorThrowable;
                  _this.destination = destinationOrNext;
                  destinationOrNext.add(_this);
                } else {
                  _this.syncErrorThrowable = true;
                  _this.destination = new SafeSubscriber(_this, destinationOrNext);
                }

                break;
              }

            default:
              _this.syncErrorThrowable = true;
              _this.destination = new SafeSubscriber(_this, destinationOrNext, error, complete);
              break;
          }

          return _this;
        }

        Subscriber.prototype[rxSubscriber_1.rxSubscriber] = function () {
          return this;
        };

        Subscriber.create = function (next, error, complete) {
          var subscriber = new Subscriber(next, error, complete);
          subscriber.syncErrorThrowable = false;
          return subscriber;
        };

        Subscriber.prototype.next = function (value) {
          if (!this.isStopped) {
            this._next(value);
          }
        };

        Subscriber.prototype.error = function (err) {
          if (!this.isStopped) {
            this.isStopped = true;

            this._error(err);
          }
        };

        Subscriber.prototype.complete = function () {
          if (!this.isStopped) {
            this.isStopped = true;

            this._complete();
          }
        };

        Subscriber.prototype.unsubscribe = function () {
          if (this.closed) {
            return;
          }

          this.isStopped = true;

          _super.prototype.unsubscribe.call(this);
        };

        Subscriber.prototype._next = function (value) {
          this.destination.next(value);
        };

        Subscriber.prototype._error = function (err) {
          this.destination.error(err);
          this.unsubscribe();
        };

        Subscriber.prototype._complete = function () {
          this.destination.complete();
          this.unsubscribe();
        };

        Subscriber.prototype._unsubscribeAndRecycle = function () {
          var _parentOrParents = this._parentOrParents;
          this._parentOrParents = null;
          this.unsubscribe();
          this.closed = false;
          this.isStopped = false;
          this._parentOrParents = _parentOrParents;
          return this;
        };

        return Subscriber;
      }(Subscription_1.Subscription);

      exports.Subscriber = Subscriber;

      var SafeSubscriber = function (_super) {
        __extends(SafeSubscriber, _super);

        function SafeSubscriber(_parentSubscriber, observerOrNext, error, complete) {
          var _this = _super.call(this) || this;

          _this._parentSubscriber = _parentSubscriber;
          var next;
          var context = _this;

          if (isFunction_1.isFunction(observerOrNext)) {
            next = observerOrNext;
          } else if (observerOrNext) {
            next = observerOrNext.next;
            error = observerOrNext.error;
            complete = observerOrNext.complete;

            if (observerOrNext !== Observer_1.empty) {
              context = Object.create(observerOrNext);

              if (isFunction_1.isFunction(context.unsubscribe)) {
                _this.add(context.unsubscribe.bind(context));
              }

              context.unsubscribe = _this.unsubscribe.bind(_this);
            }
          }

          _this._context = context;
          _this._next = next;
          _this._error = error;
          _this._complete = complete;
          return _this;
        }

        SafeSubscriber.prototype.next = function (value) {
          if (!this.isStopped && this._next) {
            var _parentSubscriber = this._parentSubscriber;

            if (!config_1.config.useDeprecatedSynchronousErrorHandling || !_parentSubscriber.syncErrorThrowable) {
              this.__tryOrUnsub(this._next, value);
            } else if (this.__tryOrSetError(_parentSubscriber, this._next, value)) {
              this.unsubscribe();
            }
          }
        };

        SafeSubscriber.prototype.error = function (err) {
          if (!this.isStopped) {
            var _parentSubscriber = this._parentSubscriber;
            var useDeprecatedSynchronousErrorHandling = config_1.config.useDeprecatedSynchronousErrorHandling;

            if (this._error) {
              if (!useDeprecatedSynchronousErrorHandling || !_parentSubscriber.syncErrorThrowable) {
                this.__tryOrUnsub(this._error, err);

                this.unsubscribe();
              } else {
                this.__tryOrSetError(_parentSubscriber, this._error, err);

                this.unsubscribe();
              }
            } else if (!_parentSubscriber.syncErrorThrowable) {
              this.unsubscribe();

              if (useDeprecatedSynchronousErrorHandling) {
                throw err;
              }

              hostReportError_1.hostReportError(err);
            } else {
              if (useDeprecatedSynchronousErrorHandling) {
                _parentSubscriber.syncErrorValue = err;
                _parentSubscriber.syncErrorThrown = true;
              } else {
                hostReportError_1.hostReportError(err);
              }

              this.unsubscribe();
            }
          }
        };

        SafeSubscriber.prototype.complete = function () {
          var _this = this;

          if (!this.isStopped) {
            var _parentSubscriber = this._parentSubscriber;

            if (this._complete) {
              var wrappedComplete = function wrappedComplete() {
                return _this._complete.call(_this._context);
              };

              if (!config_1.config.useDeprecatedSynchronousErrorHandling || !_parentSubscriber.syncErrorThrowable) {
                this.__tryOrUnsub(wrappedComplete);

                this.unsubscribe();
              } else {
                this.__tryOrSetError(_parentSubscriber, wrappedComplete);

                this.unsubscribe();
              }
            } else {
              this.unsubscribe();
            }
          }
        };

        SafeSubscriber.prototype.__tryOrUnsub = function (fn, value) {
          try {
            fn.call(this._context, value);
          } catch (err) {
            this.unsubscribe();

            if (config_1.config.useDeprecatedSynchronousErrorHandling) {
              throw err;
            } else {
              hostReportError_1.hostReportError(err);
            }
          }
        };

        SafeSubscriber.prototype.__tryOrSetError = function (parent, fn, value) {
          if (!config_1.config.useDeprecatedSynchronousErrorHandling) {
            throw new Error('bad call');
          }

          try {
            fn.call(this._context, value);
          } catch (err) {
            if (config_1.config.useDeprecatedSynchronousErrorHandling) {
              parent.syncErrorValue = err;
              parent.syncErrorThrown = true;
              return true;
            } else {
              hostReportError_1.hostReportError(err);
              return true;
            }
          }

          return false;
        };

        SafeSubscriber.prototype._unsubscribe = function () {
          var _parentSubscriber = this._parentSubscriber;
          this._context = null;
          this._parentSubscriber = null;

          _parentSubscriber.unsubscribe();
        };

        return SafeSubscriber;
      }(Subscriber);

      exports.SafeSubscriber = SafeSubscriber; //# sourceMappingURL=Subscriber.js.map

      /***/
    },

    /***/
    "./node_modules/rxjs/internal/Subscription.js":
    /*!****************************************************!*\
      !*** ./node_modules/rxjs/internal/Subscription.js ***!
      \****************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalSubscriptionJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var isArray_1 = __webpack_require__(
      /*! ./util/isArray */
      "./node_modules/rxjs/internal/util/isArray.js");

      var isObject_1 = __webpack_require__(
      /*! ./util/isObject */
      "./node_modules/rxjs/internal/util/isObject.js");

      var isFunction_1 = __webpack_require__(
      /*! ./util/isFunction */
      "./node_modules/rxjs/internal/util/isFunction.js");

      var UnsubscriptionError_1 = __webpack_require__(
      /*! ./util/UnsubscriptionError */
      "./node_modules/rxjs/internal/util/UnsubscriptionError.js");

      var Subscription = function () {
        function Subscription(unsubscribe) {
          this.closed = false;
          this._parentOrParents = null;
          this._subscriptions = null;

          if (unsubscribe) {
            this._unsubscribe = unsubscribe;
          }
        }

        Subscription.prototype.unsubscribe = function () {
          var errors;

          if (this.closed) {
            return;
          }

          var _a = this,
              _parentOrParents = _a._parentOrParents,
              _unsubscribe = _a._unsubscribe,
              _subscriptions = _a._subscriptions;

          this.closed = true;
          this._parentOrParents = null;
          this._subscriptions = null;

          if (_parentOrParents instanceof Subscription) {
            _parentOrParents.remove(this);
          } else if (_parentOrParents !== null) {
            for (var index = 0; index < _parentOrParents.length; ++index) {
              var parent_1 = _parentOrParents[index];
              parent_1.remove(this);
            }
          }

          if (isFunction_1.isFunction(_unsubscribe)) {
            try {
              _unsubscribe.call(this);
            } catch (e) {
              errors = e instanceof UnsubscriptionError_1.UnsubscriptionError ? flattenUnsubscriptionErrors(e.errors) : [e];
            }
          }

          if (isArray_1.isArray(_subscriptions)) {
            var index = -1;
            var len = _subscriptions.length;

            while (++index < len) {
              var sub = _subscriptions[index];

              if (isObject_1.isObject(sub)) {
                try {
                  sub.unsubscribe();
                } catch (e) {
                  errors = errors || [];

                  if (e instanceof UnsubscriptionError_1.UnsubscriptionError) {
                    errors = errors.concat(flattenUnsubscriptionErrors(e.errors));
                  } else {
                    errors.push(e);
                  }
                }
              }
            }
          }

          if (errors) {
            throw new UnsubscriptionError_1.UnsubscriptionError(errors);
          }
        };

        Subscription.prototype.add = function (teardown) {
          var subscription = teardown;

          if (!teardown) {
            return Subscription.EMPTY;
          }

          switch (typeof teardown) {
            case 'function':
              subscription = new Subscription(teardown);

            case 'object':
              if (subscription === this || subscription.closed || typeof subscription.unsubscribe !== 'function') {
                return subscription;
              } else if (this.closed) {
                subscription.unsubscribe();
                return subscription;
              } else if (!(subscription instanceof Subscription)) {
                var tmp = subscription;
                subscription = new Subscription();
                subscription._subscriptions = [tmp];
              }

              break;

            default:
              {
                throw new Error('unrecognized teardown ' + teardown + ' added to Subscription.');
              }
          }

          var _parentOrParents = subscription._parentOrParents;

          if (_parentOrParents === null) {
            subscription._parentOrParents = this;
          } else if (_parentOrParents instanceof Subscription) {
            if (_parentOrParents === this) {
              return subscription;
            }

            subscription._parentOrParents = [_parentOrParents, this];
          } else if (_parentOrParents.indexOf(this) === -1) {
            _parentOrParents.push(this);
          } else {
            return subscription;
          }

          var subscriptions = this._subscriptions;

          if (subscriptions === null) {
            this._subscriptions = [subscription];
          } else {
            subscriptions.push(subscription);
          }

          return subscription;
        };

        Subscription.prototype.remove = function (subscription) {
          var subscriptions = this._subscriptions;

          if (subscriptions) {
            var subscriptionIndex = subscriptions.indexOf(subscription);

            if (subscriptionIndex !== -1) {
              subscriptions.splice(subscriptionIndex, 1);
            }
          }
        };

        Subscription.EMPTY = function (empty) {
          empty.closed = true;
          return empty;
        }(new Subscription());

        return Subscription;
      }();

      exports.Subscription = Subscription;

      function flattenUnsubscriptionErrors(errors) {
        return errors.reduce(function (errs, err) {
          return errs.concat(err instanceof UnsubscriptionError_1.UnsubscriptionError ? err.errors : err);
        }, []);
      } //# sourceMappingURL=Subscription.js.map

      /***/

    },

    /***/
    "./node_modules/rxjs/internal/config.js":
    /*!**********************************************!*\
      !*** ./node_modules/rxjs/internal/config.js ***!
      \**********************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalConfigJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });
      var _enable_super_gross_mode_that_will_cause_bad_things = false;
      exports.config = {
        Promise: undefined,

        set useDeprecatedSynchronousErrorHandling(value) {
          if (value) {
            var error = new Error();
            console.warn('DEPRECATED! RxJS was set to use deprecated synchronous error handling behavior by code at: \n' + error.stack);
          } else if (_enable_super_gross_mode_that_will_cause_bad_things) {
            console.log('RxJS: Back to a better error behavior. Thank you. <3');
          }

          _enable_super_gross_mode_that_will_cause_bad_things = value;
        },

        get useDeprecatedSynchronousErrorHandling() {
          return _enable_super_gross_mode_that_will_cause_bad_things;
        }

      }; //# sourceMappingURL=config.js.map

      /***/
    },

    /***/
    "./node_modules/rxjs/internal/observable/empty.js":
    /*!********************************************************!*\
      !*** ./node_modules/rxjs/internal/observable/empty.js ***!
      \********************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalObservableEmptyJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var Observable_1 = __webpack_require__(
      /*! ../Observable */
      "./node_modules/rxjs/internal/Observable.js");

      exports.EMPTY = new Observable_1.Observable(function (subscriber) {
        return subscriber.complete();
      });

      function empty(scheduler) {
        return scheduler ? emptyScheduled(scheduler) : exports.EMPTY;
      }

      exports.empty = empty;

      function emptyScheduled(scheduler) {
        return new Observable_1.Observable(function (subscriber) {
          return scheduler.schedule(function () {
            return subscriber.complete();
          });
        });
      } //# sourceMappingURL=empty.js.map

      /***/

    },

    /***/
    "./node_modules/rxjs/internal/operators/take.js":
    /*!******************************************************!*\
      !*** ./node_modules/rxjs/internal/operators/take.js ***!
      \******************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalOperatorsTakeJs(module, exports, __webpack_require__) {
      "use strict";

      var __extends = this && this.__extends || function () {
        var _extendStatics2 = function extendStatics(d, b) {
          _extendStatics2 = Object.setPrototypeOf || {
            __proto__: []
          } instanceof Array && function (d, b) {
            d.__proto__ = b;
          } || function (d, b) {
            for (var p in b) {
              if (b.hasOwnProperty(p)) d[p] = b[p];
            }
          };

          return _extendStatics2(d, b);
        };

        return function (d, b) {
          _extendStatics2(d, b);

          function __() {
            this.constructor = d;
          }

          d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
        };
      }();

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var Subscriber_1 = __webpack_require__(
      /*! ../Subscriber */
      "./node_modules/rxjs/internal/Subscriber.js");

      var ArgumentOutOfRangeError_1 = __webpack_require__(
      /*! ../util/ArgumentOutOfRangeError */
      "./node_modules/rxjs/internal/util/ArgumentOutOfRangeError.js");

      var empty_1 = __webpack_require__(
      /*! ../observable/empty */
      "./node_modules/rxjs/internal/observable/empty.js");

      function take(count) {
        return function (source) {
          if (count === 0) {
            return empty_1.empty();
          } else {
            return source.lift(new TakeOperator(count));
          }
        };
      }

      exports.take = take;

      var TakeOperator = function () {
        function TakeOperator(total) {
          this.total = total;

          if (this.total < 0) {
            throw new ArgumentOutOfRangeError_1.ArgumentOutOfRangeError();
          }
        }

        TakeOperator.prototype.call = function (subscriber, source) {
          return source.subscribe(new TakeSubscriber(subscriber, this.total));
        };

        return TakeOperator;
      }();

      var TakeSubscriber = function (_super) {
        __extends(TakeSubscriber, _super);

        function TakeSubscriber(destination, total) {
          var _this = _super.call(this, destination) || this;

          _this.total = total;
          _this.count = 0;
          return _this;
        }

        TakeSubscriber.prototype._next = function (value) {
          var total = this.total;
          var count = ++this.count;

          if (count <= total) {
            this.destination.next(value);

            if (count === total) {
              this.destination.complete();
              this.unsubscribe();
            }
          }
        };

        return TakeSubscriber;
      }(Subscriber_1.Subscriber); //# sourceMappingURL=take.js.map

      /***/

    },

    /***/
    "./node_modules/rxjs/internal/symbol/observable.js":
    /*!*********************************************************!*\
      !*** ./node_modules/rxjs/internal/symbol/observable.js ***!
      \*********************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalSymbolObservableJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports.observable = function () {
        return typeof Symbol === 'function' && Symbol.observable || '@@observable';
      }(); //# sourceMappingURL=observable.js.map

      /***/

    },

    /***/
    "./node_modules/rxjs/internal/symbol/rxSubscriber.js":
    /*!***********************************************************!*\
      !*** ./node_modules/rxjs/internal/symbol/rxSubscriber.js ***!
      \***********************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalSymbolRxSubscriberJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports.rxSubscriber = function () {
        return typeof Symbol === 'function' ? Symbol('rxSubscriber') : '@@rxSubscriber_' + Math.random();
      }();

      exports.$$rxSubscriber = exports.rxSubscriber; //# sourceMappingURL=rxSubscriber.js.map

      /***/
    },

    /***/
    "./node_modules/rxjs/internal/util/ArgumentOutOfRangeError.js":
    /*!********************************************************************!*\
      !*** ./node_modules/rxjs/internal/util/ArgumentOutOfRangeError.js ***!
      \********************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalUtilArgumentOutOfRangeErrorJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var ArgumentOutOfRangeErrorImpl = function () {
        function ArgumentOutOfRangeErrorImpl() {
          Error.call(this);
          this.message = 'argument out of range';
          this.name = 'ArgumentOutOfRangeError';
          return this;
        }

        ArgumentOutOfRangeErrorImpl.prototype = Object.create(Error.prototype);
        return ArgumentOutOfRangeErrorImpl;
      }();

      exports.ArgumentOutOfRangeError = ArgumentOutOfRangeErrorImpl; //# sourceMappingURL=ArgumentOutOfRangeError.js.map

      /***/
    },

    /***/
    "./node_modules/rxjs/internal/util/UnsubscriptionError.js":
    /*!****************************************************************!*\
      !*** ./node_modules/rxjs/internal/util/UnsubscriptionError.js ***!
      \****************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalUtilUnsubscriptionErrorJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var UnsubscriptionErrorImpl = function () {
        function UnsubscriptionErrorImpl(errors) {
          Error.call(this);
          this.message = errors ? errors.length + " errors occurred during unsubscription:\n" + errors.map(function (err, i) {
            return i + 1 + ") " + err.toString();
          }).join('\n  ') : '';
          this.name = 'UnsubscriptionError';
          this.errors = errors;
          return this;
        }

        UnsubscriptionErrorImpl.prototype = Object.create(Error.prototype);
        return UnsubscriptionErrorImpl;
      }();

      exports.UnsubscriptionError = UnsubscriptionErrorImpl; //# sourceMappingURL=UnsubscriptionError.js.map

      /***/
    },

    /***/
    "./node_modules/rxjs/internal/util/canReportError.js":
    /*!***********************************************************!*\
      !*** ./node_modules/rxjs/internal/util/canReportError.js ***!
      \***********************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalUtilCanReportErrorJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var Subscriber_1 = __webpack_require__(
      /*! ../Subscriber */
      "./node_modules/rxjs/internal/Subscriber.js");

      function canReportError(observer) {
        while (observer) {
          var _a = observer,
              closed_1 = _a.closed,
              destination = _a.destination,
              isStopped = _a.isStopped;

          if (closed_1 || isStopped) {
            return false;
          } else if (destination && destination instanceof Subscriber_1.Subscriber) {
            observer = destination;
          } else {
            observer = null;
          }
        }

        return true;
      }

      exports.canReportError = canReportError; //# sourceMappingURL=canReportError.js.map

      /***/
    },

    /***/
    "./node_modules/rxjs/internal/util/hostReportError.js":
    /*!************************************************************!*\
      !*** ./node_modules/rxjs/internal/util/hostReportError.js ***!
      \************************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalUtilHostReportErrorJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      function hostReportError(err) {
        setTimeout(function () {
          throw err;
        }, 0);
      }

      exports.hostReportError = hostReportError; //# sourceMappingURL=hostReportError.js.map

      /***/
    },

    /***/
    "./node_modules/rxjs/internal/util/identity.js":
    /*!*****************************************************!*\
      !*** ./node_modules/rxjs/internal/util/identity.js ***!
      \*****************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalUtilIdentityJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      function identity(x) {
        return x;
      }

      exports.identity = identity; //# sourceMappingURL=identity.js.map

      /***/
    },

    /***/
    "./node_modules/rxjs/internal/util/isArray.js":
    /*!****************************************************!*\
      !*** ./node_modules/rxjs/internal/util/isArray.js ***!
      \****************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalUtilIsArrayJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      exports.isArray = function () {
        return Array.isArray || function (x) {
          return x && typeof x.length === 'number';
        };
      }(); //# sourceMappingURL=isArray.js.map

      /***/

    },

    /***/
    "./node_modules/rxjs/internal/util/isFunction.js":
    /*!*******************************************************!*\
      !*** ./node_modules/rxjs/internal/util/isFunction.js ***!
      \*******************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalUtilIsFunctionJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      function isFunction(x) {
        return typeof x === 'function';
      }

      exports.isFunction = isFunction; //# sourceMappingURL=isFunction.js.map

      /***/
    },

    /***/
    "./node_modules/rxjs/internal/util/isObject.js":
    /*!*****************************************************!*\
      !*** ./node_modules/rxjs/internal/util/isObject.js ***!
      \*****************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalUtilIsObjectJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      function isObject(x) {
        return x !== null && typeof x === 'object';
      }

      exports.isObject = isObject; //# sourceMappingURL=isObject.js.map

      /***/
    },

    /***/
    "./node_modules/rxjs/internal/util/pipe.js":
    /*!*************************************************!*\
      !*** ./node_modules/rxjs/internal/util/pipe.js ***!
      \*************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalUtilPipeJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var identity_1 = __webpack_require__(
      /*! ./identity */
      "./node_modules/rxjs/internal/util/identity.js");

      function pipe() {
        var fns = [];

        for (var _i = 0; _i < arguments.length; _i++) {
          fns[_i] = arguments[_i];
        }

        return pipeFromArray(fns);
      }

      exports.pipe = pipe;

      function pipeFromArray(fns) {
        if (fns.length === 0) {
          return identity_1.identity;
        }

        if (fns.length === 1) {
          return fns[0];
        }

        return function piped(input) {
          return fns.reduce(function (prev, fn) {
            return fn(prev);
          }, input);
        };
      }

      exports.pipeFromArray = pipeFromArray; //# sourceMappingURL=pipe.js.map

      /***/
    },

    /***/
    "./node_modules/rxjs/internal/util/toSubscriber.js":
    /*!*********************************************************!*\
      !*** ./node_modules/rxjs/internal/util/toSubscriber.js ***!
      \*********************************************************/

    /*! no static exports found */

    /***/
    function node_modulesRxjsInternalUtilToSubscriberJs(module, exports, __webpack_require__) {
      "use strict";

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      var Subscriber_1 = __webpack_require__(
      /*! ../Subscriber */
      "./node_modules/rxjs/internal/Subscriber.js");

      var rxSubscriber_1 = __webpack_require__(
      /*! ../symbol/rxSubscriber */
      "./node_modules/rxjs/internal/symbol/rxSubscriber.js");

      var Observer_1 = __webpack_require__(
      /*! ../Observer */
      "./node_modules/rxjs/internal/Observer.js");

      function toSubscriber(nextOrObserver, error, complete) {
        if (nextOrObserver) {
          if (nextOrObserver instanceof Subscriber_1.Subscriber) {
            return nextOrObserver;
          }

          if (nextOrObserver[rxSubscriber_1.rxSubscriber]) {
            return nextOrObserver[rxSubscriber_1.rxSubscriber]();
          }
        }

        if (!nextOrObserver && !error && !complete) {
          return new Subscriber_1.Subscriber(Observer_1.empty);
        }

        return new Subscriber_1.Subscriber(nextOrObserver, error, complete);
      }

      exports.toSubscriber = toSubscriber; //# sourceMappingURL=toSubscriber.js.map

      /***/
    },

    /***/
    "./src/app/pages/home-add-notice/home-add-notice-routing.module.ts":
    /*!*************************************************************************!*\
      !*** ./src/app/pages/home-add-notice/home-add-notice-routing.module.ts ***!
      \*************************************************************************/

    /*! exports provided: HomeAddNoticePageRoutingModule */

    /***/
    function srcAppPagesHomeAddNoticeHomeAddNoticeRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddNoticePageRoutingModule", function () {
        return HomeAddNoticePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _home_add_notice_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./home-add-notice.page */
      "./src/app/pages/home-add-notice/home-add-notice.page.ts");

      var routes = [{
        path: '',
        component: _home_add_notice_page__WEBPACK_IMPORTED_MODULE_3__["HomeAddNoticePage"]
      }];

      var HomeAddNoticePageRoutingModule = function HomeAddNoticePageRoutingModule() {
        _classCallCheck(this, HomeAddNoticePageRoutingModule);
      };

      HomeAddNoticePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], HomeAddNoticePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/home-add-notice/home-add-notice.module.ts":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/home-add-notice/home-add-notice.module.ts ***!
      \*****************************************************************/

    /*! exports provided: HomeAddNoticePageModule */

    /***/
    function srcAppPagesHomeAddNoticeHomeAddNoticeModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddNoticePageModule", function () {
        return HomeAddNoticePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _home_add_notice_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./home-add-notice-routing.module */
      "./src/app/pages/home-add-notice/home-add-notice-routing.module.ts");
      /* harmony import */


      var _home_add_notice_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./home-add-notice.page */
      "./src/app/pages/home-add-notice/home-add-notice.page.ts");

      var HomeAddNoticePageModule = function HomeAddNoticePageModule() {
        _classCallCheck(this, HomeAddNoticePageModule);
      };

      HomeAddNoticePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _home_add_notice_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeAddNoticePageRoutingModule"]],
        declarations: [_home_add_notice_page__WEBPACK_IMPORTED_MODULE_6__["HomeAddNoticePage"]]
      })], HomeAddNoticePageModule);
      /***/
    },

    /***/
    "./src/app/pages/home-add-notice/home-add-notice.page.scss":
    /*!*****************************************************************!*\
      !*** ./src/app/pages/home-add-notice/home-add-notice.page.scss ***!
      \*****************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesHomeAddNoticeHomeAddNoticePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ".notice {\n  padding: 1.125rem 1rem;\n  border-bottom: 1px solid #eaeaea;\n}\n.notice h2 {\n  margin: 0;\n  font-size: 0.875rem;\n  font-weight: 600;\n  letter-spacing: -0.2px;\n  line-height: 1.188rem;\n  color: #333333;\n}\n.notice span {\n  font-weight: 600;\n  font-size: 0.75rem;\n  color: #b3b3b3;\n  display: block;\n}\n.notice ion-badge {\n  --background: #f7bd00;\n  --color: #fff;\n  --padding-top: 4px;\n  --padding-end: 4px;\n  --padding-bottom: 4px;\n  --padding-start: 4px;\n  font-size: 0.625rem;\n  border-radius: 50%;\n  min-width: 16px;\n  min-height: 16px;\n  max-height: 16px;\n  max-width: 16px;\n  display: flex;\n  align-items: center;\n  margin-right: 4px;\n}\n.notice .ntWrap {\n  display: flex;\n  align-items: center;\n  margin-bottom: 8px;\n}\nion-list {\n  min-height: 100%;\n}\n.none {\n  height: calc(100vh - 56px);\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  font-size: 1rem;\n  font-weight: 600;\n  color: #333333;\n  letter-spacing: -0.4px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtbm90aWNlL2hvbWUtYWRkLW5vdGljZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxzQkFBQTtFQUNBLGdDQUFBO0FBQ0Y7QUFDRTtFQUNFLFNBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQkFBQTtFQUNBLGNBQUE7QUFDSjtBQUVFO0VBQ0UsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0FBQUo7QUFJRTtFQUNFLHFCQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLG9CQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGlCQUFBO0FBRko7QUFLRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBSEo7QUFNQTtFQUNFLGdCQUFBO0FBSEY7QUFLQTtFQUNFLDBCQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0Esc0JBQUE7QUFGRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUtYWRkLW5vdGljZS9ob21lLWFkZC1ub3RpY2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLm5vdGljZSB7XG4gIHBhZGRpbmc6IDEuMTI1cmVtIDFyZW07XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWFlYWVhO1xuXG4gIGgyIHtcbiAgICBtYXJnaW46IDA7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICAgIGxldHRlci1zcGFjaW5nOiAtMC4ycHg7XG4gICAgbGluZS1oZWlnaHQ6IDEuMTg4cmVtO1xuICAgIGNvbG9yOiAjMzMzMzMzO1xuICB9XG5cbiAgc3BhbiB7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgICBmb250LXNpemU6IDAuNzVyZW07XG4gICAgY29sb3I6ICNiM2IzYjM7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgLy8gcGFkZGluZy1sZWZ0OiAyMHB4O1xuICB9XG5cbiAgaW9uLWJhZGdlIHtcbiAgICAtLWJhY2tncm91bmQ6ICNmN2JkMDA7XG4gICAgLS1jb2xvcjogI2ZmZjtcbiAgICAtLXBhZGRpbmctdG9wOiA0cHg7XG4gICAgLS1wYWRkaW5nLWVuZDogNHB4O1xuICAgIC0tcGFkZGluZy1ib3R0b206IDRweDtcbiAgICAtLXBhZGRpbmctc3RhcnQ6IDRweDtcbiAgICBmb250LXNpemU6IDAuNjI1cmVtO1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICBtaW4td2lkdGg6IDE2cHg7XG4gICAgbWluLWhlaWdodDogMTZweDtcbiAgICBtYXgtaGVpZ2h0OiAxNnB4O1xuICAgIG1heC13aWR0aDogMTZweDtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgbWFyZ2luLXJpZ2h0OiA0cHg7XG4gIH1cblxuICAubnRXcmFwIHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgbWFyZ2luLWJvdHRvbTogOHB4O1xuICB9XG59XG5pb24tbGlzdCB7XG4gIG1pbi1oZWlnaHQ6IDEwMCU7XG59XG4ubm9uZSB7XG4gIGhlaWdodDogY2FsYygxMDB2aCAtIDU2cHgpO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzMzMzMzMztcbiAgbGV0dGVyLXNwYWNpbmc6IC0wLjRweDtcbn1cbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/home-add-notice/home-add-notice.page.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/home-add-notice/home-add-notice.page.ts ***!
      \***************************************************************/

    /*! exports provided: HomeAddNoticePage */

    /***/
    function srcAppPagesHomeAddNoticeHomeAddNoticePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddNoticePage", function () {
        return HomeAddNoticePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_services_db_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/services/db.service */
      "./src/services/db.service.ts");
      /* harmony import */


      var rxjs_internal_operators_take__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! rxjs/internal/operators/take */
      "./node_modules/rxjs/internal/operators/take.js");
      /* harmony import */


      var rxjs_internal_operators_take__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(rxjs_internal_operators_take__WEBPACK_IMPORTED_MODULE_4__);

      var HomeAddNoticePage = /*#__PURE__*/function () {
        function HomeAddNoticePage(navc, toastController, db) {
          _classCallCheck(this, HomeAddNoticePage);

          this.navc = navc;
          this.toastController = toastController;
          this.db = db;
          this.getData();
        }

        _createClass(HomeAddNoticePage, [{
          key: "getData",
          value: function getData() {
            var uid = localStorage.getItem('userId');
            this.alarms$ = this.db.collection$('alarm', function (ref) {
              return ref.where('userId', '==', uid).orderBy('dateCreated', 'desc');
            }).subscribe(function (a) {
              return console.log(a);
            });
            this.alarms$ = this.db.collection$('alarm', function (ref) {
              return ref.where('userId', '==', uid).orderBy('dateCreated', 'desc');
            });
          }
        }, {
          key: "ionViewWillLeave",
          value: function ionViewWillLeave() {
            var _this2 = this;

            this.alarms$.pipe(Object(rxjs_internal_operators_take__WEBPACK_IMPORTED_MODULE_4__["take"])(1)).subscribe(function (datas) {
              var nocheck = datas.filter(function (ele) {
                return !ele.check;
              });
              nocheck.forEach(function (element) {
                _this2.db.updateAt("alarm/".concat(element.id), {
                  check: true
                });
              });
            });
          }
        }, {
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "gosetting",
          value: function gosetting() {
            this.navc.navigateForward(['/home-add-notice-setting']);
          }
        }, {
          key: "goFeedback",
          value: function goFeedback(counselorId) {
            var _this3 = this;

            try {
              this.db.collection$("feedback", function (ref) {
                return ref.where('counselorId', '==', counselorId);
              }).pipe(Object(rxjs_internal_operators_take__WEBPACK_IMPORTED_MODULE_4__["take"])(1)).subscribe(function (feedback) {
                console.log(feedback[0]);

                if (feedback[0].counselorId) {
                  _this3.navc.navigateForward(['/home-add-mypage-result'], {
                    queryParams: {
                      counselorId: feedback[0].counselorId
                    }
                  });
                }
              });
            } catch (e) {
              this.noneFeedback();
            }
          }
        }, {
          key: "noneFeedback",
          value: function noneFeedback() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastController.create({
                        message: "결과서를 찾을 수 없습니다.",
                        duration: 2000
                      });

                    case 2:
                      toast = _context.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "goMypage",
          value: function goMypage() {
            this.navc.navigateForward(['/home-add-mypage']);
          }
        }]);

        return HomeAddNoticePage;
      }();

      HomeAddNoticePage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }, {
          type: src_services_db_service__WEBPACK_IMPORTED_MODULE_3__["DbService"]
        }];
      };

      HomeAddNoticePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-add-notice',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./home-add-notice.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-notice/home-add-notice.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./home-add-notice.page.scss */
        "./src/app/pages/home-add-notice/home-add-notice.page.scss"))["default"]]
      })], HomeAddNoticePage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-home-add-notice-home-add-notice-module-es5.js.map