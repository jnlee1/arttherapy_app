(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-account-signup-signup-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/signup/signup.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/signup/signup.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <div id=\"headerInner\">\n    <ion-toolbar>\n      <ion-buttons class=\"back\" slot=\"start\">\n        <ion-button (click)=\"warning()\">\n          <img src=\"assets/imgs/nav_back.png\" alt=\"\">\n        </ion-button>\n      </ion-buttons>\n      <ion-title>회원가입</ion-title>\n    </ion-toolbar>\n  </div>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <ion-input placeholder=\"이메일을 입력해주세요.\" [(ngModel)]=\"email\" type=\"email\" (ionChange)=\"emailRegularExpression()\">\n    </ion-input>\n    <span class=\"validate\" *ngIf=\"email && !emailExpressionChk\">\n      이메일 형식을 맞춰 입력해주세요.\n    </span>\n    <ion-input placeholder=\"비밀번호를 입력해주세요.\" minlength=\"6\" maxlength=\"20\" [(ngModel)]=\"password\" type=\"password\"\n      (ionChange)=\"pwRegularExpression()\">\n    </ion-input>\n    <span class=\"validate\" *ngIf=\"password && !pwExpressionChk\">\n      비밀번호는 영문+숫자 6글자 이상 입력해주세요.\n    </span>\n    <ion-input placeholder=\"비밀번호를 다시 입력해주세요.\" minlength=\"6\" maxlength=\"20\" [(ngModel)]=\"confirm_password\"\n      type=\"password\" (ionChange)=\"pwRegularExpression()\"></ion-input>\n    <span class=\"validate\" *ngIf=\" confirm_password && matchingPasswords() && !passwordCheck\">\n      비밀번호가 일치하지 않습니다.\n    </span>\n    <!-- <span class=\"validate\" *ngIf=\"confirm_password && !pwExpressionChk\">\n      비밀번호는 영문+숫자 6글자 이상 입력해주세요.\n    </span> -->\n    <ion-input placeholder=\"이름을 입력해주세요.\" minlength=\"2\" maxlength=\"10\" [(ngModel)]=\"nickname\" type=\"text\" #inputId\n      (ionChange)=\"nicknamewRegularExpression($event)\"></ion-input>\n    <span class=\"validate\" *ngIf=\"nickname &&!nicknameExpressionChk\">\n      이름은 2자 이상 10자 이하로 입력해주세요.\n    </span>\n    <ion-select [interfaceOptions]=\"yearOptions\" [(ngModel)]=\"birth\" placeholder=\"출생연도를 선택해주세요.\" okText=\"선택\"\n      cancelText=\"취소\">\n      <ion-select-option *ngFor=\"let year of yearList\">{{year}}</ion-select-option>\n    </ion-select>\n    <span class=\"validate\" *ngIf=\"birth && !birth\">\n      출생연도를 선택해주세요.\n    </span>\n    <ion-input maxlength=\"11\" minlength=\"10\" placeholder=\"핸드폰번호를 입력해주세요.\" type=\"tel\" [(ngModel)]=\"phone\"\n      (ionChange)=\"phoneRegularExpression()\"></ion-input>\n    <span class=\"validate\" *ngIf=\"!phoneExpressionChk\">\n      핸드폰번호 형식을 맞춰 입력해주세요.\n    </span>\n    <div class=\"checkWrap\">\n      <ion-item>\n        <ion-checkbox slot=\"start\" [(ngModel)]=\"agreeAllTerms\" (ionChange)=\"checkAgreeAll(0)\"></ion-checkbox>\n        <ion-label>\n          전체동의\n        </ion-label>\n      </ion-item>\n\n      <ion-item>\n        <div>\n\n          <ion-checkbox slot=\"start\" [(ngModel)]=\"agree.serviceAgree\" (ionChange)=\"checkAgreeAll(1)\"></ion-checkbox>\n          <ion-label>\n            서비스 이용 약관\n          </ion-label>\n        </div>\n        <ion-button fill=\"clear\" (click)=\"service()\">\n          전체보기\n        </ion-button>\n      </ion-item>\n\n      <ion-item>\n        <div>\n          <ion-checkbox slot=\"start\" [(ngModel)]=\"agree.personalAgree\" (ionChange)=\"checkAgreeAll(2)\"></ion-checkbox>\n          <ion-label>\n            개인정보 수집 및 이용\n          </ion-label>\n        </div>\n        <ion-button fill=\"clear\" (click)=\"personal()\">\n          전체보기\n        </ion-button>\n      </ion-item>\n\n      <ion-item>\n        <ion-checkbox slot=\"start\" [(ngModel)]=\"agree.marketing\" (ionChange)=\"checkAgreeAll(3)\"></ion-checkbox>\n        <ion-label>\n          마케팅 수신동의 (선택)\n        </ion-label>\n      </ion-item>\n    </div>\n    <span class=\"validate v2\" *ngIf=\"(!agree.serviceAgree || !agree.personalAgree) && firstChk\">\n      서비스 이용약관, 개인정보 수집 및 이용은 필수 동의사항입니다.\n    </span>\n\n    <ion-button class=\"grbtn\" expand=\"block\"\n      [disabled]=\"!pwExpressionChk || !email || !password || !confirm_password || !nickname || !birth || !phone || !phoneExpressionChk || !nicknameExpressionChk || !passwordCheck || !emailExpressionChk\"\n      (click)=\"goLogin()\">\n      회원가입\n    </ion-button>\n  </ion-list>\n\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/account/signup/signup-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/account/signup/signup-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: SignupPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageRoutingModule", function() { return SignupPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _signup_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./signup.page */ "./src/app/pages/account/signup/signup.page.ts");




const routes = [
    {
        path: '',
        component: _signup_page__WEBPACK_IMPORTED_MODULE_3__["SignupPage"]
    }
];
let SignupPageRoutingModule = class SignupPageRoutingModule {
};
SignupPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], SignupPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/account/signup/signup.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/account/signup/signup.module.ts ***!
  \*******************************************************/
/*! exports provided: SignupPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPageModule", function() { return SignupPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _signup_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./signup-routing.module */ "./src/app/pages/account/signup/signup-routing.module.ts");
/* harmony import */ var _signup_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./signup.page */ "./src/app/pages/account/signup/signup.page.ts");







let SignupPageModule = class SignupPageModule {
};
SignupPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _signup_routing_module__WEBPACK_IMPORTED_MODULE_5__["SignupPageRoutingModule"]
        ],
        declarations: [_signup_page__WEBPACK_IMPORTED_MODULE_6__["SignupPage"]]
    })
], SignupPageModule);



/***/ }),

/***/ "./src/app/pages/account/signup/signup.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/account/signup/signup.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-list {\n  padding: 1.5rem 1.5rem 1.5rem;\n}\nion-list ion-input {\n  margin-bottom: 1rem;\n}\nion-select {\n  border: 1px solid #d4d4d4;\n  border-radius: 4px;\n  --padding-top: 1.063rem;\n  --padding-end: 1rem;\n  --padding-bottom: 1rem;\n  --padding-start: 1.375rem;\n  font-size: 0.875rem;\n  font-weight: 600;\n  color: #333333;\n  --placeholder-color: #909090;\n  --placeholder-opacity: 1;\n  margin-bottom: 1.125rem;\n}\nion-select::after {\n  content: \"\";\n  display: block;\n  width: 24px;\n  height: 24px;\n  background: url('more.png') no-repeat;\n  background-size: 24px;\n  background-position: center;\n  margin-right: 2px;\n}\nion-select::part(icon) {\n  display: none;\n}\nion-checkbox {\n  --border-radius: 2px;\n  --border-width: 1px;\n  --border-style: solid;\n  --border-color: #e8e8e8;\n  --background: #fff;\n  --size: 1.125rem;\n  width: 1.125rem;\n  height: 1.125rem;\n  margin: 0;\n  margin-right: 0.6875rem;\n  --background-checked: center / cover no-repeat url('chbg.png');\n  --border-color-checked: transparent;\n  --checkmark-color: transparent;\n}\nion-item {\n  --padding-top: 0.6875rem;\n  --padding-bottom: 0.6875rem;\n}\nion-item div {\n  display: flex;\n  align-items: center;\n}\nion-item .button-clear {\n  position: absolute;\n  right: 0;\n  z-index: 999;\n  font-size: 0.75rem;\n  --color: #999999;\n  -webkit-text-decoration-line: underline;\n          text-decoration-line: underline;\n  font-weight: 600;\n}\nion-label {\n  margin: 0;\n  font-weight: 600;\n  font-size: 0.75rem !important;\n  color: #333333 !important;\n}\n.checkWrap {\n  margin-bottom: 1.75rem;\n}\n.v2 {\n  margin-top: -1.25rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWNjb3VudC9zaWdudXAvc2lnbnVwLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDZCQUFBO0FBQ0Y7QUFDRTtFQUNFLG1CQUFBO0FBQ0o7QUFHQTtFQUNFLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSx5QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsNEJBQUE7RUFDQSx3QkFBQTtFQUNBLHVCQUFBO0FBQUY7QUFHQTtFQUNFLFdBQUE7RUFDQSxjQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxxQ0FBQTtFQUNBLHFCQUFBO0VBQ0EsMkJBQUE7RUFDQSxpQkFBQTtBQUFGO0FBR0E7RUFDRSxhQUFBO0FBQUY7QUFHQTtFQUNFLG9CQUFBO0VBQ0EsbUJBQUE7RUFDQSxxQkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFNBQUE7RUFDQSx1QkFBQTtFQUNBLDhEQUFBO0VBQ0EsbUNBQUE7RUFDQSw4QkFBQTtBQUFGO0FBR0E7RUFDRSx3QkFBQTtFQUNBLDJCQUFBO0FBQUY7QUFDRTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtBQUNKO0FBRUU7RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLHVDQUFBO1VBQUEsK0JBQUE7RUFDQSxnQkFBQTtBQUFKO0FBSUE7RUFDRSxTQUFBO0VBQ0EsZ0JBQUE7RUFDQSw2QkFBQTtFQUNBLHlCQUFBO0FBREY7QUFJQTtFQUNFLHNCQUFBO0FBREY7QUFJQTtFQUNFLG9CQUFBO0FBREYiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9hY2NvdW50L3NpZ251cC9zaWdudXAucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWxpc3Qge1xuICBwYWRkaW5nOiAxLjVyZW0gMS41cmVtIDEuNXJlbTtcblxuICBpb24taW5wdXQge1xuICAgIG1hcmdpbi1ib3R0b206IDFyZW07XG4gIH1cbn1cblxuaW9uLXNlbGVjdCB7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNkNGQ0ZDQ7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbiAgLS1wYWRkaW5nLXRvcDogMS4wNjNyZW07XG4gIC0tcGFkZGluZy1lbmQ6IDFyZW07XG4gIC0tcGFkZGluZy1ib3R0b206IDFyZW07XG4gIC0tcGFkZGluZy1zdGFydDogMS4zNzVyZW07XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjMzMzMzMzO1xuICAtLXBsYWNlaG9sZGVyLWNvbG9yOiAjOTA5MDkwO1xuICAtLXBsYWNlaG9sZGVyLW9wYWNpdHk6IDE7XG4gIG1hcmdpbi1ib3R0b206IDEuMTI1cmVtO1xufVxuXG5pb24tc2VsZWN0OjphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogMjRweDtcbiAgaGVpZ2h0OiAyNHB4O1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vLi4vYXNzZXRzL2ltZ3MvbW9yZS5wbmcpIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiAyNHB4O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIG1hcmdpbi1yaWdodDogMnB4O1xufVxuXG5pb24tc2VsZWN0OjpwYXJ0KGljb24pIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuaW9uLWNoZWNrYm94IHtcbiAgLS1ib3JkZXItcmFkaXVzOiAycHg7XG4gIC0tYm9yZGVyLXdpZHRoOiAxcHg7XG4gIC0tYm9yZGVyLXN0eWxlOiBzb2xpZDtcbiAgLS1ib3JkZXItY29sb3I6ICNlOGU4ZTg7XG4gIC0tYmFja2dyb3VuZDogI2ZmZjtcbiAgLS1zaXplOiAxLjEyNXJlbTtcbiAgd2lkdGg6IDEuMTI1cmVtO1xuICBoZWlnaHQ6IDEuMTI1cmVtO1xuICBtYXJnaW46IDA7XG4gIG1hcmdpbi1yaWdodDogMC42ODc1cmVtO1xuICAtLWJhY2tncm91bmQtY2hlY2tlZDogY2VudGVyIC8gY292ZXIgbm8tcmVwZWF0IHVybCguLi8uLi8uLi8uLi9hc3NldHMvaW1ncy9jaGJnLnBuZyk7XG4gIC0tYm9yZGVyLWNvbG9yLWNoZWNrZWQ6IHRyYW5zcGFyZW50O1xuICAtLWNoZWNrbWFyay1jb2xvcjogdHJhbnNwYXJlbnQ7XG59XG5cbmlvbi1pdGVtIHtcbiAgLS1wYWRkaW5nLXRvcDogMC42ODc1cmVtO1xuICAtLXBhZGRpbmctYm90dG9tOiAwLjY4NzVyZW07XG4gIGRpdiB7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICB9XG5cbiAgLmJ1dHRvbi1jbGVhciB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHJpZ2h0OiAwO1xuICAgIHotaW5kZXg6IDk5OTtcbiAgICBmb250LXNpemU6IDAuNzVyZW07XG4gICAgLS1jb2xvcjogIzk5OTk5OTtcbiAgICB0ZXh0LWRlY29yYXRpb24tbGluZTogdW5kZXJsaW5lO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIH1cbn1cblxuaW9uLWxhYmVsIHtcbiAgbWFyZ2luOiAwO1xuICBmb250LXdlaWdodDogNjAwO1xuICBmb250LXNpemU6IDAuNzVyZW0gIWltcG9ydGFudDtcbiAgY29sb3I6ICMzMzMzMzMgIWltcG9ydGFudDtcbn1cblxuLmNoZWNrV3JhcCB7XG4gIG1hcmdpbi1ib3R0b206IDEuNzVyZW07XG59XG5cbi52MiB7XG4gIG1hcmdpbi10b3A6IC0xLjI1cmVtO1xufVxuIl19 */");

/***/ }),

/***/ "./src/app/pages/account/signup/signup.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/account/signup/signup.page.ts ***!
  \*****************************************************/
/*! exports provided: SignupPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupPage", function() { return SignupPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_loading_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../../../services/loading.service */ "./src/services/loading.service.ts");
/* harmony import */ var _services_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../../../services/auth.service */ "./src/services/auth.service.ts");
/* harmony import */ var _services_db_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../../services/db.service */ "./src/services/db.service.ts");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../../services/common.service */ "./src/services/common.service.ts");
/* harmony import */ var _services_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../../../services/user.service */ "./src/services/user.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _terms_personal_info_terms_personal_info_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../terms-personal-info/terms-personal-info.page */ "./src/app/pages/account/terms-personal-info/terms-personal-info.page.ts");
/* harmony import */ var _terms_service_terms_service_page__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../terms-service/terms-service.page */ "./src/app/pages/account/terms-service/terms-service.page.ts");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic-native/keyboard/ngx */ "./node_modules/@ionic-native/keyboard/__ivy_ngcc__/ngx/index.js");












let SignupPage = class SignupPage {
    constructor(modalController, navc, userService, alertController, toastController, common, db, auth, load, keyboard) {
        this.modalController = modalController;
        this.navc = navc;
        this.userService = userService;
        this.alertController = alertController;
        this.toastController = toastController;
        this.common = common;
        this.db = db;
        this.auth = auth;
        this.load = load;
        this.keyboard = keyboard;
        this.agreeAllTerms = false;
        this.firstChk = false;
        this.agree = {
            serviceAgree: false,
            personalAgree: false,
            marketing: false,
        };
        this.yearList = [];
        this.passwordCheck = false;
        this.emailcheck = false;
        this.yearOptions = {
            cssClass: 'yearcustom',
        };
        this.nicknameExpressionChk = false;
        this.emailExpressionChk = false;
        this.pwExpressionChk = false;
        this.phoneExpressionChk = true;
        this.user = this.userService.getUser();
        console.log(this.user);
        this.getList();
    }
    ngOnInit() { }
    getList() {
        let start = 1;
        let end = 100;
        let nowYear = new Date().getFullYear();
        for (let i = start; i <= end; i++) {
            this.yearList.push(nowYear - i + 1);
        }
    }
    service() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _terms_service_terms_service_page__WEBPACK_IMPORTED_MODULE_9__["TermsServicePage"],
                cssClass: 'my-custom-class',
            });
            return yield modal.present();
        });
    }
    personal() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _terms_personal_info_terms_personal_info_page__WEBPACK_IMPORTED_MODULE_8__["TermsPersonalInfoPage"],
                cssClass: 'my-custom-class',
            });
            return yield modal.present();
        });
    }
    nicknamewRegularExpression($event) {
        let nickname = $event.target.value;
        console.log(nickname, nickname.length);
        if (this.nickname.length < 2 || this.nickname.length > 10) {
            this.nicknameExpressionChk = false;
        }
        else {
            this.nicknameExpressionChk = true;
        }
        if (this.nickname.length >= 10 && nickname.length >= 10) {
            this.keyboard.hide();
        }
    }
    emailRegularExpression() {
        var reg = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
        if (reg.test(this.email)) {
            this.emailExpressionChk = true;
        }
        else {
            this.emailExpressionChk = false;
        }
    }
    pwRegularExpression() {
        // var regExp = /^[A-za-z0-9]{5,15}$/g;
        var regExp = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/;
        if (regExp.test(this.password)) {
            this.pwExpressionChk = true;
        }
        else {
            this.pwExpressionChk = false;
        }
    }
    phoneRegularExpression() {
        var regExp = /(01[016789])([1-9]{1}[0-9]{2,3})([0-9]{4})$/;
        if (regExp.test(this.phone)) {
            this.phoneExpressionChk = true;
        }
        else {
            this.phoneExpressionChk = false;
        }
    }
    matchingPasswords() {
        //비밀번호 같은지 비교
        if (this.password && this.confirm_password) {
            if (this.password !== this.confirm_password) {
                this.passwordCheck = false;
                return true;
            }
            else {
                this.passwordCheck = true;
                return false;
            }
        }
    }
    setData() {
        this.user.email = this.email;
        this.user.password = this.password;
        this.user.confirm_password = this.confirm_password;
        this.user.nickname = this.nickname;
        this.user.birth = this.birth;
        this.user.phone = this.phone;
        this.user.dateCreated = new Date().toISOString();
        this.user.marketing = this.agree.marketing;
    }
    // 중복된 계정
    presentAlertAlready() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            var reg = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i;
            if (reg.test(this.email)) {
                this.db
                    .collection$(`users`, ref => ref.where('email', '==', this.email).where('exitSwitch', '==', false))
                    .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["take"])(1))
                    .subscribe((e) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                    if (e[0]) {
                        this.presentAlertEmail();
                    }
                    else {
                        this.emailcheck = true;
                    }
                }));
            }
            else {
                this.presentAlertEmail();
            }
        });
    }
    goLogin() {
        if (!this.agree.serviceAgree || !this.agree.personalAgree) {
            // alert("수정");
            this.firstChk = true;
            return;
        }
        this.firstChk = false;
        this.setData();
        console.log(this.user);
        this.db
            .collection$(`users`, ref => ref.where('email', '==', this.email).where('exitSwitch', '==', false))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_10__["take"])(1))
            .subscribe(datas => {
            if (datas.length > 0) {
                this.presentAlertEmail();
            }
            else {
                this.presentAlertAlready();
            }
        });
        this.load.load();
        this.auth
            .registerUser(this.user.email, this.user.password)
            .then(userCredential => {
            delete this.user.password;
            delete this.user.confirm_password;
            this.load.hide();
            this.registerDetail(userCredential, this.user);
        })
            .catch(error => {
            this.load.hide();
            let code = error['code'];
            console.log('code', code);
        });
    }
    registerDetail(userCredential, obj) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            let user = userCredential.user;
            this.user.uid = user.uid;
            this.db.updateAt(`users/${user.uid}`, this.user).then(success => {
                this.successJoin();
                this.navc.navigateRoot(['/tabs/home']).then(() => {
                    this.auth.logoutUser();
                    this.navc.navigateForward(['/login']);
                });
            });
        });
    }
    successJoin() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: '회원가입이 정상적으로 완료되었습니다.',
                duration: 2000,
            });
            toast.present();
        });
    }
    //잘못된 email
    presentAlertEmail() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'wrong_pop wrong_t_pop already_pop',
                header: '가입된 계정',
                message: `이미 가입된 계정입니다. <br> 확인 후 로그인 해주시길 바랍니다.`,
                buttons: [
                    {
                        text: '확인',
                        cssClass: 'pop_ok',
                        handler: () => {
                            console.log('Confirm Okay');
                        },
                    },
                ],
            });
            yield alert.present();
        });
    }
    warning() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: '회원가입 종료',
                message: '입력하신 내용이  저장되지 않습니다.<br> 이전 페이지로 이동 하시겠습니까?',
                buttons: [
                    {
                        text: '종료',
                        handler: () => {
                            this.navc.pop();
                        },
                    },
                    {
                        text: '취소',
                    },
                ],
            });
            yield alert.present();
        });
    }
    checkAgreeAll(num) {
        if (this.agreeAllTerms && num == 0) {
            this.agree.marketing = true;
            this.agree.personalAgree = true;
            this.agree.serviceAgree = true;
        }
        else if (num == 0 && this.agree.marketing && this.agree.personalAgree && this.agree.serviceAgree) {
            this.agree.marketing = false;
            this.agree.personalAgree = false;
            this.agree.serviceAgree = false;
        }
        if (this.agree.marketing && this.agree.personalAgree && this.agree.serviceAgree && num != 0) {
            this.agreeAllTerms = true;
        }
        else if (num != 0) {
            this.agreeAllTerms = false;
        }
    }
};
SignupPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["NavController"] },
    { type: _services_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ToastController"] },
    { type: _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"] },
    { type: _services_db_service__WEBPACK_IMPORTED_MODULE_3__["DbService"] },
    { type: _services_auth_service__WEBPACK_IMPORTED_MODULE_2__["AuthService"] },
    { type: _services_loading_service__WEBPACK_IMPORTED_MODULE_1__["LoadingService"] },
    { type: _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_11__["Keyboard"] }
];
SignupPage.propDecorators = {
    inputEl: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__["ViewChild"], args: ['inputId', { static: false },] }]
};
SignupPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_6__["Component"])({
        selector: 'app-signup',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./signup.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/account/signup/signup.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./signup.page.scss */ "./src/app/pages/account/signup/signup.page.scss")).default]
    })
], SignupPage);



/***/ }),

/***/ "./src/services/user.service.ts":
/*!**************************************!*\
  !*** ./src/services/user.service.ts ***!
  \**************************************/
/*! exports provided: UserService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserService", function() { return UserService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let UserService = class UserService {
    constructor() {
        this.user = {
            uid: '',
            email: '',
            nickname: '',
            birth: '',
            phone: '',
            marketing: false,
            exitSwitch: false,
            dateCreated: '',
            pushId: '',
            alarmSwitch: true,
            password: '',
            confirm_password: '',
        };
    }
    setUser(user) {
        this.user = user;
    }
    getUser() {
        return this.user;
    }
};
UserService.ctorParameters = () => [];
UserService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], UserService);



/***/ })

}]);
//# sourceMappingURL=pages-account-signup-signup-module-es2015.js.map