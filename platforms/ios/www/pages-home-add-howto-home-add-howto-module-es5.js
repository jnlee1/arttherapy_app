(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-add-howto-home-add-howto-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-howto/home-add-howto.page.html":
    /*!*****************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-howto/home-add-howto.page.html ***!
      \*****************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesHomeAddHowtoHomeAddHowtoPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <div id=\"headerInner\">\n    <ion-toolbar>\n      <ion-buttons class=\"back\" slot=\"start\">\n        <ion-back-button text=\"\" icon=\"\"></ion-back-button>\n      </ion-buttons>\n      <ion-title>검사 실시 방법</ion-title>\n    </ion-toolbar>\n\n  </div>\n</ion-header>\n\n<ion-content>\n  <div class=\"allWrap\">\n    <div class=\"wrap\">\n\n      <div class=\"slideWrap\">\n        <div class=\"slideAll\">\n\n          <div class=\"conInner\">\n            <div class=\"step\">\n              <span>\n                1\n              </span>\n            </div>\n\n            <div class=\"stepInner\">\n              <div>\n                <h2>\n                  한장의 종이를 가로로 놓고 <br>\n                  “ 집을 그리세요.”\n                </h2>\n\n                <span class=\"m1\">\n                  A4 4장, 4B나 2B연필, 지우개 1개준비 <br>\n                  ( 연필심이 너무 뾰족하지 않도록 준비하세요.)\n                </span>\n\n\n              </div>\n            </div>\n          </div>\n        </div>\n\n      </div>\n    </div>\n    <div class=\"btn_wrap\">\n      <span class=\"intro\">\n        안내에 맞게 그림을 그리셨다면<br>\n        다음버튼을 눌러주세요.\n      </span>\n      <ion-button expand=\"block\" class=\"grbtn\" (click)=\"slidenext()\">\n        다음\n      </ion-button>\n    </div>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/home-add-howto/home-add-howto-routing.module.ts":
    /*!***********************************************************************!*\
      !*** ./src/app/pages/home-add-howto/home-add-howto-routing.module.ts ***!
      \***********************************************************************/

    /*! exports provided: HomeAddHowtoPageRoutingModule */

    /***/
    function srcAppPagesHomeAddHowtoHomeAddHowtoRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddHowtoPageRoutingModule", function () {
        return HomeAddHowtoPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _home_add_howto_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./home-add-howto.page */
      "./src/app/pages/home-add-howto/home-add-howto.page.ts");

      var routes = [{
        path: '',
        component: _home_add_howto_page__WEBPACK_IMPORTED_MODULE_3__["HomeAddHowtoPage"]
      }];

      var HomeAddHowtoPageRoutingModule = function HomeAddHowtoPageRoutingModule() {
        _classCallCheck(this, HomeAddHowtoPageRoutingModule);
      };

      HomeAddHowtoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], HomeAddHowtoPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/home-add-howto/home-add-howto.module.ts":
    /*!***************************************************************!*\
      !*** ./src/app/pages/home-add-howto/home-add-howto.module.ts ***!
      \***************************************************************/

    /*! exports provided: HomeAddHowtoPageModule */

    /***/
    function srcAppPagesHomeAddHowtoHomeAddHowtoModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddHowtoPageModule", function () {
        return HomeAddHowtoPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _home_add_howto_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./home-add-howto-routing.module */
      "./src/app/pages/home-add-howto/home-add-howto-routing.module.ts");
      /* harmony import */


      var _home_add_howto_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./home-add-howto.page */
      "./src/app/pages/home-add-howto/home-add-howto.page.ts");

      var HomeAddHowtoPageModule = function HomeAddHowtoPageModule() {
        _classCallCheck(this, HomeAddHowtoPageModule);
      };

      HomeAddHowtoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _home_add_howto_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeAddHowtoPageRoutingModule"]],
        declarations: [_home_add_howto_page__WEBPACK_IMPORTED_MODULE_6__["HomeAddHowtoPage"]]
      })], HomeAddHowtoPageModule);
      /***/
    },

    /***/
    "./src/app/pages/home-add-howto/home-add-howto.page.scss":
    /*!***************************************************************!*\
      !*** ./src/app/pages/home-add-howto/home-add-howto.page.scss ***!
      \***************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesHomeAddHowtoHomeAddHowtoPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "h2 {\n  white-space: pre-line;\n  margin: 0;\n  font-weight: 600;\n  font-size: 0.9375rem;\n  color: #161616;\n  line-height: 1.4rem;\n  letter-spacing: -0.2px;\n  margin-bottom: 1rem;\n  text-align: center;\n}\n\n.m1 {\n  font-size: 0.875rem;\n  font-weight: 600;\n  line-height: 1.188rem;\n  color: #333;\n  letter-spacing: -0.2px;\n  display: block;\n  text-align: center;\n}\n\n.m2 {\n  font-size: 1rem;\n  font-weight: 600;\n  line-height: 1.188rem;\n  color: #333;\n  letter-spacing: -0.2px;\n  display: block;\n  text-align: center;\n}\n\n.m1,\n.m2 {\n  white-space: pre-line;\n}\n\n.slideAll {\n  height: 100%;\n  width: 100%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n.wrap {\n  height: calc(100% - 138px);\n  background: #ffffff;\n}\n\n.btn_wrap {\n  background: #ffffff;\n}\n\n.slideWrap {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  height: 100%;\n  width: 100%;\n}\n\n.conInner {\n  background: #f9f9f9;\n  border-top: 1px solid #eee;\n  border-bottom: 1px solid #eee;\n  height: 59.86%;\n  width: 100%;\n  padding-top: 9.77%;\n  padding-bottom: 23.46%;\n  overflow: hidden;\n}\n\n.step {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  width: 50px;\n  height: 50px;\n  border-radius: 50%;\n  background: linear-gradient(313deg, #2b5ac3 0%, #1b98e7 100%);\n  margin: 0 auto;\n}\n\n.step span {\n  z-index: 1;\n  font-size: 30px;\n  text-transform: uppercase;\n  background: linear-gradient(313deg, #2b5ac3 0%, #1b98e7 100%);\n  -webkit-background-clip: text;\n  -webkit-text-fill-color: transparent;\n}\n\n.stepInner {\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  height: 100%;\n}\n\n.step::before {\n  position: absolute;\n  border-radius: 50%;\n  content: \"\";\n  display: block;\n  width: 42px;\n  height: 42px;\n  background: #fff;\n}\n\n@media (max-height: 736px) {\n  .conInner {\n    height: 75%;\n  }\n}\n\n@media (max-height: 570px) {\n  .conInner {\n    height: 80%;\n  }\n\n  .step {\n    width: 50px;\n    height: 50px;\n  }\n\n  .conInner {\n    padding-top: 7%;\n  }\n\n  .step::before {\n    width: 42px;\n    height: 42px;\n  }\n\n  h2 {\n    line-height: 20px;\n    font-size: 16px;\n  }\n}\n\n.intro {\n  font-size: 14px;\n  font-weight: 600;\n  line-height: 19px;\n  color: #545454;\n  display: block;\n  text-align: center;\n  margin-bottom: 24px;\n}\n\n.grbtn {\n  width: calc(100% - 26px - 26px);\n  margin: 0 auto;\n}\n\n.allWrap {\n  height: calc(100vh - 100px);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtaG93dG8vaG9tZS1hZGQtaG93dG8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UscUJBQUE7RUFDQSxTQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQUNGOztBQUNBO0VBQ0UsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FBRUY7O0FBQ0E7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQUVGOztBQUNBOztFQUVFLHFCQUFBO0FBRUY7O0FBQ0E7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBRUY7O0FBQ0E7RUFDRSwwQkFBQTtFQUNBLG1CQUFBO0FBRUY7O0FBQ0E7RUFDRSxtQkFBQTtBQUVGOztBQUNBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQUVGOztBQUNBO0VBQ0UsbUJBQUE7RUFDQSwwQkFBQTtFQUNBLDZCQUFBO0VBQ0EsY0FBQTtFQUVBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7QUFDRjs7QUFFQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLDZEQUFBO0VBQ0EsY0FBQTtBQUNGOztBQUNFO0VBQ0UsVUFBQTtFQUNBLGVBQUE7RUFDQSx5QkFBQTtFQUNBLDZEQUFBO0VBQ0EsNkJBQUE7RUFDQSxvQ0FBQTtBQUNKOztBQUdBO0VBQ0UsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxZQUFBO0FBQUY7O0FBR0E7RUFDRSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FBQUY7O0FBRUE7RUFDRTtJQUNFLFdBQUE7RUFDRjtBQUNGOztBQUVBO0VBQ0U7SUFDRSxXQUFBO0VBQUY7O0VBRUE7SUFDRSxXQUFBO0lBQ0EsWUFBQTtFQUNGOztFQUVBO0lBQ0UsZUFBQTtFQUNGOztFQUVBO0lBQ0UsV0FBQTtJQUNBLFlBQUE7RUFDRjs7RUFFQTtJQUNFLGlCQUFBO0lBQ0EsZUFBQTtFQUNGO0FBQ0Y7O0FBTUE7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQUpGOztBQU9BO0VBQ0UsK0JBQUE7RUFDQSxjQUFBO0FBSkY7O0FBT0E7RUFDRSwyQkFBQTtBQUpGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtaG93dG8vaG9tZS1hZGQtaG93dG8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaDIge1xuICB3aGl0ZS1zcGFjZTogcHJlLWxpbmU7XG4gIG1hcmdpbjogMDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1zaXplOiAwLjkzNzVyZW07XG4gIGNvbG9yOiAjMTYxNjE2O1xuICBsaW5lLWhlaWdodDogMS40cmVtO1xuICBsZXR0ZXItc3BhY2luZzogLTAuMnB4O1xuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG4ubTEge1xuICBmb250LXNpemU6IDAuODc1cmVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBsaW5lLWhlaWdodDogMS4xODhyZW07XG4gIGNvbG9yOiAjMzMzO1xuICBsZXR0ZXItc3BhY2luZzogLTAuMnB4O1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ubTIge1xuICBmb250LXNpemU6IDFyZW07XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGxpbmUtaGVpZ2h0OiAxLjE4OHJlbTtcbiAgY29sb3I6ICMzMzM7XG4gIGxldHRlci1zcGFjaW5nOiAtMC4ycHg7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5tMSxcbi5tMiB7XG4gIHdoaXRlLXNwYWNlOiBwcmUtbGluZTtcbn1cblxuLnNsaWRlQWxsIHtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59XG5cbi53cmFwIHtcbiAgaGVpZ2h0OiBjYWxjKDEwMCUgLSAxMzhweCk7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG59XG5cbi5idG5fd3JhcCB7XG4gIGJhY2tncm91bmQ6ICNmZmZmZmY7XG59XG5cbi5zbGlkZVdyYXAge1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgaGVpZ2h0OiAxMDAlO1xuICB3aWR0aDogMTAwJTtcbn1cblxuLmNvbklubmVyIHtcbiAgYmFja2dyb3VuZDogI2Y5ZjlmOTtcbiAgYm9yZGVyLXRvcDogMXB4IHNvbGlkICNlZWU7XG4gIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjZWVlO1xuICBoZWlnaHQ6IDU5Ljg2JTtcblxuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZy10b3A6IDkuNzclO1xuICBwYWRkaW5nLWJvdHRvbTogMjMuNDYlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4uc3RlcCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICB3aWR0aDogNTBweDtcbiAgaGVpZ2h0OiA1MHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJhY2tncm91bmQ6IGxpbmVhci1ncmFkaWVudCgzMTNkZWcsIHJnYmEoNDMsIDkwLCAxOTUsIDEpIDAlLCByZ2JhKDI3LCAxNTIsIDIzMSwgMSkgMTAwJSk7XG4gIG1hcmdpbjogMCBhdXRvO1xuICAvLyAgIG1hcmdpbi1ib3R0b206IDIzLjE4JTtcbiAgc3BhbiB7XG4gICAgei1pbmRleDogMTtcbiAgICBmb250LXNpemU6IDMwcHg7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbiAgICBiYWNrZ3JvdW5kOiBsaW5lYXItZ3JhZGllbnQoMzEzZGVnLCByZ2JhKDQzLCA5MCwgMTk1LCAxKSAwJSwgcmdiYSgyNywgMTUyLCAyMzEsIDEpIDEwMCUpO1xuICAgIC13ZWJraXQtYmFja2dyb3VuZC1jbGlwOiB0ZXh0O1xuICAgIC13ZWJraXQtdGV4dC1maWxsLWNvbG9yOiB0cmFuc3BhcmVudDtcbiAgfVxufVxuXG4uc3RlcElubmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLnN0ZXA6OmJlZm9yZSB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBjb250ZW50OiAnJztcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiA0MnB4O1xuICBoZWlnaHQ6IDQycHg7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG59XG5AbWVkaWEgKG1heC1oZWlnaHQ6IDczNnB4KSB7XG4gIC5jb25Jbm5lciB7XG4gICAgaGVpZ2h0OiA3NSU7XG4gIH1cbn1cblxuQG1lZGlhIChtYXgtaGVpZ2h0OiA1NzBweCkge1xuICAuY29uSW5uZXIge1xuICAgIGhlaWdodDogODAlO1xuICB9XG4gIC5zdGVwIHtcbiAgICB3aWR0aDogNTBweDtcbiAgICBoZWlnaHQ6IDUwcHg7XG4gIH1cblxuICAuY29uSW5uZXIge1xuICAgIHBhZGRpbmctdG9wOiA3JTtcbiAgfVxuXG4gIC5zdGVwOjpiZWZvcmUge1xuICAgIHdpZHRoOiA0MnB4O1xuICAgIGhlaWdodDogNDJweDtcbiAgfVxuXG4gIGgyIHtcbiAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICBmb250LXNpemU6IDE2cHg7XG4gIH1cblxuICAubTIge1xuICAgIC8vIGZvbnQtc2l6ZTogMTZweDtcbiAgfVxufVxuXG4uaW50cm8ge1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGxpbmUtaGVpZ2h0OiAxOXB4O1xuICBjb2xvcjogIzU0NTQ1NDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMjRweDtcbn1cblxuLmdyYnRuIHtcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDI2cHggLSAyNnB4KTtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG5cbi5hbGxXcmFwIHtcbiAgaGVpZ2h0OiBjYWxjKDEwMHZoIC0gMTAwcHgpO1xufVxuIl19 */";
      /***/
    },

    /***/
    "./src/app/pages/home-add-howto/home-add-howto.page.ts":
    /*!*************************************************************!*\
      !*** ./src/app/pages/home-add-howto/home-add-howto.page.ts ***!
      \*************************************************************/

    /*! exports provided: HomeAddHowtoPage */

    /***/
    function srcAppPagesHomeAddHowtoHomeAddHowtoPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddHowtoPage", function () {
        return HomeAddHowtoPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var HomeAddHowtoPage = /*#__PURE__*/function () {
        // how = [
        //   {
        //     step: '1',
        //     suggest: '한장의 종이를 가로로 놓고 \n “ 집을 그리세요.”',
        //     material: 'A4 4장, 4B나 2B연필, 지우개 1개준비 \n ( 연필심이 너무 뾰족하지 않도록 준비하세요.)',
        //   },
        //   // {
        //   //   step: '2',
        //   //   suggest: '집을 그리고 나면 새로운 \n 종이 한 장을 세로로 놓고 \n “ 나무를 그리세요.”',
        //   // },
        //   // {
        //   //   step: '3',
        //   //   suggest:
        //   //     '나무를 그리고 나면 새로운\n 종이 한 장을 세로로 놓고\n “사람을 그리세요. \n단, 사람을 그릴때 막대 인물상이나 \n만화처럼 그리지말고 \n사람의 전체를 그리세요.”',
        //   //   material2: '“ 얼굴만 아니라 전신을 그려보세요.”',
        //   // },
        //   // {
        //   //   step: '4',
        //   //   suggest: '사람을 그리고 나면 새로운 \n종이 한 장을 세로로 놓고 \n“ 앞에 그림 사람과 반대되는 \n성을 그리세요.”',
        //   //   material2: '예) 첫번째 남자를 그렸으면 두번째는 \n여자를 그리세요.',
        //   // },
        // ];
        function HomeAddHowtoPage(navc) {
          _classCallCheck(this, HomeAddHowtoPage);

          this.navc = navc; // slidenext() {
          //   this.slide.nativeElement.getActiveIndex().then(index => {
          //     if (index == 3) {
          //       this.navc.navigateForward('/home-add-howto-img');
          //     } else {
          //       this.slide.nativeElement.slideTo(index + 1, 300);
          //     }
          //   });
          // }

          this.slideOpts = {
            initialSlide: 0,
            speed: 400,
            allowTouchMove: false
          };
        }

        _createClass(HomeAddHowtoPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "slidenext",
          value: function slidenext() {
            this.navc.navigateForward('home-add-howto2');
          }
        }]);

        return HomeAddHowtoPage;
      }();

      HomeAddHowtoPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }];
      };

      HomeAddHowtoPage.propDecorators = {
        slide: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['slide', {
            read: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ElementRef"],
            "static": true
          }]
        }]
      };
      HomeAddHowtoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-add-howto',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./home-add-howto.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-howto/home-add-howto.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./home-add-howto.page.scss */
        "./src/app/pages/home-add-howto/home-add-howto.page.scss"))["default"]]
      })], HomeAddHowtoPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-home-add-howto-home-add-howto-module-es5.js.map