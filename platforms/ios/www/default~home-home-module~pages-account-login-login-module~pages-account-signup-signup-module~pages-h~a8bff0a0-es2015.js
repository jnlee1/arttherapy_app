(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~home-home-module~pages-account-login-login-module~pages-account-signup-signup-module~pages-h~a8bff0a0"],{

/***/ "./src/services/alert.service.ts":
/*!***************************************!*\
  !*** ./src/services/alert.service.ts ***!
  \***************************************/
/*! exports provided: AlertService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AlertService", function() { return AlertService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");




const errorMessages = {
    // Firebase의 auth 에러 메세지 정의
    accountExistsWithDifferentCredential: { title: '계정 안내', subTitle: '이미 있는 계정입니다.' },
    invalidCredential: { title: '인증 오류', subTitle: '로그인 인증에 오류가 발생했습니다.' },
    operationNotAllowed: { title: '로그인 실패!', subTitle: '로그인 과정에서 오류가 발생했습니다. 관리자에게 문의하시길 바랍니다.' },
    userDisabled: { title: '정지 계정', subTitle: '정지된 계정입니다. 관리자에게 연락하시길 바랍니다.' },
    userNotFound: { title: '계정 없음', subTitle: '해당 계정 정보가 없습니다.' },
    // userEmailNotFound: { title: '가입된 이메일 주소가 아닙니다.', subTitle: '다시 확인해 주시기 바랍니다.' },
    wrongPassword: { title: '비밀번호 재설정', subTitle: '비밀번호 재설정을 위해 가입하신 이메일 주소를\n 입력하여 주시기 바랍니다.' },
    // invalidEmail: { title: '계정 없음', subTitle: '입력하신 이메일로 가입된 회원정보가 없습니다. \n 회원가입을 진행해 주시길 바랍니다.' },
    userEmailNotFound: { title: '계정 없음', subTitle: '가입된 이메일 주소가 아닙니다.\n다시 확인해 주시기 바랍니다.' },
    // wrongPassword: { title: '비밀번호 재설정', subTitle: '비밀번호 재설정을 위해 가입하신 이메일 주소를 입력하여 주시기 바랍니다.' },
    invalidEmail: { title: '계정 없음', subTitle: '이메일 형식이 올바르지 않습니다. \n 올바른 이메일을 입력하여 주시기 바랍니다.' },
    emailAlreadyInUse: { title: '사용할 수 없는 이메일', subTitle: '이미 사용중인 이메일입니다. 다시 확인하시기 바랍니다.' },
    weakPassword: { title: '비밀번호 경고', subTitle: '입력하신 비밀번호가 보안에 취약합니다.' },
    requiresRecentLogin: { title: '인증 만료', subTitle: '인증이 만료되었습니다. 다시 로그인 하시기 바랍니다.' },
    userMismatch: { title: '사용자 불일치', subTitle: '다른 사용자의 인증정보입니다!' },
    providerAlreadyLinked: { title: 'Already Linked', subTitle: 'Sorry, but your account is already linked to this credential.' },
    credentialAlreadyInUse: { title: '불가능 인증정보', subTitle: 'Sorry, but this credential is already used by another user.' },
    toManyrequests: { title: '확인 횟수 초과', subTitle: '잘못된 비밀번호로 확인 횟수가 초과 되었습니다. \n잠시 후 다시 시도하시기 바랍니다.' },
};
let AlertService = class AlertService {
    constructor(alertCtr, toastCtr) {
        this.alertCtr = alertCtr;
        this.toastCtr = toastCtr;
    }
    presentAlert(header, subtitle, message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertCtr.create({
                header,
                subHeader: subtitle,
                message,
                buttons: ['확인'],
            });
            yield alert.present();
        });
    }
    askAlert(header, subtitle, message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const alert = yield this.alertCtr.create({
                header: header,
                subHeader: subtitle,
                message: message,
                buttons: ['확인'],
            });
            yield alert.present();
        });
    }
    showErrorMessage(code) {
        switch (code) {
            // Firebase Error Messages
            case 'auth/account-exists-with-different-credential':
                this.presentAlert(errorMessages.accountExistsWithDifferentCredential['title'], errorMessages.accountExistsWithDifferentCredential['subTitle']);
                break;
            case 'auth/invalid-credential':
                this.presentAlert(errorMessages.invalidCredential['title'], errorMessages.invalidCredential['subTitle']);
                break;
            case 'auth/operation-not-allowed':
                this.presentAlert(errorMessages.operationNotAllowed['title'], errorMessages.operationNotAllowed['subTitle']);
                break;
            case 'auth/user-disabled':
                this.presentAlert(errorMessages.userDisabled['title'], errorMessages.userDisabled['subTitle']);
                break;
            case 'auth/user-not-found':
                this.presentAlert(errorMessages.userEmailNotFound['title'], errorMessages.userEmailNotFound['subTitle']);
                break;
            case 'auth/wrong-password':
                this.presentAlert(errorMessages.wrongPassword['title'], errorMessages.wrongPassword['subTitle']);
                break;
            case 'auth/invalid-email':
                this.presentAlert(errorMessages.invalidEmail['title'], errorMessages.invalidEmail['subTitle']);
                break;
            case 'auth/email-already-in-use':
                this.presentAlert(errorMessages.emailAlreadyInUse['title'], errorMessages.emailAlreadyInUse['subTitle']);
                break;
            case 'auth/weak-password':
                this.presentAlert(errorMessages.weakPassword['title'], errorMessages.weakPassword['subTitle']);
                break;
            case 'auth/requires-recent-login':
                this.presentAlert(errorMessages.requiresRecentLogin['title'], errorMessages.requiresRecentLogin['subTitle']);
                break;
            case 'auth/user-mismatch':
                this.presentAlert(errorMessages.userMismatch['title'], errorMessages.userMismatch['subTitle']);
                break;
            case 'auth/provider-already-linked':
                this.presentAlert(errorMessages.providerAlreadyLinked['title'], errorMessages.providerAlreadyLinked['subTitle']);
                break;
            case 'auth/credential-already-in-use':
                this.presentAlert(errorMessages.credentialAlreadyInUse['title'], errorMessages.credentialAlreadyInUse['subTitle']);
                break;
            case 'auth/too-many-requests':
                this.presentAlert(errorMessages.toManyrequests['title'], errorMessages.toManyrequests['subTitle']);
                break;
        }
    }
    ///토스트 생성///////
    presentToast(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastCtr.create({
                message: message,
                duration: 2000,
            });
            toast.present();
        });
    }
};
AlertService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
AlertService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], AlertService);



/***/ }),

/***/ "./src/services/auth.service.ts":
/*!**************************************!*\
  !*** ./src/services/auth.service.ts ***!
  \**************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/__ivy_ngcc__/auth/es2015/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _db_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./db.service */ "./src/services/db.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _alert_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./alert.service */ "./src/services/alert.service.ts");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _loading_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./loading.service */ "./src/services/loading.service.ts");
/* harmony import */ var _push_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./push.service */ "./src/services/push.service.ts");











// import { Storage } from '@ionic/storage';
// import { GooglePlus } from '@ionic-native/google-plus/ngx';


let AuthService = class AuthService {
    constructor(afAuth, db, router, platform, 
    // public gplus: GooglePlus,
    loadingController, 
    // public storage: Storage,
    alert, loadingService, push) {
        this.afAuth = afAuth;
        this.db = db;
        this.router = router;
        this.platform = platform;
        this.loadingController = loadingController;
        this.alert = alert;
        this.loadingService = loadingService;
        this.push = push;
        this.user$ = this.afAuth.authState.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["switchMap"])((user) => (user ? db.doc$(`users/${user.uid}`) : Object(rxjs__WEBPACK_IMPORTED_MODULE_4__["of"])(null))));
        this.user$.subscribe((data) => {
            console.log("data", data);
            if (data) {
                this.userInfo = data;
            }
            else {
                this.userInfo = [];
            }
        });
        this.afAuth.authState.subscribe((data) => {
            // console.log('data', data);
            if (data) {
                localStorage.setItem("userId", data.uid);
            }
        });
        this.handleRedirect();
    }
    getUser() {
        return this.user$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["first"])()).toPromise();
    }
    uid() {
        return this.user$
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])((u) => u && u.uid))
            .toPromise();
    }
    nickname() {
        return this.user$
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])((u) => u && u.nickname))
            .toPromise();
    }
    approval() {
        return this.user$
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])((u) => u && u.activeSwitch))
            .toPromise();
    }
    userId() {
        return this.user$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])((u) => u.id));
    }
    info() {
        return this.user$.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1)).toPromise();
    }
    infoR() {
        return this.user$
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["take"])(1), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])((u) => u && u.selectedParts))
            .toPromise();
    }
    anonymousLogin() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return new Promise((resolve, reject) => {
                firebase_app__WEBPACK_IMPORTED_MODULE_9__["auth"]()
                    .signInAnonymously()
                    .then((res) => resolve(res), (err) => reject(err));
            });
        });
    }
    emailLogin(obj) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loadingService.load();
            try {
                var r = yield this.afAuth.auth.signInWithEmailAndPassword(obj.email, obj.password);
                if (r) {
                    setTimeout(() => {
                        this.loadingService.hide();
                        this.router.navigate(["/"]);
                    }, 600);
                    console.log("Successfully logged in!", r);
                    localStorage.setItem("userId", r.user.uid);
                }
            }
            catch (error) {
                const code = error["code"];
                this.alert.showErrorMessage(code);
                console.error("야기아냐?", error);
            }
        });
    }
    loginUser(value) {
        return new Promise((resolve, reject) => {
            firebase_app__WEBPACK_IMPORTED_MODULE_9__["auth"]()
                .signInWithEmailAndPassword(value.email, value.password)
                .then((res) => resolve(res), (err) => reject(err));
        });
    }
    exitLogin(obj) {
        return new Promise((resolve, reject) => {
            try {
                var r = this.afAuth.auth.signInWithEmailAndPassword(obj.email, obj.password);
                if (r) {
                    console.log("Successfully logged in!");
                }
            }
            catch (error) {
                let code = error["code"];
                this.alert.showErrorMessage(code);
                resolve(error);
            }
        });
    }
    loginWithToken(token) {
        // tslint:disable-next-line:no-shadowed-variable
        return new Promise((resolve, reject) => {
            firebase_app__WEBPACK_IMPORTED_MODULE_9__["auth"]()
                .signInWithCustomToken(token)
                .then((res) => resolve(res), (err) => reject(err));
        });
    }
    resetPassword(email) {
        this.afAuth.auth
            .sendPasswordResetEmail(email)
            .then(() => {
            this.alert.presentAlert("비밀번호 재설정", '', "비밀번호 재설정 이메일을 발송하였습니다. <br> 가입한 이메일의 메일함을 확인해주세요");
        })
            .catch((err) => {
            console.error(err);
            this.alert.showErrorMessage(err.code);
        });
    }
    updateUserData(uid, obj) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            //사용자 데이터를 firestore에 업로드//
            const path = `users/${uid}`;
            const dateCreated = new Date().toISOString();
            const exitSwitch = false;
            const alarmSwitch = true;
            const data = Object.assign({
                uid,
                dateCreated,
                exitSwitch,
                alarmSwitch,
            }, obj);
            console.log("data", data);
            yield this.db.updateAt(path, data);
            yield this.loadingService.dismiss();
        });
    }
    signOut() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            yield this.afAuth.auth.signOut();
            yield this.alert.presentToast("로그아웃하였습니다.");
            return this.router.navigate(["/"]);
        });
    }
    registerUser(email, password) {
        return new Promise((resolve, reject) => {
            firebase_app__WEBPACK_IMPORTED_MODULE_9__["auth"]()
                .createUserWithEmailAndPassword(email, password)
                .then((res) => resolve(res), (err) => reject(err));
        });
    }
    register(email, password, obj) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            try {
                var r = yield this.afAuth.auth.createUserWithEmailAndPassword(email, password);
                if (r) {
                    // this.router.navigate(['/']);
                    // localStorage.setItem("userId", r.user.uid);
                    return yield this.updateUserData(r.user.uid, obj);
                }
            }
            catch (error) {
                let code = error["code"];
                this.alert.showErrorMessage(code);
                console.error(error);
            }
        });
    }
    //// GOOGLE AUTH
    handleRedirect() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (Boolean(this.isRedirect())) {
                return null;
            }
            const loading = yield this.loadingService.load();
            const result = yield this.afAuth.auth.getRedirectResult();
            if (result.user) {
                yield this.updateUserData(result.user);
            }
            this.loadingService.hide();
            yield this.setRedirect("false");
            return result;
        });
    }
    setRedirect(val) {
        localStorage.setItem("authRedirect", val);
    }
    isRedirect() {
        return localStorage.getItem("authRedirect");
    }
    changePassword(oldPassword, newPassword) {
        return new Promise((resolve, reject) => {
            const user = this.afAuth.auth.currentUser;
            return this.afAuth.auth
                .signInWithEmailAndPassword(user.email, oldPassword)
                .then((sucess) => {
                user
                    .updatePassword(newPassword)
                    .then((sucess) => {
                    resolve(true);
                    this.alert.presentToast("비밀번호가 성공적으로 변경되었습니다.");
                })
                    .catch((error) => {
                    console.log("error", error);
                    let code = error["code"];
                    reject(false);
                    this.alert.showErrorMessage(code);
                });
            })
                .catch((error) => {
                reject(false);
                let code = error["code"];
                // this.alert.showErrorMessage(code);
                console.log("oldPasswordIncorrect", error);
            });
        });
    }
    exitUser() {
        return new Promise((resolve, reject) => {
            const user = this.afAuth.auth.currentUser;
            this.db
                .updateAt(`users/${user.uid}`, { exitSwitch: true })
                .then(() => {
                user
                    .delete()
                    .then(() => {
                    localStorage.clear();
                    this.alert.presentAlert("회원탈퇴", "회원님의 계정이 탈퇴되었습니다.");
                    // this.router.navigateByUrl('/login');
                    reject(false);
                })
                    .catch((error) => {
                    console.log("error", error);
                });
            })
                .catch((error) => {
                console.log("error", error);
            });
        });
    }
    sendEmailVerificationUser(user) {
        return new Promise((resolve, reject) => {
            if (firebase_app__WEBPACK_IMPORTED_MODULE_9__["auth"]().currentUser) {
                user
                    .sendEmailVerification()
                    .then(() => {
                    console.log("LOG Out");
                    resolve();
                })
                    .catch((error) => {
                    reject();
                });
            }
        });
    }
    userDetails() {
        return this.afAuth.auth.currentUser;
    }
    logoutUser() {
        return new Promise((resolve, reject) => {
            if (firebase_app__WEBPACK_IMPORTED_MODULE_9__["auth"]().currentUser) {
                firebase_app__WEBPACK_IMPORTED_MODULE_9__["auth"]()
                    .signOut()
                    .then(() => {
                    console.log("LOG Out");
                    resolve();
                })
                    .catch((error) => {
                    reject();
                });
            }
        });
    }
};
AuthService.ctorParameters = () => [
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"] },
    { type: _db_service__WEBPACK_IMPORTED_MODULE_6__["DbService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["Platform"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["LoadingController"] },
    { type: _alert_service__WEBPACK_IMPORTED_MODULE_8__["AlertService"] },
    { type: _loading_service__WEBPACK_IMPORTED_MODULE_10__["LoadingService"] },
    { type: _push_service__WEBPACK_IMPORTED_MODULE_11__["PushService"] }
];
AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root",
    })
], AuthService);



/***/ }),

/***/ "./src/services/loading.service.ts":
/*!*****************************************!*\
  !*** ./src/services/loading.service.ts ***!
  \*****************************************/
/*! exports provided: LoadingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadingService", function() { return LoadingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");



let LoadingService = class LoadingService {
    constructor(loader) {
        this.loader = loader;
        this.isLoading = false;
    }
    load(message) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.loading = yield this.loader.create({
                duration: 5000,
                translucent: true,
                cssClass: 'custom-class custom-loading',
                backdropDismiss: true,
                message
            });
            yield this.loading.present();
        });
    }
    hide() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            if (this.loading) {
                this.loader.dismiss();
            }
        });
    }
    present() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.isLoading = true;
            return yield this.loader.create({
                cssClass: 'custom-loader-class'
            }).then(a => {
                a.present().then(() => {
                    console.log('presented');
                    if (!this.isLoading) {
                        a.dismiss().then(() => console.log('abort presenting'));
                    }
                });
            });
        });
    }
    dismiss() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.isLoading = false;
            return yield this.loader.dismiss().then(() => console.log('dismissed'));
        });
    }
};
LoadingService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
LoadingService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], LoadingService);



/***/ }),

/***/ "./src/services/push.service.ts":
/*!**************************************!*\
  !*** ./src/services/push.service.ts ***!
  \**************************************/
/*! exports provided: PushService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PushService", function() { return PushService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let PushService = class PushService {
    constructor() { }
};
PushService.ctorParameters = () => [];
PushService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], PushService);



/***/ })

}]);
//# sourceMappingURL=default~home-home-module~pages-account-login-login-module~pages-account-signup-signup-module~pages-h~a8bff0a0-es2015.js.map