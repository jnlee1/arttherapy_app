(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~home-home-module~pages-account-login-login-module~pages-account-signup-signup-module~pages-a~1389e152"],{

/***/ "./node_modules/angularfire2/firestore/index.js":
/*!******************************************************!*\
  !*** ./node_modules/angularfire2/firestore/index.js ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(__webpack_require__(/*! @angular/fire/firestore */ "./node_modules/@angular/fire/__ivy_ngcc__/firestore/es2015/index.js"));
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi93cmFwcGVyL3NyYy9maXJlc3RvcmUvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSw2Q0FBd0MifQ==

/***/ }),

/***/ "./src/services/common.service.ts":
/*!****************************************!*\
  !*** ./src/services/common.service.ts ***!
  \****************************************/
/*! exports provided: CommonService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommonService", function() { return CommonService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let CommonService = class CommonService {
    constructor() { }
    generateFilename() {
        var length = 20;
        var text = '';
        var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (var i = 0; i < length; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        return text;
    }
    /**
     * 이름 변환 함수 정재은 => 정OO, 고현 => 고O, 강다니엘 => 강OOO
     * @param name 변환할 풀네임
     * @param name 이름을 나타낼 문자
     *
     * 2020-01-22 정재은
     */
    getSecretName(name, text) {
        let tmp_name = name.substring(0, 1);
        if (tmp_name.length > 2) {
            for (var i = 1; i < name.length; i++) {
                tmp_name += text;
            }
        }
        else {
            for (var i = 1; i < 3; i++) {
                tmp_name += text;
            }
        }
        return tmp_name;
    }
    /**
     * object array 중복 제거할 함수
     * @param originalArray 중복제거할 array
     * @param prop 제거 object key 값
     *
     * 2020-01-22 정재은
     */
    removeDuplicates(originalArray, prop) {
        let newArray = [];
        let lookupObject = {};
        for (var i in originalArray) {
            lookupObject[originalArray[i][prop]] = originalArray[i];
        }
        for (i in lookupObject) {
            newArray.push(lookupObject[i]);
        }
        return newArray;
    }
    /**
     * 오름차순 정렬 함수
     * @param property 정렬 기준 컬럼
     *
     * 2020-01-22 정재은
     */
    dynamicSort(property) {
        var sortOrder = 1;
        if (property[0] === "-") {
            sortOrder = -1;
            property = property.substr(1);
        }
        return function (a, b) {
            var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
            return result * sortOrder;
        };
    }
    /**
     * 위도, 경도로 거리를 구하는 함수
     * @param coords1 현재위치 (위도 경도) {latitude: Number, longitude: Number}
     * @param coords2 목적지위치 (위도 경도) {latitude: Number, longitude: Number}
     * @param unit 타입 ('K'일 경우 km, 'N'일 경우 m)
     *
     * 2020-01-22 정재은
     */
    getDistance(facCoords, myCoords, unit) {
        let radlat1 = Math.PI * facCoords.latitude / 180;
        let radlat2 = Math.PI * myCoords.latitude / 180;
        let theta = facCoords.longitude - myCoords.longitude;
        let radtheta = Math.PI * theta / 180;
        let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        if (dist > 1) {
            dist = 1;
        }
        dist = Math.acos(dist);
        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515;
        if (unit == "K") {
            dist = dist * 1.609344;
        }
        if (unit == "N") {
            dist = dist * 0.8684;
        }
        return dist;
    }
    /**
     * 생년으로 올해 나이를 구하는 함수
     * @param birthYear 생년 (number)
     *
     * 2020-07-21 정재은
     */
    calcAge(birthYear) {
        let nowYear = new Date().getFullYear();
        let age = nowYear - birthYear + 1;
        return age;
    }
    /**
     * date를 포맷하는 함수 (new Date()) => 2020-07-20
     *
     * @param date 날짜
     *
     * 2020-07-21 정재은
     */
    formatDate(date) {
        var d = new Date(date), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('-');
    }
    /**
     * var list = {"you": 100, "me": 75, "foo": 116, "bar": 15};
     * [{key:"bar", value:15}, {key:"me", value:75}, {key:"you", value:100}, {key:"foo", value:116}]
     * object 정렬 함수
     *
     * @param obj 정렬할 obj
     *
     * 2020-07-23 정재은
     */
    sortObject(obj) {
        var arr = [];
        for (var prop in obj) {
            if (obj.hasOwnProperty(prop)) {
                arr.push({
                    'key': prop,
                    'value': obj[prop]
                });
            }
        }
        arr.sort(function (a, b) { return b.value - a.value; });
        return arr; // returns array
    }
    /**
     * 배열 중 제일 큰 값의 index를 리턴하는 함수
     * @param arr 정렬할 배열
     *
     * 2020-07-23 정재은
     */
    indexOfMax(arr) {
        if (arr.length === 0) {
            return -1;
        }
        var max = arr[0];
        var maxIndex = 0;
        for (var i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                maxIndex = i;
                max = arr[i];
            }
        }
        return maxIndex;
    }
};
CommonService.ctorParameters = () => [];
CommonService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    })
], CommonService);



/***/ }),

/***/ "./src/services/db.service.ts":
/*!************************************!*\
  !*** ./src/services/db.service.ts ***!
  \************************************/
/*! exports provided: docJoin, leftJoin, leftJoinDocument, colletionJoin, DbService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "docJoin", function() { return docJoin; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "leftJoin", function() { return leftJoin; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "leftJoinDocument", function() { return leftJoinDocument; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "colletionJoin", function() { return colletionJoin; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DbService", function() { return DbService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! angularfire2/firestore */ "./node_modules/angularfire2/firestore/index.js");
/* harmony import */ var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/auth */ "./node_modules/@angular/fire/__ivy_ngcc__/auth/es2015/index.js");
/* harmony import */ var angularfire2_database__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! angularfire2/database */ "./node_modules/angularfire2/database/index.js");
/* harmony import */ var angularfire2_database__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(angularfire2_database__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! firebase/app */ "./node_modules/firebase/app/dist/index.cjs.js");
/* harmony import */ var firebase_app__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var _common_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./common.service */ "./src/services/common.service.ts");










const docJoin = (afs, paths) => {
    return (source) => Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["defer"])(() => {
        let parent;
        const keys = Object.keys(paths);
        return source.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])((data) => {
            // Save the parent data state
            parent = data;
            // Map each path to an Observable
            const docs$ = keys.map((k) => {
                const fullPath = `${paths[k]}/${parent[k]}`;
                return afs.doc(fullPath).valueChanges();
            });
            // return combineLatest, it waits for all reads to finish
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["combineLatest"])(docs$);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((arr) => {
            // We now have all the associated douments
            // Reduce them to a single object based on the parent's keys
            const joins = keys.reduce((acc, cur, idx) => {
                return Object.assign(Object.assign({}, acc), { [cur]: arr[idx] });
            }, {});
            // Return the parent doc with the joined objects
            return Object.assign(Object.assign({}, parent), joins);
        }));
    });
};
const leftJoin = (afs, field, collection, limit = 100) => {
    return (source) => Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["defer"])(() => {
        // Operator state
        let collectionData;
        // Track total num of joined doc reads
        let totalJoins = 0;
        return source.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])((data) => {
            // Clear mapping on each emitted val ;
            // Save the parent data state
            collectionData = data;
            const reads$ = [];
            for (const doc of collectionData) {
                // Push doc read to Array
                if (doc[field]) {
                    // Perform query on join key, with optional limit
                    const q = (ref) => ref.where(field, "==", doc[field]).limit(limit);
                    reads$.push(afs.collection(collection, q).valueChanges());
                }
                else {
                    reads$.push(Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["of"])([]));
                }
            }
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["combineLatest"])(reads$);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((joins) => {
            return collectionData.map((v, i) => {
                totalJoins += joins[i].length;
                return Object.assign(Object.assign({}, v), { [collection]: joins[i] || null });
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["tap"])((final) => {
            console.log(`Queried ${final.length}, Joined ${totalJoins} docs`);
            totalJoins = 0;
        }));
    });
};
const leftJoinDocument = (afs, field, collection) => {
    return (source) => Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["defer"])(() => {
        // Operator state
        let collectionData;
        const cache = new Map();
        return source.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])((data) => {
            // Clear mapping on each emitted val ;
            cache.clear();
            // Save the parent data state
            collectionData = data;
            const reads$ = [];
            let i = 0;
            for (const doc of collectionData) {
                // Skip if doc field does not exist or is already in cache
                if (!doc[field] || cache.get(doc[field])) {
                    continue;
                }
                // Push doc read to Array
                reads$.push(afs.collection(collection).doc(doc[field]).valueChanges());
                cache.set(doc[field], i);
                i++;
            }
            return reads$.length ? Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["combineLatest"])(reads$) : Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["of"])([]);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((joins) => {
            return collectionData.map((v, i) => {
                const joinIdx = cache.get(v[field]);
                return Object.assign(Object.assign({}, v), { [field]: Object.assign(Object.assign({}, joins[joinIdx]), { id: v[field] }) || null });
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["tap"])((final) => console.log(`Queried ${final.length}, Joined ${cache.size} docs`)));
    });
};
const colletionJoin = (afs, category, collection, field, limit) => {
    return (source) => Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["defer"])(() => {
        // Operator state
        let array;
        // Track total num of joined doc reads
        let totalJoins = 0;
        return source.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])((select) => {
            // Clear mapping on each emitted val ;
            // Save the parent data state
            array = category;
            const reads$ = [];
            for (const doc of array) {
                // Push doc read to Array
                console.log("doc", select, field, doc);
                if (doc) {
                    // Perform query on join key, with optional limit
                    const q = (ref) => ref.where("genre", "==", select).where(field, "array-contains", doc).orderBy("totalLike", "desc").limit(limit);
                    const collectionMap = Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["pipe"])(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((docs) => {
                        return docs.docs.map((e) => {
                            return Object.assign({ id: e.id }, e.data());
                        });
                    }));
                    reads$.push(afs
                        .collection(collection, q)
                        .snapshotChanges()
                        .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((actions) => {
                        return actions.map((a) => {
                            const data = a.payload.doc.data();
                            const id = a.payload.doc.id;
                            return Object.assign({ id }, data);
                        });
                    })));
                }
                else {
                    reads$.push(Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["of"])([]));
                }
            }
            return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["combineLatest"])(reads$);
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((joins) => {
            return array.map((v, i) => {
                totalJoins += joins[i].length;
                return { ["name"]: v, ["photos"]: joins[i] || null };
            });
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["tap"])((final) => {
            console.log(`Queried ${final.length}, Joined ${totalJoins} docs`);
            totalJoins = 0;
        }));
    });
};
let DbService = class DbService {
    constructor(afs, angularDb, common, afAuth) {
        this.afs = afs;
        this.angularDb = angularDb;
        this.common = common;
        this.afAuth = afAuth;
        this.masterRef = this.afs.doc(`master/vw6N7ADkDdP1CkbhtBBnN0X3NlF3}`);
    }
    createFsId() {
        return this.afs.createId();
    }
    createdAt() {
        return firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"].FieldValue.serverTimestamp();
    }
    collection$(path, query) {
        return this.afs
            .collection(path, query)
            .snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((actions) => {
            return actions.map((a) => {
                const data = a.payload.doc.data();
                const id = a.payload.doc.id;
                return Object.assign({ id }, data);
            });
        }));
    }
    doc$(path) {
        return this.afs
            .doc(path)
            .snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((doc) => {
            const data = doc.payload.data();
            const id = doc.payload.id;
            return Object.assign({ id: doc.payload.id }, data);
        }));
    }
    doc2$(ref) {
        return this.doc(ref)
            .snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((doc) => {
            return doc.payload.data();
        }));
    }
    col$(ref, queryFn) {
        return this.col(ref, queryFn)
            .snapshotChanges()
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])((docs) => {
            return docs.map((a) => a.payload.doc.data());
        }));
    }
    col(ref, queryFn) {
        return typeof ref === "string" ? this.afs.collection(ref, queryFn) : ref;
    }
    doc(ref) {
        return typeof ref === "string" ? this.afs.doc(ref) : ref;
    }
    /**
     * @param  {string} path 'collection' or 'collection/docID'
     * @param  {object} data new data
     *
     * Creates or updates data on a collection or document.
     **/
    // ** 기본적인 DB처리 **//
    updateAt(path, data) {
        const segments = path.split("/").filter((v) => v);
        if (segments.length % 2) {
            // Odd is always a collection
            return this.afs.collection(path).add(data);
        }
        else {
            // Even is always document
            return this.afs.doc(path).set(data, { merge: true });
        }
    }
    delete(path) {
        return this.afs.doc(path).delete();
    }
    /**
     * @param  {string} path path to document
     *
     * Deletes document from Firestore
     **/
    deleteDoc(path) {
        return this.afs.doc(path).delete();
    }
    acceptOrder(obj) {
        return new Promise((resolve, reject) => {
            this.connectOrder(obj)
                .then((success) => {
                const ref = this.afs.doc(`order/${obj.orderId}`);
                return ref
                    .update({ connectCompanys: firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"].FieldValue.arrayUnion(obj.companyId) })
                    .then((success) => {
                    resolve(true);
                })
                    .catch((error) => {
                    reject(false);
                    console.log("error", error);
                });
            })
                .catch((error) => {
                reject(false);
                console.log("error", error);
            });
        });
    }
    acceptagOrder(obj) {
        return new Promise((resolve, reject) => {
            const ref = this.afs.doc(`arrangeorder/${obj.orderId}`);
            return ref
                .update({ connectCompany: obj.companyId })
                .then((success) => {
                resolve(true);
            })
                .catch((error) => {
                reject(false);
                console.log("error", error);
            });
        });
    }
    connectOrder(obj) {
        return new Promise((resolve, reject) => {
            return this.updateAt(`connect`, obj)
                .then((success) => {
                resolve(true);
            })
                .catch((error) => {
                reject(false);
                console.log("error", error);
            });
        });
    }
    payVideoOrder(orderId, companyId) {
        return new Promise((resolve, reject) => {
            const ref = this.afs.doc(`order/${orderId}`);
            return ref
                .update({ paidCompanys: firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"].FieldValue.arrayUnion(companyId) })
                .then((success) => {
                resolve(true);
            })
                .catch((error) => {
                reject(false);
                console.log("error", error);
            });
        });
    }
    refundOrder(orderId, companyId) {
        return new Promise((resolve, reject) => {
            firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"]().runTransaction((transaction) => {
                return transaction
                    .get(firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"]().collection("order").doc(orderId))
                    .then((docData) => {
                    const refundCompanys = docData.data().refundCompanys || {};
                    refundCompanys[companyId] = true;
                    return transaction.set(firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"]().collection("order").doc(orderId), { refundCompanys }, { merge: true });
                })
                    .then((success) => {
                    resolve(true);
                })
                    .catch((error) => {
                    reject(false);
                    console.log("error", error);
                });
            });
        });
    }
    refundagOrder(orderId, companyId) {
        return new Promise((resolve, reject) => {
            firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"]().runTransaction((transaction) => {
                return transaction
                    .get(firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"]().collection("arrangeorder").doc(orderId))
                    .then((docData) => {
                    const refundCompanys = docData.data().refundCompanys || {};
                    refundCompanys[companyId] = true;
                    return transaction.set(firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"]().collection("arrangeorder").doc(orderId), { refundCompanys }, { merge: true });
                })
                    .then((success) => {
                    resolve(true);
                })
                    .catch((error) => {
                    reject(false);
                    console.log("error", error);
                });
            });
        });
    }
    checkUsername(nickname) {
        nickname = nickname.toLowerCase();
        return this.collection$(`users`, (ref) => ref.where("nickname", "==", nickname))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1))
            .toPromise();
    }
    checkEmail(email) {
        email = email.toLowerCase();
        return this.collection$(`users`, (ref) => ref.where("email", "==", email))
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1))
            .toPromise();
    }
    userInfo(uid) {
        return this.doc$(`users/${uid}`).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1)).toPromise();
    }
    appService() {
        const id = "vw6N7ADkDdP1CkbhtBBnN0X3NlF3";
        return this.doc$(`master/${id}`).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1)).toPromise();
    }
    bookMarkEvent(eventId, userId) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.afs.doc(`users/${userId}`).update({
                bookmarkEvent: firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"].FieldValue.arrayUnion(eventId),
            });
        });
    }
    unBookMarkEvent(eventId, userId) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.afs.doc(`users/${userId}`).update({
                bookmarkEvent: firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"].FieldValue.arrayRemove(eventId),
            });
        });
    }
    checkHits(eventId, userId) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.afs.doc(`event/${eventId}`).update({
                hits: firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"].FieldValue.arrayUnion(userId),
            });
        });
    }
    /**
     *
     * @param alarmType
     * freeLike (자유 좋아요)
  
  
     * @param typeId 게시글 uid
     * @param userId 리뷰 작성자, 댓글 작성자 등 uid
     */
    addAlaram(alarmType, typeId, userId) {
        let myUid = firebase_app__WEBPACK_IMPORTED_MODULE_7__["auth"]().currentUser.uid;
        if (userId != myUid) {
            let item = {
                activeAlarmId: this.common.generateFilename(),
                dateCreated: new Date().toISOString(),
                userId: userId,
            };
            this.updateAt(`activeAlarm/${item.activeAlarmId}`, item);
        }
    }
};
DbService.ctorParameters = () => [
    { type: angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"] },
    { type: angularfire2_database__WEBPACK_IMPORTED_MODULE_4__["AngularFireDatabase"] },
    { type: _common_service__WEBPACK_IMPORTED_MODULE_8__["CommonService"] },
    { type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"] }
];
DbService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root",
    })
], DbService);



/***/ })

}]);
//# sourceMappingURL=default~home-home-module~pages-account-login-login-module~pages-account-signup-signup-module~pages-a~1389e152-es2015.js.map