(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-add-mypage-profileedit-home-add-mypage-profileedit-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-mypage-profileedit/home-add-mypage-profileedit.page.html":
/*!*******************************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-mypage-profileedit/home-add-mypage-profileedit.page.html ***!
  \*******************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header>\n  <div id=\"headerInner\">\n    <ion-toolbar>\n      <ion-buttons class=\"back\" slot=\"start\">\n        <ion-back-button text=\"\" icon=\"\"></ion-back-button>\n      </ion-buttons>\n      <ion-title>내 정보 수정</ion-title>\n      <ion-buttons slot=\"end\">\n        <ion-button\n          [disabled]='!user.nickname || !user.birth || !user.phone  || !phoneExpressionChk || !nicknameExpressionChk'\n          (click)=\"editInfo()\">\n          저장\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n\n  </div>\n</ion-header>\n\n<ion-content>\n  <div *ngIf=\"user$ | async as user\">\n    <ion-list>\n      <h2>이름 </h2>\n      <ion-input placeholder=\"이름을 입력해주세요.\" minlength=\"2\" maxlength=\"10\" [(ngModel)]=\"user.nickname\" type='text'\n        (ionChange)=\"nicknamewRegularExpression()\"></ion-input>\n      <span class=\"validate\" *ngIf=\"!nicknameExpressionChk\">\n        이름은 2자 이상 10자 이하로 입력해주세요.\n      </span>\n\n      <h2>출생연도</h2>\n      <ion-select [interfaceOptions]=\"yearOptions\" placeholder=\"출생연도를 선택해주세요.\" [(ngModel)]=\"user.birth\" okText=\"선택\"\n        cancelText=\"취소\">\n        <ion-select-option *ngFor=\"let year of yearList\">{{year}}</ion-select-option>\n      </ion-select>\n      <span class=\"validate\" *ngIf=\"!user.birth\">\n        출생연도를 선택해주세요.\n      </span>\n\n      <h2>\n        핸드폰번호\n      </h2>\n      <ion-input maxlength=\"11\" minlength=\"10\" placeholder=\"핸드폰번호를 입력해주세요.\" (ionChange)=\"phoneRegularExpression()\"\n        type=\"tel\" [(ngModel)]=\"user.phone\">\n      </ion-input>\n      <span class=\"validate\" *ngIf=\"!phoneExpressionChk\">\n        핸드폰번호 형식을 맞춰 입력해주세요.\n      </span>\n    </ion-list>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/home-add-mypage-profileedit/home-add-mypage-profileedit-routing.module.ts":
/*!*************************************************************************************************!*\
  !*** ./src/app/pages/home-add-mypage-profileedit/home-add-mypage-profileedit-routing.module.ts ***!
  \*************************************************************************************************/
/*! exports provided: HomeAddMypageProfileeditPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAddMypageProfileeditPageRoutingModule", function() { return HomeAddMypageProfileeditPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _home_add_mypage_profileedit_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home-add-mypage-profileedit.page */ "./src/app/pages/home-add-mypage-profileedit/home-add-mypage-profileedit.page.ts");




const routes = [
    {
        path: '',
        component: _home_add_mypage_profileedit_page__WEBPACK_IMPORTED_MODULE_3__["HomeAddMypageProfileeditPage"]
    }
];
let HomeAddMypageProfileeditPageRoutingModule = class HomeAddMypageProfileeditPageRoutingModule {
};
HomeAddMypageProfileeditPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], HomeAddMypageProfileeditPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/home-add-mypage-profileedit/home-add-mypage-profileedit.module.ts":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/home-add-mypage-profileedit/home-add-mypage-profileedit.module.ts ***!
  \*****************************************************************************************/
/*! exports provided: HomeAddMypageProfileeditPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAddMypageProfileeditPageModule", function() { return HomeAddMypageProfileeditPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _home_add_mypage_profileedit_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home-add-mypage-profileedit-routing.module */ "./src/app/pages/home-add-mypage-profileedit/home-add-mypage-profileedit-routing.module.ts");
/* harmony import */ var _home_add_mypage_profileedit_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-add-mypage-profileedit.page */ "./src/app/pages/home-add-mypage-profileedit/home-add-mypage-profileedit.page.ts");







let HomeAddMypageProfileeditPageModule = class HomeAddMypageProfileeditPageModule {
};
HomeAddMypageProfileeditPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _home_add_mypage_profileedit_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeAddMypageProfileeditPageRoutingModule"]
        ],
        declarations: [_home_add_mypage_profileedit_page__WEBPACK_IMPORTED_MODULE_6__["HomeAddMypageProfileeditPage"]]
    })
], HomeAddMypageProfileeditPageModule);



/***/ }),

/***/ "./src/app/pages/home-add-mypage-profileedit/home-add-mypage-profileedit.page.scss":
/*!*****************************************************************************************!*\
  !*** ./src/app/pages/home-add-mypage-profileedit/home-add-mypage-profileedit.page.scss ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ion-select {\n  border: 1px solid #d4d4d4;\n  border-radius: 0;\n  --padding-top: 17px;\n  --padding-end: 16px;\n  --padding-bottom: 16px;\n  --padding-start: 22px;\n  font-size: 0.875rem;\n  font-weight: 600;\n  color: #333333;\n  --placeholder-color: #909090;\n  --placeholder-opacity: 1;\n  margin-bottom: 18px;\n}\n\nion-select::after {\n  content: \"\";\n  display: block;\n  width: 24px;\n  height: 24px;\n  background: url('more.png') no-repeat;\n  background-size: 24px;\n  background-position: center;\n  margin-right: 2px;\n}\n\nion-select::part(icon) {\n  display: none;\n}\n\nion-input {\n  margin-bottom: 18px;\n}\n\nh2 {\n  margin: 0;\n  font-size: 0.9375rem;\n  font-weight: 600;\n  color: #333333;\n  margin-bottom: 1rem;\n}\n\nion-list {\n  padding: 40px 24px;\n}\n\nion-toolbar ion-button {\n  --color: #f7bd00;\n  font-weight: 600;\n}\n\n[disabled] {\n  --background: transparent;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtbXlwYWdlLXByb2ZpbGVlZGl0L2hvbWUtYWRkLW15cGFnZS1wcm9maWxlZWRpdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx5QkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLDRCQUFBO0VBQ0Esd0JBQUE7RUFDQSxtQkFBQTtBQUNGOztBQUVBO0VBQ0UsV0FBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHFDQUFBO0VBQ0EscUJBQUE7RUFDQSwyQkFBQTtFQUNBLGlCQUFBO0FBQ0Y7O0FBRUE7RUFDRSxhQUFBO0FBQ0Y7O0FBRUE7RUFDRSxtQkFBQTtBQUNGOztBQUVBO0VBQ0UsU0FBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7QUFDRjs7QUFFQTtFQUNFLGtCQUFBO0FBQ0Y7O0FBR0U7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0FBQUo7O0FBSUE7RUFDRSx5QkFBQTtBQURGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtbXlwYWdlLXByb2ZpbGVlZGl0L2hvbWUtYWRkLW15cGFnZS1wcm9maWxlZWRpdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tc2VsZWN0IHtcbiAgYm9yZGVyOiAxcHggc29saWQgI2Q0ZDRkNDtcbiAgYm9yZGVyLXJhZGl1czogMDtcbiAgLS1wYWRkaW5nLXRvcDogMTdweDtcbiAgLS1wYWRkaW5nLWVuZDogMTZweDtcbiAgLS1wYWRkaW5nLWJvdHRvbTogMTZweDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAyMnB4O1xuICBmb250LXNpemU6IDAuODc1cmVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzMzMzMzMztcbiAgLS1wbGFjZWhvbGRlci1jb2xvcjogIzkwOTA5MDtcbiAgLS1wbGFjZWhvbGRlci1vcGFjaXR5OiAxO1xuICBtYXJnaW4tYm90dG9tOiAxOHB4O1xufVxuXG5pb24tc2VsZWN0OjphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogMjRweDtcbiAgaGVpZ2h0OiAyNHB4O1xuICBiYWNrZ3JvdW5kOiB1cmwoLi4vLi4vLi4vYXNzZXRzL2ltZ3MvbW9yZS5wbmcpIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiAyNHB4O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIG1hcmdpbi1yaWdodDogMnB4O1xufVxuXG5pb24tc2VsZWN0OjpwYXJ0KGljb24pIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuaW9uLWlucHV0IHtcbiAgbWFyZ2luLWJvdHRvbTogMThweDtcbn1cblxuaDIge1xuICBtYXJnaW46IDA7XG4gIGZvbnQtc2l6ZTogMC45Mzc1cmVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzMzMzMzMztcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbn1cblxuaW9uLWxpc3Qge1xuICBwYWRkaW5nOiA0MHB4IDI0cHg7XG59XG5cbmlvbi10b29sYmFyIHtcbiAgaW9uLWJ1dHRvbiB7XG4gICAgLS1jb2xvcjogI2Y3YmQwMDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICB9XG59XG5cbltkaXNhYmxlZF0ge1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xufVxuIl19 */");

/***/ }),

/***/ "./src/app/pages/home-add-mypage-profileedit/home-add-mypage-profileedit.page.ts":
/*!***************************************************************************************!*\
  !*** ./src/app/pages/home-add-mypage-profileedit/home-add-mypage-profileedit.page.ts ***!
  \***************************************************************************************/
/*! exports provided: HomeAddMypageProfileeditPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeAddMypageProfileeditPage", function() { return HomeAddMypageProfileeditPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var src_services_db_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/services/db.service */ "./src/services/db.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/keyboard/ngx */ "./node_modules/@ionic-native/keyboard/__ivy_ngcc__/ngx/index.js");







let HomeAddMypageProfileeditPage = class HomeAddMypageProfileeditPage {
    constructor(db, navc, toastController, keyboard, router) {
        this.db = db;
        this.navc = navc;
        this.toastController = toastController;
        this.keyboard = keyboard;
        this.router = router;
        this.user = {};
        this.yearList = [];
        this.yearOptions = {
            cssClass: 'yearcustom',
        };
        this.nicknameExpressionChk = true;
        this.phoneExpressionChk = true;
        this.router.queryParams.subscribe((params) => {
            this.uid = params["uid"];
            this.getData();
            this.getList();
        });
    }
    ngOnInit() { }
    getList() {
        let start = 1;
        let end = 100;
        let nowYear = new Date().getFullYear();
        for (let i = start; i <= end; i++) {
            this.yearList.push(nowYear - i + 1);
        }
    }
    getData() {
        this.user$ = this.db.doc$(`users/${this.uid}`)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["map"])(datas => {
            this.user = datas;
            return datas;
        }));
    }
    editInfo() {
        this.db.updateAt(`users/${this.uid}`, this.user).then(success => {
            this.successEdit();
            this.navc.pop();
        });
    }
    successEdit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: "회원정보 수정이 완료되었습니다",
                duration: 2000,
            });
            toast.present();
        });
    }
    nicknamewRegularExpression() {
        if (this.user.nickname.length < 2 || this.user.nickname.length > 10) {
            this.nicknameExpressionChk = false;
        }
        else {
            this.nicknameExpressionChk = true;
        }
        if (this.user.nickname.length >= 10 && this.user.nickname.length >= 10) {
            this.keyboard.hide();
        }
    }
    phoneRegularExpression() {
        var regExp = /(01[016789])([1-9]{1}[0-9]{2,3})([0-9]{4})$/;
        if (regExp.test(this.user.phone)) {
            this.phoneExpressionChk = true;
        }
        else {
            this.phoneExpressionChk = false;
        }
    }
};
HomeAddMypageProfileeditPage.ctorParameters = () => [
    { type: src_services_db_service__WEBPACK_IMPORTED_MODULE_1__["DbService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_6__["Keyboard"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] }
];
HomeAddMypageProfileeditPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-home-add-mypage-profileedit',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./home-add-mypage-profileedit.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-mypage-profileedit/home-add-mypage-profileedit.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./home-add-mypage-profileedit.page.scss */ "./src/app/pages/home-add-mypage-profileedit/home-add-mypage-profileedit.page.scss")).default]
    })
], HomeAddMypageProfileeditPage);



/***/ })

}]);
//# sourceMappingURL=pages-home-add-mypage-profileedit-home-add-mypage-profileedit-module-es2015.js.map