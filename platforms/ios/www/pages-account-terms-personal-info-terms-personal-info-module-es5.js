(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-account-terms-personal-info-terms-personal-info-module"], {
    /***/
    "./src/app/pages/account/terms-personal-info/terms-personal-info-routing.module.ts":
    /*!*****************************************************************************************!*\
      !*** ./src/app/pages/account/terms-personal-info/terms-personal-info-routing.module.ts ***!
      \*****************************************************************************************/

    /*! exports provided: TermsPersonalInfoPageRoutingModule */

    /***/
    function srcAppPagesAccountTermsPersonalInfoTermsPersonalInfoRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TermsPersonalInfoPageRoutingModule", function () {
        return TermsPersonalInfoPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _terms_personal_info_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./terms-personal-info.page */
      "./src/app/pages/account/terms-personal-info/terms-personal-info.page.ts");

      var routes = [{
        path: '',
        component: _terms_personal_info_page__WEBPACK_IMPORTED_MODULE_3__["TermsPersonalInfoPage"]
      }];

      var TermsPersonalInfoPageRoutingModule = function TermsPersonalInfoPageRoutingModule() {
        _classCallCheck(this, TermsPersonalInfoPageRoutingModule);
      };

      TermsPersonalInfoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], TermsPersonalInfoPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/account/terms-personal-info/terms-personal-info.module.ts":
    /*!*********************************************************************************!*\
      !*** ./src/app/pages/account/terms-personal-info/terms-personal-info.module.ts ***!
      \*********************************************************************************/

    /*! exports provided: TermsPersonalInfoPageModule */

    /***/
    function srcAppPagesAccountTermsPersonalInfoTermsPersonalInfoModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TermsPersonalInfoPageModule", function () {
        return TermsPersonalInfoPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _terms_personal_info_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./terms-personal-info-routing.module */
      "./src/app/pages/account/terms-personal-info/terms-personal-info-routing.module.ts");
      /* harmony import */


      var _terms_personal_info_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./terms-personal-info.page */
      "./src/app/pages/account/terms-personal-info/terms-personal-info.page.ts");

      var TermsPersonalInfoPageModule = function TermsPersonalInfoPageModule() {
        _classCallCheck(this, TermsPersonalInfoPageModule);
      };

      TermsPersonalInfoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _terms_personal_info_routing_module__WEBPACK_IMPORTED_MODULE_5__["TermsPersonalInfoPageRoutingModule"]],
        declarations: [_terms_personal_info_page__WEBPACK_IMPORTED_MODULE_6__["TermsPersonalInfoPage"]]
      })], TermsPersonalInfoPageModule);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-account-terms-personal-info-terms-personal-info-module-es5.js.map