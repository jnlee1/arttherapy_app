(function () {
  function _createForOfIteratorHelper(o, allowArrayLike) { var it; if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }

  function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

  function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["default~home-home-module~pages-account-login-login-module~pages-account-signup-signup-module~pages-a~1389e152"], {
    /***/
    "./node_modules/angularfire2/firestore/index.js":
    /*!******************************************************!*\
      !*** ./node_modules/angularfire2/firestore/index.js ***!
      \******************************************************/

    /*! no static exports found */

    /***/
    function node_modulesAngularfire2FirestoreIndexJs(module, exports, __webpack_require__) {
      "use strict";

      function __export(m) {
        for (var p in m) {
          if (!exports.hasOwnProperty(p)) exports[p] = m[p];
        }
      }

      Object.defineProperty(exports, "__esModule", {
        value: true
      });

      __export(__webpack_require__(
      /*! @angular/fire/firestore */
      "./node_modules/@angular/fire/__ivy_ngcc__/firestore/es2015/index.js")); //# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi8uLi93cmFwcGVyL3NyYy9maXJlc3RvcmUvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSw2Q0FBd0MifQ==

      /***/

    },

    /***/
    "./src/services/common.service.ts":
    /*!****************************************!*\
      !*** ./src/services/common.service.ts ***!
      \****************************************/

    /*! exports provided: CommonService */

    /***/
    function srcServicesCommonServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "CommonService", function () {
        return CommonService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var CommonService = /*#__PURE__*/function () {
        function CommonService() {
          _classCallCheck(this, CommonService);
        }

        _createClass(CommonService, [{
          key: "generateFilename",
          value: function generateFilename() {
            var length = 20;
            var text = '';
            var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

            for (var i = 0; i < length; i++) {
              text += possible.charAt(Math.floor(Math.random() * possible.length));
            }

            return text;
          }
          /**
           * 이름 변환 함수 정재은 => 정OO, 고현 => 고O, 강다니엘 => 강OOO
           * @param name 변환할 풀네임
           * @param name 이름을 나타낼 문자
           *
           * 2020-01-22 정재은
           */

        }, {
          key: "getSecretName",
          value: function getSecretName(name, text) {
            var tmp_name = name.substring(0, 1);

            if (tmp_name.length > 2) {
              for (var i = 1; i < name.length; i++) {
                tmp_name += text;
              }
            } else {
              for (var i = 1; i < 3; i++) {
                tmp_name += text;
              }
            }

            return tmp_name;
          }
          /**
           * object array 중복 제거할 함수
           * @param originalArray 중복제거할 array
           * @param prop 제거 object key 값
           *
           * 2020-01-22 정재은
           */

        }, {
          key: "removeDuplicates",
          value: function removeDuplicates(originalArray, prop) {
            var newArray = [];
            var lookupObject = {};

            for (var i in originalArray) {
              lookupObject[originalArray[i][prop]] = originalArray[i];
            }

            for (i in lookupObject) {
              newArray.push(lookupObject[i]);
            }

            return newArray;
          }
          /**
           * 오름차순 정렬 함수
           * @param property 정렬 기준 컬럼
           *
           * 2020-01-22 정재은
           */

        }, {
          key: "dynamicSort",
          value: function dynamicSort(property) {
            var sortOrder = 1;

            if (property[0] === "-") {
              sortOrder = -1;
              property = property.substr(1);
            }

            return function (a, b) {
              var result = a[property] < b[property] ? -1 : a[property] > b[property] ? 1 : 0;
              return result * sortOrder;
            };
          }
          /**
           * 위도, 경도로 거리를 구하는 함수
           * @param coords1 현재위치 (위도 경도) {latitude: Number, longitude: Number}
           * @param coords2 목적지위치 (위도 경도) {latitude: Number, longitude: Number}
           * @param unit 타입 ('K'일 경우 km, 'N'일 경우 m)
           *
           * 2020-01-22 정재은
           */

        }, {
          key: "getDistance",
          value: function getDistance(facCoords, myCoords, unit) {
            var radlat1 = Math.PI * facCoords.latitude / 180;
            var radlat2 = Math.PI * myCoords.latitude / 180;
            var theta = facCoords.longitude - myCoords.longitude;
            var radtheta = Math.PI * theta / 180;
            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);

            if (dist > 1) {
              dist = 1;
            }

            dist = Math.acos(dist);
            dist = dist * 180 / Math.PI;
            dist = dist * 60 * 1.1515;

            if (unit == "K") {
              dist = dist * 1.609344;
            }

            if (unit == "N") {
              dist = dist * 0.8684;
            }

            return dist;
          }
          /**
           * 생년으로 올해 나이를 구하는 함수
           * @param birthYear 생년 (number)
           *
           * 2020-07-21 정재은
           */

        }, {
          key: "calcAge",
          value: function calcAge(birthYear) {
            var nowYear = new Date().getFullYear();
            var age = nowYear - birthYear + 1;
            return age;
          }
          /**
           * date를 포맷하는 함수 (new Date()) => 2020-07-20
           *
           * @param date 날짜
           *
           * 2020-07-21 정재은
           */

        }, {
          key: "formatDate",
          value: function formatDate(date) {
            var d = new Date(date),
                month = '' + (d.getMonth() + 1),
                day = '' + d.getDate(),
                year = d.getFullYear();
            if (month.length < 2) month = '0' + month;
            if (day.length < 2) day = '0' + day;
            return [year, month, day].join('-');
          }
          /**
           * var list = {"you": 100, "me": 75, "foo": 116, "bar": 15};
           * [{key:"bar", value:15}, {key:"me", value:75}, {key:"you", value:100}, {key:"foo", value:116}]
           * object 정렬 함수
           *
           * @param obj 정렬할 obj
           *
           * 2020-07-23 정재은
           */

        }, {
          key: "sortObject",
          value: function sortObject(obj) {
            var arr = [];

            for (var prop in obj) {
              if (obj.hasOwnProperty(prop)) {
                arr.push({
                  'key': prop,
                  'value': obj[prop]
                });
              }
            }

            arr.sort(function (a, b) {
              return b.value - a.value;
            });
            return arr; // returns array
          }
          /**
           * 배열 중 제일 큰 값의 index를 리턴하는 함수
           * @param arr 정렬할 배열
           *
           * 2020-07-23 정재은
           */

        }, {
          key: "indexOfMax",
          value: function indexOfMax(arr) {
            if (arr.length === 0) {
              return -1;
            }

            var max = arr[0];
            var maxIndex = 0;

            for (var i = 1; i < arr.length; i++) {
              if (arr[i] > max) {
                maxIndex = i;
                max = arr[i];
              }
            }

            return maxIndex;
          }
        }]);

        return CommonService;
      }();

      CommonService.ctorParameters = function () {
        return [];
      };

      CommonService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], CommonService);
      /***/
    },

    /***/
    "./src/services/db.service.ts":
    /*!************************************!*\
      !*** ./src/services/db.service.ts ***!
      \************************************/

    /*! exports provided: docJoin, leftJoin, leftJoinDocument, colletionJoin, DbService */

    /***/
    function srcServicesDbServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "docJoin", function () {
        return docJoin;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "leftJoin", function () {
        return leftJoin;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "leftJoinDocument", function () {
        return leftJoinDocument;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "colletionJoin", function () {
        return colletionJoin;
      });
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "DbService", function () {
        return DbService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! angularfire2/firestore */
      "./node_modules/angularfire2/firestore/index.js");
      /* harmony import */


      var angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__);
      /* harmony import */


      var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/fire/auth */
      "./node_modules/@angular/fire/__ivy_ngcc__/auth/es2015/index.js");
      /* harmony import */


      var angularfire2_database__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! angularfire2/database */
      "./node_modules/angularfire2/database/index.js");
      /* harmony import */


      var angularfire2_database__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(angularfire2_database__WEBPACK_IMPORTED_MODULE_4__);
      /* harmony import */


      var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! rxjs */
      "./node_modules/rxjs/_esm2015/index.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var firebase_app__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! firebase/app */
      "./node_modules/firebase/app/dist/index.cjs.js");
      /* harmony import */


      var firebase_app__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(firebase_app__WEBPACK_IMPORTED_MODULE_7__);
      /* harmony import */


      var _common_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./common.service */
      "./src/services/common.service.ts");

      var docJoin = function docJoin(afs, paths) {
        return function (source) {
          return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["defer"])(function () {
            var parent;
            var keys = Object.keys(paths);
            return source.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(function (data) {
              // Save the parent data state
              parent = data; // Map each path to an Observable

              var docs$ = keys.map(function (k) {
                var fullPath = "".concat(paths[k], "/").concat(parent[k]);
                return afs.doc(fullPath).valueChanges();
              }); // return combineLatest, it waits for all reads to finish

              return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["combineLatest"])(docs$);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (arr) {
              // We now have all the associated douments
              // Reduce them to a single object based on the parent's keys
              var joins = keys.reduce(function (acc, cur, idx) {
                return Object.assign(Object.assign({}, acc), _defineProperty({}, cur, arr[idx]));
              }, {}); // Return the parent doc with the joined objects

              return Object.assign(Object.assign({}, parent), joins);
            }));
          });
        };
      };

      var leftJoin = function leftJoin(afs, field, collection) {
        var limit = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 100;
        return function (source) {
          return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["defer"])(function () {
            // Operator state
            var collectionData; // Track total num of joined doc reads

            var totalJoins = 0;
            return source.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(function (data) {
              // Clear mapping on each emitted val ;
              // Save the parent data state
              collectionData = data;
              var reads$ = [];

              var _iterator = _createForOfIteratorHelper(collectionData),
                  _step;

              try {
                var _loop = function _loop() {
                  var doc = _step.value;

                  // Push doc read to Array
                  if (doc[field]) {
                    // Perform query on join key, with optional limit
                    var q = function q(ref) {
                      return ref.where(field, "==", doc[field]).limit(limit);
                    };

                    reads$.push(afs.collection(collection, q).valueChanges());
                  } else {
                    reads$.push(Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["of"])([]));
                  }
                };

                for (_iterator.s(); !(_step = _iterator.n()).done;) {
                  _loop();
                }
              } catch (err) {
                _iterator.e(err);
              } finally {
                _iterator.f();
              }

              return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["combineLatest"])(reads$);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (joins) {
              return collectionData.map(function (v, i) {
                totalJoins += joins[i].length;
                return Object.assign(Object.assign({}, v), _defineProperty({}, collection, joins[i] || null));
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["tap"])(function (_final) {
              console.log("Queried ".concat(_final.length, ", Joined ").concat(totalJoins, " docs"));
              totalJoins = 0;
            }));
          });
        };
      };

      var leftJoinDocument = function leftJoinDocument(afs, field, collection) {
        return function (source) {
          return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["defer"])(function () {
            // Operator state
            var collectionData;
            var cache = new Map();
            return source.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(function (data) {
              // Clear mapping on each emitted val ;
              cache.clear(); // Save the parent data state

              collectionData = data;
              var reads$ = [];
              var i = 0;

              var _iterator2 = _createForOfIteratorHelper(collectionData),
                  _step2;

              try {
                for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
                  var doc = _step2.value;

                  // Skip if doc field does not exist or is already in cache
                  if (!doc[field] || cache.get(doc[field])) {
                    continue;
                  } // Push doc read to Array


                  reads$.push(afs.collection(collection).doc(doc[field]).valueChanges());
                  cache.set(doc[field], i);
                  i++;
                }
              } catch (err) {
                _iterator2.e(err);
              } finally {
                _iterator2.f();
              }

              return reads$.length ? Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["combineLatest"])(reads$) : Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["of"])([]);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (joins) {
              return collectionData.map(function (v, i) {
                var joinIdx = cache.get(v[field]);
                return Object.assign(Object.assign({}, v), _defineProperty({}, field, Object.assign(Object.assign({}, joins[joinIdx]), {
                  id: v[field]
                }) || null));
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["tap"])(function (_final2) {
              return console.log("Queried ".concat(_final2.length, ", Joined ").concat(cache.size, " docs"));
            }));
          });
        };
      };

      var colletionJoin = function colletionJoin(afs, category, collection, field, limit) {
        return function (source) {
          return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["defer"])(function () {
            // Operator state
            var array; // Track total num of joined doc reads

            var totalJoins = 0;
            return source.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(function (select) {
              // Clear mapping on each emitted val ;
              // Save the parent data state
              array = category;
              var reads$ = [];

              var _iterator3 = _createForOfIteratorHelper(array),
                  _step3;

              try {
                var _loop2 = function _loop2() {
                  var doc = _step3.value;
                  // Push doc read to Array
                  console.log("doc", select, field, doc);

                  if (doc) {
                    // Perform query on join key, with optional limit
                    var q = function q(ref) {
                      return ref.where("genre", "==", select).where(field, "array-contains", doc).orderBy("totalLike", "desc").limit(limit);
                    };

                    var collectionMap = Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["pipe"])(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (docs) {
                      return docs.docs.map(function (e) {
                        return Object.assign({
                          id: e.id
                        }, e.data());
                      });
                    }));
                    reads$.push(afs.collection(collection, q).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (actions) {
                      return actions.map(function (a) {
                        var data = a.payload.doc.data();
                        var id = a.payload.doc.id;
                        return Object.assign({
                          id: id
                        }, data);
                      });
                    })));
                  } else {
                    reads$.push(Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["of"])([]));
                  }
                };

                for (_iterator3.s(); !(_step3 = _iterator3.n()).done;) {
                  _loop2();
                }
              } catch (err) {
                _iterator3.e(err);
              } finally {
                _iterator3.f();
              }

              return Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["combineLatest"])(reads$);
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (joins) {
              return array.map(function (v, i) {
                var _ref;

                totalJoins += joins[i].length;
                return _ref = {}, _defineProperty(_ref, "name", v), _defineProperty(_ref, "photos", joins[i] || null), _ref;
              });
            }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["tap"])(function (_final3) {
              console.log("Queried ".concat(_final3.length, ", Joined ").concat(totalJoins, " docs"));
              totalJoins = 0;
            }));
          });
        };
      };

      var DbService = /*#__PURE__*/function () {
        function DbService(afs, angularDb, common, afAuth) {
          _classCallCheck(this, DbService);

          this.afs = afs;
          this.angularDb = angularDb;
          this.common = common;
          this.afAuth = afAuth;
          this.masterRef = this.afs.doc("master/vw6N7ADkDdP1CkbhtBBnN0X3NlF3}");
        }

        _createClass(DbService, [{
          key: "createFsId",
          value: function createFsId() {
            return this.afs.createId();
          }
        }, {
          key: "createdAt",
          value: function createdAt() {
            return firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"].FieldValue.serverTimestamp();
          }
        }, {
          key: "collection$",
          value: function collection$(path, query) {
            return this.afs.collection(path, query).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (actions) {
              return actions.map(function (a) {
                var data = a.payload.doc.data();
                var id = a.payload.doc.id;
                return Object.assign({
                  id: id
                }, data);
              });
            }));
          }
        }, {
          key: "doc$",
          value: function doc$(path) {
            return this.afs.doc(path).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (doc) {
              var data = doc.payload.data();
              var id = doc.payload.id;
              return Object.assign({
                id: doc.payload.id
              }, data);
            }));
          }
        }, {
          key: "doc2$",
          value: function doc2$(ref) {
            return this.doc(ref).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (doc) {
              return doc.payload.data();
            }));
          }
        }, {
          key: "col$",
          value: function col$(ref, queryFn) {
            return this.col(ref, queryFn).snapshotChanges().pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["map"])(function (docs) {
              return docs.map(function (a) {
                return a.payload.doc.data();
              });
            }));
          }
        }, {
          key: "col",
          value: function col(ref, queryFn) {
            return typeof ref === "string" ? this.afs.collection(ref, queryFn) : ref;
          }
        }, {
          key: "doc",
          value: function doc(ref) {
            return typeof ref === "string" ? this.afs.doc(ref) : ref;
          }
          /**
           * @param  {string} path 'collection' or 'collection/docID'
           * @param  {object} data new data
           *
           * Creates or updates data on a collection or document.
           **/
          // ** 기본적인 DB처리 **//

        }, {
          key: "updateAt",
          value: function updateAt(path, data) {
            var segments = path.split("/").filter(function (v) {
              return v;
            });

            if (segments.length % 2) {
              // Odd is always a collection
              return this.afs.collection(path).add(data);
            } else {
              // Even is always document
              return this.afs.doc(path).set(data, {
                merge: true
              });
            }
          }
        }, {
          key: "delete",
          value: function _delete(path) {
            return this.afs.doc(path)["delete"]();
          }
          /**
           * @param  {string} path path to document
           *
           * Deletes document from Firestore
           **/

        }, {
          key: "deleteDoc",
          value: function deleteDoc(path) {
            return this.afs.doc(path)["delete"]();
          }
        }, {
          key: "acceptOrder",
          value: function acceptOrder(obj) {
            var _this = this;

            return new Promise(function (resolve, reject) {
              _this.connectOrder(obj).then(function (success) {
                var ref = _this.afs.doc("order/".concat(obj.orderId));

                return ref.update({
                  connectCompanys: firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"].FieldValue.arrayUnion(obj.companyId)
                }).then(function (success) {
                  resolve(true);
                })["catch"](function (error) {
                  reject(false);
                  console.log("error", error);
                });
              })["catch"](function (error) {
                reject(false);
                console.log("error", error);
              });
            });
          }
        }, {
          key: "acceptagOrder",
          value: function acceptagOrder(obj) {
            var _this2 = this;

            return new Promise(function (resolve, reject) {
              var ref = _this2.afs.doc("arrangeorder/".concat(obj.orderId));

              return ref.update({
                connectCompany: obj.companyId
              }).then(function (success) {
                resolve(true);
              })["catch"](function (error) {
                reject(false);
                console.log("error", error);
              });
            });
          }
        }, {
          key: "connectOrder",
          value: function connectOrder(obj) {
            var _this3 = this;

            return new Promise(function (resolve, reject) {
              return _this3.updateAt("connect", obj).then(function (success) {
                resolve(true);
              })["catch"](function (error) {
                reject(false);
                console.log("error", error);
              });
            });
          }
        }, {
          key: "payVideoOrder",
          value: function payVideoOrder(orderId, companyId) {
            var _this4 = this;

            return new Promise(function (resolve, reject) {
              var ref = _this4.afs.doc("order/".concat(orderId));

              return ref.update({
                paidCompanys: firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"].FieldValue.arrayUnion(companyId)
              }).then(function (success) {
                resolve(true);
              })["catch"](function (error) {
                reject(false);
                console.log("error", error);
              });
            });
          }
        }, {
          key: "refundOrder",
          value: function refundOrder(orderId, companyId) {
            return new Promise(function (resolve, reject) {
              firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"]().runTransaction(function (transaction) {
                return transaction.get(firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"]().collection("order").doc(orderId)).then(function (docData) {
                  var refundCompanys = docData.data().refundCompanys || {};
                  refundCompanys[companyId] = true;
                  return transaction.set(firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"]().collection("order").doc(orderId), {
                    refundCompanys: refundCompanys
                  }, {
                    merge: true
                  });
                }).then(function (success) {
                  resolve(true);
                })["catch"](function (error) {
                  reject(false);
                  console.log("error", error);
                });
              });
            });
          }
        }, {
          key: "refundagOrder",
          value: function refundagOrder(orderId, companyId) {
            return new Promise(function (resolve, reject) {
              firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"]().runTransaction(function (transaction) {
                return transaction.get(firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"]().collection("arrangeorder").doc(orderId)).then(function (docData) {
                  var refundCompanys = docData.data().refundCompanys || {};
                  refundCompanys[companyId] = true;
                  return transaction.set(firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"]().collection("arrangeorder").doc(orderId), {
                    refundCompanys: refundCompanys
                  }, {
                    merge: true
                  });
                }).then(function (success) {
                  resolve(true);
                })["catch"](function (error) {
                  reject(false);
                  console.log("error", error);
                });
              });
            });
          }
        }, {
          key: "checkUsername",
          value: function checkUsername(nickname) {
            nickname = nickname.toLowerCase();
            return this.collection$("users", function (ref) {
              return ref.where("nickname", "==", nickname);
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1)).toPromise();
          }
        }, {
          key: "checkEmail",
          value: function checkEmail(email) {
            email = email.toLowerCase();
            return this.collection$("users", function (ref) {
              return ref.where("email", "==", email);
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1)).toPromise();
          }
        }, {
          key: "userInfo",
          value: function userInfo(uid) {
            return this.doc$("users/".concat(uid)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1)).toPromise();
          }
        }, {
          key: "appService",
          value: function appService() {
            var id = "vw6N7ADkDdP1CkbhtBBnN0X3NlF3";
            return this.doc$("master/".concat(id)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["take"])(1)).toPromise();
          }
        }, {
          key: "bookMarkEvent",
          value: function bookMarkEvent(eventId, userId) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      this.afs.doc("users/".concat(userId)).update({
                        bookmarkEvent: firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"].FieldValue.arrayUnion(eventId)
                      });

                    case 1:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "unBookMarkEvent",
          value: function unBookMarkEvent(eventId, userId) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      this.afs.doc("users/".concat(userId)).update({
                        bookmarkEvent: firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"].FieldValue.arrayRemove(eventId)
                      });

                    case 1:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "checkHits",
          value: function checkHits(eventId, userId) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      this.afs.doc("event/".concat(eventId)).update({
                        hits: firebase_app__WEBPACK_IMPORTED_MODULE_7__["firestore"].FieldValue.arrayUnion(userId)
                      });

                    case 1:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
          /**
           *
           * @param alarmType
           * freeLike (자유 좋아요)
           
           * @param typeId 게시글 uid
           * @param userId 리뷰 작성자, 댓글 작성자 등 uid
           */

        }, {
          key: "addAlaram",
          value: function addAlaram(alarmType, typeId, userId) {
            var myUid = firebase_app__WEBPACK_IMPORTED_MODULE_7__["auth"]().currentUser.uid;

            if (userId != myUid) {
              var item = {
                activeAlarmId: this.common.generateFilename(),
                dateCreated: new Date().toISOString(),
                userId: userId
              };
              this.updateAt("activeAlarm/".concat(item.activeAlarmId), item);
            }
          }
        }]);

        return DbService;
      }();

      DbService.ctorParameters = function () {
        return [{
          type: angularfire2_firestore__WEBPACK_IMPORTED_MODULE_2__["AngularFirestore"]
        }, {
          type: angularfire2_database__WEBPACK_IMPORTED_MODULE_4__["AngularFireDatabase"]
        }, {
          type: _common_service__WEBPACK_IMPORTED_MODULE_8__["CommonService"]
        }, {
          type: _angular_fire_auth__WEBPACK_IMPORTED_MODULE_3__["AngularFireAuth"]
        }];
      };

      DbService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: "root"
      })], DbService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=default~home-home-module~pages-account-login-login-module~pages-account-signup-signup-module~pages-a~1389e152-es5.js.map