(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-add-mypage-setting-home-add-mypage-setting-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-mypage-setting/home-add-mypage-setting.page.html":
    /*!***********************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-mypage-setting/home-add-mypage-setting.page.html ***!
      \***********************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesHomeAddMypageSettingHomeAddMypageSettingPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <div id=\"headerInner\">\n    <ion-toolbar>\n      <ion-buttons class=\"back\" slot=\"start\">\n        <ion-back-button text=\"\" icon=\"\"></ion-back-button>\n      </ion-buttons>\n      <ion-title>설정</ion-title>\n    </ion-toolbar>\n  </div>\n</ion-header>\n\n<ion-content>\n\n  <ion-list>\n    <h2>\n      약관\n    </h2>\n\n    <ion-item button detail=\"false\" (click)=\"service()\">\n      이용약관\n    </ion-item>\n    <ion-item button detail=\"false\" (click)=\"personal()\">\n      개인정보 취급 방침\n    </ion-item>\n  </ion-list>\n\n\n  <ion-list>\n    <h2>\n      계정\n    </h2>\n\n    <ion-item button detail=\"false\" (click)=\"gopassword()\">\n      비밀번호 변경\n    </ion-item>\n    <ion-item button detail=\"false\" (click)=\"logout()\">\n      로그아웃\n    </ion-item>\n    <ion-item button detail=\"false\" (click)=\"membership()\">\n      회원탈퇴\n    </ion-item>\n  </ion-list>\n\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/home-add-mypage-setting/home-add-mypage-setting-routing.module.ts":
    /*!*****************************************************************************************!*\
      !*** ./src/app/pages/home-add-mypage-setting/home-add-mypage-setting-routing.module.ts ***!
      \*****************************************************************************************/

    /*! exports provided: HomeAddMypageSettingPageRoutingModule */

    /***/
    function srcAppPagesHomeAddMypageSettingHomeAddMypageSettingRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddMypageSettingPageRoutingModule", function () {
        return HomeAddMypageSettingPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _home_add_mypage_setting_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./home-add-mypage-setting.page */
      "./src/app/pages/home-add-mypage-setting/home-add-mypage-setting.page.ts");

      var routes = [{
        path: '',
        component: _home_add_mypage_setting_page__WEBPACK_IMPORTED_MODULE_3__["HomeAddMypageSettingPage"]
      }];

      var HomeAddMypageSettingPageRoutingModule = function HomeAddMypageSettingPageRoutingModule() {
        _classCallCheck(this, HomeAddMypageSettingPageRoutingModule);
      };

      HomeAddMypageSettingPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], HomeAddMypageSettingPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/home-add-mypage-setting/home-add-mypage-setting.module.ts":
    /*!*********************************************************************************!*\
      !*** ./src/app/pages/home-add-mypage-setting/home-add-mypage-setting.module.ts ***!
      \*********************************************************************************/

    /*! exports provided: HomeAddMypageSettingPageModule */

    /***/
    function srcAppPagesHomeAddMypageSettingHomeAddMypageSettingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddMypageSettingPageModule", function () {
        return HomeAddMypageSettingPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _home_add_mypage_setting_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./home-add-mypage-setting-routing.module */
      "./src/app/pages/home-add-mypage-setting/home-add-mypage-setting-routing.module.ts");
      /* harmony import */


      var _home_add_mypage_setting_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./home-add-mypage-setting.page */
      "./src/app/pages/home-add-mypage-setting/home-add-mypage-setting.page.ts");

      var HomeAddMypageSettingPageModule = function HomeAddMypageSettingPageModule() {
        _classCallCheck(this, HomeAddMypageSettingPageModule);
      };

      HomeAddMypageSettingPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _home_add_mypage_setting_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeAddMypageSettingPageRoutingModule"]],
        declarations: [_home_add_mypage_setting_page__WEBPACK_IMPORTED_MODULE_6__["HomeAddMypageSettingPage"]]
      })], HomeAddMypageSettingPageModule);
      /***/
    },

    /***/
    "./src/app/pages/home-add-mypage-setting/home-add-mypage-setting.page.scss":
    /*!*********************************************************************************!*\
      !*** ./src/app/pages/home-add-mypage-setting/home-add-mypage-setting.page.scss ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesHomeAddMypageSettingHomeAddMypageSettingPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-list::after {\n  content: \"\";\n  display: block;\n  background: #f9f9f9;\n  padding-bottom: 8px;\n}\n\nion-item::after {\n  content: \"\";\n  display: inline-block;\n  width: 24px;\n  height: 24px;\n  background: url('more2.png') no-repeat;\n  background-size: 24px;\n  background-position: center;\n  position: absolute;\n  top: 50%;\n  margin-top: -12px;\n  right: 16px;\n}\n\nh2 {\n  margin: 0;\n  padding: 28px 24px 16px;\n  font-size: 0.875rem;\n  font-weight: 600;\n  color: #999999;\n}\n\nion-item {\n  --padding-top: 20px;\n  --padding-end: 24px;\n  --padding-bottom: 20px;\n  --padding-start: 24px;\n  font-size: 0.875rem;\n  font-weight: 600;\n  color: #333333;\n  position: relative;\n}\n\nion-item.ion-activated {\n  opacity: 0.7;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtbXlwYWdlLXNldHRpbmcvaG9tZS1hZGQtbXlwYWdlLXNldHRpbmcucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsV0FBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHNDQUFBO0VBQ0EscUJBQUE7RUFDQSwyQkFBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLGlCQUFBO0VBQ0EsV0FBQTtBQUVGOztBQUNBO0VBQ0UsU0FBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUFFRjs7QUFDQTtFQUNFLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQUVGOztBQUNBO0VBQ0UsWUFBQTtBQUVGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtbXlwYWdlLXNldHRpbmcvaG9tZS1hZGQtbXlwYWdlLXNldHRpbmcucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWxpc3Q6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJhY2tncm91bmQ6ICNmOWY5Zjk7XG4gIHBhZGRpbmctYm90dG9tOiA4cHg7XG59XG5pb24taXRlbTo6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHdpZHRoOiAyNHB4O1xuICBoZWlnaHQ6IDI0cHg7XG4gIGJhY2tncm91bmQ6IHVybCguLi8uLi8uLi9hc3NldHMvaW1ncy9tb3JlMi5wbmcpIG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiAyNHB4O1xuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA1MCU7XG4gIG1hcmdpbi10b3A6IC0xMnB4O1xuICByaWdodDogMTZweDtcbn1cblxuaDIge1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDI4cHggMjRweCAxNnB4O1xuICBmb250LXNpemU6IDAuODc1cmVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzk5OTk5OTtcbn1cblxuaW9uLWl0ZW0ge1xuICAtLXBhZGRpbmctdG9wOiAyMHB4O1xuICAtLXBhZGRpbmctZW5kOiAyNHB4O1xuICAtLXBhZGRpbmctYm90dG9tOiAyMHB4O1xuICAtLXBhZGRpbmctc3RhcnQ6IDI0cHg7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGNvbG9yOiAjMzMzMzMzO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbmlvbi1pdGVtLmlvbi1hY3RpdmF0ZWQge1xuICBvcGFjaXR5OiAwLjc7XG59XG4iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/home-add-mypage-setting/home-add-mypage-setting.page.ts":
    /*!*******************************************************************************!*\
      !*** ./src/app/pages/home-add-mypage-setting/home-add-mypage-setting.page.ts ***!
      \*******************************************************************************/

    /*! exports provided: HomeAddMypageSettingPage */

    /***/
    function srcAppPagesHomeAddMypageSettingHomeAddMypageSettingPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddMypageSettingPage", function () {
        return HomeAddMypageSettingPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var src_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! src/services/auth.service */
      "./src/services/auth.service.ts");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _account_terms_service_terms_service_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../account/terms-service/terms-service.page */
      "./src/app/pages/account/terms-service/terms-service.page.ts");
      /* harmony import */


      var _account_terms_personal_info_terms_personal_info_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ../account/terms-personal-info/terms-personal-info.page */
      "./src/app/pages/account/terms-personal-info/terms-personal-info.page.ts");
      /* harmony import */


      var src_services_loading_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/services/loading.service */
      "./src/services/loading.service.ts");

      var HomeAddMypageSettingPage = /*#__PURE__*/function () {
        function HomeAddMypageSettingPage(modalController, navc, load, alertController, toastController, auth) {
          _classCallCheck(this, HomeAddMypageSettingPage);

          this.modalController = modalController;
          this.navc = navc;
          this.load = load;
          this.alertController = alertController;
          this.toastController = toastController;
          this.auth = auth;
        }

        _createClass(HomeAddMypageSettingPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "service",
          value: function service() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var modal;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.modalController.create({
                        component: _account_terms_service_terms_service_page__WEBPACK_IMPORTED_MODULE_4__["TermsServicePage"],
                        cssClass: 'my-custom-class'
                      });

                    case 2:
                      modal = _context.sent;
                      _context.next = 5;
                      return modal.present();

                    case 5:
                      return _context.abrupt("return", _context.sent);

                    case 6:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "personal",
          value: function personal() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var modal;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.modalController.create({
                        component: _account_terms_personal_info_terms_personal_info_page__WEBPACK_IMPORTED_MODULE_5__["TermsPersonalInfoPage"],
                        cssClass: 'my-custom-class'
                      });

                    case 2:
                      modal = _context2.sent;
                      _context2.next = 5;
                      return modal.present();

                    case 5:
                      return _context2.abrupt("return", _context2.sent);

                    case 6:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "gopassword",
          value: function gopassword() {
            this.navc.navigateForward(['/home-add-mypage-password-edit']);
          }
        }, {
          key: "logout",
          value: function logout() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var _this = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.alertController.create({
                        cssClass: 'al',
                        header: '로그아웃',
                        message: '로그아웃 하시겠습니까?',
                        buttons: [{
                          text: '취소',
                          role: 'cancel',
                          cssClass: 'secondary'
                        }, {
                          text: '로그아웃',
                          handler: function handler() {
                            _this.auth.logoutUser().then(function () {
                              localStorage.clear();

                              _this.navc.navigateRoot(['/tabs/home']).then(function () {
                                _this.navc.navigateForward(['/login']).then(function () {
                                  _this.successLogout();
                                });
                              });
                            });
                          }
                        }]
                      });

                    case 2:
                      alert = _context3.sent;
                      _context3.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "successLogout",
          value: function successLogout() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var toast;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.toastController.create({
                        message: "정상적으로 로그아웃 처리 되었습니다.",
                        duration: 2000
                      });

                    case 2:
                      toast = _context4.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "membership",
          value: function membership() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var _this2 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      _context5.next = 2;
                      return this.alertController.create({
                        cssClass: 'al',
                        header: '회원탈퇴',
                        message: '회원탈퇴를 진행하시겠습니까?<br>탈퇴한 계정은 복구 할 수 없습니다.',
                        buttons: [{
                          text: '취소',
                          role: 'cancel',
                          cssClass: 'secondary'
                        }, {
                          text: '회원탈퇴',
                          handler: function handler() {
                            _this2.auth.exitUser();

                            _this2.navc.navigateRoot(['/login']).then(function () {
                              _this2.successExit();
                            });
                          }
                        }]
                      });

                    case 2:
                      alert = _context5.sent;
                      _context5.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }, {
          key: "successExit",
          value: function successExit() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              var toast;
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      _context6.next = 2;
                      return this.toastController.create({
                        message: "정상적으로 회원탈퇴 처리 되었습니다.",
                        duration: 2000
                      });

                    case 2:
                      toast = _context6.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }]);

        return HomeAddMypageSettingPage;
      }();

      HomeAddMypageSettingPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"]
        }, {
          type: src_services_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"]
        }, {
          type: src_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]
        }];
      };

      HomeAddMypageSettingPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-home-add-mypage-setting',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./home-add-mypage-setting.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-mypage-setting/home-add-mypage-setting.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./home-add-mypage-setting.page.scss */
        "./src/app/pages/home-add-mypage-setting/home-add-mypage-setting.page.scss"))["default"]]
      })], HomeAddMypageSettingPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-home-add-mypage-setting-home-add-mypage-setting-module-es5.js.map