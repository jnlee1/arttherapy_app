(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-add-mypage-password-edit-home-add-mypage-password-edit-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-mypage-password-edit/home-add-mypage-password-edit.page.html":
    /*!***********************************************************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-mypage-password-edit/home-add-mypage-password-edit.page.html ***!
      \***********************************************************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesHomeAddMypagePasswordEditHomeAddMypagePasswordEditPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-header>\n  <div id=\"headerInner\">\n    <ion-toolbar>\n      <ion-buttons class=\"back\" slot=\"start\">\n        <ion-back-button text=\"\" icon=\"\"></ion-back-button>\n      </ion-buttons>\n      <ion-title>비밀번호 변경</ion-title>\n      <ion-buttons slot=\"end\">\n        <ion-button (click)=\"changePassword()\"\n          [disabled]=\"!oldPassword || !password || !confirm_password || (emailPasswordForm.hasError('mismatchedPasswords') && emailPasswordForm.controls.password.valid) || !pwExpressionChk\">\n          저장\n        </ion-button>\n      </ion-buttons>\n    </ion-toolbar>\n\n  </div>\n</ion-header>\n\n<ion-content>\n  <form [formGroup]=\"emailPasswordForm\">\n    <ion-list>\n      <h2>\n        현재 비밀번호\n      </h2>\n\n      <ion-input placeholder=\"현재 비밀번호를 입력해주세요.\" type=\"password\" formControlName=\"oldPassword\" [(ngModel)]=\"oldPassword\"\n        minlength=\"6\" maxlength=\"20\">\n      </ion-input>\n      <h2>\n        새 비밀번호\n      </h2>\n\n      <ion-input placeholder=\"새 비밀번호를 입력해주세요.\" type=\"password\" formControlName=\"password\" [(ngModel)]=\"password\"\n        minlength=\"6\" maxlength=\"20\" (ionChange)=\"pwRegularExpression()\">\n      </ion-input>\n      <span class=\"validate\" *ngIf=\"password && !pwExpressionChk\">\n        비밀번호는 영문+숫자 6글자 이상 입력해주세요.\n      </span>\n\n      <h2>\n        새 비밀번호 다시 입력\n      </h2>\n\n      <ion-input placeholder=\"새 비밀번호를 다시 입력해주세요.\" formControlName=\"confirm_password\" type=\"password\" minlength=\"6\"\n        [(ngModel)]=\"confirm_password\" minlength=\"6\" maxlength=\"20\" (ionChange)=\"pwRegularExpression()\"></ion-input>\n      <span class=\"validate\" *ngIf=\"confirm_password && matchingPasswords() && !passwordCheck\">\n        비밀번호가 일치하지 않습니다.\n      </span>\n    </ion-list>\n  </form>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/home-add-mypage-password-edit/home-add-mypage-password-edit-routing.module.ts":
    /*!*****************************************************************************************************!*\
      !*** ./src/app/pages/home-add-mypage-password-edit/home-add-mypage-password-edit-routing.module.ts ***!
      \*****************************************************************************************************/

    /*! exports provided: HomeAddMypagePasswordEditPageRoutingModule */

    /***/
    function srcAppPagesHomeAddMypagePasswordEditHomeAddMypagePasswordEditRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddMypagePasswordEditPageRoutingModule", function () {
        return HomeAddMypagePasswordEditPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _home_add_mypage_password_edit_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./home-add-mypage-password-edit.page */
      "./src/app/pages/home-add-mypage-password-edit/home-add-mypage-password-edit.page.ts");

      var routes = [{
        path: '',
        component: _home_add_mypage_password_edit_page__WEBPACK_IMPORTED_MODULE_3__["HomeAddMypagePasswordEditPage"]
      }];

      var HomeAddMypagePasswordEditPageRoutingModule = function HomeAddMypagePasswordEditPageRoutingModule() {
        _classCallCheck(this, HomeAddMypagePasswordEditPageRoutingModule);
      };

      HomeAddMypagePasswordEditPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], HomeAddMypagePasswordEditPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/home-add-mypage-password-edit/home-add-mypage-password-edit.module.ts":
    /*!*********************************************************************************************!*\
      !*** ./src/app/pages/home-add-mypage-password-edit/home-add-mypage-password-edit.module.ts ***!
      \*********************************************************************************************/

    /*! exports provided: HomeAddMypagePasswordEditPageModule */

    /***/
    function srcAppPagesHomeAddMypagePasswordEditHomeAddMypagePasswordEditModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddMypagePasswordEditPageModule", function () {
        return HomeAddMypagePasswordEditPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _home_add_mypage_password_edit_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./home-add-mypage-password-edit-routing.module */
      "./src/app/pages/home-add-mypage-password-edit/home-add-mypage-password-edit-routing.module.ts");
      /* harmony import */


      var _home_add_mypage_password_edit_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./home-add-mypage-password-edit.page */
      "./src/app/pages/home-add-mypage-password-edit/home-add-mypage-password-edit.page.ts");

      var HomeAddMypagePasswordEditPageModule = function HomeAddMypagePasswordEditPageModule() {
        _classCallCheck(this, HomeAddMypagePasswordEditPageModule);
      };

      HomeAddMypagePasswordEditPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"], _home_add_mypage_password_edit_routing_module__WEBPACK_IMPORTED_MODULE_5__["HomeAddMypagePasswordEditPageRoutingModule"]],
        declarations: [_home_add_mypage_password_edit_page__WEBPACK_IMPORTED_MODULE_6__["HomeAddMypagePasswordEditPage"]]
      })], HomeAddMypagePasswordEditPageModule);
      /***/
    },

    /***/
    "./src/app/pages/home-add-mypage-password-edit/home-add-mypage-password-edit.page.scss":
    /*!*********************************************************************************************!*\
      !*** ./src/app/pages/home-add-mypage-password-edit/home-add-mypage-password-edit.page.scss ***!
      \*********************************************************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesHomeAddMypagePasswordEditHomeAddMypagePasswordEditPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "ion-toolbar ion-button {\n  --color: #f7bd00;\n  font-weight: 600;\n}\n\nion-input {\n  margin-bottom: 1.125rem;\n}\n\nh2 {\n  margin: 0;\n  font-size: 0.9375rem;\n  font-weight: 600;\n  color: #333333;\n  margin-bottom: 16px;\n}\n\nion-list {\n  padding: 2.5rem 1.5rem;\n}\n\n[disabled] {\n  --background: transparent;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS1hZGQtbXlwYWdlLXBhc3N3b3JkLWVkaXQvaG9tZS1hZGQtbXlwYWdlLXBhc3N3b3JkLWVkaXQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsZ0JBQUE7RUFDQSxnQkFBQTtBQUFKOztBQUdBO0VBQ0UsdUJBQUE7QUFBRjs7QUFHQTtFQUNFLFNBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0FBQUY7O0FBR0E7RUFDRSxzQkFBQTtBQUFGOztBQUdBO0VBQ0UseUJBQUE7QUFBRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUtYWRkLW15cGFnZS1wYXNzd29yZC1lZGl0L2hvbWUtYWRkLW15cGFnZS1wYXNzd29yZC1lZGl0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFyIHtcbiAgaW9uLWJ1dHRvbiB7XG4gICAgLS1jb2xvcjogI2Y3YmQwMDtcbiAgICBmb250LXdlaWdodDogNjAwO1xuICB9XG59XG5pb24taW5wdXQge1xuICBtYXJnaW4tYm90dG9tOiAxLjEyNXJlbTtcbn1cblxuaDIge1xuICBtYXJnaW46IDA7XG4gIGZvbnQtc2l6ZTogMC45Mzc1cmVtO1xuICBmb250LXdlaWdodDogNjAwO1xuICBjb2xvcjogIzMzMzMzMztcbiAgbWFyZ2luLWJvdHRvbTogMTZweDtcbn1cblxuaW9uLWxpc3Qge1xuICBwYWRkaW5nOiAyLjVyZW0gMS41cmVtO1xufVxuXG5bZGlzYWJsZWRdIHtcbiAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbn1cbiJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/home-add-mypage-password-edit/home-add-mypage-password-edit.page.ts":
    /*!*******************************************************************************************!*\
      !*** ./src/app/pages/home-add-mypage-password-edit/home-add-mypage-password-edit.page.ts ***!
      \*******************************************************************************************/

    /*! exports provided: HomeAddMypagePasswordEditPage */

    /***/
    function srcAppPagesHomeAddMypagePasswordEditHomeAddMypagePasswordEditPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomeAddMypagePasswordEditPage", function () {
        return HomeAddMypagePasswordEditPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_services_loading_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! src/services/loading.service */
      "./src/services/loading.service.ts");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var firebase__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! firebase */
      "./node_modules/firebase/dist/index.cjs.js");
      /* harmony import */


      var firebase__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_5__);
      /* harmony import */


      var src_services_alert_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/services/alert.service */
      "./src/services/alert.service.ts");
      /* harmony import */


      var src_validator__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/validator */
      "./src/validator.ts");

      var HomeAddMypagePasswordEditPage = /*#__PURE__*/function () {
        function HomeAddMypagePasswordEditPage(toastController, alertController, loading, navc, alert, formBuilder) {
          _classCallCheck(this, HomeAddMypagePasswordEditPage);

          this.toastController = toastController;
          this.alertController = alertController;
          this.loading = loading;
          this.navc = navc;
          this.alert = alert;
          this.formBuilder = formBuilder;
          this.passwordCheck = false;
          this.pwExpressionChk = false;
          this.uid = localStorage.getItem("userId");
          this.emailPasswordForm = formBuilder.group({
            oldPassword: src_validator__WEBPACK_IMPORTED_MODULE_7__["Validator"].passwordValidator,
            password: src_validator__WEBPACK_IMPORTED_MODULE_7__["Validator"].passwordValidator,
            confirm_password: src_validator__WEBPACK_IMPORTED_MODULE_7__["Validator"].confirm_passwordValidator
          });
        }

        _createClass(HomeAddMypagePasswordEditPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {}
        }, {
          key: "matchingPasswords",
          value: function matchingPasswords() {
            //비밀번호 같은지 비교
            if (this.password && this.confirm_password) {
              if (this.password !== this.confirm_password) {
                this.passwordCheck = false;
                return true;
              } else {
                this.passwordCheck = true;
                return false;
              }
            }
          }
        }, {
          key: "successPassword",
          value: function successPassword() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
              var toast;
              return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                  switch (_context.prev = _context.next) {
                    case 0:
                      _context.next = 2;
                      return this.toastController.create({
                        message: "비밀번호가 정상적으로 변경되었습니다.",
                        duration: 2000
                      });

                    case 2:
                      toast = _context.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context.stop();
                  }
                }
              }, _callee, this);
            }));
          }
        }, {
          key: "failPassword",
          value: function failPassword() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
              var alert;
              return regeneratorRuntime.wrap(function _callee2$(_context2) {
                while (1) {
                  switch (_context2.prev = _context2.next) {
                    case 0:
                      _context2.next = 2;
                      return this.alertController.create({
                        cssClass: "existEmail",
                        header: "비밀번호 변경 실패",
                        message: "현재 비밀번호가 일치하지 않습니다. <br> 확인 후 다시 시도해주세요.",
                        buttons: [{
                          text: '확인',
                          handler: function handler() {}
                        }]
                      });

                    case 2:
                      alert = _context2.sent;
                      _context2.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context2.stop();
                  }
                }
              }, _callee2, this);
            }));
          }
        }, {
          key: "changePassword",
          value: function changePassword() {
            var _this = this;

            this.loading.load();
            console.log(this.emailPasswordForm.value["password"]);
            var user = firebase__WEBPACK_IMPORTED_MODULE_5__["auth"]().currentUser;
            console.log(user);
            firebase__WEBPACK_IMPORTED_MODULE_5__["auth"]().signInWithEmailAndPassword(user.email, this.emailPasswordForm.value["oldPassword"]).then(function (sucess) {
              user.updatePassword(_this.emailPasswordForm.value["password"]).then(function (sucess) {
                _this.loading.hide();

                _this.successPassword().then(function () {
                  _this.navc.pop();
                });
              })["catch"](function (error) {
                _this.loading.hide();

                console.log("error", error);
                var code = error["code"];

                _this.failPassword();
              });
            })["catch"](function (error) {
              _this.loading.hide();

              var code = error["code"];

              _this.alert.showErrorMessage(code);

              console.log("oldPasswordIncorrect", error);
            });
          }
        }, {
          key: "pwRegularExpression",
          value: function pwRegularExpression() {
            // var regExp = /^[A-za-z0-9]{5,15}$/g;
            var regExp = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{6,}$/;

            if (regExp.test(this.password)) {
              this.pwExpressionChk = true;
            } else {
              this.pwExpressionChk = false;
            }
          }
        }]);

        return HomeAddMypagePasswordEditPage;
      }();

      HomeAddMypagePasswordEditPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }, {
          type: src_services_loading_service__WEBPACK_IMPORTED_MODULE_3__["LoadingService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"]
        }, {
          type: src_services_alert_service__WEBPACK_IMPORTED_MODULE_6__["AlertService"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"]
        }];
      };

      HomeAddMypagePasswordEditPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home-add-mypage-password-edit',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./home-add-mypage-password-edit.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home-add-mypage-password-edit/home-add-mypage-password-edit.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./home-add-mypage-password-edit.page.scss */
        "./src/app/pages/home-add-mypage-password-edit/home-add-mypage-password-edit.page.scss"))["default"]]
      })], HomeAddMypagePasswordEditPage);
      /***/
    },

    /***/
    "./src/services/alert.service.ts":
    /*!***************************************!*\
      !*** ./src/services/alert.service.ts ***!
      \***************************************/

    /*! exports provided: AlertService */

    /***/
    function srcServicesAlertServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AlertService", function () {
        return AlertService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var errorMessages = {
        // Firebase의 auth 에러 메세지 정의
        accountExistsWithDifferentCredential: {
          title: '계정 안내',
          subTitle: '이미 있는 계정입니다.'
        },
        invalidCredential: {
          title: '인증 오류',
          subTitle: '로그인 인증에 오류가 발생했습니다.'
        },
        operationNotAllowed: {
          title: '로그인 실패!',
          subTitle: '로그인 과정에서 오류가 발생했습니다. 관리자에게 문의하시길 바랍니다.'
        },
        userDisabled: {
          title: '정지 계정',
          subTitle: '정지된 계정입니다. 관리자에게 연락하시길 바랍니다.'
        },
        userNotFound: {
          title: '계정 없음',
          subTitle: '해당 계정 정보가 없습니다.'
        },
        // userEmailNotFound: { title: '가입된 이메일 주소가 아닙니다.', subTitle: '다시 확인해 주시기 바랍니다.' },
        wrongPassword: {
          title: '비밀번호 재설정',
          subTitle: '비밀번호 재설정을 위해 가입하신 이메일 주소를\n 입력하여 주시기 바랍니다.'
        },
        // invalidEmail: { title: '계정 없음', subTitle: '입력하신 이메일로 가입된 회원정보가 없습니다. \n 회원가입을 진행해 주시길 바랍니다.' },
        userEmailNotFound: {
          title: '계정 없음',
          subTitle: '가입된 이메일 주소가 아닙니다.\n다시 확인해 주시기 바랍니다.'
        },
        // wrongPassword: { title: '비밀번호 재설정', subTitle: '비밀번호 재설정을 위해 가입하신 이메일 주소를 입력하여 주시기 바랍니다.' },
        invalidEmail: {
          title: '계정 없음',
          subTitle: '이메일 형식이 올바르지 않습니다. \n 올바른 이메일을 입력하여 주시기 바랍니다.'
        },
        emailAlreadyInUse: {
          title: '사용할 수 없는 이메일',
          subTitle: '이미 사용중인 이메일입니다. 다시 확인하시기 바랍니다.'
        },
        weakPassword: {
          title: '비밀번호 경고',
          subTitle: '입력하신 비밀번호가 보안에 취약합니다.'
        },
        requiresRecentLogin: {
          title: '인증 만료',
          subTitle: '인증이 만료되었습니다. 다시 로그인 하시기 바랍니다.'
        },
        userMismatch: {
          title: '사용자 불일치',
          subTitle: '다른 사용자의 인증정보입니다!'
        },
        providerAlreadyLinked: {
          title: 'Already Linked',
          subTitle: 'Sorry, but your account is already linked to this credential.'
        },
        credentialAlreadyInUse: {
          title: '불가능 인증정보',
          subTitle: 'Sorry, but this credential is already used by another user.'
        },
        toManyrequests: {
          title: '확인 횟수 초과',
          subTitle: '잘못된 비밀번호로 확인 횟수가 초과 되었습니다. \n잠시 후 다시 시도하시기 바랍니다.'
        }
      };

      var AlertService = /*#__PURE__*/function () {
        function AlertService(alertCtr, toastCtr) {
          _classCallCheck(this, AlertService);

          this.alertCtr = alertCtr;
          this.toastCtr = toastCtr;
        }

        _createClass(AlertService, [{
          key: "presentAlert",
          value: function presentAlert(header, subtitle, message) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
              var alert;
              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                while (1) {
                  switch (_context3.prev = _context3.next) {
                    case 0:
                      _context3.next = 2;
                      return this.alertCtr.create({
                        header: header,
                        subHeader: subtitle,
                        message: message,
                        buttons: ['확인']
                      });

                    case 2:
                      alert = _context3.sent;
                      _context3.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context3.stop();
                  }
                }
              }, _callee3, this);
            }));
          }
        }, {
          key: "askAlert",
          value: function askAlert(header, subtitle, message) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var alert;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.alertCtr.create({
                        header: header,
                        subHeader: subtitle,
                        message: message,
                        buttons: ['확인']
                      });

                    case 2:
                      alert = _context4.sent;
                      _context4.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "showErrorMessage",
          value: function showErrorMessage(code) {
            switch (code) {
              // Firebase Error Messages
              case 'auth/account-exists-with-different-credential':
                this.presentAlert(errorMessages.accountExistsWithDifferentCredential['title'], errorMessages.accountExistsWithDifferentCredential['subTitle']);
                break;

              case 'auth/invalid-credential':
                this.presentAlert(errorMessages.invalidCredential['title'], errorMessages.invalidCredential['subTitle']);
                break;

              case 'auth/operation-not-allowed':
                this.presentAlert(errorMessages.operationNotAllowed['title'], errorMessages.operationNotAllowed['subTitle']);
                break;

              case 'auth/user-disabled':
                this.presentAlert(errorMessages.userDisabled['title'], errorMessages.userDisabled['subTitle']);
                break;

              case 'auth/user-not-found':
                this.presentAlert(errorMessages.userEmailNotFound['title'], errorMessages.userEmailNotFound['subTitle']);
                break;

              case 'auth/wrong-password':
                this.presentAlert(errorMessages.wrongPassword['title'], errorMessages.wrongPassword['subTitle']);
                break;

              case 'auth/invalid-email':
                this.presentAlert(errorMessages.invalidEmail['title'], errorMessages.invalidEmail['subTitle']);
                break;

              case 'auth/email-already-in-use':
                this.presentAlert(errorMessages.emailAlreadyInUse['title'], errorMessages.emailAlreadyInUse['subTitle']);
                break;

              case 'auth/weak-password':
                this.presentAlert(errorMessages.weakPassword['title'], errorMessages.weakPassword['subTitle']);
                break;

              case 'auth/requires-recent-login':
                this.presentAlert(errorMessages.requiresRecentLogin['title'], errorMessages.requiresRecentLogin['subTitle']);
                break;

              case 'auth/user-mismatch':
                this.presentAlert(errorMessages.userMismatch['title'], errorMessages.userMismatch['subTitle']);
                break;

              case 'auth/provider-already-linked':
                this.presentAlert(errorMessages.providerAlreadyLinked['title'], errorMessages.providerAlreadyLinked['subTitle']);
                break;

              case 'auth/credential-already-in-use':
                this.presentAlert(errorMessages.credentialAlreadyInUse['title'], errorMessages.credentialAlreadyInUse['subTitle']);
                break;

              case 'auth/too-many-requests':
                this.presentAlert(errorMessages.toManyrequests['title'], errorMessages.toManyrequests['subTitle']);
                break;
            }
          } ///토스트 생성///////

        }, {
          key: "presentToast",
          value: function presentToast(message) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
              var toast;
              return regeneratorRuntime.wrap(function _callee5$(_context5) {
                while (1) {
                  switch (_context5.prev = _context5.next) {
                    case 0:
                      _context5.next = 2;
                      return this.toastCtr.create({
                        message: message,
                        duration: 2000
                      });

                    case 2:
                      toast = _context5.sent;
                      toast.present();

                    case 4:
                    case "end":
                      return _context5.stop();
                  }
                }
              }, _callee5, this);
            }));
          }
        }]);

        return AlertService;
      }();

      AlertService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]
        }];
      };

      AlertService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], AlertService);
      /***/
    },

    /***/
    "./src/services/loading.service.ts":
    /*!*****************************************!*\
      !*** ./src/services/loading.service.ts ***!
      \*****************************************/

    /*! exports provided: LoadingService */

    /***/
    function srcServicesLoadingServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "LoadingService", function () {
        return LoadingService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");

      var LoadingService = /*#__PURE__*/function () {
        function LoadingService(loader) {
          _classCallCheck(this, LoadingService);

          this.loader = loader;
          this.isLoading = false;
        }

        _createClass(LoadingService, [{
          key: "load",
          value: function load(message) {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee6() {
              return regeneratorRuntime.wrap(function _callee6$(_context6) {
                while (1) {
                  switch (_context6.prev = _context6.next) {
                    case 0:
                      _context6.next = 2;
                      return this.loader.create({
                        duration: 5000,
                        translucent: true,
                        cssClass: 'custom-class custom-loading',
                        backdropDismiss: true,
                        message: message
                      });

                    case 2:
                      this.loading = _context6.sent;
                      _context6.next = 5;
                      return this.loading.present();

                    case 5:
                    case "end":
                      return _context6.stop();
                  }
                }
              }, _callee6, this);
            }));
          }
        }, {
          key: "hide",
          value: function hide() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee7() {
              return regeneratorRuntime.wrap(function _callee7$(_context7) {
                while (1) {
                  switch (_context7.prev = _context7.next) {
                    case 0:
                      if (this.loading) {
                        this.loader.dismiss();
                      }

                    case 1:
                    case "end":
                      return _context7.stop();
                  }
                }
              }, _callee7, this);
            }));
          }
        }, {
          key: "present",
          value: function present() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee8() {
              var _this2 = this;

              return regeneratorRuntime.wrap(function _callee8$(_context8) {
                while (1) {
                  switch (_context8.prev = _context8.next) {
                    case 0:
                      this.isLoading = true;
                      _context8.next = 3;
                      return this.loader.create({
                        cssClass: 'custom-loader-class'
                      }).then(function (a) {
                        a.present().then(function () {
                          console.log('presented');

                          if (!_this2.isLoading) {
                            a.dismiss().then(function () {
                              return console.log('abort presenting');
                            });
                          }
                        });
                      });

                    case 3:
                      return _context8.abrupt("return", _context8.sent);

                    case 4:
                    case "end":
                      return _context8.stop();
                  }
                }
              }, _callee8, this);
            }));
          }
        }, {
          key: "dismiss",
          value: function dismiss() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee9() {
              return regeneratorRuntime.wrap(function _callee9$(_context9) {
                while (1) {
                  switch (_context9.prev = _context9.next) {
                    case 0:
                      this.isLoading = false;
                      _context9.next = 3;
                      return this.loader.dismiss().then(function () {
                        return console.log('dismissed');
                      });

                    case 3:
                      return _context9.abrupt("return", _context9.sent);

                    case 4:
                    case "end":
                      return _context9.stop();
                  }
                }
              }, _callee9, this);
            }));
          }
        }]);

        return LoadingService;
      }();

      LoadingService.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]
        }];
      };

      LoadingService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], LoadingService);
      /***/
    },

    /***/
    "./src/validator.ts":
    /*!**************************!*\
      !*** ./src/validator.ts ***!
      \**************************/

    /*! exports provided: Validator */

    /***/
    function srcValidatorTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Validator", function () {
        return Validator;
      });
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js"); // tslint:disable-next-line: no-namespace


      var Validator;

      (function (Validator) {
        // Set your validators here, don't forget to import and use them in the appropriate class that uses formGroups.
        // In this example, they are used on LoginPage where a formGroup for email and passwords is used.
        Validator.emailValidator = ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].pattern("^[a-z0-9]+(.[_a-z0-9]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,15})$")]]; // export const email1Validator = [
        //   "",
        //   [
        //     // Validators.required,
        //     Validators.pattern("^[a-z0-9]+(.[_a-z0-9]+)$")
        //   ]
        // ];
        // export const email2Validator = [
        //   "",
        //   [
        //     // Validators.required,
        //     Validators.pattern("^@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,15})$")
        //   ]
        // ];
        // export const email3Validator = [
        //   "",
        //   [
        //     // Validators.required,
        //     Validators.pattern("^[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,15})$")
        //   ]
        // ];

        Validator.passwordValidator = ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].minLength(5), _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].pattern("^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$")]];

        var PasswordValidator = /*#__PURE__*/function () {
          function PasswordValidator() {
            _classCallCheck(this, PasswordValidator);
          }

          _createClass(PasswordValidator, null, [{
            key: "areEqual",
            value: function areEqual(formGroup) {
              var val;
              var valid = true;

              for (var key in formGroup.controls) {
                if (formGroup.controls.hasOwnProperty(key)) {
                  var control = formGroup.controls[key];

                  if (val === undefined) {
                    val = control.value;
                  } else {
                    if (val !== control.value) {
                      valid = false;
                      break;
                    }
                  }
                }
              }

              if (valid) {
                return null;
              }

              return {
                areEqual: true
              };
            }
          }]);

          return PasswordValidator;
        }();

        Validator.PasswordValidator = PasswordValidator;
        Validator.confirm_passwordValidator = ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required]];
        Validator.nameValidator = ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].maxLength(10), _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].minLength(1), _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].pattern("^[ㄱ-ㅎ가-힣a-zA-Z0-9]*$")]];
        Validator.addressDetail = ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].maxLength(20), _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].minLength(1), _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].pattern("^[ㄱ-ㅎ가-힣a-zA-Z0-9]*$")]];
        Validator.phoneValidator = ["", [// Validators.maxLength(10), 01067496719
        _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].pattern("^[0-9]*$")]];
        Validator.numberValidator = ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].maxLength(11), _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].pattern("^[0-9]*$")]];
        Validator.nicknameValidator = ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].maxLength(16), _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].minLength(2), _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required]]; // 한글 or 영어만 가능해서 지움
        // Validators.pattern("^[ㄱ-ㅎ가-힣a-zA-Z0-9]*$")

        Validator.typeValidator = ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required]];
        Validator.serviceTermsValidator = [false, [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].pattern("true")]];
        Validator.personalInfoValidator = [false, [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].pattern("true")]];
        Validator.siValidator = ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required]];
        Validator.dongValidator = ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required]];
        Validator.guValidator = ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required]]; // Set your prompt input validators here, don't forget to import and use them on the AlertController prompt.
        // In this example they are used by home.ts where the user are allowed to change their profile.
        // errorMessages are used by the AlertProvider class and is imported inside AlertProvider.errorMessages which is used by showErrorMessage().

        Validator.profileNameValidator = {
          maxLength: 7,
          lengthError: {
            title: "닉네임/이름 오류!!",
            subTitle: "닉네임/이름은 최대 7자까지 입력가능합니다."
          },
          pattern: /^[a-zA-Z0-9\s]*$/g,
          patternError: {
            title: "닉네임/이름 오류!",
            subTitle: "닉네임/이름은 한글, 영문, 숫자만 입력가능합니다."
          }
        };
        Validator.profileEmailValidator = {
          pattern: /^[a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,15})$/g,
          patternError: {
            title: "Invalid Email Address!",
            subTitle: "Sorry, but the email you have entered is invalid."
          }
        };
        Validator.profilePasswordValidator = {
          minLength: 5,
          lengthError: {
            title: "비밀번호 오류!",
            subTitle: "비밀번호는 최소 6자 이상으로 설정해주시기 바랍니다."
          },
          pattern: /^[a-zA-Z0-9!@#$%^&*()_+-=]*$/g,
          patternError: {
            title: "비밀번호 오류!",
            subTitle: "비밀번호에 특수기호가 포함되어 있습니다."
          }
        }; // Group Form Validators

        Validator.groupNameValidator = ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].minLength(1)]];
        Validator.groupDescriptionValidator = ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_0__["Validators"].minLength(1)]];
      })(Validator || (Validator = {}));
      /***/

    }
  }]);
})();
//# sourceMappingURL=pages-home-add-mypage-password-edit-home-add-mypage-password-edit-module-es5.js.map